class RecorderWorkletProcessor extends AudioWorkletProcessor {
  static get parameterDescriptors() {
    return [{
      name: 'isRecording',
      defaultValue: 0
    }];
  }

  constructor() {
    super();
    this._bufferSize = 512;
    this._buffer = new Float32Array(this._bufferSize);
    this._initBuffer();
  }

  _initBuffer() {
    this._bytesWritten = 0;
  }

  _isBufferEmpty() {
    return this._bytesWritten === 0;
  }

  _isBufferFull() {
    return this._bytesWritten === this._bufferSize;
  }

  _appendToBuffer(value) {
    if (this._isBufferFull()) {
      this._flush();
    }

    this._buffer[this._bytesWritten] = value;
    this._bytesWritten += 1;
  }

  _flush() {
    let buffer = this._buffer;
    if (this._bytesWritten < this._bufferSize) {
      buffer = buffer.slice(0, this._bytesWritten);
    }

    this.port.postMessage({
      eventType: 'data',
      audioBuffer: buffer
    });

    this._initBuffer();
  }

  _recordingStopped() {
    this.port.postMessage({
      eventType: 'stop'
    });
  }

  process(inputs, outputs, parameters) {
    const input = inputs[0];
    const inputChannel = input[0];
    const isRecordingValues = parameters.isRecording;

    for (
      let dataIndex = 0;
      dataIndex < inputChannel.length;
      dataIndex++
    ) {
      let isRecordingValue;

      if (isRecordingValues.length === 1) {
        isRecordingValue = isRecordingValues[0];
      }

      if (isRecordingValues.length > 1) {
        isRecordingValue = isRecordingValues[dataIndex];
      }

      const shouldRecord = 1.0 - isRecordingValue < 0.1;

      if (!shouldRecord && !this._isBufferEmpty()) {
        this._flush();
        this._recordingStopped();
      }

      if (shouldRecord) {
        this._appendToBuffer(inputs[0][0][dataIndex]);
      }
    }

    return true;
  }

}

registerProcessor('recorder-worklet', RecorderWorkletProcessor);
