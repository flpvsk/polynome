import React from 'react';
import { DebounceInput } from 'react-debounce-input';
import theme from './theme';
import { styles as textStyles } from './TextWrapper';

const styles = {
  block: {
    height: 40,
    background: 'none',
    appearance: 'none',
    border: 'none',
    backgroundColor: theme.color.containerOnSurface,
    borderBottom: `1px solid ${theme.color.outline}`,
    borderTopLeftRadius: theme.shape.borderRadius,
    borderTopRightRadius: theme.shape.borderRadius,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    paddingLeft: 12,
    paddingRight: 12,
    color: theme.color.onSurface,
    textAlign: 'left',
    ...textStyles.subtitle1,
    width: '100%',
  },
};

class InputText extends React.Component {
  constructor() {
    super();
    this.state = {
      value: '',
    };

    this._onChange = e => {
      const value = e.target.value;
      this.setState({ value });
      this.props.onChange(value);
    };
  }

  componentDidMount() {
    this.setState({ value: this.props.value });
  }

  render() {
    let blockStyle = styles.block;
    const timeout =
      typeof this.props.debounceTimeout === 'number'
        ? this.props.debounceTimeout
        : 500;

    return (
      <DebounceInput
        type="text"
        debounceTimeout={timeout}
        autoComplete={this.props.autocomplete}
        style={blockStyle}
        onChange={this._onChange}
        value={this.state.value}
        placeholder={this.props.placeholder}
        maxLength={this.props.maxLength}
        disabled={this.props.isDisabled}
        readOnly={this.props.readOnly}
        autoFocus={this.props.isAutoFocus}
        inputRef={this.props.inputRef}
        onClick={e => e.stopPropagation()}
      />
    );
  }
}

export default InputText;
