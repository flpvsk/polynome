import { Component } from 'react';
import shouldComponentUpdate from './shouldComponentUpdate';
import Api from './Api.js';

class AudioLoader extends Component {
  static getDerivedStateFromProps(props, state) {
    const { recordings, lines } = props;
    const sounds = new Map(state.sounds.entries());
    lines.forEach(l => {
      const recordingId = l.playingRecordingId;

      if (!recordingId) {
        return;
      }

      const recording = recordings.find(
        r => r.recordingId === recordingId
      );
      sounds.set(l.lineId, recording.sound);
    });

    return {
      ...state,
      sounds,
    };
  }

  constructor() {
    super();
    this.state = {
      sounds: new Map(),
    };

    this.shouldComponentUpdate = shouldComponentUpdate;
  }

  componentDidMount() {
    this._loadResources();
  }

  componentDidUpdate() {
    if (this._request) {
      this._request.cancel();
    }

    this._loadResources();
  }

  componentWillUnmout() {
    this._request.allOff();
    this._request.cancel();
    this._unmounted = true;
  }

  _loadResources() {
    const soundsMap = new Map(this.state.sounds.entries());
    const toLoadIds = this.props.soundsIds.filter(id => {
      return !this.state.sounds.has(id);
    });

    if (!toLoadIds.length) {
      return;
    }

    this.props.onLoadStarted(toLoadIds);

    const resourceRequest = Api.loadSoundsByIds(toLoadIds);

    resourceRequest
      .then(sounds => {
        if (this._unmounted) {
          return;
        }

        return Promise.all(
          sounds.map(sound => {
            return new Promise((resolve, reject) => {
              this.props.audioContext.decodeAudioData(
                sound.rawData,
                resolve,
                reject
              );
            }).then(audioData => {
              soundsMap.set(sound.id, {
                ...sound,
                audioData,
              });
            });
          })
        );
      })
      .then(sounds => {
        if (this._unmounted) {
          return;
        }

        this.setState({ sounds: soundsMap });
        this.props.onLoadCompleted();
        this._request = null;
      })
      .catch(e => {
        this._request = null;

        if (!e) {
          // request cancelled
          return;
        }
        this.props.onLoadFailed(e);
      });

    resourceRequest.on('progress', progress => {
      this.props.onLoadProgressed(progress);
    });

    this._request = resourceRequest;
  }

  render() {
    return this.props.children(this.state.sounds);
  }
}

export default AudioLoader;
