import React, { Component } from 'react';
import { StripeProvider } from 'react-stripe-elements';

class WithStripeWrapper extends Component {
  constructor() {
    super();
    this.state = {
      stripeScriptState: 'loading',
      error: null,
    };

    this._removeEventListeners = () => {
      window.stripeScript.removeEventListener('load', this._setLoaded);
      window.stripeScript.removeEventListener('error', this._setError);
    };

    this._setLoaded = () => {
      this.setState({
        stripeScriptState: 'ready',
      });

      this._removeEventListeners();
    };

    this._setError = e => {
      this.setState({ stripeScriptState: 'error', error: e });
      this._removeEventListeners();
    };
  }

  componentDidMount() {
    if (!window.stripeScript) {
      const script = document.createElement('script');
      script.addEventListener('load', this._setLoaded);
      script.addEventListener('error', this._setError);
      script.id = 'stripeScript';
      script.src = 'https://js.stripe.com/v3/';
      window.document.body.appendChild(script);
    }
  }

  componentWillUnmount() {
    if (window.stripeScript) {
      this._removeEventListeners();
      window.stripeScript.remove();
    }
  }

  componentDidCatch(e) {
    this.setState({ stripeScriptState: 'error', error: e });
    this._removeEventListeners();
  }

  render() {
    const { Wrapped, ownProps } = this.props;
    const { stripeScriptState, error } = this.state;

    if (stripeScriptState === 'error') {
      return (
        <Wrapped
          didStripeFail={true}
          stripeError={error}
          {...ownProps}
        />
      );
    }

    if (stripeScriptState === 'loading') {
      return (
        <Wrapped
          didStripeFail={false}
          stripeError={null}
          {...ownProps}
        />
      );
    }

    return (
      <StripeProvider apiKey={process.env.REACT_APP_STRIPE_API_KEY}>
        <Wrapped
          didStripeFail={false}
          stripeError={error}
          {...ownProps}
        />
      </StripeProvider>
    );
  }
}

export default function withStripe(Wrapped) {
  return props => (
    <WithStripeWrapper Wrapped={Wrapped} ownProps={props} />
  );
}
