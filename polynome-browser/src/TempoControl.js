import React, { Component } from 'react';

import theme from './theme';

const PATTERN_SIZE = 88;
const CROSS_SIZE = 48;

const styles = {
  block: {
    width: CROSS_SIZE,
    visibility: 'visible',
    border: `1px solid ${theme.color.outline}`,
    paddingTop: 16,
    paddingLeft: 16,
    paddingRight: 16,
    paddingBottom: 4,
    borderRadius: theme.shape.borderRadius,
  },

  _isHidden: {
    visibility: 'hidden',
  },

  __scale: {},

  __currentBpmBlock: {
    color: theme.color.onSurface,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
    textAlign: 'center',
  },

  __currentBpmWrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
    width: '100%',
  },

  __currentBpmInput: {
    position: 'relative',
    background: 'none',
    border: 'none',
    color: theme.color.onSurface,
    textAlign: 'right',
    userSelect: 'none',
    width: '45%',
  },

  __currentBpmLabel: {
    userSelect: 'none',
    width: '45%',
    textAlign: 'left',
  },

  __currentBpmText: {
    userSelect: 'none',
    fontSize: '0.98em',
    letterSpacing: 0.5,
    textAlign: 'center',
    color: theme.color.onSurface,
  },

  __scaleValueText: {
    userSelect: 'none',
    fontSize: '0.73em',
    letterSpacing: 0.4,
    color: theme.color.onSurfaceMediumEmphasis,
    textAlign: 'center',
    background: 'none',
    border: 'none',
  },
};

class TempoControl extends Component {
  constructor() {
    super();
    this._changeTempo = e => {
      let value = e.target.value;
      if (value === '') {
        this.setState({ tempo: value });
        return;
      }

      value = value.trim();
      value = value.replace(/,/g, '.');

      let tempo = parseFloat(value);

      if (isNaN(tempo)) {
        this.setState({ tempo: value });
        return;
      }

      tempo = Math.round(tempo * 10) / 10;

      this.setState({ tempo: value });
      this.props.onTempoChange(this.props.beatId, tempo);
    };

    this._startTrackingTouch = e => {
      let x;

      if (e.touches) {
        x = e.touches[0].clientX;
      }

      if (!e.touches) {
        x = e.pageX;
      }

      const tempoNumber = this._getCurrentTempoNumber();

      this.setState({
        touchStartTempo: tempoNumber,
        touchStartX: x,
      });
    };

    this._trackTouchMove = e => {
      if (!this.state.touchStartX) {
        return;
      }

      let x;

      if (e.touches) {
        x = e.touches[0].clientX;
      }

      if (!e.touches) {
        x = e.pageX;
      }

      const diff = this.state.touchStartX - x;
      const tempoDiff = diff / PATTERN_SIZE * 10;
      let newTempo = Math.round(this.state.touchStartTempo + tempoDiff);

      if (newTempo < 1) {
        newTempo = 1;
      }

      this.setState({
        tempo: newTempo,
      });
    };

    this._commitChangeTempo = () => {
      if (!this.state.touchStartX) {
        return;
      }

      this.setState({
        touchStartX: null,
        touchStartTempo: null,
      });

      this.props.onTempoChange(this.props.beatId, this.getTempo());
    };

    this._setTempoTo = tempoValue => {
      if (tempoValue === 0) {
        tempoValue = 1;
      }

      return () => {
        this.setState({ tempo: tempoValue });
        this.props.onTempoChange(this.props.beatId, tempoValue);
      };
    };

    this.state = {
      tempo: null,
      touchStartX: null,
      touchStartTempo: null,
    };

    this.getTempo = () => {
      let tempo = this.state.tempo || this.props.tempo;
      return Math.round(tempo * 10) / 10;
    };
  }

  componentDidMount() {
    window.document.addEventListener(
      'mouseup',
      this._commitChangeTempo,
      true
    );
  }

  componentWillUnMount() {
    window.document.removeEventListener(
      'mouseup',
      this._commitChangeTempo,
      true
    );
  }

  componentDidUpdate(oldProps) {
    if (oldProps.tempo !== this.props.tempo) {
      this.setState({
        tempo: this.props.tempo,
      });
    }
  }

  _renderScale() {
    const currentTempo = this._getCurrentTempoNumber();
    let { width } = this.props;
    const scaleSize = width - 32;

    const tempoStep = 10 / 4;
    const scaleStep = PATTERN_SIZE / 4;
    const scaleStepsCount = scaleSize / scaleStep;

    const items = [];
    let halfScale = scaleSize / 2;
    let scaleTempoDiff =
      Math.floor((currentTempo * 10) % (tempoStep * 10)) / 10;
    let scaleMarksOffset = scaleStep * scaleTempoDiff / tempoStep;
    let lowerScaleValueTempo = currentTempo - scaleTempoDiff;
    let minTempoValue =
      lowerScaleValueTempo - Math.ceil(scaleStepsCount / 2) * tempoStep;

    let alphaRange = 1;
    for (
      let stepNumber = 0;
      stepNumber <= scaleStepsCount + 1;
      stepNumber++
    ) {
      const scaleValue =
        stepNumber * scaleStep - scaleMarksOffset - scaleStep * 0.6;
      const tempoValue = minTempoValue + stepNumber * tempoStep;
      let alpha = 0.33;

      const stepsFromCenter =
        Math.abs(scaleValue - halfScale) / scaleStep;

      if (stepsFromCenter < alphaRange) {
        alpha = Math.max(1 - stepsFromCenter / alphaRange, 0.33);
      }

      if (alpha > 0.8) {
        alpha = 1;
      }

      if (tempoValue < 0) {
        continue;
      }

      if (tempoValue % 10 === 0) {
        const style = {
          ...styles.__scaleValueText,
          opacity: alpha,
        };
        items.push(
          <text
            key={`scale-tempo-value-${stepNumber}`}
            onClick={this._setTempoTo(Math.round(tempoValue))}
            style={style}
            fill={theme.color.onSurfaceMediumEmphasis}
            y={CROSS_SIZE * 0.5 + 3.5}
            x={scaleValue - 10}
          >
            {tempoValue}
          </text>
        );
      }

      if (tempoValue % 10 !== 0) {
        items.push(
          <circle
            key={`scale-tempo-value-${stepNumber}`}
            style={{ opacity: alpha }}
            fill={theme.color.onSurfaceMediumEmphasis}
            cx={scaleValue}
            cy={CROSS_SIZE / 2}
            r="1"
          />
        );
      }

      items.push(
        <rect
          key={`scale-click-handler-${stepNumber}`}
          onClick={this._setTempoTo(Math.round(tempoValue))}
          y="0"
          x={scaleValue - scaleStep * 0.5 + 3}
          height={CROSS_SIZE}
          width={scaleStep}
          fill="rgba(255, 255, 255, 0)"
          strokeWidth={0}
          stroke="none"
        />
      );
    }

    return (
      <svg
        height={CROSS_SIZE}
        width={scaleSize}
        style={styles.__scale}
        onTouchStart={this._startTrackingTouch}
        onTouchEnd={this._commitChangeTempo}
        onTouchMove={this._trackTouchMove}
        onMouseDown={this._startTrackingTouch}
        onMouseMove={this._trackTouchMove}
        onMouseUp={this._commitChangeTempo}
      >
        {items}
      </svg>
    );
  }

  _getCurrentTempoNumber() {
    const { tempo } = this.state;
    let tempoNumber = parseFloat(tempo);

    if (isNaN(tempoNumber)) {
      return this.props.tempo;
    }

    return tempoNumber;
  }

  render() {
    const tempo = this._getCurrentTempoNumber();

    let blockStyle = {
      ...styles.block,
      width: this.props.width,
    };

    if (isNaN(tempo)) {
      blockStyle = {
        ...blockStyle,
        ...styles._isHidden,
      };

      return <div style={blockStyle} />;
    }

    return (
      <div style={blockStyle}>
        <div style={styles.__currentBpmBlock}>
          <div style={styles.__currentBpmWrapper}>
            <input
              id="tempo-main"
              style={styles.__currentBpmInput}
              value={this.getTempo()}
              onChange={this._changeTempo}
            />
            <label
              htmlFor="tempo-main"
              style={styles.__currentBpmLabel}
            >
              {` bpm`}
            </label>
          </div>
        </div>
        {this._renderScale()}
      </div>
    );
  }
}

export default TempoControl;
