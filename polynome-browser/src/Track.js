import React, { Component } from 'react';
import shouldComponentUpdate from './shouldComponentUpdate';
import * as trackUtils from './trackUtils';
import theme from './theme';
import color from './color';

import eventGetTouch from './eventGetTouch';

import AccentButton from './ButtonAccent';
import IconButton from './ButtonIcon';

import IconAddLine from './IconAddLine';

const LINE_HEIGHT = 48;
const DELIMITER_HEIGHT = 28;
const LINES_VERT_PADDING = 10;
const SIDE_BAR_WIDTH = 72 - 16;

const style = {
  block: {
    color: theme.color.onSurface,
    display: 'flex',
    paddingTop: '14px',
    paddingBottom: '14px',
  },

  __stickCanvas: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },

  __waveformCanvas: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },

  __canvasWrapper: {
    position: 'relative',
    flexShrink: 0,
    flexGrow: 0,
  },

  __sideBar: {
    display: 'flex',
    flexDirection: 'column',
    flex: `0 0 ${SIDE_BAR_WIDTH}px`,
    paddingTop: LINES_VERT_PADDING,
  },

  __sideBarItem: {
    display: 'flex',
    alignItems: 'center',
    height: LINE_HEIGHT,
    marginBottom: LINES_VERT_PADDING,
  },
};

function drawRect(context, { x, y, width, height, color, hasShadow }) {
  context.beginPath();
  context.lineWidth = 0;
  context.fillStyle = color;

  context.rect(x, y, width, height);

  if (hasShadow) {
    context.shadowColor = '#222';
    context.shadowBlur = 8;
  }

  context.fill();

  context.shadowBlur = null;
  context.shadowColor = null;
}

function drawRectWithBorderRadius(
  context,
  {
    x,
    y,
    width,
    height,
    color,
    borderRadiusTopLeft = 0,
    borderRadiusTopRight = theme.shape.borderRadius,
    borderRadiusBottomLeft = 0,
    borderRadiusBottomRight = theme.shape.borderRadius,
    hasShadow = false,
    hasOutline = false,
  }
) {
  const right = x + width;
  const bottom = y + height;

  context.beginPath();
  context.lineWidth = 2;
  context.strokeStyle = color;
  context.fillStyle = color;

  context.moveTo(x + borderRadiusTopLeft, y);
  context.lineTo(right - borderRadiusTopRight, y);
  context.quadraticCurveTo(right, y, right, y + borderRadiusTopRight);
  context.lineTo(right, bottom - borderRadiusBottomRight);
  context.quadraticCurveTo(
    right,
    bottom,
    right - borderRadiusBottomRight,
    bottom
  );
  context.lineTo(x + borderRadiusBottomLeft, bottom);
  context.quadraticCurveTo(
    x,
    bottom,
    x,
    bottom - borderRadiusBottomLeft
  );
  context.lineTo(x, y + borderRadiusTopLeft);
  context.quadraticCurveTo(x, y, x + borderRadiusTopLeft, y);

  // context.rect(x, y, width, height);

  if (hasShadow) {
    context.shadowColor = '#222';
    context.shadowBlur = 8;
  }

  context.fill();

  if (hasOutline) {
    context.lineWidth = 2;
    context.strokeStyle = '#fff';
    context.stroke();

    context.lineWidth = 0;
  }

  context.shadowBlur = null;
  context.shadowColor = null;
}

const WAVEFORM_CACHE = new Map();
const MAX_CACHE_SIZE = 100;
const CACHE_PURGE_SIZE = 20;

function cacheWaveform({
  recordingId,
  lastSignificantByteIndex,
  waveformData,
  color,
}) {
  const cacheEntry = {
    lastSignificantByteIndex,
    waveformData,
    lastHit: Date.now(),
    color,
  };

  WAVEFORM_CACHE.set(recordingId, cacheEntry);

  if (WAVEFORM_CACHE.size <= MAX_CACHE_SIZE) {
    return;
  }

  let hitArray = Array.from(WAVEFORM_CACHE.entries()).sort(
    (a, b) => a[1].lastHit - b[1].lastHit
  );

  for (let i = 0; i < CACHE_PURGE_SIZE; i++) {
    const entry = hitArray[i];
    if (entry) {
      WAVEFORM_CACHE.delete(entry[0]);
    }
  }
}

function getWaveformDataFromCache(recordingId) {
  const cacheEntry = WAVEFORM_CACHE.get(recordingId);
  if (cacheEntry) {
    cacheEntry.lastHit = Date.now();
  }
  return cacheEntry;
}

const LAST_RECORDING_ID_BY_Y = new Map();

function shouldClearCanvasForWaveform({ recording, color, y }) {
  const lastDrawn = LAST_RECORDING_ID_BY_Y.get(y);

  if (!lastDrawn) {
    return false;
  }

  if (!lastDrawn.recording) {
    return false;
  }

  if (recording.recordingId !== lastDrawn.recording.recordingId) {
    return true;
  }

  if (color !== lastDrawn.color) {
    return true;
  }

  return false;
}

function setLastRecordingDrawn(opts) {
  const { y } = opts;
  LAST_RECORDING_ID_BY_Y.set(y, opts);
}

function drawWaveform(context, opts) {
  const {
    x,
    y,
    width,
    height,
    color,
    audioData,
    recording,
    backgroundColor,
    lineDurationPx,
    isRecording,
  } = opts;

  if (!isRecording || shouldClearCanvasForWaveform(opts)) {
    context.clearRect(x, y, width, height);
  }

  setLastRecordingDrawn(opts);

  const { sound } = recording;
  const columnVertPadding = 14;
  const columnMaxHeight = height - columnVertPadding;
  const columnMinHeight = 2;
  const columnHeightStep = Math.floor(columnMaxHeight / 24);
  const columnFillWidth = 2;
  const columnStrokeWidth = 1;
  const columnWidth = columnFillWidth + columnStrokeWidth * 2;
  const columnCount = Math.floor(width / columnWidth) - 1;

  const { latency } = sound;
  const channelData = audioData.getChannelData(0);
  const latencyLength = Math.ceil(sound.sampleRate * latency);
  const dataStep = Math.floor(
    (channelData.length - latencyLength) / columnCount
  );

  const cacheEntry = getWaveformDataFromCache(recording.recordingId);
  let waveformData = [];
  let lastSignificantByteIndex = 0;

  if (cacheEntry) {
    waveformData = cacheEntry.waveformData;
    lastSignificantByteIndex = cacheEntry.lastSignificantByteIndex;
  }

  let columnStartIndex = 0;
  let shouldUpdateCache = false;

  if (isRecording) {
    columnStartIndex = Math.round(lastSignificantByteIndex / dataStep);
    shouldUpdateCache = columnStartIndex < columnCount;
  }

  if (cacheEntry && cacheEntry.color !== color) {
    for (let waveformPiece of waveformData) {
      let [
        xPosition,
        columnYAdjusted,
        columnHeightAdjusted,
      ] = waveformPiece;

      if (xPosition > lineDurationPx) {
        break;
      }

      context.beginPath();
      context.lineWidth = columnStrokeWidth;
      context.strokeStyle = backgroundColor;
      context.fillStyle = color;
      context.rect(
        xPosition + x,
        columnYAdjusted + columnVertPadding / 2 + y,
        columnFillWidth,
        columnHeightAdjusted
      );

      context.stroke();
      context.fill();
    }
  }

  for (
    let columnIndex = columnStartIndex;
    columnIndex < columnCount;
    columnIndex++
  ) {
    const dataStart = columnIndex * dataStep;
    const dataStop = (columnIndex + 1) * dataStep;

    const xPosition = columnIndex * columnWidth;

    if (xPosition > lineDurationPx) {
      break;
    }

    let minAmplitude = 1.0;
    let maxAmplitude = -1.0;
    for (let dataIndex = dataStart; dataIndex < dataStop; dataIndex++) {
      const amplitude = channelData[dataIndex + latencyLength];
      minAmplitude = Math.min(minAmplitude, amplitude);
      maxAmplitude = Math.max(maxAmplitude, amplitude);

      if (amplitude !== 0) {
        lastSignificantByteIndex = dataIndex;
      }
    }

    const columnHeight =
      (maxAmplitude - minAmplitude) * columnMaxHeight / 2;
    let columnHeightAdjusted =
      Math.round(columnHeight / columnHeightStep) * columnHeightStep;

    if (columnHeightAdjusted > columnMaxHeight) {
      columnHeightAdjusted = columnHeightAdjusted - columnHeightStep;
    }

    const columnY = (columnMaxHeight - columnHeightAdjusted) / 2;
    const columnYAdjusted = columnY;

    if (columnHeightAdjusted < columnMinHeight) {
      continue;
    }

    waveformData.push([
      xPosition,
      columnYAdjusted,
      columnHeightAdjusted,
    ]);

    context.beginPath();
    context.lineWidth = columnStrokeWidth;
    context.strokeStyle = backgroundColor;
    context.fillStyle = color;
    context.rect(
      xPosition + x,
      columnYAdjusted + columnVertPadding / 2 + y,
      columnFillWidth,
      columnHeightAdjusted
    );

    context.stroke();
    context.fill();
  }

  if (shouldUpdateCache) {
    cacheWaveform({
      recordingId: recording.recordingId,
      waveformData,
      lastSignificantByteIndex,
      color,
    });
  }
}

function getCanvasWidth(props) {
  return props.width - SIDE_BAR_WIDTH;
}

class Track extends Component {
  static getDerivedStateFromProps(props) {
    const { track, displayConfig } = props;
    const width = getCanvasWidth(props);

    const trackBeatsCount = trackUtils.getTrackBeatsCount(track);
    const trackDuration = trackBeatsCount * 60 / track.commonBeatsTempo;
    const cyclesCount = trackBeatsCount / track.commonBeatsCount;

    const { cycleDelimiterWidth } = displayConfig;
    const cycleWidth = (width - cycleDelimiterWidth) / cyclesCount;
    const cycleMarksNum = width / cycleWidth;

    const commonBeatsDistance = cycleWidth / track.commonBeatsCount;

    return {
      trackBeatsCount,
      cyclesCount,
      cycleWidth,
      cycleMarksNum,
      commonBeatsDistance,
      trackDuration,
    };
  }

  constructor() {
    super();

    this.shouldComponentUpdate = shouldComponentUpdate;
    this._setCanvasRef = c => (this._canvas = c);
    this._setStickCanvasRef = c => (this._stickCanvas = c);
    this._setWaveformCanvasRef = c => (this._waveformCanvas = c);

    this._loop = () => {
      if (this._stopTheLoop) {
        return;
      }

      this._renderStick();

      if (this.props.isRecording) {
        this._renderLines();
      }

      const { isPaused, track } = this.props;
      const { lines, beats } = track;
      const noLines = !lines || !lines.length;
      const noBeats = !beats || !beats.length;
      const noContent = noLines && noBeats;

      if (isPaused || noContent) {
        return;
      }

      const nextFrame =
        window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame;
      nextFrame(this._loop);
    };

    this._startSelectItem = e => {
      const y = eventGetTouch(e)[1];
      this.setState({
        touchY: y,
      });
    };

    this._moveSelectItem = e => {
      const y = eventGetTouch(e)[1];
      const { touchY } = this.state;

      if (touchY === null) {
        return;
      }

      if (!y || y - touchY < 4) {
        return;
      }

      this.setState({
        touchY: null,
      });
    };

    this._endSelectItem = e => {
      let y = this.state.touchY;

      if (!y) {
        return;
      }

      const canvasY = this._canvas.getBoundingClientRect().y;
      const coord = y - canvasY; // + window.pageYOffset;
      const item = this._getItemByCoordinate(coord);

      if (!item) {
        return;
      }

      if (item.itemType === 'beat') {
        this.props.onBeatSelected(item.beatId);
        return;
      }

      if (item.itemType === 'line') {
        this.props.onLineSelected(item.lineId);
        return;
      }

      this.setState({
        touchY: null,
      });
    };

    this.state = {};

    const touchHandlers = {};

    if ('ontouchstart' in document.documentElement) {
      touchHandlers.onTouchStart = this._startSelectItem;
      touchHandlers.onTouchEnd = this._endSelectItem;
      touchHandlers.onTouchMove = this._moveSelectItem;
    } else {
      touchHandlers.onMouseDown = this._startSelectItem;
      touchHandlers.onMouseUp = this._endSelectItem;
    }

    this._touchHandlers = touchHandlers;
  }

  componentDidMount() {
    this._stopTheLoop = false;
    this._clearCanvas();
    this._renderCycleMarksOnPadding();
    this._loop();
    this._renderBeats();
    this._renderLines();
  }

  componentDidUpdate() {
    this._clearCanvas();
    this._renderCycleMarksOnPadding();
    this._loop();
    this._renderBeats();
    this._renderLines();
  }

  componentWillUnmount() {
    this._stopTheLoop = true;
  }

  _getItemByCoordinate(y) {
    const { track } = this.props;
    const beatsSpaceHeight = this._getBeatsSpaceHeight();
    const linesSpaceHeight = this._getLinesSpaceHeight();

    y = y - LINES_VERT_PADDING;

    if (y < 0) {
      return null;
    }

    if (y < beatsSpaceHeight) {
      const index = Math.floor(y / (LINE_HEIGHT + LINES_VERT_PADDING));
      return {
        itemType: 'beat',
        beatId: track.beats[index].id,
      };
    }

    if (y > beatsSpaceHeight + linesSpaceHeight) {
      return null;
    }

    const index = Math.floor(
      (y - beatsSpaceHeight) / (LINE_HEIGHT + LINES_VERT_PADDING)
    );

    return {
      itemType: 'line',
      lineId: track.lines[index].lineId,
    };
  }

  _renderCycleMarksOnPadding() {
    const height = this._getCanvasHeight();
    this._renderCycleMarks({
      y: 0,
      height,
    });
  }

  _getLinesSpaceHeight() {
    const { track } = this.props;
    return (
      (track.lines.length || 0) * (LINE_HEIGHT + LINES_VERT_PADDING)
    );
  }

  _getBeatsSpaceHeight() {
    const { track } = this.props;
    return (
      (track.beats.length || 0) * (LINE_HEIGHT + LINES_VERT_PADDING)
    );
  }

  _getCanvasHeight() {
    return (
      LINES_VERT_PADDING +
      this._getBeatsSpaceHeight() +
      this._getLinesSpaceHeight()
    );
  }

  _getCanvasWidth() {
    return getCanvasWidth(this.props);
  }

  _renderStick() {
    const {
      getCurrentTime,
      startTime,
      displayConfig,
      isPaused,
    } = this.props;
    const { trackDuration } = this.state;

    const context = this._stickCanvas.getContext('2d');
    const height = this._getCanvasHeight();
    const width = this._getCanvasWidth();
    const currentTime = getCurrentTime();
    const stickWidth = displayConfig.stickWidth;

    context.clearRect(0, 0, width, height);

    if (isPaused) {
      return;
    }
    const tracksPassed = (currentTime - startTime) / trackDuration;
    const tracksPassedNumber = Math.floor(tracksPassed);
    let position =
      (tracksPassed - tracksPassedNumber) * width - stickWidth / 2;

    drawRect(context, {
      color: '#eee',
      x: position,
      y: 0,
      width: stickWidth, // TODO: displayConfig
      height: height,
      hasShadow: false,
    });
  }

  _clearCanvas() {
    const canvas = this._canvas;
    const context = canvas.getContext('2d');
    const width = this._getCanvasWidth();
    const height = this._getCanvasHeight();

    context.clearRect(0, 0, width, height);
  }

  _renderLines() {
    const {
      track,
      isRecording,
      recordingForLineId,
      getCurrentTime,
      startTime,
      selectedItem,
    } = this.props;
    const { cycleWidth, trackDuration } = this.state;

    const canvas = this._canvas;
    const context = canvas.getContext('2d');

    const { lines, recordings, recordingsAudioMap, soloItem } = track;
    const beatsSpaceHeight = this._getBeatsSpaceHeight();
    const width = this._getCanvasWidth();

    const currentTime = getCurrentTime();

    for (let trackNum = 0; trackNum < lines.length; trackNum++) {
      const line = lines[trackNum];
      const isMute = soloItem && soloItem.lineId !== line.lineId;
      const isSolo = soloItem && soloItem.lineId === line.lineId;
      const isLineRecording = recordingForLineId === line.lineId;

      const colorStr = color.colorStrByLine({
        line,
        isMute,
        isSolo,
        isRecording: isLineRecording,
      });

      const beatWidth = cycleWidth / line.beatsCount;
      const lineStart = line.lineStart;
      const lineDuration = line.lineDuration;

      if (!lineDuration) {
        continue;
      }

      const lineStartPx = lineStart * beatWidth;
      let lineDurationPx = lineDuration * beatWidth;
      let lineBorderRadius = theme.shape.borderRadius;
      let waveformRecordingId = line.playingRecordingId;

      const lineHasRecordings =
        waveformRecordingId || line.writingRecordingId;
      const timePassed = currentTime - startTime;
      const tracksPassed = timePassed / trackDuration;
      const tracksPassedNumber = Math.floor(tracksPassed);

      let shouldRedrawWave = false;

      if (isRecording && isLineRecording) {
        if (tracksPassedNumber % 2 !== 0) {
          waveformRecordingId = line.writingRecordingId;
          lineDurationPx =
            (tracksPassed - tracksPassedNumber) * lineDurationPx;
          lineBorderRadius = 0;
          shouldRedrawWave = true;
        }

        if (tracksPassedNumber === 0 && !lineHasRecordings) {
          lineDurationPx = 0;
        }
      }

      if (!isRecording) {
        shouldRedrawWave = true;
      }

      if (
        isRecording &&
        !isLineRecording &&
        tracksPassedNumber % 2 === 0 &&
        timePassed - tracksPassedNumber * trackDuration < 0.1
      ) {
        shouldRedrawWave = true;
      }

      const y =
        LINES_VERT_PADDING +
        beatsSpaceHeight +
        trackNum * (LINE_HEIGHT + LINES_VERT_PADDING);
      const height = LINE_HEIGHT;

      context.clearRect(0, y, width, height);

      drawRectWithBorderRadius(context, {
        color: colorStr,
        x: lineStartPx,
        y,
        width: lineDurationPx,
        height,
        borderRadiusTopLeft: 0,
        borderRadiusTopRight: lineBorderRadius,
        borderRadiusBottomLeft: 0,
        borderRadiusBottomRight: lineBorderRadius,
        hasOutline: selectedItem && line.lineId === selectedItem.lineId,
      });

      const waveformContext = this._waveformCanvas.getContext('2d');
      const audio = recordingsAudioMap[waveformRecordingId];
      if (
        shouldRedrawWave &&
        waveformRecordingId &&
        audio.loadStatus !== 'pending'
      ) {
        const recording = recordings.find(
          r => r.recordingId === waveformRecordingId
        );
        const { audioData } = audio;

        const delimiterColorStr = color.delimiterColorStrByLine({
          line,
          isMute,
          isSolo,
          isRecording: isLineRecording,
        });

        drawWaveform(waveformContext, {
          isRecording: isLineRecording && isRecording,
          recording,
          audioData,
          backgroundColor: colorStr,
          color: delimiterColorStr,
          x: lineStartPx,
          y,
          width: width - lineStartPx,
          height: LINE_HEIGHT,
          lineDurationPx,
        });
      }
    }
  }

  _renderCycleMarks({ y, height }) {
    const canvas = this._canvas;
    const context = canvas.getContext('2d');
    const { displayConfig } = this.props;
    const { cycleWidth, cycleMarksNum } = this.state;

    const { cycleDelimiterWidth } = displayConfig;

    // Cycle marks
    for (let i = 0; i < cycleMarksNum - 1; i++) {
      drawRect(context, {
        color: '#777',
        x: i * cycleWidth,
        y,
        width: cycleDelimiterWidth,
        height,
      });
    }
  }

  _renderBeats() {
    const { track, displayConfig, selectedItem } = this.props;
    const {
      cycleWidth,
      commonBeatsDistance,
      cycleMarksNum,
    } = this.state;

    const canvas = this._canvas;
    const context = canvas.getContext('2d');

    const { beatDelimiterWidth } = displayConfig;

    const { beats, soloItem } = track;

    for (let trackNum = 0; trackNum < beats.length; trackNum++) {
      const beat = beats[trackNum];

      const beatWidth = cycleWidth / beat.beatsCount;
      const lineStart = beat.lineStart;
      const lineDuration = beat.lineDuration;

      const lineStartPx = lineStart * beatWidth;
      const lineDurationPx = lineDuration * beatWidth;

      const y =
        LINES_VERT_PADDING +
        trackNum * (LINE_HEIGHT + LINES_VERT_PADDING);
      const height = LINE_HEIGHT;

      const isBeatSolo = soloItem && soloItem.beatId === beat.id;
      const isBeatMute = soloItem && soloItem.beatId !== beat.id;

      const trackColor = color.colorStrByBeat({
        beat,
        isMute: isBeatMute,
        isSolo: isBeatSolo,
      });

      drawRectWithBorderRadius(context, {
        color: trackColor,
        x: lineStartPx,
        y,
        width: lineDurationPx,
        height,
        hasShadow: true,
        hasOutline: selectedItem && selectedItem.beatId === beat.id,
      });

      if (isBeatMute && !isBeatSolo) {
        continue;
      }

      const mutedBeats = beat.mutedBeats || [];
      const delimitersDistance = cycleWidth / beat.beatsCount;
      let distance = (beat.offset || 0) * commonBeatsDistance;
      let beatNumber = 1;

      while (distance <= cycleWidth) {
        let delimiterColor = color.delimiterColorStrByBeat({
          beat,
          isMute: isBeatMute,
          isSolo: isBeatSolo,
        });

        if (mutedBeats.indexOf(beatNumber) > -1) {
          delimiterColor = trackColor;
        }

        for (
          let cycleNum = 0;
          cycleNum < cycleMarksNum - 1;
          cycleNum++
        ) {
          let x = cycleNum * cycleWidth + distance;

          if (x < lineStartPx) {
            continue;
          }

          if (x >= lineStartPx + lineDurationPx) {
            continue;
          }

          drawRect(context, {
            color: delimiterColor,
            x,
            y: y + (LINE_HEIGHT - DELIMITER_HEIGHT) / 2,
            width: beatDelimiterWidth,
            height: DELIMITER_HEIGHT,
          });
        }

        distance += delimitersDistance;
        beatNumber += 1;
      }
    }
  }

  _renderSideBar() {
    const { track, recordingForLineId, selectedItem } = this.props;
    const { beats, soloItem } = track;

    const beatsItems = beats.map(beat => {
      const isBeatMute = soloItem && soloItem.beatId !== beat.id;
      const isBeatSolo = soloItem && soloItem.beatId === beat.id;

      const colorStr = color.colorStrByBeat({
        beat,
        isMute: isBeatMute,
        isSolo: isBeatSolo,
      });

      return (
        <div key={beat.id} style={style.__sideBarItem}>
          <AccentButton
            onTouchTap={() => this.props.onBeatSelected(beat.id)}
            backgroundColor={colorStr}
            hasOutline={selectedItem && beat.id === selectedItem.beatId}
          >
            {beat.beatsCount}
          </AccentButton>
        </div>
      );
    });

    const linesItems = track.lines.map(line => {
      const isMute = soloItem && soloItem.lineId !== line.lineId;
      const isSolo = soloItem && soloItem.lineId === line.lineId;
      const isRecording = recordingForLineId === line.lineId;

      const colorStr = color.colorStrByLine({
        line,
        isMute,
        isSolo,
        isRecording,
      });

      return (
        <div key={line.lineId} style={style.__sideBarItem}>
          <AccentButton
            onTouchTap={() => this.props.onLineSelected(line.lineId)}
            backgroundColor={colorStr}
            hasOutline={
              selectedItem && line.lineId === selectedItem.lineId
            }
          >
            {line.lineName}
          </AccentButton>
        </div>
      );
    });

    let addLine;

    if (this.props.onAddLine) {
      addLine = (
        <div key={'add-line'} style={style.__sideBarItem}>
          <IconButton
            styleName="outlined"
            onTouchTap={this.props.onAddLine}
          >
            <IconAddLine
              width={24}
              height={24}
              color={theme.color.onBackgroundMediumEmphasis}
            />
          </IconButton>
        </div>
      );
    }

    return (
      <div style={style.__sideBar}>
        {beatsItems}
        {linesItems}
        {addLine}
      </div>
    );
  }

  render() {
    const height = this._getCanvasHeight();
    const width = this._getCanvasWidth();

    const blockStyle = {
      ...style.block,
      width: this.props.width,
    };
    const canvasWrapperStyle = {
      ...style.__canvasWrapper,
      flexBasis: width,
    };

    return (
      <div style={blockStyle}>
        {this._renderSideBar()}
        <div
          key="canvas-wrapper"
          {...this._touchHandlers}
          style={canvasWrapperStyle}
        >
          <canvas
            ref={this._setCanvasRef}
            width={width}
            height={height}
          />

          <canvas
            style={style.__waveformCanvas}
            ref={this._setWaveformCanvasRef}
            width={width}
            height={height}
          />

          <canvas
            style={style.__stickCanvas}
            ref={this._setStickCanvasRef}
            width={width}
            height={height}
          />
        </div>
      </div>
    );
  }
}

Track.defaultProps = {
  displayConfig: {
    cycleDelimiterWidth: 2,
    beatDelimiterWidth: 4,
    stickWidth: 2,
  },
};

export default Track;
