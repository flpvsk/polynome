export default function beatsSortFn(b1, b2) {
  let result = b1.beatsCount - b2.beatsCount;
  if (result === 0) {
    let b1Muted = b1.mutedBeats || [];
    let b2Muted = b2.mutedBeats || [];

    return b2Muted.length - b1Muted.length;
  }

  return result;
}

export function reverseBeatsSortFn(b1, b2) {
  return -beatsSortFn(b1, b2);
}
