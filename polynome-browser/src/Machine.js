import React, { Component } from 'react';
import AudioContext from './AudioContext';
import getFullUrl from './getFullUrl';
import { Link } from 'react-router-dom';

import nanoid from 'nanoid';

import viewport from './viewport';
import theme from './theme';
import { colorStrByBeat } from './color';

import * as trackUtils from './trackUtils';

import shouldComponentUpdate from './shouldComponentUpdate';

import TopBarContainer from './AppBarTopContainer';
import FloatingActionButton from './ButtonFloatingAction';
import TextWrapper from './TextWrapper';
import Switch from './Switch';
import InputText from './InputText';
import ButtonText from './ButtonText';
import ButtonIcon from './ButtonIcon';

import LoadingIndicatorCircular from './LoadingIndicatorCircular';
import MenuItem from './MenuItem';
import Divider from './Divider';
import MenuHeader from './MenuHeader';

import ListItem from './ListItem';

import TempoControl from './TempoControl';
import BeatsControls from './BeatsControls';
import CircleControls from './CircleControls';
import BeatBottomSheetContent from './BeatBottomSheetContent';
import ChopFooter from './ChopFooter';

import QuickStart from './QuickStart';
import Player from './Player';
import AudioPlayer from './AudioPlayer';
import AudioLoader from './AudioLoader';

import IconPlay from './IconPlay';
import IconStop from './IconStop';
import IconShare from './IconShare';
import IconForm from './IconForm';
import IconRemix from './IconRemix';
import IconHome from './IconHome';

import connectChopEditor from './connectChopEditor';

const styles = {
  __topBarButton: {
    appearance: 'none',
    background: 'none',
    border: 'none',
  },

  __tempoControlWrapper: {
    marginTop: 24,
    marginBottom: 8,
    width: '100%',
    display: 'flex',
    paddingLeft: 16,
    paddingRight: 16,
    justifyContent: 'space-between',
  },

  __playerContainer: {
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    marginTop: 14,
    marginBottom: 14,
  },
};

const KEY_WHITESPACE = 32;
const DEFAULT_TEMPO = 120;
const DELAY = 0.05;

class Machine extends Component {
  static getDerivedStateFromProps(props, state) {
    let isInitialized = props.chop && props.kits && props.kits.length;

    if (props.chop) {
      return {
        ...state,
        ...props.chop,
        beats: props.chop.beats.slice().sort(trackUtils.beatsSortFn),
        isInitialized,
        isChopFetched: true,
        kits: props.kits,
      };
    }

    if (!props.chop) {
      return {
        isChopFetched: false,
      };
    }

    return state;
  }

  constructor() {
    super();
    let windowWidth = viewport.getWidth();

    this._audioContext = new AudioContext();

    this.state = {
      isInitialized: false,
      showShareDialog: false,
      showChangeNameDialog: false,
      startTime: 0,
      commonBeatsCount: 0,
      commonBeatsTempo: DEFAULT_TEMPO,
      beats: [],
      lines: [],
      recordings: [],
      windowWidth,
      isPaused: true,
      pauseTime: undefined,
      activeSoundType: 'oscilator',
      activeKitId: undefined,
      kits: [],
      savedBeats: [],
      loadingState: {
        isLoading: false,
        progress: 0,
      },
      selectedItem: null,
      soloItem: null,
    };

    this.shouldComponentUpdate = shouldComponentUpdate;

    this._toggleIsDiscoverable = () => {
      this.props.onUpdateChop({
        update: {
          isDiscoverable: !this.state.isDiscoverable,
        },
      });
    };

    this._share = () => {
      this.setState({
        showShareDialog: true,
      });
    };

    this._showChangeNameDialog = () => {
      this.setState({ showChangeNameDialog: true });
    };

    this._hideChangeNameDialog = () => {
      this.setState({ showChangeNameDialog: false });
    };

    this._changeName = value => {
      this.props.onChangeName({ name: value });
    };

    this._hideShareDialog = () => {
      this.setState({
        showShareDialog: false,
      });
    };

    this._getAudioContextTime = () => {
      if (!this._audioContext) {
        return 0;
      }

      return this._audioContext.currentTime;
    };

    this._toggleBeat = beatId => {
      if (
        this.state.selectedItem &&
        this.state.selectedItem.beatId === beatId
      ) {
        this.setState({ selectedItem: null });
        return;
      }

      this.setState({
        selectedItem: {
          itemType: 'beat',
          beatId,
        },
      });
    };

    this._onBeatAdded = beat => {
      if (!this.props.me) {
        return;
      }

      const { beats } = this.state;
      beat = {
        id: nanoid(),
        personId: this.props.me.personId,
        ...beat,
      };

      let { startTime, isPaused } = this.state;

      if (!beats.length) {
        startTime = this._audioContext.currentTime + DELAY;
        isPaused = false;
      }

      const updateStateAndRhythm = () => {
        this.props.onAddBeat({ beat });
        this.setState({ startTime, isPaused });
      };

      this._runWhenAudioIsReady(updateStateAndRhythm);
    };

    this._addDefaultBeat = () => {
      this._onBeatAdded({
        beatsCount: 1,
        tempo: 120,
        lineStart: 0,
        lineDuration: 1,
      });
    };

    this._clear = () => {
      throw new Error('Not implemented');
    };

    this._changeTempo = (beatId, tempo) => {
      this.setState({
        startTime: this._audioContext.currentTime + DELAY,
      });

      this.props.onChangeTempo({ beatId, tempo });
    };

    this._changeOffset = (beatId, offset) => {
      this.props.onChangeOffset({ beatId, offset });
    };

    this._deselectAll = () => {
      this.setState({ selectedItem: null });
    };

    this._toggleMuteSection = (beatId, sectionNumber) => {
      this.props.onToggleMuteSection({ beatId, sectionNumber });
    };

    this._toggleMuteBeat = beatId => {
      this.props.onToggleMuteBeat({ beatId });
    };

    this._toggleSoloBeat = beatId => {
      this.props.onToggleSoloItem({ itemType: 'beat', beatId });
    };

    this._togglePause = () => {
      const { isPaused } = this.state;

      if (!isPaused) {
        this.setState({
          isPaused: true,
          startTime: undefined,
        });

        return;
      }

      const play = () => {
        this.setState({
          startTime: this._audioContext.currentTime + DELAY,
          isPaused: false,
        });
      };

      this._runWhenAudioIsReady(play);
    };

    this._startPlaying = () => {
      const play = () => {
        this.setState({
          startTime: this._audioContext.currentTime + DELAY,
          isPaused: false,
        });
      };

      this._runWhenAudioIsReady(play);
    };

    this._runWhenAudioIsReady = async fn => {
      try {
        await this._prepareAudio();
        fn();
      } catch (e) {
        console.warn('Error starting AudioContext', e);
      }
    };

    this._prepareAudio = () => {
      return new Promise((resolve, reject) => {
        if (this._audioContext.state === 'running') {
          resolve();
          return;
        }

        if (this._audioContext.state === 'suspended') {
          this._audioContext
            .resume()
            .then(resolve)
            .catch(reject);
          return;
        }

        reject(
          new Error(
            `Unknown AudioContext state ${this._audioContext.state}`
          )
        );
      });
    };

    this._togglePauseAndTrack = () => {
      this._togglePause();
    };

    this._globalKeyEventHandler = e => {
      const { keyCode } = e;

      if ([KEY_WHITESPACE].indexOf(keyCode) === -1) {
        return true;
      }

      const tagName = e.target.tagName;
      if (tagName === 'INPUT' || tagName === 'TEXTAREA') {
        return true;
      }

      if (!this.state.beats || !this.state.beats.length) {
        return true;
      }

      e.cancelBubble = true;
      e.stopPropagation();
      e.stopImmediatePropagation();
      e.preventDefault();

      this._togglePause();

      return false;
    };

    this._deleteBeat = beatId => {
      this.setState({ selectedItem: null });
      this.props.onDeleteBeat({ beatId });
    };

    this._changeBeatsCount = (beatId, beatsCount) => {
      this.props.onChangeBeatsCount({ beatId, beatsCount });
    };

    this._changeSoundType = (soundType, kitId) => {
      this.props.onChangeSoundType({ soundType, kitId });
    };

    this._changeSound = (beatId, sound) => {
      this.props.onChangeSound({ beatId, sound });
    };

    this._removeBeat = beatId => {
      this.setState({ selectedItem: null });
      this.props.onDeleteBeat({ beatId });
    };

    this._skipQuickStart = () => {
      this.setState({
        quickStartSkip: true,
      });
    };

    this._chooseTemplate = template => {
      this.props.onChooseTemplate(this.props.chop.chopId, template);
    };
  }

  componentWillUnmount() {
    window.document.removeEventListener(
      'keydown',
      this._globalKeyEventHandler,
      true
    );
  }

  componentDidMount() {
    if (!this.props.kits.length) {
      this.props.listKits();
    }

    if (!this.state.isChopFetched) {
      this.props.getChop();
    }

    window.document.addEventListener(
      'keydown',
      this._globalKeyEventHandler,
      true
    );
  }

  render() {
    const {
      commonBeatsCount,
      commonBeatsTempo,
      beats,
      kits,
      selectedItem,
      soloItem,
      isInitialized,
      isPaused,
      showShareDialog,
      showChangeNameDialog,
      composedBy,
      quickStartSkip,
    } = this.state;
    const { me } = this.props;
    let isMyChop;
    let isEmpty;
    let chopFooter;
    let showQuickStart;

    if (isInitialized) {
      isMyChop = me && me.personId === composedBy;
      isEmpty = beats.length === 0;
    }

    showQuickStart = isEmpty && !quickStartSkip;

    if (isInitialized && beats.length) {
      chopFooter = (
        <ChopFooter
          me={me}
          myAccount={this.props.myAccount}
          onClap={this.props.onClap}
          onSendComment={this.props.onSendComment}
          chop={this.props.chop}
        />
      );
    }

    let loadingIndicator;
    if (!isInitialized) {
      loadingIndicator = <LoadingIndicatorCircular />;
    }

    const selectedBeat = beats.reduce((a, b) => {
      if (a) {
        return a;
      }

      if (selectedItem && b.id === selectedItem.beatId) {
        return {
          ...b,
          tempo: b.beatsCount * commonBeatsTempo / commonBeatsCount,
        };
      }

      return a;
    }, undefined);

    let tempoValue;
    let tempoBeatId;

    let tempoBeat = selectedBeat || beats[beats.length - 1];

    if (tempoBeat) {
      tempoBeatId = tempoBeat.id;
      tempoValue =
        tempoBeat.beatsCount * commonBeatsTempo / commonBeatsCount;
    }

    const activeKit = this.state.kits.reduce((acc, k) => {
      if (k.id === this.state.activeKitId) {
        return k;
      }

      return acc;
    }, undefined);

    let content;
    if (this.state.isInitialized) {
      let circleControls;

      if (isMyChop && !showQuickStart) {
        circleControls = (
          <CircleControls
            width={Math.min(360, this.state.windowWidth - 32)}
            defaultTempo={DEFAULT_TEMPO}
            onBeatAdded={this._onBeatAdded}
            onPauseToggle={this._togglePauseAndTrack}
            onShowSettings={this._showSettings}
            canClear={beats.length > 0 && !selectedBeat}
            onChangeInputDeviceId={this._changeInputDeviceId}
            selectedInputDeviceId={this.state.selectedInputDeviceId}
            inputDevices={[]}
            shouldShowPausePlay={beats.length > 0 && !selectedBeat}
            shouldShowEmptyScreenBlock={beats.length === 0}
            isPaused={this.state.isPaused}
            onClear={this._clear}
            isEmptyState={!beats || !beats.length}
          />
        );
      }

      content = [
        <div
          key="tempo-control-wrap"
          style={styles.__tempoControlWrapper}
        >
          <TempoControl
            key="tempo-control"
            width={this.state.windowWidth - 32}
            tempo={tempoValue}
            beatId={tempoBeatId}
            onTempoChange={this._changeTempo}
          />
        </div>,

        <BeatsControls
          key="beats-controls"
          width={this.state.windowWidth - 32}
          beats={beats}
          soloItem={soloItem}
          selectedItem={selectedItem}
          commonBeatsCount={commonBeatsCount}
          commonBeatsTempo={commonBeatsTempo}
          onMute={this._toggleMuteBeat}
          onSolo={this._toggleSoloBeat}
          onBeatClick={this._toggleBeat}
        />,

        <div key="relative-container" style={styles.__playerContainer}>
          <Player
            width={Math.min(360, this.state.windowWidth - 32)}
            commonBeatsTempo={commonBeatsTempo}
            commonBeatsCount={commonBeatsCount}
            getCurrentTime={this._getAudioContextTime}
            startTime={this.state.startTime}
            beats={beats}
            isPaused={this.state.isPaused}
            pauseTime={this.state.pauseTime}
            soloItem={this.state.soloItem}
            selectedItem={selectedItem}
            onBeatSelected={this._toggleBeat}
          />

          {circleControls}
        </div>,
      ];

      if (isInitialized && isMyChop && showQuickStart) {
        content = (
          <QuickStart
            kits={kits}
            audioContext={this._audioContext}
            runWhenAudioIsReady={this._runWhenAudioIsReady}
            onSkipQuickStart={this._skipQuickStart}
            onChooseTemplate={this._chooseTemplate}
          />
        );
      }
    }

    let audioBeats = trackUtils.beatsToAudio(this.state);

    let bottomSheetIcon;
    let bottomSheetIconBackgroundColor;
    let bottomSheetContent = null;
    let bottomSheetTitle;

    if (selectedItem && selectedItem.itemType === 'beat') {
      const isMuteDisabled = !!soloItem;
      const isMute =
        soloItem && soloItem.beatId !== selectedItem.beatId;
      const isSolo =
        soloItem && soloItem.beatId === selectedItem.beatId;

      bottomSheetIcon = selectedBeat.beatsCount;
      bottomSheetIconBackgroundColor = colorStrByBeat({
        beat: selectedBeat,
        isMute,
        isSolo,
      });

      bottomSheetTitle = `Subdivision of ${selectedBeat.beatsCount}`;
      bottomSheetContent = () => (
        <BeatBottomSheetContent
          key="selected-beat-controls"
          title={bottomSheetTitle}
          icon={bottomSheetIcon}
          iconBackgroundColor={bottomSheetIconBackgroundColor}
          width={this.state.windowWidth}
          onDismiss={() => this._toggleBeat(selectedItem.beatId)}
          onChangeTempo={this._changeTempo}
          onChangeOffset={this._changeOffset}
          onChangeBeatsCount={this._changeBeatsCount}
          onDeleteBeat={this._deleteBeat}
          onMuteSection={this._toggleMuteSection}
          onToggleSoloBeat={this._toggleSoloBeat}
          onToggleMuteBeat={this._toggleMuteBeat}
          onChangeSound={this._changeSound}
          commonBeatsCount={commonBeatsCount}
          activeSoundType={this.state.activeSoundType}
          activeKit={activeKit}
          isMuteDisabled={isMuteDisabled}
          isMute={isMute}
          isSolo={isSolo}
          selectedBeat={selectedBeat}
          isMyChop={isMyChop}
        />
      );
    }

    let fab;

    if (isInitialized && isPaused && beats.length) {
      fab = (
        <FloatingActionButton
          key="fab-play"
          onTouchTap={this._startPlaying}
        >
          <IconPlay
            color={theme.color.onPrimary}
            width={24}
            height={24}
          />
        </FloatingActionButton>
      );
    }

    if (isInitialized && !isPaused && beats.length) {
      fab = (
        <FloatingActionButton
          key="fab-stop"
          onTouchTap={this._togglePause}
        >
          <IconStop
            color={theme.color.onPrimary}
            width={24}
            height={24}
          />
        </FloatingActionButton>
      );
    }

    const fabs = [fab];

    let menuFn;
    if (isInitialized && isMyChop && !showQuickStart) {
      const isPitchActive = this.state.activeSoundType === 'oscilator';
      const pitchIcon = isPitchActive ? 'done' : null;

      const soundChoices = [
        <MenuItem
          key="menu-item-sound-type-pitch"
          text={'Pitch'}
          iconName={pitchIcon}
          onTouchTap={() => {
            this.props.onChangeSoundType({ soundType: 'oscilator' });
          }}
        >
          Pitch
        </MenuItem>,
      ];

      for (let kit of kits) {
        let kitIconName = null;
        if (
          this.state.activeSoundType === 'kit' &&
          this.state.activeKitId === kit.id
        ) {
          kitIconName = 'done';
        }
        soundChoices.push(
          <MenuItem
            key={`menu-item-sound-type-kit-${kit.id}`}
            text={kit.name}
            iconName={kitIconName}
            onTouchTap={() => {
              this.props.onChangeSoundType({
                soundType: 'kit',
                kitId: kit.id,
              });
            }}
          >
            {kit.name}
          </MenuItem>
        );
      }

      menuFn = dismissMenu => {
        const addBeat = () => {
          this._addDefaultBeat();
          dismissMenu();
        };

        return [
          <MenuItem
            key="menu-item-add-beat"
            iconName="plus"
            onTouchTap={addBeat}
            text={'Add beat'}
          />,
          <Divider key="menu-divider-1" />,
          <MenuHeader
            key="menu-header-sound"
            text="Sound of the beat"
          />,
          ...soundChoices,
        ];
      };
    }

    let dialogContentFn = null;
    let dialogHeader = null;
    let dialogActions = [];

    if (showShareDialog && isMyChop && !showQuickStart) {
      dialogHeader = 'Sharing options';

      dialogContentFn = () => {
        let sharingDesc = `
          This chop is not listed on the home page.
          Others can only access it via its URL.
        `;

        if (this.state.isDiscoverable) {
          sharingDesc = `
          This chop is public and is listed on the home page.
          You can also share it via its URL.
          `;
        }

        const url = getFullUrl(this.props.location.pathname);

        return [
          <div key="dialog-item-text" style={{ marginBottom: 28 }}>
            <TextWrapper
              styleName={'body1'}
              color={theme.color.onSurfaceDisabled}
            >
              {sharingDesc}
            </TextWrapper>
          </div>,

          <ListItem key="dialog-item-name" noPadding>
            <div style={{ marginRight: 12 }}>
              <TextWrapper
                styleName="subtitle1"
                color={theme.color.onSurfaceMediumEmphasis}
              >
                {`Name`}
              </TextWrapper>
            </div>

            <InputText
              maxLength={16}
              onChange={this._changeName}
              value={this.state.name}
            />
          </ListItem>,

          <ListItem key="dialog-item-url" noPadding>
            <div style={{ marginRight: 12 }}>
              <TextWrapper
                styleName="subtitle1"
                color={theme.color.onSurfaceMediumEmphasis}
              >
                {`URL`}
              </TextWrapper>
            </div>
            <InputText readOnly value={url} />
          </ListItem>,

          <ListItem
            key="dialog-item-is-discoverable"
            onTouchTap={this._toggleIsDiscoverable}
            noPadding
          >
            <TextWrapper
              styleName="subtitle1"
              color={theme.color.onSurfaceMediumEmphasis}
            >
              {`Make public`}
            </TextWrapper>

            <Switch isOn={this.state.isDiscoverable} />
          </ListItem>,
        ];
      };

      dialogActions = [
        <ButtonText
          onTouchTap={this._hideShareDialog}
          color={theme.color.primary}
          key="dialog-done-button"
        >
          Done
        </ButtonText>,
      ];
    }

    if (showChangeNameDialog && isMyChop && !showQuickStart) {
      dialogHeader = 'Change name';
      dialogContentFn = () => {
        return (
          <ListItem key="dialog-item-name" noPadding>
            <div style={{ marginRight: 12 }}>
              <TextWrapper
                styleName="subtitle1"
                color={theme.color.onSurfaceMediumEmphasis}
              >
                {`Name`}
              </TextWrapper>
            </div>

            <InputText
              maxLength={16}
              onChange={this._changeName}
              value={this.state.name}
            />
          </ListItem>
        );
      };

      dialogActions = [
        <ButtonText
          onTouchTap={this._hideChangeNameDialog}
          color={theme.color.primary}
          key="dialog-done-button"
        >
          Done
        </ButtonText>,
      ];
    }

    let appBarActions = [];
    if (isInitialized && this.props.myAccount) {
      let shareOrRemix;

      if (isMyChop && !showQuickStart) {
        shareOrRemix = (
          <ButtonIcon
            key="topbar-action-share"
            width={24}
            onTouchTap={this._share}
          >
            <IconShare
              width={24}
              height={24}
              color={theme.color.onSurfaceMediumEmphasis}
            />
          </ButtonIcon>
        );
      }

      if (!isMyChop) {
        shareOrRemix = (
          <ButtonIcon
            key="topbar-action-remix"
            width={24}
            onTouchTap={this._remix}
          >
            <IconRemix
              width={24}
              height={24}
              color={theme.color.onSurfaceMediumEmphasis}
            />
          </ButtonIcon>
        );
      }

      appBarActions.push(shareOrRemix);
    }

    if (!showQuickStart) {
      appBarActions.push(
        <Link
          key="app-bar-action-form-link"
          to={`/form/${this.props.match.params.chopId}`}
          title="Switch to bar view"
        >
          <ButtonIcon key="topbar-action-rhythm" width={24}>
            <IconForm
              width={24}
              height={24}
              color={theme.color.onSurfaceMediumEmphasis}
            />
          </ButtonIcon>
        </Link>
      );
    }

    return (
      <TopBarContainer
        menuContent={menuFn}
        bottomSheetIcon={bottomSheetIcon}
        bottomSheetIconBackgroundColor={bottomSheetIconBackgroundColor}
        bottomSheetTitle={bottomSheetTitle}
        onDismissBottomSheet={this._deselectAll}
        dialogContent={dialogContentFn}
        dialogHeader={dialogHeader}
        dialogActions={dialogActions}
        appBarTitle={
          <button
            style={styles.__topBarButton}
            onClick={this._showChangeNameDialog}
          >
            <TextWrapper
              styleName={'topBarTitle'}
              color={theme.color.onSurfaceMediumEmphasis}
            >
              {this.state.name}
            </TextWrapper>
          </button>
        }
        appBarLeftIcon={
          <Link to={`/`} style={{ height: '100%', width: '100%' }}>
            <IconHome
              width={24}
              color={theme.color.onSurfaceMediumEmphasis}
            />
          </Link>
        }
        appBarActions={appBarActions}
        fabs={fabs}
      >
        <AudioLoader
          recordings={[]}
          beats={this.state.beats}
          lines={[]}
          kits={this.state.kits}
          audioContext={this._audioContext}
        >
          {({ recordingByLineId, beatsAudio, recordingsAudio }) => (
            <AudioPlayer
              key="sounds-audio-player"
              startTime={this.state.startTime}
              beats={audioBeats}
              lines={[]}
              commonBeatsCount={commonBeatsCount}
              commonBeatsTempo={commonBeatsTempo}
              recordingByLineId={recordingByLineId}
              beatsAudio={beatsAudio}
              recordingsAudio={recordingsAudio}
              audioContext={this._audioContext}
            />
          )}
        </AudioLoader>

        {loadingIndicator}
        {content}
        {bottomSheetContent ? bottomSheetContent() : chopFooter}
      </TopBarContainer>
    );
  }
}

export default connectChopEditor(Machine);
