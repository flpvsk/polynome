import nanoid from 'nanoid';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as chops from './data/chops';
import * as mediaDevices from './data/mediaDevices';
import * as kits from './data/kits';

import withMyAccountProvider from './data/withMyAccountProvider';
import withMyselfProvider from './data/withMyselfProvider';

const mapStateToProps = (state, props) => {
  return {
    chop: chops.select.getChopByIdFromMatch(state, props),
    recordingsAudioMap: chops.select.getRecordingsAudioMap(state),

    mediaDevices: mediaDevices.select.listMediaDevices(state),
    selectedInputDeviceId: mediaDevices.select.getSelectedInputDeviceId(
      state
    ),
    defaultLatency: mediaDevices.select.getDefaultLatency(state),

    kits: kits.select.listKits(state),

    canCreateChop: chops.select.canCreateChop(state, props),
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    getChop() {
      chops.run.getChopById(
        {
          chopId: props.match.params.chopId,
        },
        dispatch
      );
    },

    onChangeDefaultLatency({ deviceId, latency }) {
      mediaDevices.run.setDefaultLatency(
        { deviceId, latency },
        dispatch
      );
    },

    listMediaDevices() {
      mediaDevices.run.listMediaDevices(null, dispatch);
    },

    listKits() {
      kits.run.listKits(null, dispatch);
    },

    onChangeSelectedInputDeviceId: ({ deviceId }) => {
      mediaDevices.run.changeSelectedInputDeviceId(
        { deviceId },
        dispatch
      );
    },

    onChangeSound: ({ beatId, sound }) => {
      const { chopId } = props.chop;
      chops.run.changeSound({ chopId, beatId, sound }, dispatch);
    },

    onChangeSoundType: ({ soundType, kitId }) => {
      const { chopId } = props.chop;
      chops.run.changeSoundType({ chopId, soundType, kitId }, dispatch);
    },

    onAddBeat: ({ beat }) => {
      const { chopId } = props.chop;
      chops.run.addBeat({ chopId, beat }, dispatch);
    },

    onAddLine: ({ line, writingRecordingId }) => {
      const { chopId } = props.chop;
      chops.run.addLine({ chopId, line, writingRecordingId }, dispatch);
    },

    onStartRecording: ({ lineId, recording, audioData }) => {
      const { chopId } = props.chop;
      chops.run.startRecording(
        {
          chopId,
          lineId,
          recording,
          audioData,
        },
        dispatch
      );
    },

    onFinishRecording: ({
      lineId,
      recordingId,
      recording,
      audioData,
    }) => {
      const { chopId } = props.chop;
      chops.run.finishRecording(
        {
          chopId,
          lineId,
          recordingId,
          recording,
          audioData,
        },
        dispatch
      );
    },

    onDeleteBeat: ({ beatId }) => {
      const { chopId } = props.chop;
      chops.run.deleteBeat({ chopId, beatId }, dispatch);
    },

    onDeleteLine: ({ lineId }) => {
      const { chopId } = props.chop;
      chops.run.deleteLine({ chopId, lineId }, dispatch);
    },

    onUpdateChop: ({ update }) => {
      const { chopId } = props.chop;
      chops.run.updateChop({ chopId, update }, dispatch);
    },

    onUpdateLine: ({ lineId, update }) => {
      const { chopId } = props.chop;
      chops.run.updateLine({ chopId, lineId, update }, dispatch);
    },

    onChangeRecordingLatency: ({ recordingId, latency }) => {
      const { chopId } = props.chop;
      chops.run.changeRecordingLatency(
        {
          chopId,
          recordingId,
          latency,
        },
        dispatch
      );
    },

    onChangeName: ({ name }) => {
      const { chopId } = props.chop;
      chops.run.changeName({ chopId, name }, dispatch);
    },

    onChangeTempo: ({ beatId, tempo }) => {
      const { chopId } = props.chop;
      chops.run.changeTempo({ chopId, beatId, tempo }, dispatch);
    },

    onChangeBarsCount: ({ barsCount }) => {
      const { chopId } = props.chop;
      chops.run.changeBarsCount({ chopId, barsCount }, dispatch);
    },

    onChangeOffset: ({ beatId, offset }) => {
      const { chopId } = props.chop;
      chops.run.changeOffset({ chopId, beatId, offset }, dispatch);
    },

    onChangeBeatsCount: ({ beatId, beatsCount }) => {
      const { chopId } = props.chop;
      chops.run.changeBeatsCount(
        { chopId, beatId, beatsCount },
        dispatch
      );
    },

    onToggleMuteBeat: ({ beatId }) => {
      const { chopId } = props.chop;
      chops.run.toggleMuteBeat({ chopId, beatId }, dispatch);
    },

    onToggleMuteSection: ({ beatId, sectionNumber }) => {
      const { chopId } = props.chop;
      chops.run.toggleMuteSection(
        { chopId, beatId, sectionNumber },
        dispatch
      );
    },

    onToggleMuteLine: ({ lineId }) => {
      const { chopId } = props.chop;
      chops.run.toggleMuteLine({ chopId, lineId }, dispatch);
    },

    onToggleSoloItem: ({ beatId, lineId, itemType }) => {
      const { chopId } = props.chop;
      chops.run.toggleSoloItem(
        { chopId, lineId, beatId, itemType },
        dispatch
      );
    },

    onClap: ({ chopId, personId, clapsCount }) => {
      chops.run.clap({ chopId, personId, clapsCount }, dispatch);
    },

    onSendComment: ({ text, chopId, personId }) => {
      chops.run.sendComment(
        {
          text,
          chopId,
          personId,
        },
        dispatch
      );
    },

    onRemix: ({ remixOf, personId, type }) => {
      if (!props.canCreateChop) {
        props.history.push(`/upgrade`);
        return;
      }

      type = type || 'form';
      const chopId = nanoid();
      chops.run.remixChop(
        {
          composedBy: personId,
          chopId,
          remixOf,
          type: 'form',
        },
        dispatch
      );

      props.history.push(`/${type}/${chopId}`);
    },

    onChooseTemplate: (chopId, template) => {
      chops.run.chooseTemplate({ chopId, template }, dispatch);
    },
  };
};

const connectChopEditor = Component =>
  withRouter(
    withMyselfProvider(
      withMyAccountProvider(
        connect(mapStateToProps)(
          connect(null, mapDispatchToProps)(Component)
        )
      )
    )
  );

export default connectChopEditor;
