import React, { Component } from 'react';
import { connect } from 'react-redux';

import LoadingIndicatorCircular from './LoadingIndicatorCircular';

import * as alerts from './data/alerts';

import withMyselfProvider from './data/withMyselfProvider';

import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';

import Home from './Home';
import Start from './Start';
import Machine from './Machine';
import Tape from './Tape';
import Profile from './Profile';
import Account from './Account';
import Subscription from './Subscription';
import Notifications from './Notifications';
import Upgrade from './Upgrade';
import PageNotFound from './PageNotFound';

import Analytics from './Analytics';

import TextWrapper from './TextWrapper';
import ButtonText from './ButtonText';

import Snackbar from './Snackbar';

import theme from './theme';
import './App.css';

const styles = {
  block: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    color: theme.color.onBackground,
    height: '100%',
  },
};

class App extends Component {
  constructor() {
    super();

    this._reloadAndDismiss = ({ alertId }) => {
      this.props.dismissAlert({ alertId });
      window.location.reload(true);
    };
  }

  // TODO: figure out router basename
  render() {
    const { me, isGetMyselfLoading } = this.props;
    if (isGetMyselfLoading) {
      return (
        <div style={styles.block}>
          <LoadingIndicatorCircular />
        </div>
      );
    }

    let alert = this.props.alertsList[0];
    let alertContent;

    if (alert && alert.type === alerts.UPDATES_AVAILABLE) {
      const { alertId } = alert;

      alertContent = [
        <div
          style={{ flexGrow: 1 }}
          key={`${alert.alertId}-alert-wrap`}
        >
          <TextWrapper
            key={`${alert.alertId}-alert-text`}
            color={theme.color.onSnackbar}
            styleName={'body2'}
          >
            Updates available
          </TextWrapper>
        </div>,

        <ButtonText
          key={`${alert.alertId}-dismiss`}
          onTouchTap={() =>
            this.props.dismissAlert({
              alertId,
            })
          }
        >
          hide
        </ButtonText>,

        <ButtonText
          key={`${alert.alertId}-reload`}
          onTouchTap={() => this._reloadAndDismiss({ alertId })}
        >
          reload
        </ButtonText>,
      ];
    }

    return (
      <div style={styles.block}>
        <Router baseName="/">
          <Analytics>
            <Switch>
              <Route
                key="home-route"
                exact
                path="/"
                render={props => {
                  if (me) {
                    return <Home />;
                  }

                  return <Start />;
                }}
              />
              <Route
                key="machine-route"
                path="/feel/:chopId"
                component={Machine}
              />
              <Route
                key="tape-route"
                path="/form/:chopId"
                component={Tape}
              />
              <Route
                key="profile-route"
                path="/people/:personId"
                component={Profile}
              />
              <Route
                key="notifications-route"
                path="/notifications"
                component={Notifications}
              />

              <Route
                key="account-route"
                path="/account"
                component={Account}
              />

              <Route
                key="subscription-route"
                path="/subscription"
                component={Subscription}
              />

              <Route
                key="upgrade-route"
                path="/upgrade"
                component={Upgrade}
              />

              <Route key="not-found" component={PageNotFound} />
            </Switch>
          </Analytics>
        </Router>
        <Snackbar isVisible={!!alert}>{alertContent}</Snackbar>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    alertsList: alerts.select.listActiveAlerts(state),
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    dismissAlert: ({ alertId }) => {
      alerts.run.dismissAlert({ alertId }, dispatch);
    },
  };
};

export default withMyselfProvider(
  connect(mapStateToProps, mapDispatchToProps)(App)
);
