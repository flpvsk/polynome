import React from 'react';
import TextWrapper from './TextWrapper';
import theme from './theme';

const styles = {
  block: {
    fontSize: '1em',
    textAlign: 'center',
    lineHeight: '1em',
    color: theme.color.onSurface,

    boxShadow: '0 0 8px #222',
    borderTopLeftRadius: theme.shape.borderRadius,
    borderBottomLeftRadius: theme.shape.borderRadius,
    outline: 'none',
    border: 'none',
    userSelect: 'none',
    height: '48px',
    width: '48px',
    cursor: 'pointer',
  },

  _isPressed: {
    border: '2px solid #eee',
    boxShadow: 'none',
    color: '#eee',
    fontWeight: 600,
  },

  _hasOutline: {
    border: `2px solid #fff`,
  },
};

const AccentButton = props => {
  let buttonStyle = {
    ...styles.block,
  };

  if (props.backgroundColor) {
    buttonStyle = {
      ...buttonStyle,
      backgroundColor: props.backgroundColor,
    };
  }

  if (props.isPressed) {
    buttonStyle = {
      ...buttonStyle,
      ...styles._isPressed,
    };
  }

  if (props.hasOutline) {
    buttonStyle = {
      ...buttonStyle,
      ...styles._hasOutline,
    };
  }

  return (
    <button
      style={buttonStyle}
      title={props.title}
      onClick={props.onTouchTap}
    >
      <TextWrapper
        styleName="h6"
        color={props.textColor || theme.color.onSecondaryMediumEmphasis}
      >
        {props.children}
      </TextWrapper>
    </button>
  );
};

export default AccentButton;
