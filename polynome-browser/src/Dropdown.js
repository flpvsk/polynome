import React from 'react';

// TODO: not done

const styles = {
  block: {
    height: 58,
    background: 'none',
    appearance: 'none',
    border: 0,
  },
};

class Dropdown extends React.Component {
  render() {
    const options = this.props.options.map(o => {
      return (
        <option key={o.key} value={o.value}>
          {o.text}
        </option>
      );
    });

    return (
      <select
        style={styles.block}
        onChange={this.props.onChange}
        value={this.props.value}
        disabled={this.props.isDisabled}
      >
        {options}
      </select>
    );
  }
}

export default Dropdown;
