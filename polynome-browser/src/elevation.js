import color from './color';

const UMBRA_COLOR = [0, 0, 0, 0.2];
const UMBRA_COLOR_STR = color.toHslaString(UMBRA_COLOR);
const PENUMBRA_COLOR = [0, 0, 0, 0.14];
const PENUMBRA_COLOR_STR = color.toHslaString(PENUMBRA_COLOR);
const AMBIENT_COLOR = [0, 0, 0, 0.12];
const AMBIENT_COLOR_STR = color.toHslaString(AMBIENT_COLOR);

const UMBRA_MAP = {
  0: '0px 0px 0px 0px',
  1: '0px 2px 1px -1px',
  2: '0px 3px 1px -2px',
  3: '0px 3px 3px -2px',
  4: '0px 2px 4px -1px',
  5: '0px 3px 5px -1px',
  6: '0px 3px 5px -1px',
  7: '0px 4px 5px -2px',
  8: '0px 5px 5px -3px',
  9: '0px 5px 6px -3px',
  10: '0px 6px 6px -3px',
  11: '0px 6px 7px -4px',
  12: '0px 7px 8px -4px',
  13: '0px 7px 8px -4px',
  14: '0px 7px 9px -4px',
  15: '0px 8px 9px -5px',
  16: '0px 8px 10px -5px',
  17: '0px 8px 11px -5px',
  18: '0px 9px 11px -5px',
  19: '0px 9px 12px -6px',
  20: '0px 10px 13px -6px',
  21: '0px 10px 13px -6px',
  22: '0px 10px 14px -6px',
  23: '0px 11px 14px -7px',
  24: '0px 11px 15px -7px',
};

const PENUMBRA_MAP = {
  0: '0px 0px 0px 0px',
  1: '0px 1px 1px 0px',
  2: '0px 2px 2px 0px',
  3: '0px 3px 4px 0px',
  4: '0px 4px 5px 0px',
  5: '0px 5px 8px 0px',
  6: '0px 6px 10px 0px',
  7: '0px 7px 10px 1px',
  8: '0px 8px 10px 1px',
  9: '0px 9px 12px 1px',
  10: '0px 10px 14px 1px',
  11: '0px 11px 15px 1px',
  12: '0px 12px 17px 2px',
  13: '0px 13px 19px 2px',
  14: '0px 14px 21px 2px',
  15: '0px 15px 22px 2px',
  16: '0px 16px 24px 2px',
  17: '0px 17px 26px 2px',
  18: '0px 18px 28px 2px',
  19: '0px 19px 29px 2px',
  20: '0px 20px 31px 3px',
  21: '0px 21px 33px 3px',
  22: '0px 22px 35px 3px',
  23: '0px 23px 36px 3px',
  24: '0px 24px 38px 3px',
};

const AMBIENT_MAP = {
  0: '0px 0px 0px 0px',
  1: '0px 1px 3px 0px',
  2: '0px 1px 5px 0px',
  3: '0px 1px 8px 0px',
  4: '0px 1px 10px 0px',
  5: '0px 1px 14px 0px',
  6: '0px 1px 18px 0px',
  7: '0px 2px 16px 1px',
  8: '0px 3px 14px 2px',
  9: '0px 3px 16px 2px',
  10: '0px 4px 18px 3px',
  11: '0px 4px 20px 3px',
  12: '0px 5px 22px 4px',
  13: '0px 5px 24px 4px',
  14: '0px 5px 26px 4px',
  15: '0px 6px 28px 5px',
  16: '0px 6px 30px 5px',
  17: '0px 6px 32px 5px',
  18: '0px 7px 34px 6px',
  19: '0px 7px 36px 6px',
  20: '0px 8px 38px 7px',
  21: '0px 8px 40px 7px',
  22: '0px 8px 42px 7px',
  23: '0px 9px 44px 8px',
  24: '0px 9px 46px 8px',
};

export function elevationToStyle(elevation) {
  if (elevation === 0) {
    return {
      boxShadow: 'none',
      zIndex: 0,
    };
  }

  if (elevation < 0 && elevation > -24) {
    let el = Math.abs(elevation);
    return {
      boxShadow:
        `${UMBRA_MAP[el]} ${UMBRA_COLOR_STR} inset, ` +
        `${PENUMBRA_MAP[el]} ${PENUMBRA_COLOR_STR} inset, ` +
        `${AMBIENT_MAP[el]} ${AMBIENT_COLOR_STR} inset`,
    };
  }

  if (elevation > 24 || elevation < -24) {
    throw new Error(`Unsupported elevation level ${elevation}`);
  }

  return {
    boxShadow:
      `${UMBRA_MAP[elevation]} ${UMBRA_COLOR_STR}, ` +
      `${PENUMBRA_MAP[elevation]} ${PENUMBRA_COLOR_STR}, ` +
      `${AMBIENT_MAP[elevation]} ${AMBIENT_COLOR_STR}`,
    zIndex: elevation,
  };
}

export function elevationToTextStyle(elevation) {
  if (elevation === 0) {
    return {};
  }

  if (elevation > 24) {
    throw new Error(`Unsupported elevation level ${elevation}`);
  }

  const shadows = elevationToShadows(elevation);
  const textShadow = shadows
    .reduce((a, v) => {
      a.push(`${v.offsetX}px ${v.offsetY}px ${v.blur}px ${v.color}`);
      return a;
    }, [])
    .join(', ');

  const style = { textShadow };

  return style;
}

export function elevationToShadows(elevation) {
  if (elevation === 0) {
    return [];
  }

  if (elevation > 24) {
    throw new Error(`Unsupported elevation level ${elevation}`);
  }

  const umbraValues = UMBRA_MAP[elevation]
    .split(' ')
    .map(v => parseInt(v, 10));
  const penumbraValues = PENUMBRA_MAP[elevation]
    .split(' ')
    .map(v => parseInt(v, 10));
  const ambientValues = AMBIENT_MAP[elevation]
    .split(' ')
    .map(v => parseInt(v, 10));

  return [
    {
      offsetX: umbraValues[0],
      offsetY: umbraValues[1],
      blur: umbraValues[2],
      spread: umbraValues[3],
      color: UMBRA_COLOR_STR,
    },
    {
      offsetX: penumbraValues[0],
      offsetY: penumbraValues[1],
      blur: penumbraValues[2],
      spread: penumbraValues[3],
      color: PENUMBRA_COLOR_STR,
    },
    {
      offsetX: ambientValues[0],
      offsetY: ambientValues[1],
      blur: ambientValues[2],
      spread: ambientValues[3],
      color: AMBIENT_COLOR_STR,
    },
  ];
}

const module = {
  elevationToStyle,
  elevationToShadows,
};

export default module;
