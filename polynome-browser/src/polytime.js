const SECONDS_IN_ONE_MINUTE = 60;

// to, from and start are in seconds
function polytime(config, from, to) {
  if (!config.beats || !config.beats.length) {
    return [];
  }

  if (!config.tempo) {
    return [];
  }

  const { tempo, beats } = config;
  const start = config.start || 0;
  from = from || start;
  to = to || undefined;

  const interval = SECONDS_IN_ONE_MINUTE / tempo;

  // 0: start
  // 1: start + interval
  // 2: start + interval * 2
  // 3: start + interval * 3
  //
  // timeOfBeatN = start + interval * N
  //
  // start + interval * N >= from
  // N >= (from - start) / interval

  const firstBeatNumber = Math.ceil((from - start) / interval);
  let baseBeat = firstBeatNumber;
  let baseBeatTime = start + interval * baseBeat;

  const reduceFn = (ids, beat) => {
    const offset = beat.offset || 0;
    const every = beat.every || 1;
    const shouldSound = (baseBeat - offset) % every === 0;

    if (shouldSound) {
      ids.push(beat.id);
    }

    return ids;
  };

  const results = [];
  while (baseBeatTime < to) {
    const beatsIds = beats.reduce(reduceFn, []);

    if (beatsIds.length > 0) {
      results.push({
        time: baseBeatTime,
        beatsIds,
      });
    }

    baseBeat += 1;
    baseBeatTime += interval;
  }

  return results;
}

export default polytime;
