import { Component } from 'react';
import { withRouter } from 'react-router-dom';
import shouldComponentUpdate from './shouldComponentUpdate';

class Analytics extends Component {
  constructor() {
    super();
    this.shouldComponentUpdate = shouldComponentUpdate;
    this._onHistoryUpdate = (location, action) => {
      if (!window.gtag) {
        return;
      }

      const locationStr = `${location.pathname}${location.search}${
        location.hash
      }`;

      window.gtag(
        'config',
        process.env.REACT_APP_GOOGLE_ANALYTICS_TRACKING_CODE,
        {
          page_path: locationStr,
          anonymize_ip: true,
        }
      );
    };
  }

  componentDidMount() {
    this._unlisten = this.props.history.listen(this._onHistoryUpdate);
  }

  componentWillUnmount() {
    this._unlisten();
  }

  render() {
    return this.props.children;
  }
}

export default withRouter(Analytics);
