import React from 'react';
import theme from './theme';
import isNumber from 'lodash/isNumber';
import isNaN from 'lodash/isNaN';
import { styles as textStyles } from './TextWrapper';

const styles = {
  block: {
    height: 40,
    background: 'none',
    appearance: 'none',
    border: 'none',
    backgroundColor: theme.color.containerOnSurface,
    borderBottom: `1px solid ${theme.color.outline}`,
    borderTopLeftRadius: theme.shape.borderRadius,
    borderTopRightRadius: theme.shape.borderRadius,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    paddingLeft: 12,
    paddingRight: 12,
    color: theme.color.onSurface,
    textAlign: 'right',
    ...textStyles.subtitle1,
  },

  _isDisabled: {
    color: theme.color.onSurfaceDisabled,
  },
};

class NumberFieldListItem extends React.Component {
  static getDerivedStateFromProps(nextProps, prevState) {
    const propsValue = nextProps.value;
    const stateValue = prevState.value;

    if (parseFloat(stateValue, 10) === propsValue) {
      return { value: stateValue };
    }

    return { value: propsValue };
  }

  constructor() {
    super();
    this.state = {
      value: 0,
    };

    this._onChange = e => {
      const value = e.target.value.replace(',', '.');
      const floatValue = parseFloat(value, 10);
      this.setState({ value });
      if (isNumber(floatValue) && !isNaN(floatValue)) {
        this.props.onChange(floatValue);
      }
    };
  }

  render() {
    let blockStyle = styles.block;

    if (this.props.isDisabled) {
      blockStyle = {
        ...blockStyle,
        ...styles._isDisabled,
      };
    }

    return (
      <input
        type="number"
        style={blockStyle}
        onChange={this._onChange}
        value={this.state.value}
        disabled={this.props.isDisabled}
        onClick={e => e.stopPropagation()}
      />
    );
  }
}

export default NumberFieldListItem;
