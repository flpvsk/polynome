import React, { Component } from 'react';

import theme from './theme';

import TextWrapper from './TextWrapper';
import IconMinus from './IconMinus';
import IconPlus from './IconPlus';

const styles = {
  block: {
    height: 48,
    width: '100%',
    minWidth: '10em',
    paddingLeft: 16,
    paddingRight: 16,
    display: 'flex',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'stretch',
  },

  __decreaseButton: {
    borderTopLeftRadius: theme.shape.borderRadius,
    borderBottomLeftRadius: theme.shape.borderRadius,
    borderWidth: 1,
    borderColor: theme.color.outline,
    background: 'none',
    display: 'flex',
    width: 32,
    height: 32,
    justifyContent: 'center',
    marginLeft: -6,
    outline: 'none',
    boxShadow: 'none',
  },

  __increaseButton: {
    borderTopRightRadius: theme.shape.borderRadius,
    borderBottomRightRadius: theme.shape.borderRadius,
    borderWidth: 1,
    borderColor: theme.color.outline,
    background: 'none',
    height: 32,
    width: 32,
    display: 'flex',
    justifyContent: 'center',
    marginRight: -6,
    outline: 'none',
    boxShadow: 'none',
  },

  __textBlock: {
    flexGrow: 1,
    textAlign: 'center',
    borderTop: `1px solid ${theme.color.outline}`,
    borderBottom: `1px solid ${theme.color.outline}`,
    height: 32,
    paddingLeft: 20,
    paddingRight: 20,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
};

class MenuItemBarsCountControl extends Component {
  constructor() {
    super();
    this._changeBarsCount = value => () => {
      let newValue = this.props.barsCount + value;

      if (newValue <= 0) {
        newValue = 1;
      }

      if (newValue === this.props.barsCount) {
        return;
      }

      this.props.onChangeBarsCount(newValue);
    };
  }

  render() {
    const { props } = this;
    let text = props.barsCount;
    let iconMinusColor = theme.color.onSurfaceMediumEmphasis;
    let minusButtonDisabled = false;

    if (props.barsCount === 1) {
      text += ' bar';
      iconMinusColor = theme.color.onSurfaceDisabled;
      minusButtonDisabled = true;
    }

    if (props.barsCount > 1) {
      text += ' bars';
    }

    return (
      <div style={styles.block}>
        <button
          disabled={minusButtonDisabled}
          style={styles.__decreaseButton}
          onClick={this._changeBarsCount(-1)}
        >
          <IconMinus color={iconMinusColor} height={24} width={24} />
        </button>
        <div style={styles.__textBlock}>
          <TextWrapper
            styleName="body1"
            color={theme.color.onSurfaceMediumEmphasis}
          >
            {text}
          </TextWrapper>
        </div>
        <button
          style={styles.__increaseButton}
          onClick={this._changeBarsCount(1)}
        >
          <IconPlus
            color={theme.color.onSurfaceMediumEmphasis}
            height={24}
            width={24}
          />
        </button>
      </div>
    );
  }
}

export default MenuItemBarsCountControl;
