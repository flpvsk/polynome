import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import nanoid from 'nanoid';

import LoadingIndicatorCircular from './LoadingIndicatorCircular';

import { connect } from 'react-redux';
import * as people from './data/people';
import * as chops from './data/chops';

import theme from './theme';

import shouldComponentUpdate from './shouldComponentUpdate';

import PolyAudioContext, { prepareContext } from './AudioContext';

import AppBarTopContainer from './AppBarTopContainer';
import TextWrapper from './TextWrapper';
import ButtonIcon from './ButtonIcon';
import ProfileHeader from './ProfileHeader';
import ListItem from './ListItem';
import ButtonText from './ButtonText';
import ButtonContained from './ButtonContained';
import Switch from './Switch';
import InputComment from './InputComment';

import CardFeed from './CardFeed';
import ChopPlayer from './ChopPlayer';
import Divider from './Divider';

import IconMenu from './IconMenu';
import IconHome from './IconHome';

import withNavigation from './withNavigation';
import withMyAccountProvider from './data/withMyAccountProvider';

const styles = {
  __content: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    width: '100%',
    paddingLeft: 16,
    paddingRight: 16,
  },

  __subheader: {
    width: '100%',
    height: 48,
    display: 'flex',
    alignItems: 'center',
  },

  __removeButtonWrapper: {
    marginLeft: -16,
  },

  __loading: {
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'center',
    width: '100%',
    height: 72,
  },
};

class Profile extends Component {
  constructor() {
    super();
    this.state = {
      playingChop: null,
      selectedChopId: null,
      commentingOn: null,
      commentsToShowByChopId: {},
      loadingAudioFor: [],
    };

    this.shouldComponentUpdate = shouldComponentUpdate;

    this._audioContext = new PolyAudioContext();

    this._playChop = async chop => {
      await prepareContext(this._audioContext);
      this.setState({
        playingChop: chop,
      });
    };

    this._stop = () => {
      this.setState({
        playingChop: null,
      });
    };

    this._onStartAudioLoad = chopId => {
      const { loadingAudioFor } = this.state;
      const loadingAudioSet = new Set(loadingAudioFor);
      loadingAudioSet.add(chopId);

      this.setState({
        loadingAudioFor: [...loadingAudioSet],
      });
    };

    this._onFinishAudioLoad = chopId => {
      const { loadingAudioFor } = this.state;
      this.setState({
        loadingAudioFor: loadingAudioFor.filter(c => c !== chopId),
      });
    };

    this._showChopOptions = chop => {
      this.setState({ selectedChopId: chop.chopId });
    };

    this._hideChopOptions = () => {
      this.setState({ selectedChopId: null });
    };

    this._toggleIsDiscoverable = () => {
      const { selectedChopId } = this.state;
      const { chopsList } = this.props;
      const chop = chopsList.find(c => c.chopId === selectedChopId);

      this.props.onSetDiscoverable({
        chopId: chop.chopId,
        isDiscoverable: !chop.isDiscoverable,
      });
    };

    this._deleteSelectedChop = () => {
      const chopId = this.state.selectedChopId;
      this.props.onDeleteChop({ chopId });
    };

    this._restoreSelectedChop = () => {
      const chopId = this.state.selectedChopId;
      this.props.onRestoreChop({ chopId });
    };

    this._createChop = () => {
      const { me, onCreateChop } = this.props;
      onCreateChop({ composedBy: me.personId });
    };

    this._clap = opts => {
      if (!this.props.myAccount) {
        return;
      }

      this.props.onClap(opts);
    };

    this._commentOn = chopId => {
      if (!this.props.myAccount) {
        return;
      }

      this.setState({
        commentingOn: chopId,
      });
    };

    this._stopCommenting = e => {
      e.preventDefault();
      e.stopPropagation();

      this.setState({
        commentingOn: null,
      });
    };

    this._sendComment = text => {
      const chopId = this.state.commentingOn;
      const personId = this.props.me.personId;

      this.setState({
        commentingOn: null,
      });

      const chop = this._findChop(chopId);

      this._changeCommentsCountToShow(chopId, chop.comments.length + 1);

      this.props.onSendComment({
        chopId,
        text,
        personId,
      });
    };

    this._getCommentsToShowByChopId = chopId => {
      const commentsToShow = this.state.commentsToShowByChopId[chopId];
      if (!commentsToShow) {
        return 2;
      }
    };

    this._changeCommentsCountToShow = (chopId, count) => {
      this.setState({
        commentsToShowByChopId: {
          ...this.state.commentsToShowByChopId,
          [chopId]: count,
        },
      });
    };

    this._findChop = chopId => {
      return this.props.chopsList.find(c => c.chopId === chopId);
    };

    this._goHome = () => {
      this.props.history.push(`/`);
    };
  }

  componentDidMount() {
    const personId = this.props.match.params.personId;

    if (!this.props.person) {
      this.props.getPerson({ personId });
    }

    this.props.listChops({ personId });
  }

  componentDidUpdate() {}

  render() {
    const { me, person, chopsList } = this.props;
    const {
      playingChop,
      selectedChopId,
      commentingOn,
      loadingAudioFor,
    } = this.state;
    const chopsFeed = chopsList.filter(c => !c.isRemoved);

    let audio = null;
    let dismissInputComment = null;
    let inputComment = null;

    if (!this.props.person) {
      return (
        <div style={styles.__loading}>
          <LoadingIndicatorCircular />
        </div>
      );
    }

    if (playingChop) {
      audio = (
        <ChopPlayer
          audioContext={this._audioContext}
          chop={playingChop}
          startTime={0}
          onStartLoad={this._onStartAudioLoad}
          onFinishLoad={this._onFinishAudioLoad}
        />
      );
    }

    let isMyProfile = me && person.personId === me.personId;

    let onTriggerMenu;
    if (isMyProfile) {
      onTriggerMenu = this._showChopOptions;
    }

    let commentingOnChop = this._findChop(commentingOn);
    if (commentingOnChop) {
      inputComment = (
        <InputComment
          key={`${commentingOnChop.chopId}-new-comment`}
          onSend={this._sendComment}
        />
      );
      dismissInputComment = this._stopCommenting;
    }

    const chopsCards = chopsFeed.map(chop => {
      return (
        <CardFeed
          key={`chop-card-${chop.chopId}`}
          me={me}
          myAccount={this.props.myAccount}
          chop={chop}
          commentsToShow={this._getCommentsToShowByChopId(chop.chopId)}
          isPlaying={playingChop && playingChop.chopId === chop.chopId}
          isLoading={loadingAudioFor.indexOf(chop.chopId) > -1}
          onTriggerMenu={onTriggerMenu}
          onPlay={this._playChop}
          onStop={this._stop}
          onRemix={this.props.onRemix}
          onClap={this._clap}
          onComment={this._commentOn}
          onChangeCommentsCountToShow={this._changeCommentsCountToShow}
        />
      );
    });

    let createFirstChop;
    if (chopsFeed.length === 0 && isMyProfile) {
      createFirstChop = (
        <div>
          <ButtonContained onTouchTap={this._createChop}>
            Create chop
          </ButtonContained>
        </div>
      );
    }

    let bottomSheetContent;
    let bottomSheetTitle;

    if (selectedChopId) {
      const chop = chopsList.find(c => c.chopId === selectedChopId);

      bottomSheetTitle = chop.name;
      let removeRestore = (
        <ButtonText onTouchTap={this._deleteSelectedChop}>
          remove chop
        </ButtonText>
      );

      if (chop.isRemoved) {
        removeRestore = (
          <ButtonText onTouchTap={this._restoreSelectedChop}>
            restore chop
          </ButtonText>
        );
      }

      bottomSheetContent = () => [
        <ListItem
          key="item-is-discoverable"
          onTouchTap={this._toggleIsDiscoverable}
        >
          <TextWrapper
            styleName="subtitle1"
            color={theme.color.onSurfaceMediumEmphasis}
          >
            {`Make public`}
          </TextWrapper>

          <Switch isOn={chop.isDiscoverable} />
        </ListItem>,

        <ListItem key="item-remove">
          <div style={styles.__removeButtonWrapper}>
            {removeRestore}
          </div>
        </ListItem>,
      ];
    }

    let appBarLeftAction = (
      <ButtonIcon onTouchTap={this._goHome}>
        <IconHome
          width="24"
          height="24"
          color={theme.color.onBackgroundMediumEmphasis}
        />
      </ButtonIcon>
    );

    if (this.props.me) {
      appBarLeftAction = (
        <ButtonIcon onTouchTap={this.props.onShowNavigation}>
          <IconMenu
            width="24"
            height="24"
            color={theme.color.onBackgroundMediumEmphasis}
          />
        </ButtonIcon>
      );
    }

    return (
      <AppBarTopContainer
        showNavigation={this.props.showNavigation}
        bottomSheetTitle={bottomSheetTitle}
        bottomSheetHeaderText={bottomSheetTitle}
        bottomSheetContent={bottomSheetContent}
        onDismissBottomSheet={this._hideChopOptions}
        navigationContent={this.props.navigationContent}
        appBarTitle={
          <TextWrapper
            styleName={'topBarTitle'}
            color={theme.color.onSurfaceMediumEmphasis}
          >
            {isMyProfile
              ? 'My music profile'
              : (person.handle || 'anonymous') + ' music profile'}
          </TextWrapper>
        }
        appBarLeftIcon={appBarLeftAction}
        appBarActions={[]}
      >
        <div style={styles.__content} onClick={dismissInputComment}>
          <ProfileHeader
            me={me}
            person={person}
            onChangeHandle={this.props.changeHandle}
            onChangeUserpic={this.props.changeUserpic}
          />

          <Divider />

          <div key="my-chops-header" style={styles.__subheader}>
            <TextWrapper
              styleName={'subtitle2'}
              color={theme.color.onBackgroundDisabled}
            >
              {isMyProfile ? 'My chops' : 'Chops'}
            </TextWrapper>
          </div>

          {chopsCards}
          {createFirstChop}
          {inputComment}
          {audio}
        </div>
      </AppBarTopContainer>
    );
  }
}

const mapStateToProps = (state, props) => {
  // const personId = props.match.params.personId;
  return {
    person: people.select.getPersonFromMatch(state, props),
    me: people.select.getMyself(state),
    chopsList: chops.select.listChopsByPersonFromMatch(state, props),
    canCreateChop: chops.select.canCreateChop(state, props),
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    listChops: ({ personId }) => {
      chops.run.listChopsByPerson({ composedBy: personId }, dispatch);
    },

    getPerson: ({ personId }) => {
      people.run.getPersonById({ personId }, dispatch);
    },

    changeHandle: ({ handle, personId }) => {
      people.run.changeHandle({ personId: personId, handle }, dispatch);
    },

    changeUserpic: ({ personId, file, dataUrl }) => {
      people.run.changeUserpic({ personId, dataUrl, file }, dispatch);
    },

    onSetDiscoverable: ({ chopId, isDiscoverable }) => {
      chops.run.updateChop(
        {
          chopId,
          update: { isDiscoverable },
        },
        dispatch
      );
    },

    onDeleteChop: ({ chopId }) => {
      chops.run.updateChop(
        {
          chopId,
          update: { isRemoved: true },
        },
        dispatch
      );
    },

    onRestoreChop: ({ chopId }) => {
      chops.run.updateChop(
        {
          chopId,
          update: { isRemoved: false },
        },
        dispatch
      );
    },

    onCreateChop: ({ composedBy }) => {
      if (!props.canCreateChop) {
        props.history.push(`/upgrade`);
        return;
      }

      const chopId = nanoid();

      chops.run.createNewChop(
        {
          composedBy: composedBy,
          chopId,
          type: 'feel',
        },
        dispatch
      );

      props.history.push(`/feel/${chopId}`);
    },

    onRemix: ({ remixOf, remixOfChop, personId }) => {
      if (!props.canCreateChop) {
        props.history.push(`/upgrade`);
        return;
      }

      const chopId = nanoid();
      chops.run.remixChop(
        {
          composedBy: personId,
          chopId,
          remixOf,
          type: 'form',
        },
        dispatch
      );

      if (remixOfChop.lines && remixOfChop.lines.length > 0) {
        props.history.push(`/form/${chopId}`);
        return;
      }

      props.history.push(`/feel/${chopId}`);
    },

    onClap: ({ chopId, personId, clapsCount }) => {
      chops.run.clap({ chopId, personId, clapsCount }, dispatch);
    },

    onSendComment: ({ text, chopId, personId }) => {
      chops.run.sendComment(
        {
          text,
          chopId,
          personId,
        },
        dispatch
      );
    },
  };
};

export default withRouter(
  withMyAccountProvider(
    connect(mapStateToProps)(
      connect(null, mapDispatchToProps)(withNavigation(Profile))
    )
  )
);
