import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

import theme from './theme';

import AppBarTopContainer from './AppBarTopContainer';
import ButtonIcon from './ButtonIcon';
import TextWrapper from './TextWrapper';
import ButtonContained from './ButtonContained';
import IconMenu from './IconMenu';

import withNavigation from './withNavigation';

import UpgradePleaseGif from './UpgradePlease.gif';

const styles = {
  __content: {
    display: 'flex',
    flexDirection: 'column',
    paddingLeft: 16,
    paddingRight: 16,
    alignItems: 'flex-start',
    alignSelf: 'stretch',
    marginTop: 16,
  },

  __textLine: {
    marginBottom: 8,
  },

  __actions: {
    marginTop: 16,
  },

  __img: {
    maxWidth: 500,
    borderRadius: theme.shape.borderRadius,
  },
};

class Upgrade extends Component {
  render() {
    return (
      <AppBarTopContainer
        showNavigation={this.props.showNavigation}
        navigationContent={this.props.navigationContent}
        appBarTitle={
          <TextWrapper
            styleName={'topBarTitle'}
            color={theme.color.onSurfaceMediumEmphasis}
          >
            {`Please upgrade`}
          </TextWrapper>
        }
        appBarLeftIcon={
          <ButtonIcon onTouchTap={this.props.onShowNavigation}>
            <IconMenu
              width="24"
              height="24"
              color={theme.color.onBackgroundMediumEmphasis}
            />
          </ButtonIcon>
        }
      >
        <div style={styles.__content}>
          <div style={styles.__textLine}>
            <TextWrapper styleName="h5">
              {`You've reached the limits of the free plan.`}
            </TextWrapper>
          </div>

          <div style={styles.__textLine}>
            <TextWrapper styleName="body1">
              {`Please upgrade to one of our paid plans to continue using
                Polychops.`}
            </TextWrapper>
          </div>

          <div style={styles.__textLine}>
            <img
              style={styles.__img}
              src={UpgradePleaseGif}
              alt="Upgrade please"
            />
          </div>

          <div style={styles.__actions}>
            <ButtonContained
              onTouchTap={() =>
                this.props.history.replace('/subscription')
              }
            >
              Upgrade
            </ButtonContained>
          </div>
        </div>
      </AppBarTopContainer>
    );
  }
}

export default withNavigation(withRouter(Upgrade));
