import React from 'react';
import sortBy from 'lodash/sortBy';

import theme from './theme';

import viewport from './viewport';

import BeatScale from './BeatScale';
import ListItem from './ListItem';
import TextWrapper from './TextWrapper';
import Divider from './Divider';
import Switch from './Switch';
import IconButton from './ButtonIcon';
import TextButton from './ButtonText';
import DropdownListItem from './DropdownListItem';
import ButtonContained from './ButtonContained';
import ButtonRadio from './ButtonRadio';

import InputNumber from './InputNumber';

import IconPlus from './IconPlus';
import IconMinus from './IconMinus';

import {
  defaultEnvelopConfigByType,
  getDefaultEnvelopBySoundType,
} from './data/envelops';

const styles = {
  __numberActionsWrapper: {
    display: 'flex',
    marginRight: -16,
  },

  __rightSideWrapper: {
    display: 'flex',
    alignItems: 'center',
    flexGrow: 0,
    flexShrink: 1,
    flexBasis: '60%',
    justifyContent: 'space-between',
  },

  __rightSideRadioWrapper: {
    display: 'flex',
    flexGrow: 0,
    flexShrink: 1,
    flexBasis: '60%',
    justifyContent: 'space-between',
  },

  __numberActionsWrapper__number: {
    display: 'flex',
    width: 24,
    justifyContent: 'center',
    alignItems: 'center',
  },

  __autoToggleBtn: {
    background: 'none',
    border: 'none',
  },

  __removeButtonWrapper: {
    marginLeft: -16,
  },

  __icon: {
    backgroundColor: theme.color.primary,
    color: theme.color.onPrimary,
    borderRadius: theme.shape.borderRadius,
    display: 'flex',
    alignItems: 'center',
    height: 36,
    paddingLeft: 16,
    paddingRight: 16,
    border: 'none',
  },

  __radioWrapper: {
    display: 'flex',
    alignItems: 'center',
    height: 48,
  },

  __radioLabel: {
    display: 'flex',
    alignItems: 'center',
    height: '100%',
    marginLeft: 8,
    marginRight: 8,
  },

  _isLastRadio: {
    marginRight: 0,
  },

  __numberGroup: {
    display: 'flex',
    alignItems: 'center',
    height: 48,
    flexBasis: '45%',
  },

  __numberGroupLabel: {
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    marginRight: 8,
  },
};

const PITCH_LIST = ['A', 'B', 'C', 'D', 'E', 'F', 'G'];

class BeatBottomSheetContent extends React.Component {
  constructor() {
    super();

    this._changeBeatsCount = inc => {
      const { selectedBeat } = this.props;

      if (selectedBeat.beatsCount === 1 && inc === -1) {
        return;
      }

      this.props.onChangeBeatsCount(
        selectedBeat.id,
        selectedBeat.beatsCount + inc
      );
    };

    this._changeOffset = inc => {
      const { selectedBeat, commonBeatsCount } = this.props;
      const offset = selectedBeat.offset || 0;

      if (offset === 0 && inc === -1) {
        return;
      }

      const maxOffset = commonBeatsCount / selectedBeat.beatsCount - 1;

      if (offset === maxOffset && inc === 1) {
        return;
      }

      this.props.onChangeOffset(selectedBeat.id, offset + inc);
    };

    this._changeSoundId = value => {
      const {
        activeSoundType,
        onChangeSound,
        selectedBeat,
      } = this.props;

      const valueObj = {
        ...selectedBeat.sound,
        isAuto: false,
      };

      if (activeSoundType === 'oscilator') {
        valueObj.pitch = value;
      }

      if (activeSoundType === 'kit') {
        valueObj.soundId = value;
      }

      onChangeSound(selectedBeat.id, {
        type: activeSoundType,
        ...valueObj,
      });
    };

    this._toggleAutoSound = () => {
      const { selectedBeat, onChangeSound } = this.props;
      const isAuto = !selectedBeat.sound.isAuto;

      onChangeSound(selectedBeat.id, {
        ...selectedBeat.sound,
        isAuto,
      });
    };

    this._deleteBeat = () => {
      this.props.onDeleteBeat(this.props.selectedBeat.id);
    };

    const setEnvelopType = type => {
      return () => {
        const { selectedBeat, onChangeSound } = this.props;
        const { sound } = selectedBeat;
        const envelop = sound.envelop || defaultEnvelopConfigByType;

        const newEnvelop = {
          ...envelop,
          type,
        };

        const newSound = {
          ...sound,
          envelop: newEnvelop,
        };

        onChangeSound(selectedBeat.id, newSound);
      };
    };

    this._setEnvelopTypeDisabled = setEnvelopType('disabled');
    this._setEnvelopTypeBeat = setEnvelopType('beat');
    this._setEnvelopTypeTime = setEnvelopType('time');

    const changeEnvelopParam = (type, param) => {
      return value => {
        const { selectedBeat, onChangeSound } = this.props;
        const { sound } = selectedBeat;
        const envelop = sound.envelop || defaultEnvelopConfigByType;
        const typeConfig = envelop[type];

        const newEnvelop = {
          ...envelop,
          type,
          [type]: {
            ...typeConfig,
            [param]: value,
          },
        };

        const newSound = {
          ...sound,
          envelop: newEnvelop,
        };

        onChangeSound(selectedBeat.id, newSound);
      };
    };

    this._onEnvelopBeatAttackTimeChange = changeEnvelopParam(
      'beat',
      'attackTime'
    );
    this._onEnvelopBeatAttackLevelChange = changeEnvelopParam(
      'beat',
      'attackLevel'
    );
    this._onEnvelopBeatDecayTimeChange = changeEnvelopParam(
      'beat',
      'decayTime'
    );
    this._onEnvelopBeatSustainTimeChange = changeEnvelopParam(
      'beat',
      'sustainTime'
    );
    this._onEnvelopBeatSustainLevelChange = changeEnvelopParam(
      'beat',
      'sustainLevel'
    );
    this._onEnvelopBeatReleaseTimeChange = changeEnvelopParam(
      'beat',
      'releaseTime'
    );

    this._onEnvelopTimeAttackTimeChange = changeEnvelopParam(
      'time',
      'attackTime'
    );
    this._onEnvelopTimeAttackLevelChange = changeEnvelopParam(
      'time',
      'attackLevel'
    );
    this._onEnvelopTimeDecayTimeChange = changeEnvelopParam(
      'time',
      'decayTime'
    );
    this._onEnvelopTimeSustainTimeChange = changeEnvelopParam(
      'time',
      'sustainTime'
    );
    this._onEnvelopTimeSustainLevelChange = changeEnvelopParam(
      'time',
      'sustainLevel'
    );
    this._onEnvelopTimeReleaseTimeChange = changeEnvelopParam(
      'time',
      'releaseTime'
    );
  }

  render() {
    const { selectedBeat, commonBeatsCount } = this.props;
    const {
      isMuteDisabled,
      onToggleMuteBeat,
      onToggleSoloBeat,
      isSolo,
      activeSoundType,
      activeKit,
      isMyChop,
    } = this.props;

    const beatId = selectedBeat.id;
    const maxOffset = commonBeatsCount / selectedBeat.beatsCount - 1;
    const offset = selectedBeat.offset || 0;

    const beatsCountDecrDisabled =
      !isMyChop || selectedBeat.beatsCount <= 1;
    const beatsCountIncrDisabled = !isMyChop;
    const offsetDecrDisabled = !isMyChop || offset <= 0;
    const offsetIncrDisabled = !isMyChop || offset >= maxOffset;

    const sound = selectedBeat.sound;
    let soundSelect;
    let soundLabel;
    let soundAutoTitle = 'Press to choose sound automatically';

    if (sound.isAuto) {
      soundAutoTitle = 'Press to choose sound manually';
    }

    const soundAutoButton = (
      <button
        style={styles.__autoToggleBtn}
        title={soundAutoTitle}
        onClick={isMyChop && this._toggleAutoSound}
      >
        <TextWrapper
          styleName={'subtitle1'}
          color={
            sound.isAuto
              ? theme.color.onSurface
              : theme.color.onSurfaceDisabled
          }
        >
          auto
        </TextWrapper>
      </button>
    );

    if (activeSoundType === 'oscilator') {
      soundLabel = 'Pitch';

      const soundOptions = PITCH_LIST.map(pitch => {
        return {
          key: `pitch-${pitch}`,
          value: pitch,
          text: pitch,
        };
      });

      soundSelect = (
        <DropdownListItem
          onChange={this._changeSoundId}
          value={sound.pitch}
          options={soundOptions}
          isDisabled={!isMyChop}
        />
      );
    }

    if (activeSoundType === 'kit' && activeKit) {
      soundLabel = 'Sound';

      const sounds = sortBy(activeKit.sounds, s => s.name);
      const soundOptions = sounds.map(sound => {
        return {
          key: `kit-${sound.id}`,
          value: sound.id,
          text: sound.name,
        };
      });

      soundSelect = (
        <DropdownListItem
          onChange={this._changeSoundId}
          value={sound.soundId}
          options={soundOptions}
          isDisabled={!isMyChop}
        />
      );
    }

    const envelop =
      sound.envelop || getDefaultEnvelopBySoundType(activeSoundType);

    let removeItem;
    if (isMyChop) {
      removeItem = (
        <ListItem key="item-remove">
          <div style={styles.__removeButtonWrapper}>
            <TextButton onTouchTap={this._deleteBeat}>
              remove subdivision
            </TextButton>
          </div>
        </ListItem>
      );
    }

    let envelopTypeWrapperStyle = {
      ...styles.__rightSideRadioWrapper,
    };

    let secondsText = 'Seconds';
    let levelText = 'Level';
    if (viewport.getWidth() <= 550) {
      envelopTypeWrapperStyle = {
        ...envelopTypeWrapperStyle,
        flexDirection: 'column',
      };
      secondsText = 'Sec';
      levelText = 'Lvl';
    }

    let envelopType = (
      <ListItem key="item-envelop-type">
        <TextWrapper
          styleName="subtitle1"
          color={theme.color.onSurfaceMediumEmphasis}
        >
          {'Volume envelop'}
        </TextWrapper>

        <div style={envelopTypeWrapperStyle}>
          <div style={styles.__radioWrapper}>
            <ButtonRadio
              id="radio-envelop-disabled"
              name="radio-envelop"
              isSelected={envelop.type === 'disabled'}
              onChange={this._setEnvelopTypeDisabled}
              enabledColor={theme.color.onSurfaceMediumEmphasis}
              selectedColor={theme.color.primaryLight}
            />

            <label
              style={styles.__radioLabel}
              htmlFor="radio-envelop-disabled"
            >
              <TextWrapper
                styleName="body1"
                color={theme.color.onSurface}
              >
                Off
              </TextWrapper>
            </label>
          </div>

          <div style={styles.__radioWrapper}>
            <ButtonRadio
              id="radio-envelop-time"
              name="radio-envelop"
              isSelected={envelop.type === 'time'}
              onChange={this._setEnvelopTypeTime}
              enabledColor={theme.color.onSurfaceMediumEmphasis}
              selectedColor={theme.color.primaryLight}
            />

            <label
              style={styles.__radioLabel}
              htmlFor="radio-envelop-time"
            >
              <TextWrapper
                styleName="body1"
                color={theme.color.onSurface}
              >
                {'Time-based'}
              </TextWrapper>
            </label>
          </div>

          <div style={styles.__radioWrapper}>
            <ButtonRadio
              id="radio-envelop-beat"
              name="radio-envelop"
              isSelected={envelop.type === 'beat'}
              onChange={this._setEnvelopTypeBeat}
              enabledColor={theme.color.onSurfaceMediumEmphasis}
              selectedColor={theme.color.primaryLight}
            />
            <label
              style={{ ...styles.__radioLabel, ...styles._isLastRadio }}
              htmlFor="radio-envelop-beat"
            >
              <TextWrapper
                styleName="body1"
                color={theme.color.onSurface}
              >
                {'Beat-based'}
              </TextWrapper>
            </label>
          </div>
        </div>
      </ListItem>
    );

    let envelopConfig = [];

    if (envelop.type === 'beat') {
      envelopConfig = [
        <ListItem key="envelop-beat-attack">
          <TextWrapper
            styleName="subtitle1"
            color={theme.color.onSurfaceMediumEmphasis}
          >
            {'Attack'}
          </TextWrapper>

          <div style={styles.__rightSideWrapper}>
            <div style={styles.__numberGroup}>
              <label
                htmlFor="envelop-beat-attack-time"
                style={styles.__numberGroupLabel}
              >
                <TextWrapper
                  styleName="body1"
                  color={theme.color.onSurfaceMediumEmphasis}
                  noWrap
                >
                  {'Beats'}
                </TextWrapper>
              </label>

              <InputNumber
                id="envelop-beat-attack-time"
                minValue={0}
                step={0.1}
                value={envelop.beat.attackTime}
                onChange={this._onEnvelopBeatAttackTimeChange}
              />
            </div>

            <div style={styles.__numberGroup}>
              <label
                htmlFor="envelop-beat-attack-level"
                style={styles.__numberGroupLabel}
              >
                <TextWrapper
                  styleName="body1"
                  color={theme.color.onSurfaceMediumEmphasis}
                  noWrap
                >
                  {levelText}
                </TextWrapper>
              </label>

              <InputNumber
                id="envelop-beat-attack-level"
                minValue={0}
                maxValue={1}
                step={0.1}
                value={envelop.beat.attackLevel}
                onChange={this._onEnvelopBeatAttackLevelChange}
              />
            </div>
          </div>
        </ListItem>,

        <ListItem key="envelop-beat-decay">
          <TextWrapper
            styleName="subtitle1"
            color={theme.color.onSurfaceMediumEmphasis}
          >
            {'Decay'}
          </TextWrapper>

          <div style={styles.__rightSideWrapper}>
            <div style={styles.__numberGroup}>
              <label
                htmlFor="envelop-beat-decay-time"
                style={styles.__numberGroupLabel}
              >
                <TextWrapper
                  styleName="body1"
                  color={theme.color.onSurfaceMediumEmphasis}
                  noWrap
                >
                  {'Beats'}
                </TextWrapper>
              </label>

              <InputNumber
                id="envelop-beat-decay-time"
                minValue={0}
                step={0.1}
                value={envelop.beat.decayTime}
                onChange={this._onEnvelopBeatDecayTimeChange}
              />
            </div>
          </div>
        </ListItem>,

        <ListItem key="envelop-beat-sustain">
          <TextWrapper
            styleName="subtitle1"
            color={theme.color.onSurfaceMediumEmphasis}
          >
            {'Sustain'}
          </TextWrapper>

          <div style={styles.__rightSideWrapper}>
            <div style={styles.__numberGroup}>
              <label
                htmlFor="envelop-beat-sustain-time"
                style={styles.__numberGroupLabel}
              >
                <TextWrapper
                  styleName="body1"
                  color={theme.color.onSurfaceMediumEmphasis}
                  noWrap
                >
                  {'Beats'}
                </TextWrapper>
              </label>

              <InputNumber
                id="envelop-beat-sustain-time"
                minValue={0}
                step={0.1}
                value={envelop.beat.sustainTime}
                onChange={this._onEnvelopBeatSustainTimeChange}
              />
            </div>

            <div style={styles.__numberGroup}>
              <label
                htmlFor="envelop-beat-sustain-level"
                style={styles.__numberGroupLabel}
              >
                <TextWrapper
                  styleName="body1"
                  color={theme.color.onSurfaceMediumEmphasis}
                  noWrap
                >
                  {levelText}
                </TextWrapper>
              </label>

              <InputNumber
                id="envelop-beat-sustain-level"
                minValue={0}
                maxValue={1}
                step={0.1}
                value={envelop.beat.sustainLevel}
                onChange={this._onEnvelopBeatSustainLevelChange}
              />
            </div>
          </div>
        </ListItem>,

        <ListItem key="envelop-beat-release">
          <TextWrapper
            styleName="subtitle1"
            color={theme.color.onSurfaceMediumEmphasis}
          >
            {'Release'}
          </TextWrapper>

          <div style={styles.__rightSideWrapper}>
            <div style={styles.__numberGroup}>
              <label
                htmlFor="envelop-beat-release-time"
                style={styles.__numberGroupLabel}
              >
                <TextWrapper
                  styleName="body1"
                  color={theme.color.onSurfaceMediumEmphasis}
                  noWrap
                >
                  {'Beats'}
                </TextWrapper>
              </label>

              <InputNumber
                id="envelop-beat-release-time"
                minValue={0}
                step={0.1}
                value={envelop.beat.releaseTime}
                onChange={this._onEnvelopBeatReleaseTimeChange}
              />
            </div>
          </div>
        </ListItem>,
      ];
    }

    if (envelop.type === 'time') {
      envelopConfig = [
        <ListItem key="envelop-time-attack">
          <TextWrapper
            styleName="subtitle1"
            color={theme.color.onSurfaceMediumEmphasis}
          >
            {'Attack'}
          </TextWrapper>

          <div style={styles.__rightSideWrapper}>
            <div style={styles.__numberGroup}>
              <label
                htmlFor="envelop-time-attack-time"
                style={styles.__numberGroupLabel}
              >
                <TextWrapper
                  styleName="body1"
                  color={theme.color.onSurfaceMediumEmphasis}
                  noWrap
                >
                  {secondsText}
                </TextWrapper>
              </label>

              <InputNumber
                id="envelop-time-attack-time"
                minValue={0}
                step={0.1}
                value={envelop.time.attackTime}
                onChange={this._onEnvelopTimeAttackTimeChange}
              />
            </div>

            <div style={styles.__numberGroup}>
              <label
                htmlFor="envelop-time-attack-level"
                style={styles.__numberGroupLabel}
              >
                <TextWrapper
                  styleName="body1"
                  color={theme.color.onSurfaceMediumEmphasis}
                  noWrap
                >
                  {levelText}
                </TextWrapper>
              </label>

              <InputNumber
                id="envelop-time-attack-level"
                minValue={0}
                maxValue={1}
                step={0.1}
                value={envelop.time.attackLevel}
                onChange={this._onEnvelopTimeAttackLevelChange}
              />
            </div>
          </div>
        </ListItem>,

        <ListItem key="envelop-time-decay">
          <TextWrapper
            styleName="subtitle1"
            color={theme.color.onSurfaceMediumEmphasis}
          >
            {'Decay'}
          </TextWrapper>

          <div style={styles.__rightSideWrapper}>
            <div style={styles.__numberGroup}>
              <label
                htmlFor="envelop-time-decay-time"
                style={styles.__numberGroupLabel}
              >
                <TextWrapper
                  styleName="body1"
                  color={theme.color.onSurfaceMediumEmphasis}
                  noWrap
                >
                  {secondsText}
                </TextWrapper>
              </label>

              <InputNumber
                id="envelop-time-decay-time"
                minValue={0}
                step={0.1}
                value={envelop.time.decayTime}
                onChange={this._onEnvelopTimeDecayTimeChange}
              />
            </div>
          </div>
        </ListItem>,

        <ListItem key="envelop-time-sustain">
          <TextWrapper
            styleName="subtitle1"
            color={theme.color.onSurfaceMediumEmphasis}
          >
            {'Sustain'}
          </TextWrapper>

          <div style={styles.__rightSideWrapper}>
            <div style={styles.__numberGroup}>
              <label
                htmlFor="envelop-time-sustain-time"
                style={styles.__numberGroupLabel}
              >
                <TextWrapper
                  styleName="body1"
                  color={theme.color.onSurfaceMediumEmphasis}
                  noWrap
                >
                  {secondsText}
                </TextWrapper>
              </label>

              <InputNumber
                id="envelop-time-sustain-time"
                minValue={0}
                step={0.1}
                value={envelop.time.sustainTime}
                onChange={this._onEnvelopTimeSustainTimeChange}
              />
            </div>

            <div style={styles.__numberGroup}>
              <label
                htmlFor="envelop-time-sustain-level"
                style={styles.__numberGroupLabel}
              >
                <TextWrapper
                  styleName="body1"
                  color={theme.color.onSurfaceMediumEmphasis}
                  noWrap
                >
                  {levelText}
                </TextWrapper>
              </label>

              <InputNumber
                id="envelop-time-sustain-level"
                minValue={0}
                maxValue={1}
                step={0.1}
                value={envelop.time.sustainLevel}
                onChange={this._onEnvelopTimeSustainLevelChange}
              />
            </div>
          </div>
        </ListItem>,

        <ListItem key="envelop-time-release">
          <TextWrapper
            styleName="subtitle1"
            color={theme.color.onSurfaceMediumEmphasis}
          >
            {'Release'}
          </TextWrapper>

          <div style={styles.__rightSideWrapper}>
            <div style={styles.__numberGroup}>
              <label
                htmlFor="envelop-time-release-time"
                style={styles.__numberGroupLabel}
              >
                <TextWrapper
                  styleName="body1"
                  color={theme.color.onSurfaceMediumEmphasis}
                  noWrap
                >
                  {secondsText}
                </TextWrapper>
              </label>

              <InputNumber
                id="envelop-time-release-time"
                minValue={0}
                step={0.1}
                value={envelop.time.releaseTime}
                onChange={this._onEnvelopTimeReleaseTimeChange}
              />
            </div>
          </div>
        </ListItem>,
      ];
    }

    envelopConfig.push(<Divider key="menu-actions-divider" />);

    if (!process.env.REACT_APP_POLYNOME_EXPERIMENTS_ON) {
      envelopType = null;
      envelopConfig = [];
    }

    return [
      <ListItem key="item-header" height={72}>
        <TextWrapper
          styleName={'h4'}
          color={theme.color.onBackgroundMediumEmphasis}
        >
          {'Subdivision'}
        </TextWrapper>
        <ButtonContained
          elevation={0}
          background={this.props.iconBackgroundColor}
          textColor={theme.color.onSecondaryMediumEmphasis}
          onTouchTap={this.props.onDismiss}
        >
          {this.props.icon}
        </ButtonContained>
      </ListItem>,
      <ListItem
        key="item-mute"
        onTouchTap={() => !isMuteDisabled && onToggleMuteBeat(beatId)}
      >
        <TextWrapper
          styleName="subtitle1"
          color={theme.color.onSurfaceMediumEmphasis}
        >
          Mute
        </TextWrapper>

        <Switch
          isOn={selectedBeat.isMute}
          isDisabled={isMuteDisabled}
        />
      </ListItem>,

      <ListItem
        key="item-solo"
        onTouchTap={() => onToggleSoloBeat(beatId)}
      >
        <TextWrapper
          styleName="subtitle1"
          color={theme.color.onSurfaceMediumEmphasis}
        >
          Solo
        </TextWrapper>

        <Switch isOn={isSolo} />
      </ListItem>,

      <BeatScale
        key="item-beat-scale"
        beat={selectedBeat}
        commonBeatsCount={commonBeatsCount}
        onMuteSection={isMyChop && this.props.onMuteSection}
        width={viewport.getWidth()}
      />,

      <ListItem key="item-beats-count">
        <TextWrapper
          styleName="subtitle1"
          color={theme.color.onSurfaceMediumEmphasis}
        >
          Subdivision of
        </TextWrapper>

        <div style={styles.__numberActionsWrapper}>
          <IconButton
            isDisabled={beatsCountDecrDisabled}
            onTouchTap={() => this._changeBeatsCount(-1)}
          >
            <IconMinus
              color={
                beatsCountDecrDisabled
                  ? theme.color.onSurfaceDisabled
                  : theme.color.onSurface
              }
              width={24}
              height={24}
            />
          </IconButton>

          <div style={styles.__numberActionsWrapper__number}>
            <TextWrapper
              styleName="subtitle1"
              color={theme.color.onSurface}
            >
              {selectedBeat.beatsCount}
            </TextWrapper>
          </div>

          <IconButton
            onTouchTap={() => this._changeBeatsCount(1)}
            isDisabled={beatsCountIncrDisabled}
          >
            <IconPlus
              color={
                beatsCountIncrDisabled
                  ? theme.color.onSurfaceDisabled
                  : theme.color.onSurface
              }
              width={24}
              height={24}
            />
          </IconButton>
        </div>
      </ListItem>,

      <ListItem key="item-offset">
        <TextWrapper
          styleName="subtitle1"
          color={theme.color.onSurfaceMediumEmphasis}
        >
          Starts on
        </TextWrapper>

        <div style={styles.__numberActionsWrapper}>
          <IconButton
            isDisabled={offsetDecrDisabled}
            onTouchTap={() => this._changeOffset(-1)}
          >
            <IconMinus
              color={
                offsetDecrDisabled
                  ? theme.color.onSurfaceDisabled
                  : theme.color.onSurface
              }
              width={24}
              height={24}
            />
          </IconButton>

          <div style={styles.__numberActionsWrapper__number}>
            <TextWrapper
              styleName="subtitle1"
              color={theme.color.onSurface}
            >
              {offset + 1}
            </TextWrapper>
          </div>

          <IconButton
            isDisabled={offsetIncrDisabled}
            onTouchTap={() => this._changeOffset(1)}
          >
            <IconPlus
              color={
                offsetIncrDisabled
                  ? theme.color.onSurfaceDisabled
                  : theme.color.onSurface
              }
              width={24}
              height={24}
            />
          </IconButton>
        </div>
      </ListItem>,

      <ListItem key="item-sound">
        <TextWrapper
          styleName="subtitle1"
          color={theme.color.onSurfaceMediumEmphasis}
        >
          {soundLabel}
        </TextWrapper>

        <div style={styles.__rightSideWrapper}>
          {soundSelect}
          {soundAutoButton}
        </div>
      </ListItem>,

      <Divider key="menu-envelop-divider" />,

      envelopType,
      ...envelopConfig,

      removeItem,
    ];
  }
}

export default BeatBottomSheetContent;
