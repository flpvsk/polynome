import React, { Component } from 'react';
import pick from 'lodash/pick';
import theme from './theme';
import { elevationToStyle } from './elevation';

import IconMoreVert from './IconMoreVert';
import ButtonIcon from './ButtonIcon';

import viewport from './viewport';

const styles = {
  block: {
    // ...elevationToStyle(2),
    width: '100%',
    height: theme.size.appBarTopHeight,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: theme.color.background,
  },

  __contentArea: {
    maxWidth: viewport.getWidth(),
    width: '100%',
    marginLeft: 'auto',
    marginRight: 'auto',
    height: '100%',
    alignItems: 'center',
    display: 'flex',
    paddingLeft: 16,
    paddingRight: 16,
  },

  __titleAndActions: {
    display: 'flex',
    justifyContent: 'space-between',
    height: '100%',
    alignItems: 'center',
    flexGrow: 1,
  },

  __title: {
    flexGrow: 1,
    height: '100%',
    display: 'flex',
    alignItems: 'center',
  },

  __actions: {
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    alignSelf: 'flex-end',
    justifyContent: 'space-between',
    flexGrow: 0,
  },

  __leftIcon: {
    width: 24,
    height: 24,
    marginRight: 32,
    display: 'flex',
    cursor: 'pointer',
    alignItems: 'center',
  },
};

class TopBar extends Component {
  render() {
    const styleFromProps = pick(this.props, ['backgroundColor']);
    const {
      showMenuIcon,
      onMenuClick,
      title,
      leftIcon,
      elevation,
      actions,
    } = this.props;

    let menuIcon;

    if (showMenuIcon) {
      menuIcon = (
        <ButtonIcon width={24} onTouchTap={onMenuClick}>
          <IconMoreVert
            width={24}
            height={24}
            color={theme.color.onSurfaceMediumEmphasis}
          />
        </ButtonIcon>
      );
    }

    const blockStyle = {
      ...styles.block,
      ...styleFromProps,
      ...elevationToStyle(elevation || 0),
    };

    let actionsStyle = {
      ...styles.__actions,
    };

    if (actions) {
      let menuIconWidth = menuIcon ? 24 : 0;
      actionsStyle = {
        ...actionsStyle,
        width: menuIconWidth + 48 * actions.length,
      };
    }

    return (
      <div style={blockStyle}>
        <div style={styles.__contentArea}>
          <div style={styles.__leftIcon}>{leftIcon}</div>
          <div style={styles.__titleAndActions}>
            <div style={styles.__title}>{title}</div>
            <div style={actionsStyle}>
              {actions}
              {menuIcon}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default TopBar;
