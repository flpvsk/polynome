import React, { Component } from 'react';
import TextWrapper from './TextWrapper';
import Userpic from './Userpic';
import theme from './theme';

const styles = {
  block: {
    minHeight: 72,
    paddingTop: 16,
    paddingLeft: 16,
    paddingRight: 16,
    display: 'flex',
  },

  __leftSide: {
    marginRight: 16,
    width: 40,
  },

  __rightSide: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
  },
};

class Comment extends Component {
  render() {
    const props = this.props;
    return (
      <div style={styles.block}>
        <div style={styles.__leftSide}>
          <Userpic person={props.comment.person} />
        </div>

        <div style={styles.__rightSide}>
          <div>
            <TextWrapper
              styleName={'subtitle2'}
              color={theme.color.onSurface}
            >
              {props.comment.person.handle || 'anonymous'}
            </TextWrapper>
          </div>

          <div>
            <TextWrapper
              styleName={'body1'}
              color={theme.color.onSurfaceMediumEmphasis}
            >
              {props.comment.text}
            </TextWrapper>
          </div>
        </div>
      </div>
    );
  }
}

export default Comment;
