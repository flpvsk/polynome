import React from 'react';
import TextWrapper from './TextWrapper';
import theme from './theme';

const styles = {
  block: {
    background: 'none',
    border: 'none',
    textTransform: 'uppercase',
    color: theme.color.primaryLight,
    paddingLeft: '16px',
    paddingRight: '16px',
    minWidth: '64px',
    height: '2.25em',
    letterSpacing: '0.05em',
    fontWeight: 500,
    whiteSpace: 'nowrap',
    cursor: 'pointer',
  },
};

const TextButton = props => {
  const textColor = props.textColor;

  let blockStyle = { ...styles.block };

  if (textColor) {
    blockStyle.color = textColor;
  }

  let textStyleName = props.textStyleName || 'button';

  return (
    <button
      disabled={props.isDisabled}
      style={blockStyle}
      onClick={props.onTouchTap}
    >
      <TextWrapper styleName={textStyleName} color={textColor}>
        {props.children}
      </TextWrapper>
    </button>
  );
};

export default TextButton;
