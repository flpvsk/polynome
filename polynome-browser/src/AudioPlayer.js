import { Component } from 'react';
import shouldComponentUpdate from './shouldComponentUpdate';
import * as trackUtils from './trackUtils';
import polytime from './polytime';

import { getDefaultEnvelopBySoundType } from './data/envelops';

const SECONDS_IN_MINUTE = 60;
const SCHEDULE_AHEAD = 3;

const FREQ_BY_PITCH = {
  A: 440.0,
  'A#': 466.16,
  B: 493.88,
  C: 523.25,
  'C#': 554.37,
  D: 587.33,
  'D#': 622.25,
  E: 659.25,
  F: 698.46,
  'F#': 739.99,
  G: 783.99,
  'G#': 830.61,
};

const GAIN_RANDOMIZE = [0.8, 0.85, 0.87, 0.9, 0.95, 1];

function getEnvelopValues({
  gain,
  envelop,
  commonBeatDuration,
  startTime,
  maxDuration,
}) {
  if (envelop.type === 'disabled') {
    const sustainTime = maxDuration - 0.01;
    return {
      startTime,
      sustainLevel: gain,
      sustainTime: sustainTime < 0 ? 0 : sustainTime,
      releaseTime: sustainTime < 0 ? 0 : 0.01,
    };
  }

  const mult = envelop.type === 'beat' ? commonBeatDuration : 1;
  const values = envelop[envelop.type];

  const result = {
    startTime,
    attackTime: (values.attackTime || 0) * mult,
    attackLevel: (values.attackLevel || 0) * gain,
    decayTime: (values.decayTime || 0) * mult,
    sustainLevel: (values.sustainLevel || 0) * gain,
    sustainTime: (values.sustainTime || 0) * mult,
    releaseTime: (values.releaseTime || 0) * mult,
  };

  return result;
}

function scheduleAdsr(
  gainNode,
  {
    startTime = 0,

    attackLevel,
    attackTime = 0,

    decayTime = 0,

    sustainLevel,
    sustainTime,

    releaseTime = 0,
  }
) {
  let startLevel = 0;
  let time = startTime;

  if (!attackTime) {
    startLevel = sustainLevel;
  }

  if (time >= 0.001) {
    gainNode.gain.setValueAtTime(0, time - 0.001);
  }

  gainNode.gain.setValueAtTime(startLevel, time);

  if (attackTime) {
    time += attackTime;
    gainNode.gain.linearRampToValueAtTime(attackLevel, time);
  }

  if (decayTime) {
    time += decayTime;
    gainNode.gain.linearRampToValueAtTime(sustainLevel, time);
  }

  if (sustainTime) {
    time += sustainTime;
  }

  gainNode.gain.setValueAtTime(sustainLevel, time);

  if (releaseTime) {
    time += releaseTime;
    gainNode.gain.linearRampToValueAtTime(0, time);
  }

  gainNode.gain.setValueAtTime(0, time);
}

function deriveState(props) {
  const { beats, commonBeatsCount } = props;
  const lines = props.lines.filter(l => l.playingRecordingId);
  let beatsStartStopById = trackUtils.getBeatsStartStopMap({
    commonBeatsCount,
    beats,
  });

  let trackBeatsCount = trackUtils.getTrackBeatsCount({
    commonBeatsCount,
    beats: beats.concat(lines),
  });

  let cyclesCount = Math.ceil(trackBeatsCount / commonBeatsCount);

  let trackBeats = beats.reduce((acc, beat) => {
    const mutedBeats = beat.mutedBeats || [];
    const { beatsCount } = beat;
    const offset = beat.offset || 0;
    const every = commonBeatsCount / beatsCount;
    const [start, stop] = beatsStartStopById[beat.id];

    for (let cycleNum = 0; cycleNum < cyclesCount; cycleNum++) {
      for (let i = 0; i < beatsCount; i++) {
        if (mutedBeats.indexOf(i + 1) !== -1) {
          continue;
        }

        let commonBeatNum =
          offset + i * every + cycleNum * beatsCount * every;

        if (commonBeatNum < start || commonBeatNum >= stop) {
          continue;
        }

        acc.push({
          id: beat.id,
          sound: beat.sound,
          beatsCount: 1,
          offset: commonBeatNum,
        });
      }
    }

    return acc;
  }, []);

  let polytimeBeats = trackBeats.map(b => {
    return {
      id: b.id,
      every: trackBeatsCount / b.beatsCount,
      offset: b.offset || 0,
    };
  });

  polytimeBeats = polytimeBeats.concat(
    lines.map(l => {
      const recording = props.recordingByLineId[l.lineId];
      let everyTrackBeats = trackBeatsCount / l.beatsCount;
      let everyRecordingBeats = Infinity;
      if (recording) {
        everyRecordingBeats = l.lineDuration * recording.beatsCount;
      }

      const every = Math.min(everyTrackBeats, everyRecordingBeats);
      return {
        id: l.lineId,
        offset: l.lineStart,
        every: every,
      };
    })
  );

  return {
    polytimeBeats,
    commonBeatsCount: trackBeatsCount,
  };
}

class AudioPlayer extends Component {
  constructor() {
    super();
    this._destroyFunctions = [];
    this._scheduleFunctionById = {};
    this._scheduledUntil = 0;
    this._cache = null;
    this._mounted = false;
    this.shouldComponentUpdate = shouldComponentUpdate;
  }

  componentDidMount() {
    this._mounted = true;
    this._cache = null;
    this._createAudioNodes();
    this._scheduleAudio();
  }

  componentDidUpdate() {
    this._scheduledUntil = 0;
    this._destroyAudioNodes();
    this._cache = null;
    this._createAudioNodes();
    this._scheduleAudio();
  }

  componentWillUnmount() {
    this._destroyAudioNodes();
    this._cache = null;
    this._scheduledUntil = 0;
    this._mounted = false;
  }

  _scheduleAudio() {
    if (!this.props.audioContext) {
      console.warn(
        '[AudioPlayer] no props.audioContext in scheduleAudio'
      );
      return;
    }

    let scheduledUntil = this._scheduledUntil;

    const { currentTime } = this.props.audioContext;

    if (!this._cache) {
      this._cache = deriveState(this.props);
    }

    const { polytimeBeats, commonBeatsCount } = this._cache;

    const { commonBeatsTempo, startTime } = this.props;

    if (!this._mounted) {
      return;
    }

    if (!polytimeBeats || !polytimeBeats.length || !commonBeatsCount) {
      return;
    }

    if (typeof startTime === 'undefined') {
      return;
    }

    const cycleTime =
      commonBeatsCount * SECONDS_IN_MINUTE / commonBeatsTempo;

    const lookAhead = SCHEDULE_AHEAD * cycleTime;
    let from = scheduledUntil;

    if (currentTime > scheduledUntil) {
      scheduledUntil = currentTime;
    }

    if (currentTime + lookAhead < scheduledUntil) {
      setTimeout(() => {
        this._scheduleAudio(scheduledUntil);
      }, lookAhead * 0.5 * 1000);
      return;
    }

    const config = {
      beats: polytimeBeats,
      tempo: commonBeatsTempo,
      start: startTime,
    };

    const beatTimes = polytime(
      config,
      from,
      scheduledUntil + lookAhead
    );

    for (let t of beatTimes) {
      const { time } = t;
      for (let beatId of t.beatsIds) {
        const schedule = this._scheduleFunctionById[beatId];
        const gain = 1 + Math.log(1 / t.beatsIds.length) / 3;
        schedule({
          time,
          gain,
          currentTime,
        });
      }
    }

    this._scheduledUntil = scheduledUntil + lookAhead;
    setTimeout(() => {
      this._scheduleAudio(scheduledUntil + lookAhead);
    }, lookAhead * 0.5 * 1000);
  }

  _destroyAudioNodes() {
    for (let fn of this._destroyFunctions) {
      fn();
    }
    this._scheduledUntil = 0;
    this._destroyFunctions = [];
  }

  _createAudioNodes() {
    const {
      audioContext,
      recordingByLineId,
      beatsAudio,
      recordingsAudio,
      commonBeatsTempo,
      beats,
      lines,
    } = this.props;

    if (!this._cache) {
      this._cache = deriveState(this.props);
    }

    const { commonBeatsCount } = this._cache;

    const cycleTime =
      commonBeatsCount * SECONDS_IN_MINUTE / commonBeatsTempo;
    const commonBeatDuration = SECONDS_IN_MINUTE / commonBeatsTempo;

    // let compressor;
    // if (window.DynamicsCompressorNode) {
    //   try {
    //     compressor = new window.DynamicsCompressorNode(audioContext, {
    //       knee: 3,
    //       attack: 0.03,
    //       release: 0.15,
    //       ratio: 3,
    //       threshold: -5,
    //     });
    //   } catch (e) {
    //     console.log(
    //       '[AudioPlayer] Error constructing compressor. Are we in Safari?',
    //       e
    //     );
    //   }
    // }

    // this._destroyFunctions.push(() => {
    //   // destroy the compressor
    //   if (compressor) {
    //     compressor.disconnect();
    //     compressor = null;
    //   }
    // });

    for (let line of lines) {
      const { lineId } = line;

      // eslint-disable-next-line
      this._scheduleFunctionById[lineId] = ({ time, currentTime }) => {
        let bufferSource;
        const recording = recordingByLineId[lineId];

        if (!recording) {
          return;
        }

        const sound = recording.sound;

        if (!sound) {
          return;
        }

        const latency = sound.latency || 0;
        const { audioData } = recordingsAudio[recording.recordingId];

        time = time - latency;

        if (time < 0) {
          time = 0;
        }

        // too late for that beat
        if (currentTime > time + audioData.duration) {
          return;
        }

        let gain = audioContext.createGain();
        gain.gain.setValueAtTime(0, 0);

        let soundStart = 0;
        let gainStart = Math.max(0, time - 0.005);
        let gainValue = 0.8;
        let connectBufferTo = gain;

        if (currentTime > time) {
          gainStart = time;
          soundStart = currentTime - time;
        }

        let timeStop = time + cycleTime;

        // if (compressor) {
        //   gainValue = 0.4;
        //   connectBufferTo = compressor;
        //   compressor.connect(gain);
        // }

        bufferSource = audioContext.createBufferSource();
        bufferSource.buffer = audioData;
        bufferSource.connect(connectBufferTo);

        gain.connect(audioContext.destination);
        gain.gain.setValueAtTime(0, gainStart);
        gain.gain.linearRampToValueAtTime(gainValue, time);
        gain.gain.setValueAtTime(gainValue, timeStop - 0.01);
        gain.gain.linearRampToValueAtTime(0, timeStop);

        bufferSource.start(time, soundStart);
        try {
          bufferSource.stop(timeStop);
        } catch (e) {
          console.log('[AudioPlayer] error stopping bufferSource', e);
        }

        this._destroyFunctions.push(() => {
          if (!bufferSource) {
            return;
          }

          try {
            bufferSource.stop(0);
          } catch (e) {
            console.log('[AudioPlayer] error stopping bufferSource', e);
          }
          gain.disconnect();
          bufferSource.disconnect();
          bufferSource = null;
          gain = null;
        });
      };
    }

    for (let beat of beats) {
      if (beat.sound.type === 'kit') {
        let gain = audioContext.createGain();
        gain.connect(audioContext.destination);

        this._destroyFunctions.push(() => {
          gain.gain.cancelScheduledValues(0);
          gain.disconnect();
          gain = null;
        });

        this._scheduleFunctionById[beat.id] = ({
          time,
          currentTime,
        }) => {
          let bufferSource;
          const { audioData } = beatsAudio[beat.sound.soundId];

          if (!audioData) {
            return;
          }

          // too late for that beat
          if (currentTime > time + audioData.duration) {
            return;
          }

          let soundStart;
          if (currentTime > time) {
            soundStart = currentTime - time;
          }

          bufferSource = audioContext.createBufferSource();
          bufferSource.buffer = audioData;
          bufferSource.connect(gain);

          let sustainLevelIndex = Math.round(
            Math.random() * (GAIN_RANDOMIZE.length - 1)
          );

          let sustainMult = GAIN_RANDOMIZE[sustainLevelIndex];

          const envelop =
            beat.sound.envelop ||
            getDefaultEnvelopBySoundType(beat.sound.type);

          if (envelop.type !== 'disabled') {
            const envelopValues = getEnvelopValues({
              envelop,
              gain: sustainMult * 1,
              commonBeatDuration,
              maxDuration:
                commonBeatDuration *
                (commonBeatsCount / beat.beatsCount),
              startTime: time,
            });

            scheduleAdsr(gain, envelopValues);
          }

          if (envelop.type === 'disabled') {
            gain.gain.setValueAtTime(sustainMult, time);
          }

          bufferSource.start(time, soundStart);

          this._destroyFunctions.push(() => {
            if (!bufferSource) {
              return;
            }

            bufferSource.stop(0);
            bufferSource.disconnect();

            bufferSource = null;
          });
        };
      }

      if (beat.sound.type === 'oscilator') {
        let osc = audioContext.createOscillator();
        let gain = audioContext.createGain();

        osc.frequency.setValueAtTime(
          FREQ_BY_PITCH[beat.sound.pitch],
          0
        );

        osc.type = 'triangle';
        osc.connect(gain);

        gain.gain.setValueAtTime(0, 0);
        gain.connect(audioContext.destination);

        osc.start();

        this._scheduleFunctionById[beat.id] = opts => {
          const time = opts.time;

          const envelop =
            beat.sound.envelop ||
            getDefaultEnvelopBySoundType(beat.sound.type);

          const envelopValues = getEnvelopValues({
            envelop,
            gain: opts.gain,
            commonBeatDuration,
            maxDuration:
              commonBeatDuration * (commonBeatsCount / beat.beatsCount),
            startTime: time,
          });

          scheduleAdsr(gain, envelopValues);
        };

        this._destroyFunctions.push(() => {
          gain.gain.setValueAtTime(0, 0);
          gain.gain.cancelScheduledValues(0);
          osc.frequency.cancelScheduledValues(0);

          osc.disconnect();
          gain.disconnect();

          osc = null;
          gain = null;
        });
      }
    }
  }

  render() {
    return null;
  }
}

export default AudioPlayer;
