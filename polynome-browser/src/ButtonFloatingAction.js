import React from 'react';
import pick from 'lodash/pick';
import theme from './theme';
import { elevationToStyle } from './elevation';

const styles = {
  block: {
    width: theme.size.fabSize,
    height: theme.size.fabSize,

    border: 'none',
    borderRadius: '50%',

    backgroundColor: theme.color.primary,

    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',

    cursor: 'pointer',

    ...elevationToStyle(6),
  },
};

const FloatingActionButton = props => {
  const styleProps = pick(props, ['backgroundColor']);

  const blockStyle = {
    ...styles.block,
    ...styleProps,
  };

  return (
    <button onClick={props.onTouchTap} style={blockStyle}>
      {props.children}
    </button>
  );
};

export default FloatingActionButton;
