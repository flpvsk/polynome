import React from 'react';
import theme from './theme';
import { elevationToStyle } from './elevation';

const styles = {
  block: {
    width: 36,
    height: 20,
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
  },

  __rail: {
    width: 36,
    height: 16,
    borderRadius: 7,
    backgroundColor: theme.color.surfaceLight,
  },

  __knob: {
    position: 'absolute',
    left: 0,
    top: 0,
    width: 20,
    height: 20,
    borderRadius: '50%',
    transition: 'transform 150ms ease-out',
    transform: 'translateX(0px)',
    backgroundColor: theme.color.onSurface,
    ...elevationToStyle(1),
  },

  _isOn: {
    __rail: {
      backgroundColor: theme.color.primaryLight,
      opacity: 0.38,
    },

    __knob: {
      backgroundColor: theme.color.primaryLight,
      transform: 'translateX(16px)',
    },
  },

  _isOff: {
    __rail: {},

    __knob: {},
  },

  _isDisabled: {
    block: {
      opacity: 0.38,
    },
  },
};

class Switch extends React.Component {
  constructor() {
    super();

    this._change = () => {
      if (this.props.isDisabled) {
        return;
      }

      if (!this.props.onChange) {
        return;
      }

      this.props.onChange(!this.props.isOn);
    };
  }
  render() {
    let { isOn, isDisabled } = this.props;

    let blockStyle = styles.block;
    let railStyle = styles.__rail;
    let knobStyle = styles.__knob;

    if (isOn) {
      railStyle = {
        ...railStyle,
        ...styles._isOn.__rail,
      };

      knobStyle = {
        ...knobStyle,
        ...styles._isOn.__knob,
      };
    }

    if (!isOn) {
      railStyle = {
        ...railStyle,
        ...styles._isOff.__rail,
      };

      knobStyle = {
        ...knobStyle,
        ...styles._isOff.__knob,
      };
    }

    if (isDisabled) {
      blockStyle = {
        ...blockStyle,
        ...styles._isDisabled.block,
      };
    }

    return (
      <div onClick={this._change} style={blockStyle}>
        <div style={railStyle} />
        <div style={knobStyle} />
      </div>
    );
  }
}

export default Switch;
