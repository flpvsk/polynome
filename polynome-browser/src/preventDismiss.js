const IGNORE_TAGS = ['INPUT', 'BUTTON', 'TEXTAREA'];
export default function preventDismiss(e) {
  if (IGNORE_TAGS.indexOf(e.target.tagName) > -1) {
    return;
  }

  e.preventDefault();
  e.stopPropagation();
}
