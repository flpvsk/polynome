import React from 'react';
import theme from './theme';
import { elevationToStyle } from './elevation';
import preventDismiss from './preventDismiss';
import TextWrapper from './TextWrapper';

const styles = {
  block: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,

    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },

  __surface: {
    backgroundColor: theme.color.surface,
    minWidth: 280,
    width: '40%',
    ...elevationToStyle(24),
  },

  __content: {
    paddingLeft: 24,
    paddingRight: 24,
  },

  __header: {
    height: 64,
    paddingLeft: 24,
    paddingRight: 24,
    display: 'flex',
    alignItems: 'center',
  },

  __actions: {
    display: 'flex',
    width: '100%',
    height: 52,
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginTop: 28,
  },

  __actionWrapper: {
    marginTop: 8,
    marginRight: 8,
  },
};

const Dialog = props => {
  let header, actions;

  if (props.header) {
    header = (
      <div style={styles.__header}>
        <TextWrapper styleName="h6" color={theme.color.onSurface}>
          {props.header}
        </TextWrapper>
      </div>
    );
  }

  if (props.actions && props.actions.length > 0) {
    actions = (
      <div style={styles.__actions}>
        {props.actions.map((c, i) => {
          return (
            <div
              key={`dialog-action-${i}`}
              style={styles.__actionWrapper}
            >
              {c}
            </div>
          );
        })}
      </div>
    );
  }

  return (
    <div style={styles.block}>
      <div style={styles.__surface} onClick={preventDismiss}>
        {header}
        <div style={styles.__content}>{props.children}</div>
        {actions}
      </div>
    </div>
  );
};

export default Dialog;
