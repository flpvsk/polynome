import humanFormat from 'human-format';

function toPrettyNumber(n) {
  if (!n) {
    return ` `;
  }

  return humanFormat(n, { decimals: 1 });
}

export default toPrettyNumber;
