import React from 'react';

const IconStop = ({ width, height, color }) => {
  return (
    <svg fill={color} height={height} viewBox="0 0 24 24" width={width}>
      <path d="M0 0h24v24H0z" fill="none" />
      <path d="M6 6h12v12H6z" />
    </svg>
  );
};

export default IconStop;
