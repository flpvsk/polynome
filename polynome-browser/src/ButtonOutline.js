import React from 'react';
import pick from 'lodash/pick';

import theme from './theme';

const styles = {
  block: {
    background: 'none',
    border: `1px solid ${theme.color.outline}`,
    borderRadius: theme.shape.borderRadius,
    textTransform: 'uppercase',
    color: theme.color.primary,
    paddingLeft: '16px',
    paddingRight: '16px',
    minWidth: '64px',
    height: '36px',
    fontSize: '0.8em',
    letterSpacing: '0.05em',
    fontWeight: 500,
    cursor: 'pointer',
  },
};

const OutlineButton = props => {
  const styleMix = pick(props, ['marginLeft', 'width', 'height']);

  const blockStyle = {
    ...styles.block,
    ...styleMix,
  };

  return (
    <button style={blockStyle} onClick={props.onTouchTap}>
      {props.children}
    </button>
  );
};

export default OutlineButton;
