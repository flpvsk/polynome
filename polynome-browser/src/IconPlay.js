import React from 'react';

const IconPlay = ({ color, width, height }) => {
  return (
    <svg fill={color} height={height} viewBox="0 0 24 24" width={width}>
      <path d="M8 5v14l11-7z" />
      <path d="M0 0h24v24H0z" fill="none" />
    </svg>
  );
};

export default IconPlay;
