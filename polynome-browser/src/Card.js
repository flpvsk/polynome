import React from 'react';
import pick from 'lodash/pick';

import theme from './theme';
import { elevationToStyle } from './elevation';

const styles = {
  block: {
    backgroundColor: theme.color.surface,
    borderRadius: theme.shape.borderRadius,

    marginBottom: 16,
  },

  __body: {
    paddingTop: 16,
    paddingLeft: 16,
    paddingRight: 16,
  },
};

const Card = props => {
  const styleProps = pick(props, ['width', 'backgroundColor']);
  const elevation = props.elevation || 1;

  const blockStyle = {
    ...styles.block,
    ...styleProps,
    ...elevationToStyle(elevation),
  };

  return (
    <div onClick={props.onTouchTap} style={blockStyle}>
      <div style={styles.__body}>{props.children}</div>
    </div>
  );
};

export default Card;
