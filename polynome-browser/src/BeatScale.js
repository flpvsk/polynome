import React from 'react';
import theme from './theme';
import color from './color';
import { elevationToStyle } from './elevation';

import TextWrapper from './TextWrapper';

const styles = {
  block: {
    height: 96,
    marginTop: 8,
    paddingTop: 16,
    paddingBottom: 8,
    borderTop: `1px solid ${theme.color.outline}`,
  },

  __slider: {
    width: '100%',
    position: 'relative',
    height: 36,
  },

  __sliderButton: {
    position: 'absolute',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',

    textAlign: 'center',
    borderRadius: '50%',
    height: 32,
    width: 32,
    border: 'none',
    outline: 'none',
  },

  _isMute: {
    backgroundColor: theme.color.onSurfaceDisabled,
  },
};

const BeatScale = props => {
  const { beat, commonBeatsCount, width } = props;

  const svgWidth = width - 32;

  const { beatsCount, mutedBeats = [], offset = 0 } = beat;

  const every = commonBeatsCount / beatsCount;
  const inc = svgWidth / commonBeatsCount;
  const elements = [];
  const buttons = [];
  let texts = [];

  for (let i = 0; i < commonBeatsCount; i++) {
    let x;

    if (i === 0) {
      x = inc / 2;
    }

    if (i > 0) {
      x = inc / 2 + i * inc;
    }

    const isBeat = (i - offset) % every === 0;
    const isSuperBeat = (i - offset) % (every * 2) === 0;

    elements.push(
      <rect
        key={`commonbeat-rect-${i + 1}`}
        x={x + 16}
        y="4px"
        width="1"
        height="8"
      />
    );

    let showText =
      i + 1 === 1 ||
      isSuperBeat ||
      commonBeatsCount <= 15 ||
      (beatsCount <= 10 && isBeat) ||
      i + 1 === commonBeatsCount;

    if (showText) {
      texts.push(
        <tspan key={`commonbeat-text-${i + 1}`} x={x + 1 + 16} y="36">
          {i + 1}
        </tspan>
      );
    }

    if (isBeat) {
      const beatNumber = (i - offset) / every + 1;
      const isMute = mutedBeats.indexOf(beatNumber) !== -1;
      let btnStyle = styles.__sliderButton;
      let circleColor = color.baseColorByBeat(beat);

      if (isMute) {
        btnStyle = { ...btnStyle, ...styles._isMute };
        circleColor = color.muteColor(circleColor);
      }

      if (!isMute) {
        btnStyle = { ...btnStyle, ...elevationToStyle(1) };
      }

      const style = {
        ...btnStyle,
        left: x - 32 / 2 + 2 + 16,
        backgroundColor: color.toHslString(circleColor),
      };

      const onClick = () => {
        if (!props.onMuteSection) {
          return;
        }

        props.onMuteSection(beat.id, beatNumber);
      };

      buttons.push(
        <button
          key={`beat-slider-${beat.id}-${beatNumber}`}
          onClick={onClick}
          style={style}
        >
          <TextWrapper
            styleName="button"
            color={theme.color.onSecondary}
          >
            {beatNumber}
          </TextWrapper>
        </button>
      );
    }
  }

  return (
    <div style={{ ...styles.block, width }}>
      <div style={styles.__slider}>{buttons}</div>
      <svg width={width} height={40} viewBox={`0 0 ${width} 40`}>
        <g fill={theme.color.outline}>
          <rect x="0" y="12" width={width} height="1" />
          {elements}
          <text
            fontFamily="Futura, Roboto-Medium, Roboto, sans-serif"
            fontSize="0.859em"
            letterSpacing="0.1"
            fontWeight="400"
            textAnchor="middle"
          >
            {texts}
          </text>
        </g>
      </svg>
    </div>
  );
};

export default BeatScale;
