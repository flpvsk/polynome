export default function sort(arr, fn) {
  return [...arr].sort(fn);
}
