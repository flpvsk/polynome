import React from 'react';
import { DebounceInput } from 'react-debounce-input';
import pick from 'lodash/pick';

import theme from './theme';

import { styles as textStyles } from './TextWrapper';

const styles = {
  block: {
    height: 40,
    background: 'none',
    appearance: 'none',
    border: 'none',
    backgroundColor: theme.color.containerOnSurface,
    borderBottom: `1px solid ${theme.color.outline}`,
    borderTopLeftRadius: theme.shape.borderRadius,
    borderTopRightRadius: theme.shape.borderRadius,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    paddingLeft: 12,
    paddingRight: 12,
    color: theme.color.onSurface,
    textAlign: 'left',
    ...textStyles.subtitle1,
    width: '100%',
  },
};

class InputEmail extends React.Component {
  static getDerivedStateFromProps(nextProps, prevState) {
    const propsValue = nextProps.value;
    return { value: propsValue };
  }

  constructor() {
    super();
    this.state = {
      value: '',
    };

    this._onChange = e => {
      const value = e.target.value;
      this.setState({ value });
      this.props.onChange(value);
    };
  }

  render() {
    let propsStyle = pick(this.props, ['backgroundColor']);

    let style = {
      ...styles.block,
      ...propsStyle,
    };

    if (this.props.noPadding) {
      style = {
        ...style,
        paddingLeft: 0,
        paddingRight: 0,
      };
    }

    return (
      <DebounceInput
        type="password"
        debounceTimeout={500}
        style={style}
        onChange={this._onChange}
        value={this.state.value}
        placeholder={this.props.placeholder}
        title={this.props.title}
        pattern={this.props.pattern}
        maxLength={this.props.maxLength}
        disabled={this.props.isDisabled}
        required={this.props.required}
        readOnly={this.props.readOnly}
        autoFocus={this.props.isAutoFocus}
        onClick={e => e.stopPropagation()}
      />
    );
  }
}

export default InputEmail;
