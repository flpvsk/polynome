import React from 'react';
import theme from './theme';

const styles = {
  block: {
    height: 1,
    width: '100%',
    borderTop: `1px solid ${theme.color.outline}`,
    marginTop: 8,
    marginBottom: 8,
  },
};

const Divider = props => {
  return <div style={styles.block} />;
};

export default Divider;
