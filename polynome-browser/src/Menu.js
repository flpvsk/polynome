import React from 'react';
import theme from './theme';
import viewport from './viewport';
import { elevationToStyle } from './elevation';

const styles = {
  block: {
    backgroundColor: theme.color.surface,
    color: theme.color.onSurfaceMediumEmphasis,
    minWidth: 112,
    maxWidth: 280,
    minHeight: 48,
    paddingTop: 8,
    paddingBottom: 8,
    borderRadius: theme.shape.borderRadius,

    position: 'absolute',
    top: theme.size.appBarTopHeight * 0.7,
    right: 8,
    ...elevationToStyle(6),
  },
};

class Menu extends React.Component {
  constructor() {
    super();

    this._preventDismiss = ev => {
      ev.preventDefault();
      ev.stopPropagation();
    };
  }

  render() {
    const { props } = this;
    const windowWidth = window.innerWidth;
    let right = styles.block.right;

    if (windowWidth > viewport.getWidth()) {
      right = right + (windowWidth - viewport.getWidth()) / 2;
    }

    const blockStyle = {
      ...styles.block,
      right,
    };

    return (
      <div
        className="menu"
        onClick={this._preventDismiss}
        style={blockStyle}
      >
        {props.children}
      </div>
    );
  }
}

export default Menu;
