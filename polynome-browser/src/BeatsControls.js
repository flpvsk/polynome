import React, { Component } from 'react';

import { beatsSortFn } from './trackUtils';
import color from './color';
import theme from './theme';

import Button from './ButtonContained';
import './BeatsControls.css';

class BeatsControls extends Component {
  constructor() {
    super();

    this._onBeatClick = beatId => {
      this.props.onBeatClick(beatId);

      const { selectedItem } = this.props;

      if (selectedItem && selectedItem.beatId === beatId) {
        return;
      }

      this._block.scrollIntoView({
        block: 'start',
        inline: 'nearest',
        behavior: 'smooth',
      });
    };
  }

  shouldComponentUpdate(newProps) {
    return Object.keys(this.props).reduce((shouldUpdate, key) => {
      if (shouldUpdate) {
        return true;
      }

      return this.props[key] !== newProps[key];
    }, false);
  }

  render() {
    const { beats, width, selectedItem, soloItem } = this.props;

    const beatsSections = beats
      .slice()
      .sort(beatsSortFn)
      .reduce((acc, b, i) => {
        let headerSectionClassName = 'BeatsControls__Section';
        let elevation = 4;
        let isOutlined = false;

        if (selectedItem && b.id === selectedItem.beatId) {
          headerSectionClassName += ' _isSelected';
          elevation = 0;
          isOutlined = true;
        }

        const background = color.colorStrByBeat({
          beat: b,
          isMute: b.isMute || (soloItem && soloItem.beatId !== b.id),
          isSolo: soloItem && soloItem.beatId === b.id,
        });

        acc.push(
          <div
            key={`section-${b.id}`}
            className={headerSectionClassName}
          >
            <div className="BeatsControls__SectionRow">
              <Button
                onTouchTap={() => this._onBeatClick(b.id)}
                background={background}
                textColor={theme.color.onSecondaryMediumEmphasis}
                elevation={elevation}
                isOutlined={isOutlined}
              >
                {b.beatsCount}
              </Button>
            </div>
          </div>
        );
        return acc;
      }, []);

    return (
      <div
        ref={r => (this._block = r)}
        style={{ width: width }}
        className="BeatsControls"
      >
        {beatsSections}
      </div>
    );
  }
}

export default BeatsControls;
