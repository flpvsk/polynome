import React, { Component } from 'react';
import InputEmail from './InputEmail';
import InputPassword from './InputPassword';
import ButtonText from './ButtonText';
import ButtonContained from './ButtonContained';
import TextWrapper from './TextWrapper';
import LoadingIndicatorCircular from './LoadingIndicatorCircular';

import Player from './Player';

import logoTextDescriptive from './logo-text-descriptive.svg';
import startHeroPoster from './startHeroPoster.png';
import startLooper from './startLooper.png';
import startBrowser from './startBrowser.png';
import startMobileBg from './StartMobileBg.png';
import startCommunity from './startCommunity.png';

import requestAnimationFrame from './requestAnimationFrame';
import shouldComponentUpdate from './shouldComponentUpdate';

import theme from './theme';
import viewport from './viewport';

import withInitialScreenProvider from './data/withInitialScreenProvider';

const BEATS = [
  [
    {
      id: '1-1',
      beatsCount: 1,
      lineStart: 0,
      lineDuration: 1,
    },
    {
      id: '1-3',
      beatsCount: 3,
      lineStart: 0,
      lineDuration: 3,
    },
    {
      id: '1-4',
      beatsCount: 4,
      lineStart: 0,
      lineDuration: 4,
      offset: 0,
      mutedBeats: [1, 3],
    },
  ],
  [
    {
      id: '2-2',
      beatsCount: 2,
      lineStart: 0,
      lineDuration: 2,
      offset: 0,
    },
    {
      id: '2-5',
      beatsCount: 5,
      lineStart: 0,
      lineDuration: 5,
    },
  ],
  [
    {
      id: '3-1',
      beatsCount: 1,
      offset: 7,
      lineStart: 0,
      lineDuration: 1,
    },
    {
      id: '3-4',
      beatsCount: 4,
      lineStart: 0,
      lineDuration: 4,
      mutedBeats: [2, 4],
    },
    {
      id: '3-8',
      beatsCount: 8,
      lineStart: 0,
      lineDuration: 8,
      offset: 0,
      mutedBeats: [1, 3, 5, 7],
    },
  ],
];

const styles = {
  block: {
    display: 'grid',
    gridTemplateColumns: '100%',
    gridTemplateRows: '[hero] auto',

    flexGrow: 1,
    flexBasis: '100%',
    width: '100%',
  },

  block_medium: {},

  __hero: {
    display: 'grid',

    gridTemplateColumns:
      `[left] minmax(0, 1fr) ` +
      `[right-start] 32px [right-content] 1fr [right-end] 32px`,

    gridTemplateRows:
      `[top] 40px ` +
      `[logo] 80px 32px ` +
      `[login] auto 32px ` +
      `[actions] auto 64px ` +
      `[image] 1fr`,

    gridAutoRows: 0,
    backgroundColor: theme.color.background,
    backgroundImage:
      `linear-gradient(` +
      `${theme.color.background} 60%, ` +
      `${theme.color.surface}` +
      `)`,

    minHeight: '90vh',
  },

  __hero_medium: {
    gridTemplateColumns: `[right-start] 32px [right-content] 1fr [right-end] 32px`,
    backgroundImage: `url(${startMobileBg})`,
    backgroundSize: 'cover',
  },

  __hero_small: {
    gridTemplateColumns: `[right-start] 12px [right-content] 1fr [right-end] 12px`,
  },

  __rowCopy: {
    backgroundColor: theme.color.surfaceDark,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'stretch',
    minHeight: '96vh',
  },

  __row2: {
    borderTop: `1px solid ${theme.color.outline}`,
    backgroundColor: theme.color.background,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'stretch',
    minHeight: '96vh',
  },

  __row2Header: {
    marginTop: 32,
    marginBottom: 12,
    paddingLeft: 32,
    paddingRight: 32,
  },

  __row2Header_medium: {
    paddingLeft: 16,
    paddingRight: 16,
  },

  __rowFeatureHeader: {
    marginTop: 16,
    marginBottom: 64,
  },

  __videoWrapper: {
    display: 'flex',
    flexGrow: 1,
    alignItems: 'center',
  },

  __video: {
    marginLeft: 32,
    marginRight: 32,
    marginBottom: 32,
    width: '70vw',
    height: 0.5625 * 70 + 'vw',
    maxWidth: 1024,
    maxHeight: 576,
    border: `1px solid ${theme.color.outline}`,
    borderRadus: theme.shape.borderRadius,
    justifySelf: 'center',
  },

  __rowFeature: {
    borderTop: `1px solid ${theme.color.outline}`,
    backgroundColor: theme.color.background,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    minHeight: '80vh',
  },

  __rowFeature_medium: {
    flexDirection: 'column',
    minHeight: 'auto',
  },

  __rowFeatureContent: {
    flex: '0 0 50%',
    paddingLeft: 32,
    paddingRight: 32,
    minWidth: 320,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },

  __rowFeatureContent_medium: {
    flex: '1 0',
    paddingLeft: 16,
    paddingRight: 16,
    justifyContent: 'flex-start',
  },

  __featureListWrapper: {
    display: 'flex',
    paddingBottom: 32,
  },

  __featureList: {
    paddingLeft: 0,
    margin: 0,
    listStyle: 'none',
  },

  __featureListItem: {
    marginTop: 16,
  },

  __playerWrapper: {
    flex: '0 0 50%',
    display: 'flex',
    paddingTop: 16,
    paddingLeft: 16,
    paddingBottom: 16,
    alignItems: 'center',
    justifyContent: 'center',
  },

  __looperImageWrapper: {
    display: 'flex',
    flex: '0 0 50%',
    background: `url(${startLooper}) left center / contain no-repeat`,
    marginBottom: 0,
  },

  __looperImage: {
    display: 'none',
    width: 600,
    height: 261,
  },

  __browserImageWrapper: {
    display: 'flex',
    flex: '0 0 50%',
    maxWidth: '80vw',
    background: `url(${startBrowser}) center center / contain no-repeat`,
    marginBottom: 0,
  },

  __communityImageWrapper: {
    display: 'flex',
    flex: '0 0 50%',
    maxWidth: 'calc(50vw - 64px)',
    paddingLeft: 16,
    background: `url(${startCommunity}) center center / contain no-repeat`,
    marginBottom: 0,
  },

  __video_medium: {
    marginLeft: 0,
    marginRight: 0,
    marginBottom: 0,
    width: '100vw',
    height: 0.5625 * 100 + 'vw',
  },

  __rowPurpose: {
    borderTop: `1px solid ${theme.color.outline}`,
    backgroundColor: theme.color.background,
    display: 'flex',
    flexDirection: 'column',
    paddingBottom: 32,
  },

  __rowPurpose_medium: {},

  __rowPurposeText: {
    marginTop: 0,
    marginBottom: 12,
    maxWidth: 640,
    paddingLeft: 32,
    paddingRight: 32,
  },

  __rowPurposeText_medium: {
    paddingLeft: 16,
    paddingRight: 16,
  },

  __rowFooter: {
    backgroundColor: theme.color.surfaceDark,
    display: 'flex',
    flexDirection: 'column',
    paddingLeft: 32,
    paddingRight: 32,
    paddingBottom: 8,
    paddingTop: 8,
  },

  __rowFooter_medium: {
    paddingLeft: 16,
    paddingRight: 16,
  },

  __heroImage: {
    gridArea: `top / left / footer / left`,
    backgroundImage: `url(${startHeroPoster})`,
    backgroundSize: 'cover',
  },

  __heroImage_medium: {
    display: 'none',
  },

  __logo: {
    gridArea: `logo / right-content / logo / right-content`,
  },

  __loginForm: {
    gridArea: `login / right-content / login / right-content`,

    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    alignContent: 'flex-start',
    maxWidth: 600,
  },

  __formError: {
    flexBasis: '100%',
    flexShrink: 0,
  },

  __loginFormInputs: {
    display: 'flex',
    flexWrap: 'wrap',
  },

  __loginFormActions: {
    display: 'flex',
    flexWrap: 'nowrap',
    height: 48,
    alignItems: 'center',
  },

  __loginFormInputsAndLink: {
    display: 'flex',
    flexDirection: 'column',
  },

  __loginFormElement: {
    height: 48,
    flexBasis: 224,
    marginRight: 8,
    flexGrow: 1,
    display: 'flex',
    alignItems: 'center',
  },

  _isLoginFormLast: {
    marginRight: 0,
  },

  __loginFormButton: {
    height: 48,
    flexBasis: 'auto',
    marginRight: 8,
    flexGrow: 0,
    display: 'flex',
    alignItems: 'center',
  },

  __loginLinkWrapper: {
    flexBasis: '100%',
    flexShrink: 0,
  },

  __copy: {
    display: 'flex',
    alignItems: 'center',
    maxWidth: 600,
    alignContent: 'flex-start',
    textAlign: 'center',
    margin: 'auto',
  },

  __formImage: {
    gridArea: `image / right-start / image / span 3`,
    justifySelf: 'stretch',
    alignSelf: 'end',
    display: 'flex',
    alignItems: 'flex-end',
  },

  __actions: {
    gridArea: `actions / right-content / actions / right-content`,
    maxWidth: 600,
  },

  __actionsWrapper: {
    display: 'flex',
    width: '100%',
    height: '100%',
    marginLeft: -16,
    flexWrap: 'wrap',
    fontSize: '1.3em',
    alignItems: 'center',
    alignContent: 'flex-start',
  },

  __logoSocial: {
    height: 64,
  },

  __newsletterForm: {
    marginTop: 48,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'stretch',
    maxWidth: 640,
    paddingLeft: 32,
    paddingRight: 32,
  },

  __newsletterForm_medium: {
    paddingLeft: 16,
    paddingRight: 16,
  },

  __newsletterText: {},

  __newsletterInputsWrapper: {
    display: 'flex',
    alignItems: 'center',
  },

  __newsletterInput: {
    flex: '1 0 40%',
    marginRight: 8,
  },

  __newsletterButton: {},
};

const SvgRect = ({
  fillColor,
  strokeColor,
  strokeWidth,
  yStepHeight,
  yStep,
  yMaxHeight,
  yMinHeight,
  xStepWidth,
  xStep,
  xGutterWidth,
}) => {
  const x = xStep * (xStepWidth + xGutterWidth);
  const yHeight = Math.min(
    Math.max(yStep * yStepHeight, yMinHeight),
    yMaxHeight
  );
  const y = yMaxHeight - yHeight;
  return (
    <rect
      x={x}
      y={y}
      fill={fillColor}
      fillOpacity={0.6}
      stroke={strokeColor}
      strokeWidth={strokeWidth}
      width={xStepWidth}
      height={yHeight}
    />
  );
};

class SvgWave extends Component {
  constructor() {
    super();
    this.shouldComponentUpdate = shouldComponentUpdate;
  }

  render() {
    const rectConfig = {
      fillColor: 'url(#gradient)',
      strokeColor: this.props.strokeColor,
      strokeWidth: 0,
      yStepHeight: 12,
      yMinHeight: 8,
      yMaxHeight: 96,
      xStepWidth: 8,
      xGutterWidth: 4,
    };
    const { fillColor } = this.props;

    const steps = [
      <SvgRect key={`step-0`} {...rectConfig} xStep={0} yStep={4} />,
    ];

    for (let xStep = 1; xStep < 50; xStep++) {
      const yStep = Math.max(0, Math.round(Math.random() * 14) - 3);
      steps.push(
        <SvgRect
          key={`step-${xStep}`}
          {...rectConfig}
          xStep={xStep}
          yStep={yStep}
        />
      );
    }
    return (
      <svg style={{ width: '100%' }} viewBox="0 0 440 96">
        <linearGradient id="gradient" x1="0" x2="0" y1="0" y2="1">
          <stop stopColor={fillColor} stopOpacity={0} offset="0%" />
          <stop stopColor={fillColor} stopOpacity={0.5} offset="60%" />
          <stop stopColor={fillColor} stopOpacity={1} offset="100%" />
        </linearGradient>
        {steps}
      </svg>
    );
  }
}

class StartScreen extends Component {
  constructor() {
    super();

    this.state = {
      emailValue: '',
      passwordValue: '',
      newsletterEmail: '',
      mode: 'login',
      previousScrollPosition: null,
      scrollAmount: 0,
      beatIndex: 0,
    };

    this._changeEmailValue = emailValue => {
      this.setState({ emailValue });
    };

    this._changePasswordValue = passwordValue => {
      this.setState({ passwordValue });
    };

    this._changeModeToSignup = () => {
      this.setState({ mode: 'signup', passwordValue: '' });
    };

    this._changeModeToLogin = e => {
      e.preventDefault();
      this.setState({ mode: 'login', passwordValue: '' });
    };

    this._changeModeToEmailLink = () => {
      this.setState({ mode: 'link', passwordValue: '' });
    };

    this._login = e => {
      e.preventDefault();
      this.props.onLogin({
        email: this.state.emailValue,
        password: this.state.passwordValue,
      });
    };

    this._signup = e => {
      e.preventDefault();
      this.props.onSignup({
        email: this.state.emailValue,
        password: this.state.passwordValue,
      });
    };

    this._emailLink = e => {
      e.preventDefault();
      this.props.onEmailLink({
        email: this.state.emailValue,
      });
    };

    this._incrementScroll = () => {
      const { previousScrollPosition, scrollAmount } = this.state;
      const scrollY = window.scrollY;
      const prev = previousScrollPosition || scrollY;

      this.setState({
        previousScrollPosition: scrollY,
        scrollAmount: Math.abs(scrollY - prev) + scrollAmount,
      });
    };

    this._incrementScrollThrottled = () => {
      requestAnimationFrame(this._incrementScroll);
    };

    this._getTime = () => {
      return this.state.scrollAmount / 50;
    };

    this._incrementBeatIndex = () => {
      this.setState({
        beatIndex: (this.state.beatIndex + 1) % BEATS.length,
      });
    };

    this._updateNewsletterEmail = value => {
      this.setState({ newsletterEmail: value });
    };
  }

  componentDidMount() {
    window.addEventListener('scroll', this._incrementScrollThrottled);
  }

  componentWillUnmount() {
    window.removeEventListener(
      'scroll',
      this._incrementScrollThrottled
    );
  }

  componentWillUpdate(newProps) {
    if (newProps.emailLinkSent && !this.props.emailLinkSent) {
      this.setState({ mode: 'emailSent' });
    }
  }

  _renderLoginMode() {
    const isLoading = this.props.loginLoading;

    let button = <ButtonContained>Enter</ButtonContained>;

    if (isLoading) {
      button = <LoadingIndicatorCircular />;
    }

    let firstInputStyle = styles.__loginFormElement;

    if (viewport.getOriginalWidth() < 544) {
      firstInputStyle = {
        ...firstInputStyle,
        marginRight: 0,
      };
    }

    return (
      <form onSubmit={this._login} style={styles.__loginForm}>
        <div style={styles.__formError}>
          <TextWrapper styleName="subtitle1" color={theme.color.error}>
            {this.props.loginError}
          </TextWrapper>
        </div>
        <div style={styles.__loginFormInputsAndLink}>
          <div style={styles.__loginFormInputs}>
            <div style={firstInputStyle}>
              <InputEmail
                value={this.state.emailValue}
                onChange={this._changeEmailValue}
                isDisabled={isLoading}
                placeholder="Email"
              />
            </div>

            <div style={styles.__loginFormElement}>
              <InputPassword
                value={this.state.passwordValue}
                onChange={this._changePasswordValue}
                isDisabled={isLoading}
                placeholder="Password"
              />
            </div>

            <div
              style={{
                ...styles.__loginFormButton,
                ...styles._isLoginFormLast,
              }}
            >
              {button}
            </div>

            <div style={styles.__loginLinkWrapper}>
              <TextWrapper
                styleName="link"
                color={theme.color.onBackgroundDisabled}
                onTouchTap={this._changeModeToEmailLink}
              >
                Send a login link to my email
              </TextWrapper>
            </div>
          </div>
        </div>
      </form>
    );
  }

  _renderSignupMode() {
    const isLoading = this.props.signupLoading;

    let firstInputStyle = styles.__loginFormElement;
    if (viewport.getOriginalWidth() < 544) {
      firstInputStyle = {
        ...firstInputStyle,
        marginRight: 0,
      };
    }

    let actions = [
      <div key="action-submit" style={styles.__loginFormButton}>
        <ButtonContained type="submit">Create account</ButtonContained>
      </div>,

      <div
        key="action-cancel"
        style={{
          ...styles.__loginFormButton,
          ...styles._isLoginFormLast,
          justifyContent: 'flex-end',
        }}
      >
        <ButtonText onTouchTap={this._changeModeToLogin}>
          Cancel
        </ButtonText>
      </div>,
    ];

    if (isLoading) {
      actions = <LoadingIndicatorCircular />;
    }

    return (
      <form onSubmit={this._signup} style={styles.__loginForm}>
        <div style={styles.__formError}>
          <TextWrapper styleName="body1" color={theme.color.error}>
            {this.props.signupError}
          </TextWrapper>
        </div>
        <div style={styles.__loginFormInputs}>
          <div style={firstInputStyle}>
            <InputEmail
              value={this.state.emailValue}
              onChange={this._changeEmailValue}
              style={styles.__loginFormElement}
              isDisabled={isLoading}
              isAutoFocus={true}
              placeholder="Email"
            />
          </div>

          <div style={styles.__loginFormElement}>
            <InputPassword
              value={this.state.passwordValue}
              onChange={this._changePasswordValue}
              style={styles.__loginFormElement}
              isDisabled={isLoading}
              placeholder="Password"
            />
          </div>
        </div>

        <div style={styles.__loginFormActions}>{actions}</div>
      </form>
    );
  }

  _renderEmailLinkMode() {
    const isLoading = this.props.emailLinkLoading;

    let actions = [
      <div key="action-send-link" style={styles.__loginFormButton}>
        <ButtonContained>Send link</ButtonContained>
      </div>,

      <div
        key="action-cancel"
        style={{
          ...styles.__loginFormButton,
          ...styles._isLoginFormLast,
        }}
      >
        <ButtonText onTouchTap={this._changeModeToLogin}>
          Cancel
        </ButtonText>
      </div>,
    ];

    if (isLoading) {
      actions = <LoadingIndicatorCircular />;
    }

    return (
      <form onSubmit={this._emailLink} style={styles.__loginForm}>
        <div style={styles.__formError}>
          <TextWrapper styleName="body1" color={theme.color.error}>
            {this.props.emailLinkError}
          </TextWrapper>
        </div>
        <div style={styles.__loginFormElement}>
          <InputEmail
            isAutoFocus={true}
            value={this.state.emailValue}
            onChange={this._changeEmailValue}
            disabled={isLoading}
            placeholder="Email"
          />
        </div>

        <div style={styles.__loginFormActions}>{actions}</div>

        <div style={{ flexBasis: '100%' }}>
          <TextWrapper
            styleName="body1"
            color={theme.color.onBackgroundDisabled}
          >
            {`If you already have an account  ` +
              `we will send you a link that will log you in.`}
          </TextWrapper>
        </div>
      </form>
    );
  }

  _renderEmailSent() {
    return (
      <form
        onSubmit={this._changeModeToLogin}
        style={styles.__loginForm}
      >
        <div
          style={{ display: 'flex', alignItems: 'center', flexGrow: 1 }}
        >
          <TextWrapper
            styleName="body1"
            color={theme.color.onBackgroundDisabled}
          >
            {`Email with a sign in link was sent to your address.`}
          </TextWrapper>
        </div>

        <div style={styles.__loginFormActions}>
          <ButtonText>OK</ButtonText>
        </div>
      </form>
    );
  }

  _renderForm() {
    const { mode } = this.state;

    if (mode === 'login') {
      return this._renderLoginMode();
    }

    if (mode === 'signup') {
      return this._renderSignupMode();
    }

    if (mode === 'link') {
      return this._renderEmailLinkMode();
    }

    if (mode === 'emailSent') {
      return this._renderEmailSent();
    }
  }

  render() {
    let isLoading =
      this.props.loginLoading ||
      this.props.signupLoading ||
      this.props.emailLinkLoading;

    let blockStyle = styles.block;
    let heroStyle = styles.__hero;
    let heroImageStyle = styles.__heroImage;
    let heroTextStyleName = 'h3';
    let videoStyle = styles.__video;
    let row2HeaderStyle = styles.__row2Header;
    let rowFeatureStyle = styles.__rowFeature;
    let rowFeatureContentStyle = styles.__rowFeatureContent;
    let rowPurposeStyle = styles.__rowPurpose;
    let rowPurposeTextStyle = styles.__rowPurposeText;
    let rowFooterStyle = styles.__rowFooter;
    let looperImageWrapperStyle = styles.__looperImageWrapper;
    let browserImageWrapperStyle = styles.__browserImageWrapper;
    let communityImageWrapperStyle = styles.__communityImageWrapper;
    let newsletterFormStyle = styles.__newsletterForm;
    let waveFillColor = theme.color.primaryLight;

    if (viewport.getOriginalWidth() < 800) {
      blockStyle = {
        ...blockStyle,
        ...styles.block_medium,
      };

      heroImageStyle = {
        ...heroImageStyle,
        ...styles.__heroImage_medium,
      };

      heroStyle = {
        ...heroStyle,
        ...styles.__hero_medium,
      };

      heroTextStyleName = 'h4';

      videoStyle = {
        ...styles.__video,
        ...styles.__video_medium,
      };

      row2HeaderStyle = {
        ...row2HeaderStyle,
        ...styles.__row2Header_medium,
      };

      rowFeatureStyle = {
        ...rowFeatureStyle,
        ...styles.__rowFeature_medium,
      };

      rowFeatureContentStyle = {
        ...rowFeatureContentStyle,
        ...styles.__rowFeatureContent_medium,
      };

      looperImageWrapperStyle = {
        ...looperImageWrapperStyle,
        marginBottom: 32,
        flex: '1 0 43vw',
      };

      browserImageWrapperStyle = {
        ...browserImageWrapperStyle,
        flex: '0 0 43vw',
        maxWidth: undefined,
        marginBottom: 32,
        background: `url(${startBrowser}) center center / contain no-repeat`,
      };

      communityImageWrapperStyle = {
        ...communityImageWrapperStyle,
        flex: '1 0 43vw',
        maxWidth: '100%',
        marginBottom: 32,
      };

      rowPurposeStyle = {
        ...rowPurposeStyle,
        ...styles.__rowPurpose_medium,
      };

      rowPurposeTextStyle = {
        ...rowPurposeTextStyle,
        ...styles.__rowPurposeText_medium,
      };

      rowFooterStyle = {
        ...rowFooterStyle,
        ...styles.__rowFooter_medium,
      };

      waveFillColor = theme.color.surface;

      newsletterFormStyle = {
        ...newsletterFormStyle,
        ...styles.__newsletterForm_medium,
      };
    }

    if (viewport.getOriginalWidth() < 450) {
      heroStyle = {
        ...heroStyle,
        ...styles.__hero_small,
      };
    }

    return (
      <div style={blockStyle}>
        <div style={heroStyle}>
          <div style={heroImageStyle} title={`Poster image`} />

          <header style={styles.__logo}>
            <img
              src={logoTextDescriptive}
              alt="Polychops | Deliberate practice for musicians"
            />
          </header>

          {this._renderForm()}

          <div style={styles.__actions}>
            <div style={styles.__actionsWrapper}>
              <ButtonText
                disabled={isLoading}
                textColor={theme.color.primaryLight}
                onTouchTap={this._changeModeToSignup}
              >
                Create account
              </ButtonText>

              <TextWrapper
                styleName="button"
                color={theme.color.onBackground}
              >
                or
              </TextWrapper>

              <ButtonText
                disabled={isLoading}
                textColor={theme.color.primaryLight}
                onTouchTap={this.props.onLoginAnonymously}
              >
                Try it now
              </ButtonText>
            </div>
          </div>

          <div style={styles.__formImage}>
            <SvgWave
              fillColor={waveFillColor}
              strokeColor={theme.color.surface}
            />
          </div>
        </div>

        <div style={styles.__rowCopy}>
          <div style={styles.__copy}>
            <div>
              <TextWrapper
                styleName={heroTextStyleName}
                elevation={1}
                color={theme.color.primaryLight}
              >
                {`Practice `}
              </TextWrapper>
              <TextWrapper
                styleName={heroTextStyleName}
                color={theme.color.onBackground}
              >
                {`your sense of rhythm `}
              </TextWrapper>

              <TextWrapper
                styleName={heroTextStyleName}
                elevation={1}
                color={theme.color.primaryLight}
              >
                {`Record `}
              </TextWrapper>
              <TextWrapper
                styleName={heroTextStyleName}
                color={theme.color.onBackground}
              >
                {`and `}
              </TextWrapper>
              <TextWrapper
                styleName={heroTextStyleName}
                elevation={1}
                color={theme.color.primaryLight}
              >
                {`Share `}
              </TextWrapper>
              <TextWrapper
                styleName={heroTextStyleName}
                color={theme.color.onBackground}
              >
                {`your musical ideas `}
              </TextWrapper>

              <TextWrapper
                styleName={heroTextStyleName}
                elevation={1}
                color={theme.color.primaryLight}
              >
                {`Inspire `}
              </TextWrapper>
              <TextWrapper
                styleName={heroTextStyleName}
                color={theme.color.onBackground}
              >
                {`and `}
              </TextWrapper>
              <TextWrapper
                noWrap
                styleName={heroTextStyleName}
                elevation={1}
                color={theme.color.primaryLight}
              >
                {`Get Inspired `}
              </TextWrapper>
              <TextWrapper
                styleName={heroTextStyleName}
                color={theme.color.onBackground}
              >
                {`by the community`}
              </TextWrapper>
            </div>
          </div>
        </div>

        <div style={styles.__row2}>
          <div style={row2HeaderStyle}>
            <TextWrapper
              styleName={'h2'}
              elevation={1}
              color={theme.color.onBackground}
            >
              {`30 seconds promo`}
            </TextWrapper>
          </div>

          <div style={styles.__videoWrapper}>
            <iframe
              style={videoStyle}
              title="Polychops promo video"
              src="https://www.youtube-nocookie.com/embed/meLO5KtHv0U?rel=0&amp;showinfo=0"
              frameBorder="0"
              allow="autoplay; encrypted-media"
              allowFullScreen
            />
          </div>
        </div>

        <div style={rowFeatureStyle}>
          <div style={rowFeatureContentStyle}>
            <div style={styles.__rowFeatureHeader}>
              <TextWrapper
                styleName={'h2'}
                elevation={1}
                color={theme.color.onBackground}
              >
                {`Metronome`}
              </TextWrapper>
            </div>
            <div style={styles.__featureListWrapper}>
              <ul style={styles.__featureList}>
                <li>
                  <TextWrapper
                    styleName={'h5'}
                    color={theme.color.onBackground}
                  >
                    {`Make a beat to practice along with.`}
                  </TextWrapper>
                </li>

                <li style={styles.__featureListItem}>
                  <TextWrapper
                    styleName={'h5'}
                    color={theme.color.onBackground}
                  >
                    {`Odd meters and polyrhythms support.`}
                  </TextWrapper>
                </li>

                <li style={styles.__featureListItem}>
                  <TextWrapper
                    styleName={'h5'}
                    color={theme.color.onBackground}
                  >
                    {`Use beeps or built-in sound packs.`}
                  </TextWrapper>
                </li>
              </ul>
            </div>
          </div>
          <div style={styles.__playerWrapper}>
            <Player
              width={Math.min(400, viewport.getOriginalWidth() / 2)}
              commonBeatsTempo={20}
              commonBeatsCount={12}
              getCurrentTime={this._getTime}
              startTime={0}
              isPaused={false}
              pauseTime={null}
              onBeatSelected={this._incrementBeatIndex}
              beats={BEATS[this.state.beatIndex]}
              displayConfig={{
                stickWidth: 2,
                stickColor: '#eee',
                backgroundColor: 'transparent',
                lipMax: 12,
                lightenMax: 8,
                beatWidth: 4,
                baseCircleBeatWidth: 0,
                baseCircleBeatLength: 0,
                baseCircleColor: 'transparent',
                circleShadowColor: '#212121',
                circleShadowBlur: 20,
                outlineColor: '#fff',
              }}
            />
          </div>
        </div>

        <div style={rowFeatureStyle}>
          <div style={rowFeatureContentStyle}>
            <div style={styles.__rowFeatureHeader}>
              <TextWrapper
                styleName={'h2'}
                elevation={1}
                color={theme.color.onBackground}
              >
                {`Looper`}
              </TextWrapper>
            </div>
            <div style={styles.__featureListWrapper}>
              <ul style={styles.__featureList}>
                <li>
                  <TextWrapper
                    styleName={'h5'}
                    color={theme.color.onBackground}
                  >
                    {`Record and listen back to yourself.`}
                  </TextWrapper>
                </li>

                <li style={styles.__featureListItem}>
                  <TextWrapper
                    styleName={'h5'}
                    color={theme.color.onBackground}
                  >
                    {`Overlay several pieces of music.`}
                  </TextWrapper>
                </li>

                <li style={styles.__featureListItem}>
                  <TextWrapper
                    styleName={'h5'}
                    color={theme.color.onBackground}
                  >
                    {`Get in the Flow.`}
                  </TextWrapper>
                </li>
              </ul>
            </div>
          </div>

          <div style={looperImageWrapperStyle} />
        </div>

        <div style={rowFeatureStyle}>
          <div style={rowFeatureContentStyle}>
            <div style={styles.__rowFeatureHeader}>
              <TextWrapper
                styleName={'h2'}
                elevation={1}
                color={theme.color.onBackground}
              >
                {`Community`}
              </TextWrapper>
            </div>
            <div style={styles.__featureListWrapper}>
              <ul style={styles.__featureList}>
                <li>
                  <TextWrapper
                    styleName={'h5'}
                    color={theme.color.onBackground}
                  >
                    {`Discover and meet other musicians.`}
                  </TextWrapper>
                </li>

                <li style={styles.__featureListItem}>
                  <TextWrapper
                    styleName={'h5'}
                    color={theme.color.onBackground}
                  >
                    {`Remix and play along other people's tunes.`}
                  </TextWrapper>
                </li>

                <li style={styles.__featureListItem}>
                  <TextWrapper
                    styleName={'h5'}
                    color={theme.color.onBackground}
                  >
                    {`Share your own beats and loops.`}
                  </TextWrapper>
                </li>
              </ul>
            </div>
          </div>

          <div style={communityImageWrapperStyle} />
        </div>

        <div style={rowFeatureStyle}>
          <div style={rowFeatureContentStyle}>
            <div style={styles.__rowFeatureHeader}>
              <TextWrapper
                styleName={'h2'}
                elevation={1}
                color={theme.color.onBackground}
              >
                {`In your browser`}
              </TextWrapper>
            </div>
            <div style={styles.__featureListWrapper}>
              <ul style={styles.__featureList}>
                <li>
                  <TextWrapper
                    styleName={'h5'}
                    color={theme.color.onBackground}
                  >
                    {`Nothing to install, ` +
                      `Polychops works in your browser.`}
                  </TextWrapper>
                </li>

                <li style={styles.__featureListItem}>
                  <TextWrapper
                    styleName={'h5'}
                    color={theme.color.onBackground}
                  >
                    {`Share links to your music with friends, ` +
                      `bandmates or on social media. `}
                  </TextWrapper>
                </li>

                <li style={styles.__featureListItem}>
                  <TextWrapper
                    styleName={'h5'}
                    color={theme.color.onBackground}
                  >
                    {`Visitors don't need to create an account to listen.`}
                  </TextWrapper>
                </li>
              </ul>
            </div>
          </div>

          <div style={browserImageWrapperStyle} />
        </div>

        <div style={styles.__row2}>
          <div style={row2HeaderStyle}>
            <TextWrapper
              styleName={'h2'}
              elevation={1}
              color={theme.color.onBackground}
            >
              {`Polychops walk-through`}
            </TextWrapper>
          </div>

          <div style={styles.__videoWrapper}>
            <iframe
              style={videoStyle}
              title="Polychops walk-through video"
              src="https://www.youtube-nocookie.com/embed/rioOlyWDOnU?rel=0&amp;showinfo=0"
              frameBorder="0"
              allow="autoplay; encrypted-media"
              allowFullScreen
            />
          </div>
        </div>

        <div style={rowPurposeStyle}>
          <div style={row2HeaderStyle}>
            <TextWrapper
              styleName={'h2'}
              elevation={1}
              color={theme.color.onBackground}
            >
              {`About us`}
            </TextWrapper>
          </div>

          <div>
            <div style={rowPurposeTextStyle}>
              <TextWrapper
                styleName={'h5'}
                color={theme.color.onSurface}
              >
                {`
                    We are musicians.
                    We enjoy music styles from different parts of the
                    world.
                    We build modern tools that help us practice and learn.
                  `}
              </TextWrapper>
            </div>
            <div style={rowPurposeTextStyle}>
              <TextWrapper
                styleName={'h5'}
                color={theme.color.onSurface}
              >
                {`
                   We made Polychops to help live musicians
                   improve and discover new horizons.
                  `}
              </TextWrapper>
            </div>
          </div>
          <form
            style={newsletterFormStyle}
            id="signup-form"
            method="post"
            action="https://placesapp.us3.list-manage.com/subscribe/post?u=442481fe1647be76049870aef&amp;id=259f656efb"
          >
            <p style={styles.__newsletterText}>
              <TextWrapper
                styleName={'body1'}
                color={theme.color.onSurface}
              >
                {`
                     Subscribe to our newsletter to get the
                     latest content and updates.
                    `}
              </TextWrapper>
            </p>
            <div style={styles.__newsletterInputsWrapper}>
              <div style={styles.__newsletterInput}>
                <InputEmail
                  name="EMAIL"
                  id="mce-EMAIL"
                  placeholder="Email"
                  value={this.state.newsletterEmail}
                  onChange={this._updateNewsletterEmail}
                  required
                />
                <div
                  style={{ position: 'absolute', left: -5000 }}
                  aria-hidden={true}
                >
                  <input
                    type="text"
                    name="b_442481fe1647be76049870aef_259f656efb"
                    tabIndex="-1"
                    value=""
                  />
                </div>
              </div>

              <div style={styles.__newsletterButton}>
                <ButtonContained
                  name="subscribe"
                  id="mc-embedded-subscribe"
                  value="Subscribe"
                >
                  {`Subscribe`}
                </ButtonContained>
              </div>
            </div>
          </form>
        </div>

        <div style={rowFooterStyle}>
          <div>
            <TextWrapper
              styleName={'body1'}
              color={theme.color.onSurfaceDisabled}
            >
              {`Made with `}
            </TextWrapper>
            <span role="img" aria-label="guitar">
              🎸
            </span>
            <TextWrapper
              styleName={'body1'}
              color={theme.color.onSurfaceDisabled}
            >
              {` in Berlin. `}
            </TextWrapper>
            <TextWrapper
              styleName={'link'}
              href={`mailto:hey@polychops.com`}
              title="Write us"
              color={theme.color.onSurfaceDisabled}
            >
              {`Write us`}
            </TextWrapper>
            <TextWrapper
              styleName={'body1'}
              color={theme.color.onSurfaceDisabled}
            >
              {`.`}
            </TextWrapper>
          </div>
        </div>
      </div>
    );
  }
}

export default withInitialScreenProvider(StartScreen);
