import React, { Component } from 'react';
import isEqual from 'lodash/isEqual';

import theme from './theme';

import IconPlus from './IconPlus';
import IconRemove from './IconRemove';
import IconCancel from './IconCancel';
import IconDone from './IconDone';
import './CircleControls.css';

class CircleControls extends Component {
  constructor() {
    super();
    this.state = {
      time: Date.now(),
      beatButton: {
        lastClicked: 0,
        clickCount: 0,
      },
    };

    this._changeInputDeviceId = e => {
      this.props.onChangeInputDeviceId(e.target.value);
    };

    this._updateTime = () => {
      if (this._stopUpdatingTime) {
        return;
      }

      const time = Date.now();
      const { beatButton } = this.state;
      const { beatButtonWait } = this.props;
      const isWaitExpired =
        time - beatButton.lastClicked > beatButtonWait;

      if (isWaitExpired && beatButton.clickCount > 0) {
        this._acceptBeat();
      }

      this.setState({ time });
      setTimeout(this._updateTime, this.props.beatButtonWait);
    };

    this._acceptBeat = () => {
      const { beatButton } = this.state;
      this.props.onBeatAdded({
        beatsCount: beatButton.clickCount,
        tempo: this.props.defaultTempo,
      });

      this.setState({
        beatButton: {
          ...beatButton,
          clickCount: 0,
          lastTempo: this.props.defaultTempo,
        },
      });
    };

    this._onBeatButtonClick = () => {
      let clickCount = 1;
      let { tempo, beatButton } = this.state;
      const now = Date.now();

      if (this._isBeatButtonInWait()) {
        clickCount = beatButton.clickCount + 1;
        tempo = Math.floor(60000 / (now - beatButton.lastClicked));
      }

      this.setState({
        beatButton: {
          lastClicked: now,
          lastTempo: tempo,
          clickCount,
        },
      });
    };

    this._cancelBeats = () => {
      this.setState({
        beatButton: {
          clickCount: 0,
          lastClicked: Date.now(),
        },
      });
    };

    this._decrementBeat = () => {
      let { beatButton } = this.state;
      let { clickCount } = beatButton;

      if (clickCount === 1) {
        return;
      }

      this.setState({
        beatButton: {
          ...beatButton,
          clickCount: clickCount - 1,
        },
      });
    };
  }

  componentDidMount() {
    // this._updateTime();
  }

  componentWillUnmount() {
    this._stopUpdatingTime = true;
  }

  shouldComponentUpdate(newProps, newState) {
    return (
      !isEqual(this.props, newProps) || !isEqual(this.state, newState)
    );
  }

  _isBeatButtonInWait() {
    const { lastClicked } = this.state.beatButton;
    return this.state.time - lastClicked < this.props.beatButtonWait;
  }

  render() {
    const { beatButton } = this.state;
    let plusIcon;
    let clickCountText = '';
    let buttonRadius = this.props.width / 10;
    let miniButtonRadius = this.props.width * 2 / 30;
    let distanceFromCenter = buttonRadius * 2.1;

    let buttonStyle = {
      left: this.props.width / 2 - buttonRadius,
      width: buttonRadius * 2,
      height: buttonRadius * 2,
    };
    let centerMiniButtonStyle = {
      left: this.props.width / 2 - miniButtonRadius,
      top: miniButtonRadius / 2,
      width: miniButtonRadius * 2,
      height: miniButtonRadius * 2,
    };
    let beatButtonStyle = {
      ...buttonStyle,
      backgroundColor: 'hsla(255, 61%, 9%, 1)',
    };
    let beatStyle = {
      top: this.props.width / 2 - buttonRadius,
    };

    let beatButtonClasses = 'CircleControls__BeatButton';
    let cancelButtonClasses = 'CircleControls__CancelButton';
    let acceptButtonClasses = 'CircleControls__AcceptButton';
    let decrementButtonClasses = 'CircleControls__DecrementButton';
    let clickCountClasses = 'CircleControls__ClickCount';

    let acceptButtonStyle = {
      ...centerMiniButtonStyle,
    };
    let cancelButtonStyle = {
      ...centerMiniButtonStyle,
    };
    let decrementButtonStyle = {
      ...centerMiniButtonStyle,
    };
    let clickCountStyle = {
      ...centerMiniButtonStyle,
      lineHeight: centerMiniButtonStyle.height + 'px',
    };

    if (beatButton.clickCount > 0 && this._isBeatButtonInWait()) {
      clickCountText = '' + beatButton.clickCount;
      clickCountClasses += ' _isVisible';
      cancelButtonClasses += ' _isVisible';
      acceptButtonClasses += ' _isVisible';

      clickCountStyle.transform = `translate(0, -${distanceFromCenter}px)`;
      cancelButtonStyle.transform = `translate(-${distanceFromCenter}px)`;
      acceptButtonStyle.transform = `translate(${distanceFromCenter}px)`;
    }

    plusIcon = (
      <IconPlus
        color={theme.color.onSurfaceMediumEmphasis}
        height="100%"
        width="45%"
      />
    );

    if (beatButton.clickCount === 0 && this.props.isEmptyState) {
      beatButtonClasses += ' _isPulsing';
    }

    if (beatButton.clickCount > 1) {
      decrementButtonClasses += ' _isVisible';
      decrementButtonStyle.transform = `translate(0, ${distanceFromCenter}px)`;
    }

    let beatButtonTitle = 'Add new subdivision';

    if (beatButton.clickCount) {
      beatButtonTitle = `Increase subdivision to ${beatButton.clickCount +
        1}`;
    }
    return (
      <div className="CircleControls">
        <div className="CircleControls__Beat" style={beatStyle}>
          <button
            title={beatButtonTitle}
            className={beatButtonClasses}
            onClick={this._onBeatButtonClick}
            style={beatButtonStyle}
          >
            {plusIcon}
          </button>

          <button
            title="Do not add the subdivision"
            className={cancelButtonClasses}
            style={cancelButtonStyle}
            onClick={this._cancelBeats}
          >
            <IconCancel
              width="60%"
              height="100%"
              color={theme.color.onPrimary}
            />
          </button>

          <button
            title={`Add subdivision of ${beatButton.clickCount}`}
            className={acceptButtonClasses}
            style={acceptButtonStyle}
            onClick={this._acceptBeat}
          >
            <IconDone
              width="60%"
              height="100%"
              color={theme.color.onPrimary}
            />
          </button>

          <button
            title={`Decrease subdivision to ${beatButton.clickCount -
              1}`}
            className={decrementButtonClasses}
            style={decrementButtonStyle}
            onClick={this._decrementBeat}
          >
            <IconRemove
              width="60%"
              height="100%"
              color={theme.color.onSurfaceMediumEmphasis}
            />
          </button>

          <div className={clickCountClasses} style={clickCountStyle}>
            {clickCountText}
          </div>
        </div>
      </div>
    );
  }
}

CircleControls.defaultProps = {
  beatButtonWait: 700,
};

export default CircleControls;
