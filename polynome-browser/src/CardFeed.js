import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router-dom';

import debounce from 'lodash/debounce';
import sort from './sort';

import theme from './theme';
import { elevationToStyle } from './elevation';
import toPrettyNumber from './toPrettyNumber';

import * as trackUtils from './trackUtils';

import TextWrapper from './TextWrapper';
import ButtonText from './ButtonText';
import ButtonIcon from './ButtonIcon';
import Divider from './Divider';
import Comment from './Comment';

import IconClap from './IconClap';
import IconCommentAdd from './IconCommentAdd';
import IconRemix from './IconRemix';

import IconStop from './IconStop';
import IconPlay from './IconPlay';
import IconMoreVert from './IconMoreVert';

import Userpic from './Userpic';

import LoadingIndicatorCircular from './LoadingIndicatorCircular';

const styles = {
  block: {
    backgroundColor: theme.color.surface,
    borderRadius: theme.shape.borderRadius,
    border: 'none',

    minHeight: 80,
    marginBottom: 16,
    paddingBottom: 8,

    ...elevationToStyle(1),
  },

  __header: {
    height: 72,
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    paddingLeft: 16,
    paddingRight: 16,
  },

  __headerTextArea: {
    display: 'flex',
    flexDirection: 'column',
    marginLeft: 16,
    flexGrow: 1,
  },

  __body: {
    paddingLeft: 16,
    paddingRight: 16,
    marginBottom: 16,
  },

  __actions: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },

  __actionsLeftArea: {
    display: 'flex',
    paddingLeft: 8,
  },

  __actionsRightArea: {
    display: 'flex',
  },

  __socialActionWrapper: {
    display: 'flex',
    alignItems: 'center',
    minWidth: 56,
  },
};

class CardFeed extends Component {
  constructor() {
    super();

    this.state = {
      clapsCount: 0,
    };

    this._showMoreComments = () => {
      const { chopId } = this.props.chop;

      this.props.onChangeCommentsCountToShow(
        chopId,
        this.props.commentsToShow + 10
      );
    };

    this._goToChop = () => {
      const { chop } = this.props;
      const { chopId } = chop;

      if (chop.lines && chop.lines.length) {
        this.props.history.push(`/form/${chopId}`);
        return;
      }

      this.props.history.push(`/feel/${chopId}`);
    };

    this._remix = () => {
      this.props.onRemix({
        personId: this.props.me.personId,
        remixOf: this.props.chop.chopId,
        remixOfChop: this.props.chop,
      });
    };

    this._play = () => {
      this.props.onPlay(this.props.chop);
    };

    this._stop = () => {
      this.props.onStop();
    };

    this._comment = () => {
      this.props.onComment(this.props.chop.chopId);
    };

    this._sendClaps = debounce(() => {
      const { chopId } = this.props.chop;
      const { personId } = this.props.me;
      const clapsCount = this.state.clapsCount;

      this.setState({ clapsCount: 0 });

      this.props.onClap({ chopId, clapsCount, personId });
    }, 500);

    this._clap = () => {
      if (!this.props.myAccount) {
        return;
      }

      this.setState({
        clapsCount: this.state.clapsCount + 1,
      });
      this._sendClaps();
    };
  }

  render() {
    const {
      me,
      chop,
      isPlaying,
      isLoading,
      onTriggerMenu,
    } = this.props;
    const { composedBy, composer } = chop;
    const createdByMe = me && composedBy === me.personId;
    let handle = composer.handle;
    let clapColor = theme.color.onSurfaceMediumEmphasis;

    if (!handle) {
      handle = 'anonymous';
    }

    if (createdByMe) {
      handle = 'me';
    }

    if (chop.hasMyClaps || this.state.clapsCount > 0) {
      clapColor = theme.color.primaryLight;
    }

    const clapsCount = (chop.clapsCount || 0) + this.state.clapsCount;

    let bottomLeftActions;
    let bottomRightAction;
    let menuButton;

    if (onTriggerMenu) {
      menuButton = (
        <ButtonIcon
          styleName="appBar"
          onTouchTap={() => onTriggerMenu(chop)}
        >
          <IconMoreVert
            color={theme.color.onSurfaceMediumEmphasis}
            width={24}
          />
        </ButtonIcon>
      );
    }

    let remixAction;
    if (this.props.myAccount) {
      remixAction = (
        <ButtonIcon
          key={`action-remix`}
          onTouchTap={this._remix}
          title="Remix"
          width={32}
        >
          <IconRemix
            width={20}
            color={theme.color.onSurfaceMediumEmphasis}
          />
        </ButtonIcon>
      );
    }

    bottomLeftActions = [
      <div style={styles.__socialActionWrapper} key={`action-clap`}>
        <ButtonIcon onTouchTap={this._clap} title="Clap" width={32}>
          <IconClap width={20} height={24} color={clapColor} />
        </ButtonIcon>

        <TextWrapper styleName={'subtitle2'} color={clapColor}>
          <strong>{toPrettyNumber(clapsCount)}</strong>
        </TextWrapper>
      </div>,

      <div style={styles.__socialActionWrapper} key={`action-comment`}>
        <ButtonIcon
          key={`action-comment`}
          title="Comment"
          onTouchTap={this._comment}
          width={32}
        >
          <IconCommentAdd
            width={20}
            color={theme.color.onSurfaceMediumEmphasis}
          />
        </ButtonIcon>

        <TextWrapper
          styleName={'subtitle2'}
          color={theme.color.onSurfaceMediumEmphasis}
        >
          <strong>
            {chop.comments ? toPrettyNumber(chop.comments.length) : ` `}
          </strong>
        </TextWrapper>
      </div>,

      remixAction,
    ];

    let beatSignature = sort(chop.beats, trackUtils.reverseBeatsSortFn)
      .map(b => b.beatsCount)
      .join(':');

    if (!isPlaying) {
      bottomRightAction = (
        <ButtonIcon onTouchTap={this._play}>
          <IconPlay width={28} color={theme.color.onSurface} />
        </ButtonIcon>
      );
    }

    if (isPlaying) {
      bottomRightAction = (
        <ButtonIcon onTouchTap={this._stop}>
          <IconStop width={28} color={theme.color.onSurface} />
        </ButtonIcon>
      );
    }

    if (isPlaying && isLoading) {
      bottomRightAction = (
        <ButtonIcon onTouchTap={this._stop}>
          <LoadingIndicatorCircular size={28} />
        </ButtonIcon>
      );
    }

    const visibleComments = chop.comments
      .slice(0, this.props.commentsToShow)
      .map(c => <Comment key={`comment-${c.commentId}`} comment={c} />);

    let showMoreComments;
    if (visibleComments.length < chop.comments.length) {
      showMoreComments = (
        <div>
          <ButtonText onTouchTap={this._showMoreComments}>
            Show more
          </ButtonText>
        </div>
      );
    }

    let divider;
    if (visibleComments.length) {
      divider = <Divider />;
    }

    let remixIcon;
    let remixText = '';

    if (chop.remixOf) {
      remixIcon = (
        <IconRemix color={theme.color.primaryLight} width={16} />
      );
      remixText = (
        <Link
          to={`/form/${chop.remixOf}`}
          style={{
            display: 'flex',
            textDecoration: 'none',
            alignItems: 'center',
          }}
        >
          <span style={{ paddingRight: 4, height: 16 }}>
            {remixIcon}
          </span>

          <TextWrapper
            styleName={'subtitle2'}
            color={theme.color.onSurfaceDisabled}
            onTouchTap={() =>
              this.props.history.push(`/form/${chop.remixOf}`)
            }
          >
            {` remix of `}
            <strong>{`${chop.remixOfChop.name} `}</strong>
            {`by `}
            <strong>
              {`${chop.remixOfChop.composer.handle || 'anonymous'}`}
            </strong>
          </TextWrapper>
        </Link>
      );
    }

    return (
      <div style={styles.block}>
        <div style={styles.__header}>
          <Userpic person={composer} />

          <div style={styles.__headerTextArea}>
            <span style={{ cursor: 'pointer' }}>
              <TextWrapper
                styleName={'subtitle1'}
                color={theme.color.onSurface}
                onTouchTap={this._goToChop}
              >
                {chop.name}
              </TextWrapper>
            </span>

            <TextWrapper
              styleName={'subtitle2'}
              color={theme.color.onSurfaceDisabled}
            >
              {`${beatSignature} by `}
              <strong>{`${handle}`}</strong>
            </TextWrapper>

            {remixText}
          </div>

          {menuButton}
        </div>

        <div style={styles.__actions}>
          <div style={styles.__actionsLeftArea}>
            {bottomLeftActions}
          </div>

          <div style={styles.__actionsRightArea}>
            {bottomRightAction}
          </div>
        </div>

        {divider}
        {visibleComments}
        {showMoreComments}
      </div>
    );
  }
}

export default withRouter(CardFeed);
