import React from 'react';
import isNumber from 'lodash/isNumber';
import { elevationToStyle } from './elevation';
import theme from './theme';
import TextWrapper from './TextWrapper';

const styles = {
  block: {
    backgroundColor: theme.color.primary,
    color: theme.color.onPrimary,
    borderRadius: theme.shape.borderRadius,
    display: 'flex',
    alignItems: 'center',
    height: 36,
    paddingLeft: 16,
    paddingRight: 16,
    border: 'none',
    cursor: 'pointer',
    ...elevationToStyle(2),
  },
};

const ContainedButton = props => {
  let blockStyle = {
    ...styles.block,
  };

  if (props.background) {
    blockStyle = {
      ...blockStyle,
      backgroundColor: props.background,
    };
  }

  if (isNumber(props.elevation)) {
    blockStyle = {
      ...blockStyle,
      ...elevationToStyle(props.elevation),
    };
  }

  if (props.isOutlined) {
    blockStyle = {
      ...blockStyle,
      border: '2px solid #fff',
    };
  }

  const textColor = props.textColor || theme.color.onPrimary;

  return (
    <button
      type={props.type}
      style={blockStyle}
      onClick={props.onTouchTap}
      disabled={props.isDisabled}
    >
      <TextWrapper styleName="button" color={textColor}>
        {props.children}
      </TextWrapper>
    </button>
  );
};

export default ContainedButton;
