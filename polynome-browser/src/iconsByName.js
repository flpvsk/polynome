import IconPlus from './IconPlus';
import IconDone from './IconDone';
import IconAddLine from './IconAddLine';
import IconPerson from './IconPerson';
import IconHome from './IconHome';
import IconNotifications from './IconNotifications';
import IconSettings from './IconSettings';
import IconLogout from './IconLogout';
import IconSubscribe from './IconSubscribe';

const iconsByName = {
  plus: IconPlus,
  done: IconDone,
  'add-line': IconAddLine,
  person: IconPerson,
  home: IconHome,
  notifications: IconNotifications,
  settings: IconSettings,
  subscribe: IconSubscribe,
  logout: IconLogout,
};

export default iconsByName;
