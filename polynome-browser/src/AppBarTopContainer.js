import React, { Component } from 'react';
import theme from './theme';
import TopAppBar from './AppBarTop';
import Menu from './Menu';
import BottomSheet from './SheetBottom';
import Dialog from './Dialog';
import TextWrapper from './TextWrapper';

import viewport from './viewport';

const styles = {
  block: {
    position: 'relative',
    paddingTop: theme.size.appBarTopHeight,
    width: '100%',
    height: '100%',
  },

  __contentArea: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    width: '100%',
    maxWidth: viewport.getWidth(),
    marginLeft: 'auto',
    marginRight: 'auto',
    position: 'relative',
    minHeight: '100%',
    paddingBottom: 32,
  },

  __cover: {
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'hsla(0, 0%, 0%, 0.2)',
    zIndex: 7,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-end',
    overflow: 'hidden',
  },

  __fabArea: {
    position: 'fixed',
    bottom: 16,
    right: 16,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    zIndex: 1,
  },

  __navigation: {
    backgroundColor: theme.color.surface,
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: 7,
  },

  __navigationContent: {
    width: '100%',
    height: '100%',
    maxWidth: viewport.getWidth(),
    marginLeft: 'auto',
    marginRight: 'auto',
  },

  __footer: {
    marginBottom: 16,
    maxWidth: viewport.getWidth(),
    marginLeft: 'auto',
    marginRight: 'auto',
    paddingLeft: 16,
    paddingRight: 16,
  },
};

class TopBarContainer extends Component {
  constructor() {
    super();

    this.state = {
      isMenuVisible: false,
    };

    this._toggleMenu = () => {
      if (!this.state.isMenuVisible && this.props.onShowMenu) {
        this.props.onShowMenu();
      }

      this.setState({
        isMenuVisible: !this.state.isMenuVisible,
      });
    };

    this._hideMenu = () => {
      if (this.state.isMenuVisible) {
        this.setState({ isMenuVisible: false });
      }
    };

    this._dismissBottomSheet = e => {
      if (e) {
        e.preventDefault();
        e.stopPropagation();
      }

      // TODO: timeout is a hack
      // To do it properly, opacity of the background should decrease
      // with the bottom sheet as it goes down (below certain threschold).
      setTimeout(() => {
        this.props.onDismissBottomSheet();
      }, 150);
    };
  }

  componentDidMount() {}

  render() {
    const { props } = this;
    const {
      bottomSheetContent,
      menuContent,
      bottomSheetIcon,
      bottomSheetIconBackgroundColor,
      appBarTitle,
      appBarLeftIcon,
      appBarActions,
      dialogContent,
      dialogHeader,
      dialogActions,
      fabs,
      showNavigation,
      navigationContent,
    } = props;
    const { isMenuVisible } = this.state;
    const showMenuIcon = !!props.menuContent;

    let menu;

    if (isMenuVisible && menuContent) {
      menu = <Menu>{menuContent(this._hideMenu)}</Menu>;
    }

    let bottomSheet;

    if (bottomSheetContent) {
      bottomSheet = (
        <div
          style={styles.__cover}
          onClick={props.onDismissBottomSheet}
        >
          <BottomSheet
            title={props.bottomSheetTitle}
            headerText={props.bottomSheetHeaderText}
            icon={bottomSheetIcon}
            iconBackgroundColor={bottomSheetIconBackgroundColor}
            onDismiss={this._dismissBottomSheet}
          >
            {bottomSheetContent()}
          </BottomSheet>
        </div>
      );
    }

    let dialog;
    if (dialogContent) {
      dialog = (
        <div style={styles.__cover} onClick={props.onDismissDialog}>
          <Dialog header={dialogHeader} actions={dialogActions}>
            {dialogContent()}
          </Dialog>
        </div>
      );
    }

    let fabArea;
    if (fabs && fabs.length) {
      const height =
        fabs.length * theme.size.fabSize + (fabs.length - 1) * 16;
      let rightOffset = 0;

      if (window.innerWidth > viewport.getWidth()) {
        rightOffset = (window.innerWidth - viewport.getWidth()) / 2;
      }

      const fabAreaStyle = {
        ...styles.__fabArea,
        right: styles.__fabArea.right + rightOffset,
        height,
      };

      fabArea = <div style={fabAreaStyle}>{fabs}</div>;
    }

    let navigation;
    if (showNavigation) {
      navigation = (
        <div style={styles.__navigation}>
          <div style={styles.__navigationContent}>
            {navigationContent}
          </div>
        </div>
      );
    }

    let blockStyle = styles.block;
    if (bottomSheetContent) {
      blockStyle = {
        ...blockStyle,
        overflow: 'hidden',
        touchAction: 'none',
      };
    }

    return (
      <div onClick={this._hideMenu} style={blockStyle}>
        <TopAppBar
          title={appBarTitle}
          leftIcon={appBarLeftIcon}
          showMenuIcon={showMenuIcon}
          onMenuClick={this._toggleMenu}
          actions={appBarActions}
        />
        <div style={styles.__contentArea}>{this.props.children}</div>
        {fabArea}
        {menu}
        {bottomSheet}
        {dialog}
        {navigation}
        <footer style={styles.__footer}>
          <div>
            <TextWrapper
              styleName={'subtitle'}
              color={theme.color.onBackgroundDisabled}
            >
              <span role="img" aria-label="copyright">
                {` ©️  `}
              </span>
              {`Polychops`}
            </TextWrapper>
          </div>
          <div>
            <TextWrapper
              styleName={'subtitle'}
              color={theme.color.onBackgroundDisabled}
            >
              Made with
              <span role="img" aria-label="headphones">{` 🎧  `}</span>
              {` in Berlin | `}
            </TextWrapper>
            <a
              style={{ color: theme.color.onBackgroundDisabled }}
              href={'mailto:hey@polychops.com'}
            >
              <TextWrapper
                styleName="subtitle1"
                color={theme.color.onBackgroundDisabled}
              >
                {`Write us`}
              </TextWrapper>
            </a>
          </div>
        </footer>
      </div>
    );
  }
}

export default TopBarContainer;
