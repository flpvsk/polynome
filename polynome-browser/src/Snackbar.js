import React, { Component } from 'react';
import theme from './theme';
import { elevationToStyle } from './elevation';
import viewport from './viewport';

const styles = {
  block: {
    zIndex: -1,
    opacity: 0,

    position: 'fixed',
    bottom: 48 + 2 * theme.size.fabSize,
    width: viewport.getWidth() - 32,
    height: 48,
    paddingLeft: 16,
    paddingRight: 16,

    display: 'flex',
    alignItems: 'center',

    backgroundColor: theme.color.snackbar,

    transition: 'opacity 150ms ease-out',
  },

  _isVisible: {
    opacity: 1,
    ...elevationToStyle(7),
  },
};

class Snackbar extends Component {
  render() {
    const { isVisible } = this.props;

    let blockStyle = {
      ...styles.block,
    };

    if (isVisible) {
      blockStyle = {
        ...blockStyle,
        ...styles._isVisible,
      };
    }

    return <div style={blockStyle}>{this.props.children}</div>;
  }
}

export default Snackbar;
