const MAX_WIDTH = 800;

const exports = {
  getOriginalWidth: () => {
    return window.innerWidth;
  },

  getWidth: () => {
    let width = window.innerWidth;
    if (width > MAX_WIDTH) {
      return MAX_WIDTH;
    }

    return width;
  },

  getHeight: () => {
    return window.innerHeight;
  },
};

export default exports;
