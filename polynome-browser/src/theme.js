const white = '#fff';

export const color = {
  background: '#2F2F37', // '#333',
  onBackground: white,
  onBackgroundMediumEmphasis: 'rgba(255, 255, 255, 0.77)',
  onBackgroundDisabled: 'rgba(255, 255, 255, 0.38)',

  surface: '#37373F',
  surfaceLight: '#61616a',
  surfaceDark: 'rgba(17, 17, 25, 1)',
  surfaceLifted: 'rgba(17, 17, 25, 0.1)',
  onSurface: white,
  onSurfaceLight: white,
  onSurfaceDark: white,
  onSurfaceMediumEmphasis: 'rgba(255, 255, 255, 0.77)',
  onSurfaceDisabled: 'rgba(255, 255, 255, 0.38)',
  containerOnSurface: 'rgba(255, 255, 255, 0.04)',

  primary: '#4527a0',
  primaryLight: '#7953d2',
  primaryDark: '#000070',
  onPrimary: 'white',
  onPrimaryMediumEmphasis: 'rgba(255, 255, 255, 0.60)',
  onPrimaryDisabled: 'rgba(255, 255, 255, 0.38)',

  secondary: '#4527a0', // '#fafafa',
  secondaryLight: '#7953d2',
  secondaryDark: '#000070',
  onSecondary: 'rgba(0, 0, 0, 0.97)',
  onSecondaryMediumEmphasis: 'rgba(0, 0, 0, 0.60)',
  onSecondaryDisabled: 'rgba(0, 0, 0, 0.38)',

  error: '#EB1765',

  snackbar: white,
  onSnackbar: 'rgba(0, 0, 0, 0.60)',

  outline: '#424242',

  white,
};

const borderRadius = 7;
export const shape = {
  borderRadius,
};

export const size = {
  appBarTopHeight: 56,
  fabSize: 56,
};

export default {
  color,
  shape,
  size,
};
