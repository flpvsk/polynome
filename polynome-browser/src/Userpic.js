import React from 'react';
import { Link } from 'react-router-dom';

import theme from './theme';
import userpicAnonymous1 from './userpicAnonymous1.svg';

const styles = {
  block: {
    display: 'flex',
    alignItems: 'center',
  },

  __img: {
    height: 40,
    width: 40,
    borderRadius: '50%',
    border: `1px solid ${theme.color.outline}`,
    objectFit: 'cover',
  },
};

const Userpic = props => {
  const person = props.person;
  let src = userpicAnonymous1;
  let imgStyle = {
    ...styles.__img,
  };

  if (person && person.userpic) {
    src = person.userpic;
  }

  if (props.size) {
    imgStyle = {
      ...imgStyle,
      width: props.size,
      height: props.size,
    };
  }

  let image = (
    <img
      onClick={props.onTouchTap}
      style={imgStyle}
      alt={`userpic`}
      src={src}
    />
  );

  if (person) {
    return (
      <Link style={styles.block} to={`/people/${person.personId}`}>
        {image}
      </Link>
    );
  }

  return image;
};

export default Userpic;
