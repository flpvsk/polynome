import React from 'react';

const IconRecord = ({ width, height, color }) => {
  return (
    <svg width={width} height={height} viewBox="0 0 24 24">
      <path fill="none" d="M24 24H0V0h24v24z" />
      <circle fill={color} cx="12" cy="12" r="8" />
    </svg>
  );
};

export default IconRecord;
