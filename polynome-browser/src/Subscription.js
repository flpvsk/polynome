import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

import AppBarTopContainer from './AppBarTopContainer';
import TextWrapper from './TextWrapper';
import InputText from './InputText';
import ButtonIcon from './ButtonIcon';
import ButtonText from './ButtonText';
import ButtonContained from './ButtonContained';
import Card from './Card';
import ListItem from './ListItem';
import Divider from './Divider';
import LoadingIndicatorCircular from './LoadingIndicatorCircular';

import IconBack from './IconBack';

import { Elements as StripeElements } from 'react-stripe-elements';
import FormPayment from './FormPayment';
import withStripe from './withStripe';

import withMyAccountProvider from './data/withMyAccountProvider';
import withDiscountProvider from './data/withDiscountProvider';
import withPlansProvider from './data/withPlansProvider';
import withCancelSubscriptionProvider from './data/withCancelSubscriptionProvider';
import { FREE_PLAN, getPlanFromSubscription } from './data/plans';

import theme from './theme';
import { elevationToStyle } from './elevation';
import viewport from './viewport';

import thankYouGif1 from './SubscriptionThankYou1.gif';
import thankYouGif2 from './SubscriptionThankYou2.gif';
import thankYouGif3 from './SubscriptionThankYou3.gif';

const thankYouGifs = [thankYouGif1, thankYouGif2, thankYouGif3];

const styles = {
  block: {
    width: '100%',
  },

  _padded: {
    paddingLeft: 16,
    paddingRight: 16,
  },

  __subheader: {
    width: '100%',
    height: 48,
    paddingLeft: 16,
    paddingRight: 16,
    display: 'flex',
    alignItems: 'center',
  },

  __body: {
    paddingLeft: 16,
    paddingRight: 16,
    width: '100%',
  },

  __plansList: {
    display: 'flex',
    paddingLeft: 16,
    justifyContent: 'space-between',
    flexWrap: 'wrap',
  },

  __checkout: {},

  _inColumns: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
  },

  __planWrapper: {
    flexBasis: 256,
    flexGrow: 0,
    flexShrink: 1,
    borderRight: `16px solid transparent`,
    borderBottom: `16px solid transparent`,
  },

  __planFeatureList: {
    marginTop: 16,
    marginBottom: 0,
    padding: 0,
    color: theme.color.primaryLight,
  },

  __planFeatureItem: {
    marginBottom: '8px',
  },

  __checkoutForm: {
    marginTop: 16,
    paddingBottom: 16,
  },

  __checkoutFormItem: {
    height: 56,
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    marginBottom: 8,
  },

  __thankYou: {
    display: 'flex',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
  },

  __thankYouImage: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
    flexBasis: 300,
    flexGrow: 0,
    paddingLeft: 16,
  },

  __gif: {
    marginTop: 16,
    width: '100%',
    borderRadius: theme.shape.borderRadius,
    ...elevationToStyle(4),
  },

  __thankYouText: {
    paddingTop: 16,
    paddingLeft: 16,
    paddingRight: 16,
    flexBasis: 300,
    flexGrow: 1,
    flexShrink: 0,
    display: 'flex',
    alignContent: 'center',
  },

  __disclaimer: {
    marginLeft: 16,
    marginRight: 16,
    marginBottom: 32,
    marginTop: 0,
  },

  __disclaimerLine: {
    margin: 0,
    padding: 0,
    marginTop: 8,
  },

  __stripeError: {
    paddingLeft: 16,
    paddingRight: 16,
    maxWidth: 600,
  },

  __stripeErrorHeader: {},

  __stripeErrorText: {
    marginTop: 8,
  },

  __stripeErrorActions: {
    display: 'flex',
  },
};

function formatPrice(priceObj) {
  if (!priceObj) {
    return '';
  }

  if (priceObj.currencyCode === 'USD') {
    return `${priceObj.amount}${priceObj.currencySymbol}`;
  }

  return `${priceObj.currencySymbol}${priceObj.amount}`;
}

const Plan = props => {
  const styles = {
    __content: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      paddingBottom: 32,
      minHeight: 8 * 42,
      maxWidth: 6 * 40,
      minWidth: 6 * 30,
      margin: 'auto',
    },

    __name: {
      paddingBottom: 16,
      textAlign: 'center',
    },

    __price: {
      textAlign: 'center',
      whiteSpace: 'pre-line',
    },

    __strikeOutPrice: {
      textAlign: 'center',
      height: 36,
      textDecoration: 'line-through',
    },

    __description: {
      paddingBottom: 8,
      textAlign: 'center',
      whiteSpace: 'pre-line',
      flexGrow: 1,
    },

    __status: {
      justifySelf: 'flex-end',
    },
  };

  const { isActive, canSelect } = props;
  let elevation = isActive ? 1 : 12;
  let backgroundColor = theme.color.surface;
  let status;

  if (isActive) {
    backgroundColor = theme.color.surfaceLifted;
    status = (
      <div style={styles.__status}>
        <ButtonText textColor={theme.color.onSurfaceDisabled}>
          your current plan
        </ButtonText>
      </div>
    );
  }

  if (!isActive && canSelect) {
    status = (
      <div style={styles.__status}>
        <ButtonContained>Select</ButtonContained>
      </div>
    );
  }

  const priceStr = props.price
    ? formatPrice(props.price)
    : 'free forever';
  const strikeOutPriceStr = formatPrice(props.strikeOutPrice);

  return (
    <Card
      backgroundColor={backgroundColor}
      elevation={elevation}
      onTouchTap={props.onSelect}
    >
      <div style={styles.__content}>
        <div style={styles.__name}>
          <TextWrapper styleName="h4" color={theme.color.onSurface}>
            {props.name}
          </TextWrapper>
        </div>

        <div style={styles.__price}>
          <TextWrapper styleName="h6" color={theme.color.onSurface}>
            {priceStr}
          </TextWrapper>

          <TextWrapper
            styleName="subtitle2"
            color={theme.color.onSurface}
          >
            {` ${props.frequencyAdverb || ''}`}
          </TextWrapper>
        </div>

        <div style={styles.__strikeOutPrice}>
          <TextWrapper
            styleName="subtitle2"
            color={theme.color.onSurface}
          >
            {strikeOutPriceStr}
          </TextWrapper>
        </div>

        <div style={styles.__description}>
          <TextWrapper
            styleName="body1"
            color={theme.color.onSurfaceDisabled}
          >
            {props.description}
          </TextWrapper>
        </div>

        {status}
      </div>
    </Card>
  );
};

class Subscription extends Component {
  constructor() {
    super();
    this.state = {
      selectedPlanId: null,
      showThankYouScreen: false,
      showConfirmCancelSubscription: false,
      discountInputValue: '',
    };

    this._showCancelConfirmation = () => {
      this.setState({
        showConfirmCancelSubscription: true,
      });
    };

    this._hideCancelConfirmation = () => {
      this.setState({
        showConfirmCancelSubscription: false,
      });
    };

    this._selectPlan = planId => {
      if (planId === 'undecided') {
        throw new Error('Not implemented');
      }

      this.setState({
        selectedPlanId: planId,
      });
    };

    this._cancelSelectPlan = () =>
      this.setState({
        selectedPlanId: null,
      });

    this._reload = () => {
      window.location.reload();
    };

    this._goBack = () => {
      if (this.state.showThankYouScreen) {
        this.setState({
          selectedPlanId: null,
          showThankYouScreen: false,
        });
        this.props.history.push(`/`);
      }

      if (this.state.selectedPlanId) {
        this.setState({ selectedPlanId: null });
        return;
      }

      this.props.history.goBack();
    };

    this._showThankYouScreen = () => {
      this.setState({
        showThankYouScreen: true,
      });
    };

    this._showDiscountInput = () => {
      this.setState({ showDiscountInput: true });
    };

    this._applyDiscount = e => {
      e.preventDefault();
      this.setState({ showDiscountInput: false });
      this.props.getDiscount({
        planId: this.props.planId,
        personId: this.props.myAccount.personId,
        discountCode: this.state.discountInputValue,
      });
    };

    this._setDiscountInputValue = value => {
      this.setState({
        discountInputValue: value,
      });
    };
  }

  _renderPlansList() {
    let plansListStyle = {
      ...styles.__plansList,
    };

    if (viewport.getWidth() < (8 * 30 + 16) * 3 + 32) {
      plansListStyle = {
        ...plansListStyle,
        ...styles._inColumns,
      };
    }

    const { activeSubscription } = this.props.myAccount;
    const plans = [FREE_PLAN].concat(this.props.plans);
    const activePlan = getPlanFromSubscription(activeSubscription);
    const hasSubscription = !!activePlan.planId;
    const hasCancelledSubscription =
      activeSubscription && activeSubscription.isCancelled;

    let disclaimer;

    const textProps = {
      styleName: 'body1',
      color: theme.color.onSurface,
    };

    if (hasSubscription) {
      disclaimer = (
        <div key={'disclaimer'} style={styles.__disclaimer}>
          <div style={styles.__disclaimerLine}>
            <TextWrapper {...textProps} styleName={'h5'}>
              {`Thank you for supporting the project!`}
            </TextWrapper>
          </div>
          <p style={styles.__disclaimerLine}>
            <TextWrapper {...textProps}>
              {`You are currently subscribed to the `}
            </TextWrapper>
            <TextWrapper {...textProps}>
              <strong>{activePlan.name}</strong>
            </TextWrapper>
            <TextWrapper {...textProps}>{` plan. `}</TextWrapper>
            <TextWrapper {...textProps}>
              {`Please drop us an email if you'd like to `}
            </TextWrapper>
            <a
              style={{ color: theme.color.primaryLight }}
              href={`mailto:hey@polychops.com`}
            >
              <TextWrapper
                color={theme.color.primaryLight}
                styleName="body1"
              >
                {`change the plan`}
              </TextWrapper>
            </a>
            <TextWrapper {...textProps}>{`. `}</TextWrapper>
            <TextWrapper {...textProps}>{`Click here to `}</TextWrapper>
            <TextWrapper
              styleName="link"
              onTouchTap={this._showCancelConfirmation}
              color={theme.color.primaryLight}
            >
              {`cancel your subscription`}
            </TextWrapper>
            <TextWrapper {...textProps}>{`.`}</TextWrapper>
          </p>
        </div>
      );
    }

    if (hasCancelledSubscription) {
      const validUntilStr = new Date(
        activeSubscription.validUntil
      ).toDateString();

      disclaimer = (
        <div key={'disclaimer'} style={styles.__disclaimer}>
          <div style={styles.__disclaimerLine}>
            <TextWrapper {...textProps} styleName={'h5'}>
              {`Your subscription will be cancelled on ${validUntilStr}.`}
            </TextWrapper>
          </div>
          <div style={styles.__disclaimerLine}>
            <TextWrapper {...textProps} styleName={'body1'}>
              {`If you have any questions please `}
            </TextWrapper>
            <a
              style={{ color: theme.color.primaryLight }}
              href={`mailto:hey@polychops.com`}
            >
              <TextWrapper
                color={theme.color.primaryLight}
                styleName="body1"
              >
                {`write us an email`}
              </TextWrapper>
            </a>
            <TextWrapper
              color={theme.color.primaryLight}
              styleName="body1"
            >
              {`.`}
            </TextWrapper>
          </div>
        </div>
      );
    }

    if (!hasSubscription) {
      disclaimer = (
        <div key={'disclaimer'} style={styles.__disclaimer}>
          <div style={styles.__disclaimerLine}>
            <TextWrapper {...textProps} styleName={'h5'}>
              {`Support the project`}
            </TextWrapper>
          </div>
          <div style={styles.__disclaimerLine}>
            <TextWrapper {...textProps} styleName={'body1'}>
              {`We are proud with what we have achieved with Polychops ` +
                `so far, but this is just the beginning! ` +
                `Subscribe and help us  ` +
                `make a fun place where people can practice ` +
                `and share music!`}
            </TextWrapper>
          </div>
          <div style={styles.__disclaimerLine}>
            <TextWrapper {...textProps} styleName={'body1'}>
              {`If you have any questions please `}
            </TextWrapper>
            <a
              style={{ color: theme.color.primaryLight }}
              href={`mailto:hey@polychops.com`}
            >
              <TextWrapper
                color={theme.color.primaryLight}
                styleName="body1"
              >
                {`reach out`}
              </TextWrapper>
            </a>
            <TextWrapper {...textProps}>{`.`}</TextWrapper>
          </div>
        </div>
      );
    }

    return [
      disclaimer,
      <div key={'plans-list'} style={plansListStyle}>
        {plans.map(p => (
          <div key={`plan-${p.planId}`} style={styles.__planWrapper}>
            <Plan
              onSelect={() => this._selectPlan(p.planId)}
              canSelect={!hasSubscription}
              isActive={p.planId === activePlan.planId}
              price={p.price}
              strikeOutPrice={p.strikeOutPrice}
              frequencyAdverb={p.frequencyAdverb}
              name={p.name}
              description={p.description}
            />
          </div>
        ))}
      </div>,
    ];
  }

  _renderCheckout(plan) {
    if (this.props.didStripeFail) {
      return (
        <div style={styles.__stripeError}>
          <div style={styles.__stripeErrorHeader}>
            <TextWrapper
              key="error-header"
              styleName={'h6'}
              onTouchTap={this._showDiscountInput}
            >
              {`Something went wrong and it's our fault 😢`}
            </TextWrapper>
          </div>

          <div style={styles.__stripeErrorText}>
            {TextWrapper.body1OnBackground(
              `We rely on Stripe as our payments service provider. ` +
                `Unfortunatelly we weren't able to reach Stripe. `
            )}
          </div>

          <div style={styles.__stripeErrorText}>
            {TextWrapper.body1OnBackground(`How we can try to fix it:`)}
          </div>

          <ul style={{ marginLeft: 16, padding: 0 }}>
            <li>
              {TextWrapper.body1OnBackground(
                `Make sure you have a working internet connection ` +
                  `and try reloading the page;`
              )}
            </li>
            <li>
              {TextWrapper.body1OnBackground(
                'If you use ad blockers, try disabling them;'
              )}
            </li>
            <li>
              {TextWrapper.body1OnBackground(
                'If none of that worked, write us: '
              )}
              {TextWrapper.link(
                'hello@polychops.com',
                'mailto:hellp@polychops.com'
              )}
              {TextWrapper.body1OnBackground('.')}
            </li>
          </ul>

          <div style={styles.__stripeErrorActions}>
            <ButtonContained onTouchTap={this._goBack}>
              Go back
            </ButtonContained>

            <ButtonText onTouchTap={this._reload}>Reload</ButtonText>
          </div>
        </div>
      );
    }

    let discountArea = (
      <ListItem noPadding>
        <TextWrapper
          styleName={'link'}
          onTouchTap={this._showDiscountInput}
          color={theme.color.primaryLight}
        >
          {`I have a discount!`}
        </TextWrapper>
      </ListItem>
    );

    if (this.props.discountError) {
      discountArea = (
        <ListItem noPadding>
          <TextWrapper styleName={'body1'} color={theme.color.error}>
            {this.props.discountError}
          </TextWrapper>
          <ButtonText onTouchTap={this._showDiscountInput}>
            Retry
          </ButtonText>
        </ListItem>
      );
    }

    if (this.props.isDiscountLoading) {
      discountArea = (
        <ListItem noPadding>
          <LoadingIndicatorCircular />
        </ListItem>
      );
    }

    if (this.props.discount && !this.state.showDiscountInput) {
      discountArea = (
        <ListItem noPadding>
          <TextWrapper
            styleName={'body1'}
            color={theme.color.onSurface}
          >
            <b>Discount: </b>
            {this.props.discount.description}
          </TextWrapper>
          <ButtonText onTouchTap={this._showDiscountInput}>
            Change
          </ButtonText>
        </ListItem>
      );
    }

    if (this.state.showDiscountInput) {
      discountArea = (
        <ListItem noPadding>
          <form
            style={{ width: '100%', display: 'flex', height: '100%' }}
            onSubmit={this._applyDiscount}
          >
            <div style={{ flexBasis: 300, flexGrow: 1 }}>
              <InputText
                placeholder={`Discount code`}
                value={this.state.discountInputValue}
                onChange={this._setDiscountInputValue}
              />
            </div>
            <div style={{ flexGrow: 0 }}>
              <ButtonText>Apply discount</ButtonText>
            </div>
          </form>
        </ListItem>
      );
    }

    return (
      <div style={styles.__checkout}>
        <div style={styles.__subheader}>
          <TextWrapper
            styleName={'h5'}
            color={theme.color.onBackground}
          >
            {`${plan.name} plan: ${formatPrice(plan.price)} per ${
              plan.frequencyNoun
            }`}
          </TextWrapper>
        </div>

        <div style={styles.__body}>
          <ul style={styles.__planFeatureList}>
            {plan.features.map((f, i) => (
              <li
                key={`plan-${plan.planId}-feature-${i}`}
                style={styles.__planFeatureItem}
              >
                <TextWrapper
                  styleName="body1"
                  color={theme.color.onBackground}
                >
                  {f}
                </TextWrapper>
              </li>
            ))}
          </ul>

          {discountArea}
        </div>

        <Divider />

        <div style={styles.__body}>
          <div style={styles.__checkoutForm}>
            <StripeElements>
              <FormPayment
                myAccount={this.props.myAccount}
                planId={plan.planId}
                discountCode={this.state.discountInputValue}
                onSuccess={this._showThankYouScreen}
              />
            </StripeElements>
          </div>
        </div>
      </div>
    );
  }

  _renderThankYou(selectedPlan) {
    return (
      <div style={styles.__thankYou}>
        <div style={styles.__thankYouImage}>
          <img
            style={styles.__gif}
            src={
              thankYouGifs[
                Math.floor(Math.random() * 10) % thankYouGifs.length
              ]
            }
            alt={`We're celebrating!`}
          />
        </div>

        <div style={styles.__thankYouText}>
          <div>
            <TextWrapper
              styleName="h3"
              color={theme.color.onBackground}
            >
              {`You have subscribed to the `}
            </TextWrapper>
            <TextWrapper
              styleName="h3"
              color={theme.color.primaryLight}
            >
              {selectedPlan.name}
            </TextWrapper>
            <TextWrapper
              styleName="h3"
              color={theme.color.onBackground}
            >
              {` plan. Thank you!`}
            </TextWrapper>
          </div>
        </div>
      </div>
    );
  }

  render() {
    if (this.props.isMyAccountLoading || this.props.arePlansLoading) {
      return <LoadingIndicatorCircular />;
    }

    const {
      selectedPlanId,
      showThankYouScreen,
      showConfirmCancelSubscription,
    } = this.state;
    const { myAccount } = this.props;
    const plans = [FREE_PLAN].concat(this.props.plans);
    let selectedPlan;

    if (selectedPlanId) {
      selectedPlan = plans.find(p => p.planId === selectedPlanId);
    }

    let appBarTitle = 'Choose subscription';
    let content;

    if (selectedPlan && !showThankYouScreen) {
      appBarTitle = 'Checkout';
      content = this._renderCheckout(selectedPlan);
    }

    if (selectedPlan && showThankYouScreen) {
      appBarTitle = 'Welcome to the family';
      content = this._renderThankYou(selectedPlan);
    }

    if (!selectedPlan) {
      content = this._renderPlansList();
    }

    let dialogContent;
    let dialogHeader;
    let dialogActions;

    let isSubscriptionCancelled =
      myAccount &&
      myAccount.activeSubscription &&
      myAccount.activeSubscription.isCancelled;

    if (showConfirmCancelSubscription && !isSubscriptionCancelled) {
      const {
        isCancelSubscriptionLoading,
        cancelSubscriptionError,
      } = this.props;

      dialogContent = () => [
        <div key="dialog-error">
          <TextWrapper
            key="dialog-text"
            styleName="body1"
            color={theme.color.error}
          >
            {cancelSubscriptionError}
          </TextWrapper>
        </div>,
        <div key="dialog-tip">
          <TextWrapper
            key="dialog-text"
            styleName="body1"
            color={theme.color.onSurface}
          >
            {`Are you sure you'd like to cancel your subscription?`}
          </TextWrapper>
        </div>,
      ];

      dialogActions = [
        <ButtonText
          key="dialog-dismiss"
          onTouchTap={this._hideCancelConfirmation}
          textColor={theme.color.onSurfaceMediumEmphasis}
        >
          nevermind
        </ButtonText>,
        <ButtonText
          key="dialog-cancel-subscription"
          color={theme.color.primaryLight}
          onTouchTap={this.props.onCancelSubscription}
        >
          Cancel subscription
        </ButtonText>,
      ];

      if (isCancelSubscriptionLoading) {
        dialogActions = [<LoadingIndicatorCircular key="loading" />];
      }

      dialogHeader = `Cancel subscription?`;
    }

    return (
      <AppBarTopContainer
        showNavigation={this.props.showNavigation}
        navigationContent={this.props.navigationContent}
        dialogHeader={dialogHeader}
        dialogActions={dialogActions}
        dialogContent={dialogContent}
        appBarTitle={
          <TextWrapper
            styleName={'topBarTitle'}
            color={theme.color.onSurfaceMediumEmphasis}
          >
            {appBarTitle}
          </TextWrapper>
        }
        appBarLeftIcon={
          <ButtonIcon onTouchTap={() => this._goBack()}>
            <IconBack
              width="24"
              height="24"
              color={theme.color.onBackgroundMediumEmphasis}
            />
          </ButtonIcon>
        }
        appBarActions={[]}
      >
        <div style={styles.block}>{content}</div>
      </AppBarTopContainer>
    );
  }
}

export default withStripe(
  withRouter(
    withMyAccountProvider(
      withPlansProvider(
        withCancelSubscriptionProvider(
          withDiscountProvider(Subscription)
        )
      )
    )
  )
);
