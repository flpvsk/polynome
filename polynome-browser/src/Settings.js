import React, { Component } from 'react';
import isEqual from 'lodash/isEqual';
import IconCancel from './IconCancel';
import IconDone from './IconDone';
import IconDelete from './IconDelete';
import IconPlus from './IconPlus';

import './Settings.css';

class Settings extends Component {
  constructor() {
    super();
    this.state = {
      isSavingBeat: false,
      saveBeatName: '',
    };

    this._startSaveBeat = () => {
      this.setState({
        isSavingBeat: true,
        saveBeatName: '',
      });
    };

    this._cancelSaveBeat = e => {
      e.preventDefault();
      this.setState({
        isSavingBeat: false,
        saveBeatName: '',
      });
    };

    this._changeSaveBeatName = e => {
      this.setState({
        saveBeatName: e.target.value,
      });
    };

    this._saveBeat = e => {
      e.preventDefault();
      this.props.onSaveBeat(this.state.saveBeatName);
      this.setState({
        saveBeatName: '',
        isSavingBeat: false,
      });
    };
  }

  render() {
    const {
      activeSoundType,
      activeKitId,
      beats,
      commonBeatsTempo,
      savedBeats,
    } = this.props;

    let oscilatorClassName = 'Settings__ListItem';

    if (this.props.activeSoundType === 'oscilator') {
      oscilatorClassName += ' _isActive';
    }

    const soundTypesList = [
      <li className={oscilatorClassName} key="sound-type-oscilator">
        <button
          className="Settings__SoundTypeName"
          onClick={() => this.props.onChangeSoundType('oscilator')}
        >
          Pitch
        </button>
      </li>,
    ];

    const savedBeatId = savedBeats.reduce((acc, beatInfo) => {
      if (
        beatInfo.commonBeatsTempo === commonBeatsTempo &&
        isEqual(beatInfo.beats, beats)
      ) {
        return beatInfo.id;
      }

      return acc;
    }, null);

    const savedBeatsList = savedBeats.map(beatInfo => {
      let className = 'Settings__ListItem';
      if (beatInfo.id === savedBeatId) {
        className += ' _isActive';
      }

      return (
        <li className={className} key={`saved-beat-${beatInfo.id}`}>
          <button
            className="Settings__SoundTypeName"
            onClick={() => this.props.onLoadBeat(beatInfo.id)}
          >
            {beatInfo.name}
          </button>
          <button
            className="Settings__ListItem__Action"
            onClick={() => this.props.onRemoveBeat(beatInfo.id)}
          >
            <IconDelete width="80%" height="80%" color="#777" />
          </button>
        </li>
      );
    });

    let saveBeatButton;

    if (!savedBeatId && !this.state.isSavingBeat) {
      saveBeatButton = (
        <button
          className="Settings__SubHeaderAction"
          onClick={this._startSaveBeat}
        >
          <IconPlus color="#777" width="70%" height="70%" />
        </button>
      );
    }

    let saveInputBlock;

    if (this.state.isSavingBeat) {
      saveInputBlock = (
        <form
          className="Settings__BeatNameBlock"
          onSubmit={this._saveBeat}
        >
          <button style={{ display: 'none' }} />
          <input
            autoFocus={true}
            className="Settings__BeatNameInput"
            placeholder="Beat name"
            value={this.state.saveBeatName}
            onChange={this._changeSaveBeatName}
          />
          <button
            className="Settings__BeatNameAction"
            onClick={this._cancelSaveBeat}
          >
            <IconCancel color="#777" width="70%" height="100%" />
          </button>
          <button
            type="submit"
            className="Settings__BeatNameAction"
            onClick={this._saveBeat}
          >
            <IconDone color="#777" width="50%" height="100%" />
          </button>
        </form>
      );
    }

    for (let kit of this.props.kits) {
      let className = 'Settings__ListItem';

      if (activeSoundType === 'kit' && activeKitId === kit.id) {
        className += ' _isActive';
      }

      soundTypesList.push(
        <li className={className} key={`sound-type-kit-${kit.id}`}>
          <button
            className="Settings__SoundTypeName"
            onClick={() => this.props.onChangeSoundType('kit', kit.id)}
          >
            {kit.name}
          </button>
        </li>
      );
    }

    return (
      <div style={{ width: this.props.width }} className="Settings">
        <div className="Settings__Header">
          <h1 className="Settings__HeaderText">Config</h1>
          <button
            className="Settings__CloseButton"
            onClick={this.props.onCloseSettings}
          >
            <IconCancel color="#eee" width="100%" height="100%" />
          </button>
        </div>

        <section className="Settings__Section">
          <h2 className="Settings__SubHeaderText">Sound</h2>
          <ul className="Settings__SoundTypesList">{soundTypesList}</ul>
        </section>

        <section className="Settings__Section">
          <header className="Settings__SubHeaderBlock">
            <h2 className="Settings__SubHeaderText">Saved Rhythms</h2>
            {saveBeatButton}
          </header>

          {saveInputBlock}

          <ul className="Settings__SoundTypesList">{savedBeatsList}</ul>
        </section>
      </div>
    );
  }
}

export default Settings;
