import React from 'react';
import theme from './theme';
import TextWrapper from './TextWrapper';

const styles = {
  block: {
    height: 48,
    width: '100%',
    paddingLeft: 16 + 24 + 20,
    paddingRight: 16,
    display: 'flex',
    alignItems: 'center',
    alignContent: 'center',

    background: 'none',
    border: 'none',
  },
};

const MenuHeader = props => {
  return (
    <div style={styles.block}>
      <TextWrapper
        styleName={'subtitle2'}
        color={theme.color.onSurfaceDisabled}
      >
        {props.text}
      </TextWrapper>
    </div>
  );
};

export default MenuHeader;
