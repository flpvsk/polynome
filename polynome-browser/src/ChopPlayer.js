import React, { Component } from 'react';
import { connect } from 'react-redux';

import { beatsToAudio, linesToAudio } from './trackUtils';

import AudioLoader from './AudioLoader';
import AudioPlayer from './AudioPlayer';

import * as kits from './data/kits';

function hasChopKitAudio(chop) {
  return chop.beats.reduce((a, b) => {
    return a || b.sound.type === 'kit';
  }, false);
}

class ChopPlayer extends Component {
  constructor() {
    super();

    this._startLoad = () => {
      if (this.props.onStartLoad) {
        this.props.onStartLoad(this.props.chop.chopId);
      }
    };

    this._finishLoad = () => {
      if (this.props.onFinishLoad) {
        this.props.onFinishLoad(this.props.chop.chopId);
      }
    };
  }

  componentDidMount() {
    const hasKitAudio = hasChopKitAudio(this.props.chop);

    if (hasKitAudio && !this.props.kits.length) {
      this._startLoad();
      this.props.listKits();
    }
  }

  render() {
    const { audioContext } = this.props;

    if (this.props.startTime === null) {
      return null;
    }

    if (!this.props.chop) {
      return null;
    }

    if (!this.props.kits.length && hasChopKitAudio(this.props.chop)) {
      return null;
    }

    let startTime = Math.max(
      audioContext.currentTime,
      this.props.startTime
    );

    const { chop, kits } = this.props;
    const audioBeats = beatsToAudio({
      ...chop,
      isPaused: false,
    });
    const audioLines = linesToAudio({
      ...chop,
      isPaused: false,
      isRecording: false,
    });

    return (
      <AudioLoader
        recordings={chop.recordings}
        kits={kits}
        beats={chop.beats}
        lines={chop.lines}
        onStartLoad={this._startLoad}
        onFinishLoad={this._finishLoad}
      >
        {({ recordingByLineId, beatsAudio, recordingsAudio }) => (
          <AudioPlayer
            key="sounds-audio-player"
            startTime={startTime}
            beats={audioBeats}
            lines={audioLines}
            commonBeatsCount={chop.commonBeatsCount}
            commonBeatsTempo={chop.commonBeatsTempo}
            recordingByLineId={recordingByLineId}
            beatsAudio={beatsAudio}
            recordingsAudio={recordingsAudio}
            audioContext={audioContext}
          />
        )}
      </AudioLoader>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    kits: kits.select.listKits(state),
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    listKits() {
      kits.run.listKits(null, dispatch);
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ChopPlayer);
