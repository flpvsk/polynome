import { color, lightness } from 'kewler';

const DELTA = 5;
const lighter = lightness(DELTA);
const darker = lightness(-DELTA);

/*
const violet = color('#5A1751');
const blue = color('#0F77A0');
const green = color('#2F8E3C');
const yellow = color('#FADA4D');
const orange = color('#F1472A');
const red = color('#E50A1B');
*/

/*
const colors = [
  undefined,
  violet(lighter)(),
  blue(lighter)(),
  green(lighter)(),
  yellow(lighter)(),
  orange(lighter)(),
  red(lighter),
  violet(),
  blue(),
  green(),
  yellow(),
  orange(),
  red(),
  violet(darker)(),
  blue(darker)(),
  green(darker)(),
  yellow(darker)(),
  orange(darker)(),
  red(darker)()
];
*/

const violet = color('#AD2BAD');
const blue = color('#2F2BAD');
// const green = color('#F7DB15');
const yellow = color('#F7DB15');
const orange = color('#E42692');
const red = color('#F71568');

const colors = [
  undefined,
  violet(),
  blue(),
  yellow(),
  orange(),
  red(),
  violet(lighter)(),
  blue(lighter)(),
  yellow(lighter)(),
  orange(lighter)(),
  red(lighter),
  violet(darker)(),
  blue(darker)(),
  yellow(darker)(),
  orange(darker)(),
  red(darker)(),
];

export default function freqToColor(freq) {
  if (freq > 19) {
    return '#555';
  }

  if (freq >= colors.length) {
    const darker = DELTA * (colors.length - freq - 1);
    return red(lightness(darker))();
  }

  return colors[freq];
}
