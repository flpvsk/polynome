import React from 'react';
import ReactDOM from 'react-dom';

import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import promiseMiddleware from 'redux-promise-middleware';

import rootReducer, { DEFAULT_STATE } from './data/reducers';

import * as people from './data/people';
import * as alerts from './data/alerts';

import { ApolloProvider } from 'react-apollo';
import { client, GET_MYSELF } from './data/GqlServer';

import storage from './data/storage';
import createChopMiddleware from './data/saveChopMiddleware';

import './index.css';
import App from './App';
// import Demo from './Demo';
import registerServiceWorker from './registerServiceWorker';

const composeStoreWithMiddleware = applyMiddleware(
  promiseMiddleware(),
  createChopMiddleware(storage.putChop)
)(createStore);

let store;

if (process.env.NODE_ENV === 'development') {
  store = composeStoreWithMiddleware(
    rootReducer,
    DEFAULT_STATE,
    process.env.NODE_ENV === 'development' &&
      window.__REDUX_DEVTOOLS_EXTENSION__ &&
      window.__REDUX_DEVTOOLS_EXTENSION__()
  );
  window.store = store;
}

if (process.env.NODE_ENV !== 'development') {
  store = composeStoreWithMiddleware(rootReducer, DEFAULT_STATE);
}

ReactDOM.render(
  <ApolloProvider client={client}>
    <Provider store={store}>
      <App />
    </Provider>
  </ApolloProvider>,
  document.getElementById('root')
);

client
  .watchQuery({
    query: GET_MYSELF,
    fetchPolicy: 'cache-only',
  })
  .subscribe(update => {
    if (update && update.data && update.data.myself) {
      let person = update.data.myself;
      people.run.getMyselfFulfilled(person, store.dispatch);
    }
  });

const alertUpdatesAvailable = () => {
  alerts.run.alertUpdatesAvailable({}, store.dispatch);
};

registerServiceWorker({
  alertUpdatesAvailable,
});
