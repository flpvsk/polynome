import React from 'react';

const CancelIcon = ({ width, height, color }) => {
  return (
    <svg width={width} height={height} viewBox="0 0 24 24">
      <polygon
        fill={color}
        points="17 15.59 15.59 17 12 13.41 8.41 17 7 15.59 10.59 12 7 8.41 8.41 7 12 10.59 15.59 7 17 8.41 13.41 12"
      />
    </svg>
  );
};

export default CancelIcon;
