import React from 'react';
import MDSpinner from 'react-md-spinner';
import theme from './theme';

const LoadingIndicatorCircular = props => {
  return <MDSpinner singleColor={theme.color.primaryLight} />;
};

export default LoadingIndicatorCircular;
