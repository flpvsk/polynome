import React from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import withLogoutProvider from './data/withLogoutProvider';
import withMyAccountProvider from './data/withMyAccountProvider';

import theme from './theme';

import ButtonIcon from './ButtonIcon';
import IconClose from './IconClose';

import MenuItem from './MenuItem';

import * as people from './data/people';
import LogoText from './LogoText';

const styles = {
  block: {
    height: '100%',
    backgroundColor: theme.color.surface,
  },

  __header: {
    height: theme.size.appBarTopHeight,
    display: 'flex',
    alignItems: 'center',
    borderBottom: `1px solid ${theme.color.outline}`,
    marginBottom: 8,
    paddingLeft: 16,
    paddingRight: 16,
  },
};

const NavigationContent = props => {
  const profilePath = `/people/${props.me.personId}`;
  const isHomeSelected = props.location.pathname === '/';
  const isProfileSelected = props.location.pathname === profilePath;
  const isNotificationsSelected =
    props.location.pathname === '/notifications';
  let isAccountSelected = props.location.pathname === '/account';
  let homeBackground = null;
  let profileBackground = null;
  let notificationsBackground = null;
  let accountBackground = null;

  if (isHomeSelected) {
    homeBackground = theme.color.primary;
  }

  if (isProfileSelected) {
    profileBackground = theme.color.primary;
  }

  if (isNotificationsSelected) {
    notificationsBackground = theme.color.primary;
  }

  if (isAccountSelected) {
    accountBackground = theme.color.primary;
  }

  let subscriptionItem;
  // if (props.myAccount && !props.myAccount.activeSubscription) {
  //   subscriptionItem = (
  //     <MenuItem
  //       text={'Upgrade'}
  //       iconName={'subscribe'}
  //       background={null}
  //       onTouchTap={() => props.history.push(`/subscription`)}
  //     />
  //   );
  // }

  return (
    <div style={styles.block}>
      <div style={styles.__header}>
        <ButtonIcon width={24} onTouchTap={props.hideNavigation}>
          <IconClose
            width={24}
            color={theme.color.onSurfaceMediumEmphasis}
          />
        </ButtonIcon>

        <div
          style={{
            marginLeft: 32,
            height: '100%',
            display: 'flex',
            alignItems: 'center',
          }}
        >
          <LogoText
            height={32}
            color={theme.color.onSurfaceMediumEmphasis}
          />
        </div>
      </div>

      <MenuItem
        text={'Home'}
        iconName={'home'}
        background={homeBackground}
        onTouchTap={() => props.history.push(`/`)}
      />

      <MenuItem
        text={'My profile'}
        iconName={'person'}
        background={profileBackground}
        onTouchTap={() => {
          props.history.push(profilePath);
        }}
      />

      <MenuItem
        text={'Notifications'}
        iconName={'notifications'}
        background={notificationsBackground}
        onTouchTap={() => {
          props.history.push(`/notifications`);
        }}
      />

      <MenuItem
        text={'My account'}
        iconName={'settings'}
        background={accountBackground}
        onTouchTap={() => {
          props.history.push(`/account`);
        }}
      />

      {subscriptionItem}

      <MenuItem
        text={'Logout'}
        iconName={'logout'}
        background={null}
        onTouchTap={props.logout}
      />
    </div>
  );
};

const mapStateToProps = state => {
  return {
    me: people.select.getMyself(state),
  };
};

export default connect(mapStateToProps)(
  withMyAccountProvider(
    withLogoutProvider(withRouter(NavigationContent))
  )
);
