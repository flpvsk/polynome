import React, { Component } from 'react';
import { connect } from 'react-redux';
import shouldComponentUpdate from './shouldComponentUpdate';

import * as chops from './data/chops';

function mapRecordingsToLines({ recordings, lines }) {
  const lineIdToRecording = {};

  lines.forEach(l => {
    const recordingId = l.playingRecordingId;
    const recording = recordings.find(
      r => r.recordingId === recordingId
    );

    if (recording) {
      lineIdToRecording[l.lineId] = recording;
    }
  });

  return lineIdToRecording;
}

function mapSoundsToBeats({ beats, kits }) {
  return beats.reduce((acc, beat) => {
    if (beat.sound.type !== 'kit') {
      return acc;
    }

    for (let kit of kits) {
      for (let sound of kit.sounds) {
        if (sound.id === beat.sound.soundId) {
          acc[beat.id] = sound;
          return acc;
        }
      }
    }

    return acc;
  }, {});
}

class AudioLoader extends Component {
  constructor() {
    super();
    this.state = {};
    this.shouldComponentUpdate = shouldComponentUpdate;
  }

  componentDidMount() {
    this._loadMissing();
  }

  componentDidUpdate() {
    this._loadMissing();

    if (this._isReady() && this.props.onFinishLoad) {
      this.props.onFinishLoad();
    }
  }

  _getBeatsToLoad() {
    const { soundByBeatId, beatsAudio } = this.props;
    const result = [];

    for (let sound of Object.values(soundByBeatId)) {
      if (!beatsAudio[sound.id]) {
        result.push({ soundId: sound.id, url: sound.url });
      }
    }

    return result;
  }

  _getRecordingsToLoad() {
    const { recordingByLineId, recordingsAudio } = this.props;
    const result = [];

    for (let recording of Object.values(recordingByLineId)) {
      const { recordingId } = recording;
      if (!recordingsAudio[recordingId]) {
        result.push({ recordingId, recording });
      }
    }

    return result;
  }

  _loadMissing() {
    const { loadBeatAudio, loadRecordingAudio } = this.props;
    const beatsToLoad = this._getBeatsToLoad();
    const recordingsToLoad = this._getRecordingsToLoad();

    for (let load of beatsToLoad) {
      loadBeatAudio(load);
    }

    for (let load of recordingsToLoad) {
      loadRecordingAudio(load);
    }

    if (beatsToLoad.length > 0 || recordingsToLoad.length) {
      if (this.props.onStartLoad) {
        this.props.onStartLoad();
      }
    }
  }

  _isReady() {
    const {
      soundByBeatId,
      beatsAudio,
      recordingByLineId,
      recordingsAudio,
    } = this.props;

    for (let sound of Object.values(soundByBeatId)) {
      if (!beatsAudio[sound.id]) {
        return false;
      }

      if (beatsAudio[sound.id].loadStatus !== 'ready') {
        return false;
      }
    }

    for (let recording of Object.values(recordingByLineId)) {
      const { recordingId } = recording;
      if (!recordingsAudio[recordingId]) {
        return false;
      }

      if (recordingsAudio[recordingId].loadStatus === 'pending') {
        return false;
      }
    }

    return true;
  }

  render() {
    if (!this._isReady()) {
      return null;
    }

    const {
      recordingByLineId,
      recordingsAudio,
      soundByBeatId,
      beatsAudio,
    } = this.props;

    return this.props.children({
      soundByBeatId,
      recordingByLineId,
      beatsAudio,
      recordingsAudio,
    });
  }
}

const mapStateToProps = state => {
  return {
    beatsAudio: chops.select.getBeatsAudioMap(state),
    recordingsAudio: chops.select.getRecordingsAudioMap(state),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    loadBeatAudio: ({ soundId, url }) => {
      chops.run.loadBeatAudio({ soundId, url }, dispatch);
    },

    loadRecordingAudio: ({ recordingId, recording }) => {
      chops.run.loadRecordingAudio(
        { recordingId, recording },
        dispatch
      );
    },
  };
};

const Loader = connect(mapStateToProps, mapDispatchToProps)(
  AudioLoader
);

const AudioMapper = props => {
  const { beats, lines, kits, recordings } = props;
  return (
    <Loader
      recordingByLineId={mapRecordingsToLines({ lines, recordings })}
      soundByBeatId={mapSoundsToBeats({ beats, kits })}
      {...props}
    />
  );
};

export default AudioMapper;
