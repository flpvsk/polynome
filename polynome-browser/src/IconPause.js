import React from 'react';

const IconPause = ({ width, height, color }) => {
  return (
    <svg fill={color} height={height} viewBox="0 0 24 24" width={width}>
      <path d="M6 19h4V5H6v14zm8-14v14h4V5h-4z" />
      <path d="M0 0h24v24H0z" fill="none" />
    </svg>
  );
};

export default IconPause;
