import React, { Component } from 'react';
import shouldComponentUpdate from './shouldComponentUpdate';
import color from './color';
import BezierEasing from 'bezier-easing';

import requestAnimationFrame from './requestAnimationFrame';
import eventGetTouch from './eventGetTouch';

import playerPattern from './PlayerPattern.png';

import './Player.css';

const lipInEasing = () => 1;
const lipOutEasing = BezierEasing(0, 0.44, 0.44, 1);

const ANGLE_CORRECTION = -Math.PI / 2;

const BASE_CIRCLE_WIDTH = 4;

function normalizeAngle(angle) {
  return angle + ANGLE_CORRECTION;
}

function drawArc(
  context,
  { centerX, centerY, radius, startAngle, endAngle, width, color }
) {
  context.beginPath();
  context.arc(centerX, centerY, radius, startAngle, endAngle, false);
  context.lineWidth = width;
  context.strokeStyle = color;
  context.stroke();
}

function findAnglesForWidth({
  centerX,
  centerY,
  baseAngle,
  radius,
  width,
}) {
  const alpha = Math.acos(width / (2 * radius));
  const beta = Math.PI - 2 * alpha;
  return [baseAngle - beta / 2, baseAngle + beta / 2];
}

function drawDelimiter(
  context,
  { centerX, centerY, innerRadius, outerRadius, angle, width, color }
) {
  context.beginPath();

  const [startInner, endInner] = findAnglesForWidth({
    centerX,
    centerY,
    baseAngle: angle,
    radius: innerRadius,
    width,
  });

  const [startOuter, endOuter] = findAnglesForWidth({
    centerX,
    centerY,
    baseAngle: angle,
    radius: outerRadius,
    width,
  });

  context.arc(
    centerX,
    centerY,
    innerRadius,
    startInner,
    endInner,
    false
  );

  context.arc(
    centerX,
    centerY,
    outerRadius,
    endOuter,
    startOuter,
    true
  );
  context.fillStyle = color;
  context.fill();
}

function drawWideArc(
  context,
  {
    centerX,
    centerY,
    innerRadius,
    outerRadius,
    startAngle,
    endAngle,
    color,
    contextPattern,
  }
) {
  context.beginPath();
  context.arc(
    centerX,
    centerY,
    innerRadius,
    startAngle,
    endAngle,
    false
  );

  context.arc(
    centerX,
    centerY,
    outerRadius,
    endAngle,
    startAngle,
    true
  );

  // context.globalAlpha = 0.2;
  context.fillStyle = contextPattern;
  context.fill();
  // context.globalAlpha = 1;

  context.fillStyle = color;
  context.strokeStyle = color;
  context.lineWidth = 0;

  context.globalCompositeOperation = 'hard-light';
  // context.globalCompositeOperation = 'hue';
  context.fill();
  context.globalCompositeOperation = 'source-over';
}

function drawBaseCircle(
  context,
  { centerX, centerY, radius, commonBeatsCount, displayConfig }
) {
  const beatLength = displayConfig.baseCircleBeatLength;
  const beatWidth = displayConfig.baseCircleBeatWidth;
  const beatColor = displayConfig.baseCircleColor;

  for (let i = 0; i < commonBeatsCount; i++) {
    drawDelimiter(context, {
      centerX,
      centerY,
      innerRadius: radius - beatLength,
      outerRadius: radius + BASE_CIRCLE_WIDTH / 2,
      angle: normalizeAngle(i * 2 * Math.PI / commonBeatsCount),
      width: beatWidth,
      radius,
      color: beatColor,
    });
  }
}

function drawCircle(
  context,
  {
    circleData,
    centerX,
    centerY,
    innerRadius,
    outerRadius,
    beatAngle,
    hasShadow = false,
    contextPattern,
    displayConfig = {},
  }
) {
  if (hasShadow) {
    context.beginPath();
    context.arc(centerX, centerY, outerRadius, 0, 2 * Math.PI, false);
    context.strokeStyle = null;
    context.fillStyle = displayConfig.backgroundColor;
    context.shadowColor = displayConfig.circleShadowColor;
    context.shadowBlur = displayConfig.circleShadowBlur;
    context.fill();
    context.shadowBlur = null;
    context.shadowColor = null;
  }

  for (let segment of circleData.segments) {
    const startAngle = normalizeAngle(segment.startBeat * beatAngle);
    const endAngle = normalizeAngle(segment.endBeat * beatAngle);
    drawWideArc(context, {
      centerX,
      centerY,
      color: segment.color,
      innerRadius,
      outerRadius: outerRadius + segment.lip,
      startAngle,
      endAngle,
      contextPattern,
    });
  }

  for (let segment of circleData.segments) {
    const startAngle = normalizeAngle(segment.startBeat * beatAngle);
    const endAngle = normalizeAngle(segment.endBeat * beatAngle);

    let beatColor = segment.beatColor;
    let outlineWidth = segment.outlineWidth || 0;
    let delimiterWidth = Math.max(outlineWidth, circleData.beatWidth);

    if (segment.outlineWidth) {
      beatColor = segment.outlineColor;
    }

    drawDelimiter(context, {
      centerX,
      centerY,
      innerRadius,
      outerRadius: outerRadius + segment.lip + outlineWidth / 2,
      angle: startAngle,
      color: beatColor,
      width: delimiterWidth,
    });

    drawDelimiter(context, {
      centerX,
      centerY,
      innerRadius,
      outerRadius: outerRadius + segment.lip + outlineWidth / 2,
      angle: endAngle,
      color: beatColor,
      width: delimiterWidth,
    });

    if (segment.outlineWidth) {
      drawArc(context, {
        centerX,
        centerY,
        radius: innerRadius + segment.outlineWidth / 2,
        startAngle,
        endAngle,
        width: segment.outlineWidth,
        color: segment.outlineColor,
      });

      drawArc(context, {
        centerX,
        centerY,
        radius: outerRadius + segment.lip,
        startAngle,
        endAngle,
        width: segment.outlineWidth,
        color: segment.outlineColor,
      });
    }
  }
}

function drawStick(
  context,
  {
    stickData,
    centerX,
    centerY,
    innerRadius,
    outerRadius,
    displayConfig,
  }
) {
  const { progressPercent } = stickData;

  drawDelimiter(context, {
    centerX,
    centerY,
    innerRadius,
    outerRadius,
    width: displayConfig.stickWidth,
    color: displayConfig.stickColor,
    angle: normalizeAngle(progressPercent * Math.PI * 2),
  });
}

function calculateSegments({
  progress,
  beatsCount,
  segmentsCount,
  mutedSegments = [],
  hasSoloCircle = false,
  isSoloCircle = false,
  isMuteCircle = false,
  baseColor,
  outlineWidth,
  outlineColor,
  offset = 0,
  lipGrowBeats,
  lipShrinkBeats,
  lipMax,
  lightenMax,
}) {
  // return [];

  const result = [];
  const beatPercent = 1 / beatsCount;
  const beatStep = beatsCount / segmentsCount;
  let colorArr = baseColor;

  if (hasSoloCircle && isSoloCircle) {
    colorArr = color.soloColor(colorArr);
    isMuteCircle = false;
  }

  if (hasSoloCircle && !isSoloCircle) {
    isMuteCircle = true;
  }

  const beatColorArr = color.delimiterColorByBase(colorArr);

  for (let i = 0; i < segmentsCount; i++) {
    let lipValue = 0;
    let light = 0;

    const startBeat = i * beatStep + offset;
    const endBeat = (i + 1) * beatStep + offset;

    if (mutedSegments.indexOf(i + 1) !== -1 || isMuteCircle) {
      let newColorArr = color.muteColor(colorArr);

      result.push({
        startBeat,
        endBeat,
        beatColor: color.toHslString(newColorArr),
        color: color.toHslString(newColorArr),
        outlineColor,
        outlineWidth,
        lip: 0,
      });

      continue;
    }

    const segmentStart = startBeat * beatPercent;
    const lipGrowLength = beatPercent * lipGrowBeats;
    const lipShrinkLength = beatPercent * lipShrinkBeats;
    const segmentLipOutStart = segmentStart + lipGrowLength;
    const segmentLipOutEnd = segmentLipOutStart + lipShrinkLength;

    if (progress >= segmentStart && progress < segmentLipOutStart) {
      const inEasing = lipInEasing(
        (progress - segmentStart) / lipGrowLength
      );

      lipValue = lipMax * inEasing;
      light = lightenMax * inEasing;
    }

    if (progress >= segmentLipOutStart && progress < segmentLipOutEnd) {
      const outEasing = lipOutEasing(
        (progress - segmentLipOutStart) / lipShrinkLength
      );

      lipValue = lipMax - lipMax * outEasing;
      light = lightenMax - lightenMax * outEasing;
    }

    result.push({
      startBeat,
      endBeat,
      color: color.toHslString(color.updateLightness(colorArr, light)),
      beatColor: color.toHslString(beatColorArr),
      outlineColor,
      outlineWidth,
      lip: lipValue,
    });
  }

  return result;
}

function getData({
  progress,
  commonBeatsCount,
  displayConfig = {},
  beats = [],
  soloItem,
  selectedItem,
}) {
  let minBeats = 1;

  if (beats.length > 1) {
    const beatsNumbers = beats.map(b => b.beatsCount);
    minBeats = beatsNumbers.reduce((a, v) => {
      const commonBeatsLength = commonBeatsCount / v;
      if (commonBeatsLength < a) {
        return commonBeatsLength;
      }

      return a;
    }, Infinity);
  }

  const lipGrowBeats = Math.floor(minBeats * 0.5 * 10) / 10 || 0.5;
  const lipShrinkBeats = 0.7 * (minBeats - lipGrowBeats);

  const lipMax = displayConfig.lipMax || 0;
  const lightenMax = displayConfig.lightenMax || 0;
  let circles = [];

  const hasSoloCircle = soloItem;
  for (let beat of beats) {
    const { beatsCount, offset, mutedBeats } = beat;
    const isSoloCircle = hasSoloCircle && soloItem.beatId === beat.id;
    let baseColor = color.baseColorByBeat(beat);
    let outlineWidth = 0;

    if (selectedItem && beat.id === selectedItem.beatId) {
      outlineWidth = 2;
    }

    let circle = {
      beatId: beat.id,
      beatWidth: displayConfig.beatWidth,
      segments: calculateSegments({
        progress,
        beatsCount: commonBeatsCount,
        segmentsCount: beatsCount,
        mutedSegments: mutedBeats,
        hasSoloCircle,
        isSoloCircle: isSoloCircle,
        isMuteCircle: beat.isMute,
        outlineColor: displayConfig.outlineColor,
        outlineWidth,
        baseColor,
        offset,
        lipGrowBeats,
        lipShrinkBeats,
        lipMax,
        lightenMax,
      }),
    };

    circles.push(circle);
  }

  return {
    stick: {
      progressPercent: progress,
    },
    circles,
  };
}

class Player extends Component {
  constructor() {
    super();
    this.shouldComponentUpdate = shouldComponentUpdate;

    this._saveTouchPoint = e => {
      const touch = eventGetTouch(e);
      const [x, y] = touch;
      this._touchX = x;
      this._touchY = y;
    };

    this._clearTouchPoint = e => {
      const [x, y] = eventGetTouch(e);
      const touchX = this._touchX;
      const touchY = this._touchY;

      if (touchX === null || touchY === null) {
        return;
      }

      const distance = Math.sqrt((y - touchY) ** 2 + (x - touchX) ** 2);

      if (distance < 4) {
        return;
      }

      this._touchX = null;
      this._touchY = null;
    };

    this._endSelectItem = () => {
      let x = this._touchX;
      let y = this._touchY;

      if (!x || !y) {
        return;
      }

      const canvasRect = this._canvas.getBoundingClientRect();
      const canvasX = canvasRect.x;
      const canvasY = canvasRect.y;
      const item = this._getItemByCoordinates(x - canvasX, y - canvasY);

      if (!item) {
        return;
      }

      this.props.onBeatSelected(item.beatId);

      this._touchX = null;
      this._touchY = null;
    };

    this._getItemByCoordinates = (x, y) => {
      const size = this.props.width;

      const centerX = size / 2;
      const centerY = size / 2;

      const radius = Math.sqrt((x - centerX) ** 2 + (y - centerY) ** 2);

      for (let beatId of Object.keys(this._radiusRangeByBeatId)) {
        const range = this._radiusRangeByBeatId[beatId];
        if (radius > range[0] && radius < range[1]) {
          return { beatId };
        }
      }

      return null;
    };

    const touchHandlers = {};

    if ('ontouchstart' in document.documentElement) {
      touchHandlers.onTouchStart = this._saveTouchPoint;
      touchHandlers.onTouchEnd = this._endSelectItem;
      touchHandlers.onTouchMove = this._clearTouchPoint;
    } else {
      touchHandlers.onMouseDown = this._saveTouchPoint;
      touchHandlers.onMouseUp = this._endSelectItem;
    }

    this._touchHandlers = touchHandlers;
  }

  componentWillMount() {
    this._loop = () => {
      if (this._stopTheLoop) {
        return;
      }

      this._clearDisplay();
      this._renderDisplay();

      const { isPaused, beats } = this.props;

      if (isPaused || !beats || !beats.length) {
        return;
      }

      requestAnimationFrame(this._loop);
    };
  }

  componentDidMount() {
    const patternImg = new Image();
    patternImg.src = playerPattern;
    patternImg.onload = () => {
      const canvas = this._canvas;
      const context = canvas.getContext('2d');
      this._contextPattern = context.createPattern(
        patternImg,
        'repeat'
      );
      this._loop();
    };

    this._patternImg = patternImg;
  }

  componentDidUpdate() {
    this._loop();
  }

  componentWillUnmount() {
    this._stopTheLoop = true;

    this._patternImg.onload = null;
    this._patternImg = null;
    this._contextPattern = null;
  }

  _clearDisplay() {
    const canvas = this._canvas;
    const context = canvas.getContext('2d');
    context.clearRect(0, 0, this.props.width, this.props.width);
  }

  _getProgress(currentTime) {
    const {
      startTime,
      isPaused,
      pauseTime,
      commonBeatsCount,
      commonBeatsTempo,
    } = this.props;

    const cycleTime = commonBeatsCount * 60 / commonBeatsTempo;

    let current = currentTime;

    if (isPaused) {
      current = pauseTime;
    }

    const pr = (current - startTime) / cycleTime;
    const progress = pr - Math.floor(pr);

    return progress;
  }

  _getActiveBeatIds(currentTime) {
    // Can do most of it once on props change
    const {
      startTime,
      isPaused,
      pauseTime,
      commonBeatsCount,
      commonBeatsTempo,
      beats,
    } = this.props;

    let beatsStartStopById = {};
    let trackStart = 0;
    let trackStop = 0;

    for (let beat of beats) {
      const commonBeatStart =
        (beat.lineStart || 0) * commonBeatsCount / beat.beatsCount;

      const commonBeatStop =
        ((beat.lineStart || 0) +
          (beat.lineDuration || beat.beatsCount)) *
        commonBeatsCount /
        beat.beatsCount;

      trackStart = Math.min(trackStart, commonBeatStart);
      trackStop = Math.max(trackStop, commonBeatStop);

      beatsStartStopById[beat.id] = [commonBeatStart, commonBeatStop];
    }

    const trackBeatsCount = trackStop - trackStart;
    const beatTime = 60 / commonBeatsTempo;

    let current = currentTime;

    if (isPaused) {
      current = pauseTime;
    }

    const beatsPlayed = Math.floor((current - startTime) / beatTime);
    const currentBeatInTrack =
      beatsPlayed -
      Math.floor(beatsPlayed / trackBeatsCount) * trackBeatsCount;

    const result = [];
    for (let beatId of Object.keys(beatsStartStopById)) {
      const [start, stop] = beatsStartStopById[beatId];
      if (currentBeatInTrack >= start && currentBeatInTrack < stop) {
        result.push(beatId);
      }
    }

    return result;
  }

  _renderDisplay() {
    const {
      width,
      displayConfig,
      commonBeatsCount,
      soloItem,
      isPaused,
      selectedItem,
    } = this.props;
    let { beats } = this.props;

    const currentTime = this.props.getCurrentTime();

    if (!isPaused) {
      const activeIds = this._getActiveBeatIds(currentTime);
      beats = beats.filter(b => activeIds.indexOf(b.id) > -1);
    }

    const data = getData({
      progress: this._getProgress(currentTime),
      commonBeatsCount,
      displayConfig,
      beats,
      soloItem,
      selectedItem,
    });

    const size = width;
    const canvas = this._canvas;
    const context = canvas.getContext('2d');
    const centerX = size / 2;
    const centerY = size / 2;
    const baseCirclePadding = size / 12;
    const centerRadius = size / 10;

    const beatAngle = 2 * Math.PI / commonBeatsCount;

    drawBaseCircle(context, {
      centerX,
      centerY,
      radius: size / 2 - BASE_CIRCLE_WIDTH,
      commonBeatsCount,
      displayConfig,
    });

    const sizeAvailable =
      size / 2 - BASE_CIRCLE_WIDTH - centerRadius - baseCirclePadding;

    const radiusStep = sizeAvailable / data.circles.length;

    this._radiusRangeByBeatId = {};

    for (let i = data.circles.length - 1; i >= 0; i--) {
      let circleData = data.circles[i];

      let innerRadius = centerRadius + i * radiusStep;
      let outerRadius = centerRadius + (i + 1) * radiusStep;

      this._radiusRangeByBeatId[circleData.beatId] = [
        innerRadius,
        outerRadius,
      ];

      drawCircle(context, {
        circleData,
        innerRadius,
        outerRadius,
        centerX,
        centerY,
        beatAngle,
        hasShadow: true,
        displayConfig,
        contextPattern: this._contextPattern,
      });
    }

    drawStick(context, {
      stickData: data.stick,
      centerX,
      centerY,
      innerRadius: centerRadius,
      outerRadius: size / 2 - BASE_CIRCLE_WIDTH / 2,
      displayConfig,
    });
  }

  render() {
    return (
      <div className="Player">
        <canvas
          style={{}}
          {...this._touchHandlers}
          width={this.props.width}
          height={this.props.width}
          ref={ref => (this._canvas = ref)}
        />
      </div>
    );
  }
}

Player.defaultProps = {
  displayConfig: {
    lipMax: 12,
    lightenMax: 10,
    beatWidth: 4,
    baseCircleBeatWidth: 2,
    baseCircleBeatLength: 2,
    baseCircleColor: '#777',
    circleShadowColor: '#212121',
    circleShadowBlur: 20,
    backgroundColor: '#333',
    outlineColor: '#fff',
    stickColor: '#eee',
    stickWidth: 2,
  },
};

export default Player;
