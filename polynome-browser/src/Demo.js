import React, { Component } from 'react';
import { color, lightness } from 'kewler';
import BezierEasing from 'bezier-easing';

import freqToColor from './frequencyToColor';
import './App.css';

import PolynomeDisplay from './PolynomeDisplay';

const lipMax = 10;
const lipGrowBeats = 3;
const lipShrinkBeats = 5; // shortest of the segments
const lipInEasing = () => 1;
const lipOutEasing = BezierEasing(0, 0.44, 0.44, 1);

const lightenMax = 10;

function calculateSegments({
  progress,
  beatsCount,
  segmentsCount,
  offsetBeats = 0,
}) {
  const result = [];
  const beatPercent = 1 / beatsCount;
  const beatStep = beatsCount / segmentsCount;

  const colorStr = freqToColor(segmentsCount);
  const colorFn = color(colorStr);

  for (let i = 0; i < segmentsCount; i++) {
    let lipValue = 0;
    let light = 0;

    const startBeat = i * beatStep + offsetBeats;
    const endBeat = (i + 1) * beatStep + offsetBeats;

    let segmentStart = startBeat * beatPercent;
    let lipGrowLength = beatPercent * lipGrowBeats;
    let lipShrinkLength = beatPercent * lipShrinkBeats;
    let segmentLipOutStart = segmentStart + lipGrowLength;
    let segmentLipOutEnd = segmentLipOutStart + lipShrinkLength;

    if (progress >= segmentStart && progress < segmentLipOutStart) {
      const inEasing = lipInEasing(
        (progress - segmentStart) / lipGrowLength
      );

      lipValue = lipMax * inEasing;
      light = lightenMax * inEasing;
    }

    if (progress >= segmentLipOutStart && progress < segmentLipOutEnd) {
      const outEasing = lipOutEasing(
        (progress - segmentLipOutStart) / lipShrinkLength
      );

      lipValue = lipMax - lipMax * outEasing;
      light = lightenMax - lightenMax * outEasing;
    }

    let lightFn = lightness(light);
    let lighterColorFn = colorFn(lightFn);
    let lighterColor = lighterColorFn();
    result.push({
      startBeat,
      endBeat,
      color: lighterColor,
      lip: lipValue,
    });
  }

  return result;
}

function getState(progress) {
  return {
    stick: {
      progressPercent: progress,
      color: 'white',
      width: 2,
    },
    baseCircle: {
      color: '#777',
      beats: (() => {
        const n = 30;
        let r = [];
        for (let i = 0; i < n; i++) {
          r.push({
            color: '#777',
            width: 4,
            length: 10,
          });
        }
        return r;
      })(),
    },
    circles: [
      {
        beatWidth: 4, // TODO: move to segments
        beatColor: 'rgba(119, 119, 119, 0.7)',
        segments: calculateSegments({
          progress,
          beatsCount: 30,
          segmentsCount: 2,
          offsetBeats: 4,
        }),
      },
      {
        beatWidth: 4,
        beatColor: 'rgba(122, 122, 122, 1)',
        segments: calculateSegments({
          progress,
          beatsCount: 30,
          segmentsCount: 3,
        }),
      },
      {
        beatWidth: 4,
        beatColor: 'rgba(122, 122, 122, 1)',
        segments: calculateSegments({
          progress,
          beatsCount: 30,
          segmentsCount: 5,
        }),
      },
    ],
  };
}

class App extends Component {
  constructor() {
    super();

    this._setProgressPercent = e => {
      const { value } = e.target;
      this.setState(getState(value));
    };

    this.state = getState(0.34);
  }

  componentDidMount() {
    this.start();
  }

  start() {
    const cycleTime = 3000;
    const startTime = Date.now();
    const run = () => {
      const diff = (Date.now() - startTime) / cycleTime;
      const progress = diff - Math.floor(diff);
      this._setProgressPercent({ target: { value: progress } });
      requestAnimationFrame(run);
    };

    run();
  }

  render() {
    return (
      <div className="App">
        <div style={{ padding: 30 }}>
          <input
            type="range"
            min="0"
            max="1"
            step="0.01"
            value={this.state.stick.progressPercent}
            onChange={this._setProgressPercent}
          />
        </div>
        <div>
          <PolynomeDisplay data={this.state} size={400} />
        </div>
      </div>
    );
  }
}

export default App;
