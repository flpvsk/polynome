export default function closeMediaStream(stream) {
  if (stream.getTracks) {
    stream.getTracks().forEach(track => track.stop());
  }

  if (!stream.getTracks) {
    stream.stop();
  }
}
