import React from 'react';
import theme from './theme';
import pick from 'lodash/pick';

const styles = {
  block: {
    width: 48,
    height: 48,
    border: 'none',
    background: 'none',
    display: 'flex',
    justifyContent: 'center',
    cursor: 'pointer',
  },

  _isOutlined: {
    border: `1px solid ${theme.color.outline}`,
    borderRadius: theme.shape.borderRadius,
  },
};

const IconButton = props => {
  const { isDisabled } = props;
  const styleMix = pick(props, ['width']);

  let blockStyle = {
    ...styles.block,
    ...styleMix,
  };

  if (props.styleName === 'outlined') {
    blockStyle = {
      ...blockStyle,
      ...styles._isOutlined,
    };
  }

  if (props.styleName === 'appBar') {
    blockStyle = {
      ...blockStyle,
      height: 24,
      width: 24,
    };
  }

  let action = props.onTouchTap;

  if (isDisabled) {
    action = null;
  }

  return (
    <button style={blockStyle} disabled={isDisabled} onClick={action}>
      {props.children}
    </button>
  );
};

export default IconButton;
