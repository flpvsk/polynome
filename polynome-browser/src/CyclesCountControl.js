import React from 'react';
import TextButton from './ButtonText';
import IconPlus from './IconPlus';
import IconMinus from './IconMinus';

const styles = {
  block: {
    color: '#eee',
    display: 'flex',
    width: '280px',
    fontSize: '1.3em',
    letterSpacing: '0.2em',
    justifyContent: 'space-between',
    alignContent: 'space-between',
    alignItems: 'center',
    marginTop: '14px',
  },

  __cyclesCountText: {
    lineHeight: '100%',
    paddingLeft: '1em',
    paddingRight: '1em',
  },
};

const CyclesCountControl = props => {
  const { cyclesCount } = props;
  const minusIsDisabled = props.cyclesCount === 1;
  const minusColor = minusIsDisabled ? '#777' : '#eee';

  let text = ' bar';
  if (cyclesCount > 1) {
    text = ' bars';
  }

  return (
    <div style={styles.block}>
      <TextButton
        isDisabled={minusIsDisabled}
        onTouchTap={() => props.onChangeCyclesCount(cyclesCount - 1)}
      >
        <IconMinus width="24px" height="100%" color={minusColor} />
      </TextButton>

      <div style={styles.__cyclesCountText}>
        {props.cyclesCount} {text}
      </div>

      <TextButton
        onTouchTap={() => props.onChangeCyclesCount(cyclesCount + 1)}
      >
        <IconPlus width="24px" height="100%" color="#eee" />
      </TextButton>
    </div>
  );
};

export default CyclesCountControl;
