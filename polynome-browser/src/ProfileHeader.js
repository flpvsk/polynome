import React, { Component } from 'react';

import theme from './theme';

import Userpic from './Userpic';
import ButtonIcon from './ButtonIcon';
import TextWrapper from './TextWrapper';
import InputText from './InputText';
import IconEdit from './IconEdit';
import IconDone from './IconDone';
import IconCancel from './IconCancel';

const styles = {
  block: {
    display: 'flex',
    alignItems: 'center',
    width: '100%',
    height: 80,
    marginTop: 8,
    marginBottom: 8,
  },

  __profileTextArea: {
    flexGrow: 1,
    marginLeft: 16,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
};

class ProfileHeader extends Component {
  constructor() {
    super();
    this.state = {
      isEditingHandle: false,
      handle: '',
      userpic: null,
    };

    this._editHandle = () => {
      this.setState({
        isEditingHandle: true,
        handle: this.props.me.handle,
      });
    };

    this._changeHandle = value => {
      this.setState({
        handle: value,
      });
    };

    this._confirmEditHandle = () => {
      this.setState({ isEditingHandle: false });
      this.props.onChangeHandle({
        handle: this.state.handle,
        personId: this.props.me.personId,
      });
    };

    this._rejectEditHandle = () => {
      this.setState({
        isEditingHandle: false,
        handle: this.props.me.handle,
      });
    };

    this._showChoosePicDialog = () => {
      const { me, person } = this.props;
      const isMyProfile = me && me.personId === person.personId;
      if (!isMyProfile) {
        return;
      }
      this._fileInput.click();
    };

    this._updateUserpic = () => {
      if (this._fileInput.files && this._fileInput.files[0]) {
        let reader = new FileReader();
        reader.onload = e => {
          this.setState({ userpic: e.target.result });
          this.props.onChangeUserpic({
            file: this._fileInput.files[0],
            dataUrl: e.target.result,
            personId: this.props.me.personId,
          });
        };
        reader.readAsDataURL(this._fileInput.files[0]);
      }
    };
  }

  render() {
    let { me, person } = this.props;
    const { handle, userpic } = this.state;

    let isMyProfile = me && me.personId === person.personId;

    me = {
      ...me,
      handle,
      userpic,
    };

    let editButton;
    if (isMyProfile) {
      editButton = (
        <ButtonIcon onTouchTap={this._editHandle}>
          <IconEdit
            width={24}
            color={theme.color.onBackgroundDisabled}
          />
        </ButtonIcon>
      );
    }

    if (!this.state.isEditingHandle) {
      return (
        <div style={styles.block}>
          <input
            ref={r => (this._fileInput = r)}
            accept="image/*"
            onChange={this._updateUserpic}
            type="file"
            style={{ display: 'none' }}
          />

          <Userpic
            onTouchTap={this._showChoosePicDialog}
            size={56}
            person={person}
          />

          <div style={styles.__profileTextArea}>
            <TextWrapper
              styleName={'h4'}
              color={theme.color.onBackground}
            >
              {person.handle || 'anonymous'}
            </TextWrapper>
            {editButton}
          </div>
        </div>
      );
    }

    if (this.state.isEditingHandle) {
      return (
        <div style={styles.block}>
          <Userpic size={56} person={person} />

          <div style={styles.__profileTextArea}>
            <InputText
              placeholder={'Enter your new handle'}
              value={this.state.handle || ''}
              onChange={this._changeHandle}
            />
            <div style={{ flexShrink: 0 }}>
              <ButtonIcon onTouchTap={this._confirmEditHandle}>
                <IconDone
                  width={32}
                  color={theme.color.onBackgroundDisabled}
                />
              </ButtonIcon>
            </div>

            <div style={{ flexShrink: 0 }}>
              <ButtonIcon onTouchTap={this._rejectEditHandle}>
                <IconCancel
                  width={32}
                  color={theme.color.onBackgroundDisabled}
                />
              </ButtonIcon>
            </div>
          </div>
        </div>
      );
    }
  }
}

export default ProfileHeader;
