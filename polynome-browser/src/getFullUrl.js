const getFullUrl = path => {
  let host = process.env.REACT_APP_HOST;

  if (process.env.NODE_ENV === 'development') {
    host = `${window.location.protocol}//${window.location.host}`;
  }

  return `${host}${path}`;
};

export default getFullUrl;
