import React, { Component } from 'react';
import { injectStripe, CardElement } from 'react-stripe-elements';

import TextWrapper from './TextWrapper';
import InputEmail from './InputEmail';
import InputPassword from './InputPassword';
import ButtonContained from './ButtonContained';
import InputAddress from './InputAddress';
import InputText from './InputText';
import ListItem from './ListItem';
import LoadingIndicatorCircular from './LoadingIndicatorCircular';

import Storage from './data/storage';

import { styles as textStyles } from './TextWrapper';

import theme from './theme';

const stripeStyle = {
  base: {
    ...textStyles.body1,
    fontFamily: 'Roboto, sans-serif',
    fontSize: '15.712px',
    fontSmoothing: 'antialiased',
    color: theme.color.onSurface,

    '::placeholder': {
      fontFamily: 'Roboto, sans-serif',
      fontSize: '15.712px',
      color: theme.color.onSurfaceDisabled,
    },
  },

  invalid: {
    color: theme.color.error,
    ...textStyles.body1,
  },
};

const styles = {
  __formItem: {
    marginBottom: 8,
  },

  __formActions: {
    marginTop: 8,
  },

  __cardWrapper: {
    height: 40,
    display: 'flex',
    alignItems: 'center',
    flexGrow: 1,

    backgroundColor: theme.color.containerOnSurface,
    borderBottom: `1px solid ${theme.color.outline}`,
    borderTopLeftRadius: theme.shape.borderRadius,
    borderTopRightRadius: theme.shape.borderRadius,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    paddingLeft: 12,
    paddingRight: 12,
    color: theme.color.onSurface,
  },

  __sectionHeader: {
    marginTop: 32,
    marginBottom: 4,
  },
};

class CardForm extends Component {
  constructor() {
    super();
    this.state = {
      email: '',
      password: '',
      billingAddress: {},
      nameOnCard: '',
      error: null,
      isLoading: false,
      showThankYouScreen: false,
    };
  }

  _handleSubmit = ev => {
    ev.preventDefault();
    const { planId, myAccount } = this.props;
    const email = myAccount.email || this.state.email;
    let password = this.state.password;

    const { billingAddress, nameOnCard } = this.state;

    if (!email) {
      this.setState({
        error: 'Please enter your email',
      });
      return;
    }

    if (!password && !myAccount.hasPassword) {
      this.setState({
        error: 'Please set your password',
      });
      return;
    }

    if (this.props.stripe) {
      this.setState({ isLoading: true });
      this.props.stripe
        .createToken()
        .then(payload => {
          if (payload.error) {
            console.error('[FormPayment]', 'Error getting the token');
            throw new Error(payload.error.message);
          }

          return payload.token.id;
        })
        .then(tokenId => {
          return Storage.createSubscription({
            personId: myAccount.personId,
            email,
            password,
            stripeTokenId: tokenId,
            planId,
            billingAddress,
            nameOnCard,
            discountCode: this.props.discountCode,
          });
        })
        .then(() => {
          this.setState({ isLoading: false });
          this.props.onSuccess({ planId: this.props.planId });
        })
        .catch(e => {
          this.setState({
            isLoading: false,
            error: (e.message || '').replace('GraphQL error: ', ''),
          });
        });
    }

    if (!this.props.stripe) {
      this.setState({
        error: `Stripe hasn't loaded yet, please wait and try again`,
      });
      return;
    }

    this.setState({ error: '' });
  };

  _changeEmail = value => {
    this.setState({ email: value });
  };

  _changePassword = value => {
    this.setState({ password: value });
  };

  _changeBillingAddress = value => {
    this.setState({ billingAddress: value });
  };

  _changeNameOnCard = value => {
    this.setState({ nameOnCard: value });
  };

  render() {
    let emailInput;
    let passwordInput;
    let readOnly = this.state.isLoading;

    const { myAccount } = this.props;

    if (!myAccount.email) {
      emailInput = (
        <div style={styles.__formItem}>
          <InputEmail
            required
            placeholder="Email"
            value={this.state.email}
            onChange={this._changeEmail}
            readOnly={readOnly}
          />
        </div>
      );
    }

    if (!myAccount.hasPassword) {
      passwordInput = (
        <div style={styles.__formItem}>
          <InputPassword
            required
            readOnly={readOnly}
            title="Password should be at least 4 characters long"
            pattern=".{4,}"
            placeholder="Password"
            value={this.state.password}
            onChange={this._changePassword}
          />
        </div>
      );
    }

    let button;

    if (readOnly) {
      button = <LoadingIndicatorCircular />;
    }

    if (!readOnly) {
      button = <ButtonContained>Subscribe</ButtonContained>;
    }

    let accountDetailsHeader;

    if (emailInput || passwordInput) {
      accountDetailsHeader = (
        <div style={styles.__sectionHeader}>
          <TextWrapper styleName="h6" color={theme.color.onSurface}>
            Account details
          </TextWrapper>
        </div>
      );
    }

    return (
      <form style={{ width: '100%' }} onSubmit={this._handleSubmit}>
        <div>
          <TextWrapper styleName={'body1'} color={theme.color.error}>
            {this.state.error}
          </TextWrapper>
        </div>

        {accountDetailsHeader}
        {emailInput}
        {passwordInput}

        <div style={styles.__sectionHeader}>
          <TextWrapper styleName="h6" color={theme.color.onSurface}>
            Payment details
          </TextWrapper>
        </div>

        <ListItem noPadding>
          <div style={styles.__cardWrapper}>
            <CardElement style={stripeStyle} />
          </div>
        </ListItem>

        <ListItem noPadding>
          <InputText
            readOnly={readOnly}
            placeholder={`Name on the card`}
            value={this.state.nameOnCard}
            onChange={this._changeNameOnCard}
          />
        </ListItem>

        <ListItem noPadding>
          <InputAddress
            readOnly={readOnly}
            placeholder={`Billing address`}
            onChange={this._changeBillingAddress}
          />
        </ListItem>

        <div style={styles.__formActions}>{button}</div>
      </form>
    );
  }
}

const FormPayment = injectStripe(CardForm);

export default FormPayment;
