import React, { Component } from 'react';
import shouldComponentUpdate from './shouldComponentUpdate';

const ANGLE_CORRECTION = -Math.PI / 2;

const BASE_CIRCLE_WIDTH = 4;

function normalizeAngle(angle) {
  return angle + ANGLE_CORRECTION;
}

function drawArc(
  context,
  { centerX, centerY, radius, startAngle, endAngle, width, color }
) {
  context.beginPath();
  context.arc(centerX, centerY, radius, startAngle, endAngle, false);
  context.lineWidth = width;
  context.strokeStyle = color;
  context.stroke();
}

function findAnglesForWidth({
  centerX,
  centerY,
  baseAngle,
  radius,
  width,
}) {
  const alpha = Math.acos(width / (2 * radius));
  const beta = Math.PI - 2 * alpha;
  return [baseAngle - beta / 2, baseAngle + beta / 2];
}

function drawDelimiter(
  context,
  { centerX, centerY, innerRadius, outerRadius, angle, width, color }
) {
  context.beginPath();

  const [startInner, endInner] = findAnglesForWidth({
    centerX,
    centerY,
    baseAngle: angle,
    radius: innerRadius,
    width,
  });

  const [startOuter, endOuter] = findAnglesForWidth({
    centerX,
    centerY,
    baseAngle: angle,
    radius: outerRadius,
    width,
  });

  context.arc(
    centerX,
    centerY,
    innerRadius,
    startInner,
    endInner,
    false
  );

  context.arc(
    centerX,
    centerY,
    outerRadius,
    endOuter,
    startOuter,
    true
  );
  context.fillStyle = color;
  context.fill();
}

function drawWideArc(
  context,
  {
    centerX,
    centerY,
    innerRadius,
    outerRadius,
    startAngle,
    endAngle,
    color,
  }
) {
  context.beginPath();
  context.arc(
    centerX,
    centerY,
    innerRadius,
    startAngle,
    endAngle,
    false
  );

  context.arc(
    centerX,
    centerY,
    outerRadius,
    endAngle,
    startAngle,
    true
  );

  context.fillStyle = color;
  context.strokeStyle = 'none';
  context.fill();
}

function drawBaseCircle(
  context,
  { centerX, centerY, radius, circleData }
) {
  const {
    // color,
    beats,
  } = circleData;

  // drawArc(context, {
  //   centerX,
  //   centerY,
  //   radius,
  //   startAngle: 0,
  //   endAngle: Math.PI * 2,
  //   width: BASE_CIRCLE_WIDTH,
  //   color
  // });

  for (let i = 0; i < beats.length; i++) {
    const beat = beats[i];
    drawDelimiter(context, {
      centerX,
      centerY,
      innerRadius: radius - beat.length,
      outerRadius: radius + BASE_CIRCLE_WIDTH / 2,
      angle: normalizeAngle(i * 2 * Math.PI / beats.length),
      width: beat.width,
      radius,
      color: beat.color,
    });
  }
}

function drawCircle(
  context,
  { circleData, centerX, centerY, innerRadius, outerRadius, beatAngle }
) {
  for (let segment of circleData.segments) {
    const startAngle = normalizeAngle(segment.startBeat * beatAngle);
    const endAngle = normalizeAngle(segment.endBeat * beatAngle);
    drawWideArc(context, {
      centerX,
      centerY,
      color: segment.color,
      innerRadius,
      outerRadius: outerRadius + segment.lip,
      startAngle,
      endAngle,
    });
  }

  for (let segment of circleData.segments) {
    const startAngle = normalizeAngle(segment.startBeat * beatAngle);
    const endAngle = normalizeAngle(segment.endBeat * beatAngle);

    let beatColor = segment.beatColor;
    let outlineWidth = segment.outlineWidth || 0;
    let delimiterWidth = Math.max(outlineWidth, circleData.beatWidth);

    if (segment.outlineWidth) {
      beatColor = segment.outlineColor;
    }

    drawDelimiter(context, {
      centerX,
      centerY,
      innerRadius,
      outerRadius: outerRadius + segment.lip + outlineWidth / 2,
      angle: startAngle,
      color: beatColor,
      width: delimiterWidth,
    });

    drawDelimiter(context, {
      centerX,
      centerY,
      innerRadius,
      outerRadius: outerRadius + segment.lip + outlineWidth / 2,
      angle: endAngle,
      color: beatColor,
      width: delimiterWidth,
    });

    if (segment.outlineWidth) {
      drawArc(context, {
        centerX,
        centerY,
        radius: innerRadius + segment.outlineWidth / 2,
        startAngle,
        endAngle,
        width: segment.outlineWidth,
        color: segment.outlineColor,
      });

      drawArc(context, {
        centerX,
        centerY,
        radius: outerRadius + segment.lip,
        startAngle,
        endAngle,
        width: segment.outlineWidth,
        color: segment.outlineColor,
      });
    }
  }
}

function drawStick(
  context,
  { stickData, centerX, centerY, innerRadius, outerRadius }
) {
  const { progressPercent, width, color } = stickData;

  drawDelimiter(context, {
    centerX,
    centerY,
    innerRadius,
    outerRadius,
    width,
    color,
    angle: normalizeAngle(progressPercent * Math.PI * 2),
  });
}

class Display extends Component {
  constructor() {
    super();
    this.shouldComponentUpdate = shouldComponentUpdate;
  }

  componentDidMount() {
    this._renderDisplay();
  }

  componentDidUpdate() {
    this._clearDisplay();
    this._renderDisplay();
  }

  _clearDisplay() {
    const canvas = this._canvas;
    const context = canvas.getContext('2d');
    context.clearRect(0, 0, this.props.size, this.props.size);
  }

  _renderDisplay() {
    const { size, data } = this.props;
    const canvas = this._canvas;
    const context = canvas.getContext('2d');
    const centerX = size / 2;
    const centerY = size / 2;
    const baseCirclePadding = this.props.size / 10;
    const centerRadius = size / 10;

    const beatAngle = 2 * Math.PI / data.baseCircle.beats.length;

    drawBaseCircle(context, {
      centerX,
      centerY,
      radius: size / 2 - BASE_CIRCLE_WIDTH,
      circleData: data.baseCircle,
    });

    const sizeAvailable =
      size / 2 - BASE_CIRCLE_WIDTH - centerRadius - baseCirclePadding;

    const radiusStep = sizeAvailable / data.circles.length;

    for (let i = data.circles.length - 1; i >= 0; i--) {
      drawCircle(context, {
        circleData: data.circles[i],
        innerRadius: centerRadius + i * radiusStep,
        outerRadius: centerRadius + (i + 1) * radiusStep + 3,
        centerX,
        centerY,
        beatAngle,
      });
    }

    drawStick(context, {
      stickData: data.stick,
      centerX,
      centerY,
      innerRadius: centerRadius,
      outerRadius: size / 2 - BASE_CIRCLE_WIDTH / 2,
    });
  }

  render() {
    return (
      <canvas
        width={this.props.size}
        height={this.props.size}
        ref={ref => (this._canvas = ref)}
      />
    );
  }
}

export default Display;
