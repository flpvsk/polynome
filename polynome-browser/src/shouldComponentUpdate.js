import isEqualWith from 'lodash/isEqualWith';

const comparisonFn = (o1, o2) => {
  if (o1 instanceof ArrayBuffer || o2 instanceof ArrayBuffer) {
    return o1 === o2;
  }

  return undefined;
};

export default function shouldComponentUpdate(newProps, newState) {
  return (
    !isEqualWith(this.props, newProps, comparisonFn) ||
    !isEqualWith(this.state, newState, comparisonFn)
  );
}
