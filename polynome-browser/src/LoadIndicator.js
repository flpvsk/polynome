import React, { Component } from 'react';
import './LoadIndicator.css';

class LoadIndicator extends Component {
  render() {
    const { loadingState, width } = this.props;
    const { progress } = loadingState;

    let className = 'LoadIndicator';

    if (this.props.loadingState.isLoading) {
      className += ' _isVisible';
    }

    return (
      <div className={className}>
        <div
          style={{ width: width / 2 }}
          className="LoadIndicator__Container"
        >
          <div className="LoadIndicator__Text">Loading</div>
          <svg width="100%" height="5px">
            <rect
              x="0"
              y="0"
              width={100 * progress + '%'}
              height="5px"
              fill="white"
            />

            <rect
              x={100 * progress + '%'}
              y="0"
              width={100 * (1 - progress) + '%'}
              height="5px"
              fill="#2e2e2e"
            />
          </svg>
        </div>
      </div>
    );
  }
}

export default LoadIndicator;
