const nextFrame =
  window.requestAnimationFrame || window.webkitRequestAnimationFrame;

export default nextFrame;
