import { createSelector } from 'reselect';

export const match = {};

export const run = {};

const reverseChronological = (a1, a2) => a2.createdAt - a1.createdAt;

const listNotificationsSelector = state => state.notifications;
const listNotificationsParams = ({ first, after }) => ({
  first,
  after,
});

export const select = {
  listNotificationsGroups: createSelector(
    listNotificationsSelector,
    listNotificationsParams,
    (notifications, { first, after }) => {
      const nodes = [];
      const notifList = notifications
        .toList()
        .sort(reverseChronological);
      let afterReached = false;
      let endCursor = null;
      let hasNextPage = false;

      for (let notification of notifList) {
        if (!after && first > 0) {
          after = notification.notificationId;
          afterReached = true;
          nodes.push({
            node: notification,
            cursor: notification.notificationId,
          });
          endCursor = notification.notificationId;
          continue;
        }

        if (nodes.length > first) {
          hasNextPage = true;
          break;
        }

        if (notification.notificationId === after) {
          afterReached = true;
        }

        if (!afterReached) {
          continue;
        }

        nodes.push({
          node: notification,
          cursor: notification.notificationId,
        });
        endCursor = notification.notificationId;
      }

      return {
        pageInfo: {
          endCursor,
          hasNextPage,
        },
      };
    }
  ),
};
