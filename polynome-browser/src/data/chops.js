import { createSelector } from 'reselect';
import { Map } from 'immutable';
import nanoid from 'nanoid';
import * as people from './people';

import storage from './storage';
import FileStorage from './FileStorage';

export const DEFAULT_TEMPO = 120;

const CREATE_NEW_CHOP = 'CREATE_NEW_CHOP';
const REMIX_CHOP = 'REMIX_CHOP';

// const CREATE_NEW_CHOP_PENDING = 'CREATE_NEW_CHOP_PENDING';
// const CREATE_NEW_CHOP_FULFILLED = 'CREATE_NEW_CHOP_FULFILLED';
// const CREATE_NEW_CHOP_REJECTED = 'CREATE_NEW_CHOP_REJECTED';

const LIST_PUBLIC_CHOPS = 'LIST_PUBLIC_CHOPS';
const LIST_PUBLIC_CHOPS_PENDING = 'LIST_PUBLIC_CHOPS_PENDING';
const LIST_PUBLIC_CHOPS_REJECTED = 'LIST_PUBLIC_CHOPS_REJECTED';
const LIST_PUBLIC_CHOPS_FULFILLED = 'LIST_PUBLIC_CHOPS_FULFILLED';

const LIST_MY_CHOPS = 'LIST_MY_CHOPS';
const LIST_MY_CHOPS_PENDING = 'LIST_MY_CHOPS_PENDING';
const LIST_MY_CHOPS_REJECTED = 'LIST_MY_CHOPS_REJECTED';
const LIST_MY_CHOPS_FULFILLED = 'LIST_MY_CHOPS_FULFILLED';

const LIST_CHOPS_BY_PERSON = 'LIST_CHOPS_BY_PERSON';
const LIST_CHOPS_BY_PERSON_FULFILLED = 'LIST_CHOPS_BY_PERSON_FULFILLED';

const CHANGE_SOUND = 'CHANGE_SOUND';
const CHANGE_SOUND_TYPE = 'CHANGE_SOUND_TYPE';

const ADD_BEAT = 'ADD_BEAT';
const ADD_LINE = 'ADD_LINE';

const START_RECORDING = 'START_RECORDING';
const FINISH_RECORDING = 'FINISH_RECORDING';
const FINISH_RECORDING_PENDING = 'FINISH_RECORDING_PENDING';
const FINISH_RECORDING_FULFILLED = 'FINISH_RECORDING_FULFILLED';

const GET_CHOP_BY_ID = 'GET_CHOP_BY_ID';
const GET_CHOP_BY_ID_PENDING = 'GET_CHOP_BY_ID_PENDING';
const GET_CHOP_BY_ID_FULFILLED = 'GET_CHOP_BY_ID_FULFILLED';
const GET_CHOP_BY_ID_REJECTED = 'GET_CHOP_BY_ID_REJECTED';

const UPDATE_CHOP = 'UPDATE_CHOP';
const UPDATE_LINE = 'UPDATE_LINE';

const CHANGE_NAME = 'CHANGE_NAME';
const CHANGE_TEMPO = 'CHANGE_TEMPO';
const CHANGE_OFFSET = 'CHANGE_OFFSET';
const CHANGE_BEATS_COUNT = 'CHANGE_BEATS_COUNT';
const CHANGE_BARS_COUNT = 'CHANGE_BARS_COUNT';
const CHANGE_RECORDING_LATENCY = 'CHANGE_RECORDING_LATENCY';

const TOGGLE_MUTE_SECTION = 'TOGGLE_MUTE_SECTION';
const TOGGLE_MUTE_BEAT = 'TOGGLE_MUTE_BEAT';
const TOGGLE_MUTE_LINE = 'TOGGLE_MUTE_LINE';
const TOGGLE_SOLO_ITEM = 'SOLO_ITEM';

const DELETE_BEAT = 'DELETE_BEAT';
const DELETE_LINE = 'DELETE_LINE';

const LOAD_BEAT_AUDIO = 'LOAD_BEAT_AUDIO';
const LOAD_BEAT_AUDIO_PENDING = 'LOAD_BEAT_AUDIO_PENDING';
const LOAD_BEAT_AUDIO_FULFILLED = 'LOAD_BEAT_AUDIO_FULFILLED';

const LOAD_RECORDING_AUDIO = 'LOAD_RECORDING_AUDIO';
const LOAD_RECORDING_AUDIO_PENDING = 'LOAD_RECORDING_AUDIO_PENDING';
const LOAD_RECORDING_AUDIO_FULFILLED = 'LOAD_RECORDING_AUDIO_FULFILLED';

const SEND_COMMENT = 'SEND_COMMENT';
const SEND_COMMENT_PENDING = 'SEND_COMMENT_PENDING';
const SEND_COMMENT_FULFILLED = 'SEND_COMMENT_FULFILLED';

const CLAP = 'CLAP';
const CLAP_PENDING = 'CLAP_PENDING';

const CHOOSE_TEMPLATE = 'CHOOSE_TEMPLATE';

export const CHOP_UPDATE_ACTIONS = [
  CREATE_NEW_CHOP,
  REMIX_CHOP,
  CHANGE_SOUND,
  CHANGE_SOUND_TYPE,
  ADD_BEAT,
  ADD_LINE,
  UPDATE_CHOP,
  UPDATE_LINE,
  CHANGE_NAME,
  CHANGE_TEMPO,
  CHANGE_OFFSET,
  CHANGE_BEATS_COUNT,
  CHANGE_BARS_COUNT,
  CHANGE_RECORDING_LATENCY,
  TOGGLE_MUTE_SECTION,
  TOGGLE_MUTE_BEAT,
  TOGGLE_MUTE_LINE,
  TOGGLE_SOLO_ITEM,
  DELETE_BEAT,
  DELETE_LINE,
  FINISH_RECORDING_FULFILLED,
  CHOOSE_TEMPLATE,
];

const FIRST_NAME = [
  'Anonymous',
  'Untitled',
  'Incognito',
  'Unnamed',
  'Undercover',
  'Unidentified',
  'Nameless',
];

const LAST_NAME = [
  'groove',
  'shed',
  'woogie',
  'changes',
  'goof',
  'heat',
  'plate',
  'mix',
  'jam',
  `noodlin'`,
  'scat',
  'swing',
  'cassette',
];

function createChop({ chopId, type, composedBy }) {
  const firstIndex = Math.floor(Math.random() * FIRST_NAME.length);
  const lastIndex = Math.floor(Math.random() * LAST_NAME.length);

  return {
    chopId,
    name: `${FIRST_NAME[firstIndex]} ${LAST_NAME[lastIndex]}`,
    description: ``,
    commonBeatsCount: 0,
    commonBeatsTempo: DEFAULT_TEMPO,
    beats: [],
    lines: [],
    recordings: [],
    soloItem: null,
    activeSoundType: 'oscilator',
    activeKitId: undefined,
    isDiscoverable: true,
    createdAt: Date.now(),
    composedBy,
  };
}

// Action matchers
export const match = {
  isCreateNewChop: action => {
    return action.type === CREATE_NEW_CHOP;
  },

  isRemixChop: action => {
    return action.type === REMIX_CHOP;
  },

  isListPublicChopsPending: ({ type }) => {
    return type === LIST_PUBLIC_CHOPS_PENDING;
  },

  isListPublicChopsRejected: ({ type }) => {
    return type === LIST_PUBLIC_CHOPS_REJECTED;
  },

  isListPublicChopsFulfilled: action => {
    return action.type === LIST_PUBLIC_CHOPS_FULFILLED;
  },

  isListMyChopsPending: action => {
    return action.type === LIST_MY_CHOPS_PENDING;
  },

  isListMyChopsRejected: action => {
    return action.type === LIST_MY_CHOPS_REJECTED;
  },

  isListMyChopsFulfilled: action => {
    return action.type === LIST_MY_CHOPS_FULFILLED;
  },

  isListChopsByPersonFulfilled: ({ type }) =>
    type === LIST_CHOPS_BY_PERSON_FULFILLED,

  isChangeName: ({ type }) => type === CHANGE_NAME,

  isChangeSound: ({ type }) => type === CHANGE_SOUND,

  isChangeSoundType: ({ type }) => type === CHANGE_SOUND_TYPE,

  isAddBeat: ({ type }) => type === ADD_BEAT,
  isAddLine: ({ type }) => type === ADD_LINE,

  isStartRecording: ({ type }) => type === START_RECORDING,
  isFinishRecordingPending: ({ type }) =>
    type === FINISH_RECORDING_PENDING,
  isFinishRecordingFulfilled: ({ type }) =>
    type === FINISH_RECORDING_FULFILLED,

  isChangeTempo: ({ type }) => type === CHANGE_TEMPO,
  isChangeBarsCount: ({ type }) => type === CHANGE_BARS_COUNT,

  isGetChopByIdPending: ({ type }) => type === GET_CHOP_BY_ID_PENDING,
  isGetChopByIdFulfilled: ({ type }) =>
    type === GET_CHOP_BY_ID_FULFILLED,
  isGetChopByIdRejected: ({ type }) => type === GET_CHOP_BY_ID_REJECTED,

  isUpdateChop: ({ type }) => type === UPDATE_CHOP,
  isUpdateLine: ({ type }) => type === UPDATE_LINE,

  isChangeOffset: ({ type }) => type === CHANGE_OFFSET,
  isChangeBeatsCount: ({ type }) => type === CHANGE_BEATS_COUNT,
  isChangeRecordingLatency: ({ type }) =>
    type === CHANGE_RECORDING_LATENCY,

  isToggleMuteSection: ({ type }) => type === TOGGLE_MUTE_SECTION,
  isToggleMuteBeat: ({ type }) => type === TOGGLE_MUTE_BEAT,
  isToggleMuteLine: ({ type }) => type === TOGGLE_MUTE_LINE,
  isToggleSoloItem: ({ type }) => type === TOGGLE_SOLO_ITEM,

  isDeleteBeat: ({ type }) => type === DELETE_BEAT,
  isDeleteLine: ({ type }) => type === DELETE_LINE,

  isLoadBeatAudioPending: ({ type }) =>
    type === LOAD_BEAT_AUDIO_PENDING,
  isLoadBeatAudioFulfilled: ({ type }) =>
    type === LOAD_BEAT_AUDIO_FULFILLED,

  isLoadRecordingAudioPending: ({ type }) =>
    type === LOAD_RECORDING_AUDIO_PENDING,
  isLoadRecordingAudioFulfilled: ({ type }) =>
    type === LOAD_RECORDING_AUDIO_FULFILLED,

  isSendCommentPending: ({ type }) => type === SEND_COMMENT_PENDING,
  isSendCommentFulfilled: ({ type }) => type === SEND_COMMENT_FULFILLED,

  isClapPending: ({ type }) => type === CLAP_PENDING,
  isChooseTemplate: ({ type }) => type === CHOOSE_TEMPLATE,
};

// Action creators
export const run = {
  listPublicChops: (_, dispatch) => {
    dispatch({
      type: LIST_PUBLIC_CHOPS,
      payload: storage.listPublicChops(),
    });
  },

  listMyChops: (_, dispatch) => {
    dispatch({
      type: LIST_MY_CHOPS,
      payload: storage.listMyChops(),
    });
  },

  listChopsByPerson: ({ composedBy }, dispatch) => {
    dispatch({
      type: LIST_CHOPS_BY_PERSON,
      payload: storage.listChopsByPerson({ composedBy }),
    });
  },

  createNewChop: ({ chopId, type, composedBy }, dispatch) => {
    const chop = createChop({ chopId, type, composedBy });
    return dispatch({
      type: CREATE_NEW_CHOP,
      chopId,
      chop,
    });
  },

  remixChop: ({ chopId, remixOf, composedBy }, dispatch) => {
    return dispatch({
      type: REMIX_CHOP,
      chopId,
      remixOf,
      composedBy,
    });
  },

  getChopById: ({ chopId }, dispatch) => {
    return dispatch({
      type: GET_CHOP_BY_ID,
      payload: storage.getChopById({ chopId }),
    });
  },

  updateChop: ({ chopId, update }, dispatch) => {
    return dispatch({
      type: UPDATE_CHOP,
      chopId,
      update,
    });
  },

  updateLine: ({ chopId, lineId, update }, dispatch) => {
    return dispatch({
      type: UPDATE_LINE,
      chopId,
      lineId,
      update,
    });
  },

  changeName: ({ chopId, name }, dispatch) => {
    dispatch({
      type: CHANGE_NAME,
      chopId,
      name,
    });
  },

  changeOffset: ({ chopId, beatId, offset }, dispatch) => {
    return dispatch({
      type: CHANGE_OFFSET,
      chopId,
      beatId,
      offset,
    });
  },

  changeBeatsCount: ({ chopId, beatId, beatsCount }, dispatch) => {
    return dispatch({
      type: CHANGE_BEATS_COUNT,
      chopId,
      beatId,
      beatsCount,
    });
  },

  changeRecordingLatency: (
    { chopId, recordingId, latency },
    dispatch
  ) => {
    return dispatch({
      type: CHANGE_RECORDING_LATENCY,
      chopId,
      recordingId,
      latency,
    });
  },

  addBeat: ({ chopId, beat }, dispatch) => {
    return dispatch({
      type: ADD_BEAT,
      chopId,
      beat,
    });
  },

  addLine: ({ chopId, line, writingRecordingId }, dispatch) => {
    return dispatch({
      type: ADD_LINE,
      chopId,
      line,
      writingRecordingId,
    });
  },

  startRecording: (
    { chopId, lineId, recording, audioData },
    dispatch
  ) => {
    return dispatch({
      type: START_RECORDING,
      chopId,
      lineId,
      recording,
      audioData,
    });
  },

  finishRecording: (
    { chopId, lineId, recordingId, recording, audioData },
    dispatch
  ) => {
    return dispatch({
      type: FINISH_RECORDING,
      payload: {
        promise: FileStorage.uploadRecording({
          recording,
          audioData,
        }).then(recording => ({
          chopId,
          lineId,
          recordingId,
          recording,
        })),
        data: {
          chopId,
          lineId,
          recordingId,
          recording,
        },
      },
    });
  },

  deleteBeat: ({ chopId, beatId }, dispatch) => {
    return dispatch({
      type: DELETE_BEAT,
      chopId,
      beatId,
    });
  },

  deleteLine: ({ chopId, lineId }, dispatch) => {
    return dispatch({
      type: DELETE_LINE,
      chopId,
      lineId,
    });
  },

  changeSound: ({ chopId, beatId, sound }, dispatch) => {
    return dispatch({
      type: CHANGE_SOUND,
      chopId,
      beatId,
      sound,
    });
  },

  changeSoundType: ({ chopId, soundType, kitId }, dispatch) => {
    return dispatch({
      type: CHANGE_SOUND_TYPE,
      chopId,
      soundType,
      kitId,
    });
  },

  changeTempo: ({ chopId, beatId, tempo }, dispatch) => {
    return dispatch({
      type: CHANGE_TEMPO,
      chopId,
      beatId,
      tempo,
    });
  },

  changeBarsCount: ({ chopId, barsCount }, dispatch) => {
    return dispatch({
      type: CHANGE_BARS_COUNT,
      chopId,
      barsCount,
    });
  },

  toggleMuteSection: ({ chopId, beatId, sectionNumber }, dispatch) => {
    return dispatch({
      type: TOGGLE_MUTE_SECTION,
      chopId,
      beatId,
      sectionNumber,
    });
  },

  toggleMuteBeat: ({ chopId, beatId }, dispatch) => {
    return dispatch({
      type: TOGGLE_MUTE_BEAT,
      chopId,
      beatId,
    });
  },

  toggleMuteLine: ({ chopId, lineId }, dispatch) => {
    return dispatch({
      type: TOGGLE_MUTE_LINE,
      chopId,
      lineId,
    });
  },

  toggleSoloItem: ({ chopId, lineId, beatId, itemType }, dispatch) => {
    return dispatch({
      type: TOGGLE_SOLO_ITEM,
      chopId,
      lineId,
      beatId,
      itemType,
    });
  },

  loadBeatAudio: ({ soundId, url }, dispatch) => {
    return dispatch({
      type: LOAD_BEAT_AUDIO,
      payload: {
        promise: FileStorage.loadBeatAudio({ soundId, url }),
        data: { soundId },
      },
    });
  },

  loadRecordingAudio: ({ recordingId, recording }, dispatch) => {
    const url = recording.sound.url;

    if (!url) {
      console.warn(`Can't load recording without an url`, recording);
    }

    return dispatch({
      type: LOAD_RECORDING_AUDIO,
      payload: {
        promise: FileStorage.loadRecordingAudio({ recordingId, url }),
        data: { recordingId },
      },
    });
  },

  sendComment({ text, chopId, personId }, dispatch) {
    const comment = {
      commentId: nanoid(),
      createdAt: Date.now(),
      text,
      chopId,
      personId,
    };

    return dispatch({
      type: SEND_COMMENT,
      payload: {
        promise: storage.saveComment(comment),
        data: comment,
      },
    });
  },

  clap({ chopId, personId, clapsCount }, dispatch) {
    const createdAt = Date.now();
    const clapEvent = {
      clapId: nanoid(),
      chopId,
      personId,
      clapsCount,
      createdAt,
    };

    return dispatch({
      type: CLAP,
      payload: {
        promise: storage.clap(clapEvent),
        data: clapEvent,
      },
    });
  },

  chooseTemplate({ chopId, template }, dispatch) {
    return dispatch({
      type: CHOOSE_TEMPLATE,
      chopId,
      template,
    });
  },
};

// Selectors
const chopsSelector = state => state.chops;
const publicChopsIdsSelector = state => state.publicChopsIds;

const chopIdFromMatchSelector = (_, { match }) => match.params.chopId;
const chopIdSelector = (_, { chopId }) => chopId;

const beatsAudioSelector = state => state.beatsAudio;
const recordingsAudioSelector = state => state.recordingsAudio;

const commentsSelector = state => state.commentsByChopId;

const getMyAccountFromPropsSelector = (_, { myAccount }) => myAccount;

const commentsWithPeopleSelector = createSelector(
  commentsSelector,
  people.select.listPeople,
  (commentsByChopId, people) => {
    for (let chopId of commentsByChopId.keys()) {
      commentsByChopId = commentsByChopId.update(
        chopId,
        commentsList => {
          return commentsList.map(comment => {
            return {
              ...comment,
              person: people.get(comment.personId),
            };
          });
        }
      );
    }

    return commentsByChopId;
  }
);

const getMyClapsMap = state => state.myClapsByChopId;

const mapSocialToChop = ({
  chop,
  commentsMap,
  myClapsMap,
  chops,
  peopleMap,
}) => {
  const myClaps = myClapsMap.get(chop.chopId, 0);
  const clapsCount = (chop.clapsCount || 0) + myClaps;

  const comments = commentsMap
    .get(chop.chopId, Map())
    .toList()
    .sort(sortNewestLast)
    .toJS();

  if (chop.remixOf && !chop.remixOfChop) {
    let remixOfChop = {
      ...chops[chop.remixOf],
    };

    remixOfChop.composer =
      remixOfChop.composer || peopleMap.get(remixOfChop.composedBy);

    chop = {
      ...chop,
      remixOfChop,
    };
  }

  if (!chop.composer) {
    chop = {
      ...chop,
      composer: peopleMap.get(chop.composedBy),
    };
  }

  return {
    ...chop,
    comments,
    clapsCount,
    hasMyClaps: chop.hasMyClaps || myClaps > 0,
  };
};

const listMyChopsSelector = createSelector(
  chopsSelector,
  people.select.getMyself,
  people.select.listPeople,
  commentsWithPeopleSelector,
  getMyClapsMap,
  (chops, me, peopleMap, commentsMap, myClapsMap) => {
    let chopsList = [];
    for (let chopId of Object.keys(chops)) {
      let chop = chops[chopId];

      if (me && chop.composedBy !== me.personId) {
        continue;
      }

      chop = mapSocialToChop({
        chop,
        myClapsMap,
        commentsMap,
        chops,
        peopleMap,
      });

      chopsList.push(chop);
    }

    return chopsList.sort((c1, c2) => c2.createdAt - c1.createdAt);
  }
);

const sortNewestLast = (a1, a2) => {
  return a1.createdAt - a2.createdAt;
};

const getMyChopsCountSelector = createSelector(
  chopsSelector,
  people.select.getMyself,
  (chops, myself) => {
    let chopsCount = 0;

    for (let chopId of Object.keys(chops)) {
      let chop = chops[chopId];
      if (myself && chop.composedBy === myself.personId) {
        chopsCount++;
      }
    }

    return chopsCount;
  }
);

export const select = {
  getChopByIdFromMatch: createSelector(
    chopsSelector,
    chopIdFromMatchSelector,
    people.select.listPeople,
    commentsWithPeopleSelector,
    getMyClapsMap,
    (chops, chopId, peopleMap, commentsMap, myClapsMap) => {
      let chop = chops[chopId];

      if (!chop) {
        return chop;
      }

      return mapSocialToChop({
        chop,
        myClapsMap,
        commentsMap,
        chops,
        peopleMap,
      });
    }
  ),

  getChopById: createSelector(
    chopsSelector,
    chopIdSelector,
    (chops, chopId) => chops[chopId]
  ),

  listMyRecentChops: createSelector(listMyChopsSelector, chops => {
    return chops.slice(0, 2);
  }),

  listPublicChops: createSelector(
    publicChopsIdsSelector,
    chopsSelector,
    commentsWithPeopleSelector,
    getMyClapsMap,
    people.select.listPeople,
    (publicChopsIds, chops, commentsMap, myClapsMap, peopleMap) => {
      return publicChopsIds
        .toJS()
        .map(id => {
          let chop = { ...chops[id] };
          return mapSocialToChop({
            chop,
            myClapsMap,
            commentsMap,
            peopleMap,
            chops,
          });
        })
        .sort((c1, c2) => c2.createdAt - c1.createdAt);
    }
  ),

  canCreateChop: createSelector(
    getMyChopsCountSelector,
    getMyAccountFromPropsSelector,
    (chopsCount, myAccount) => {
      return true;
      // return (
      //   chopsCount < 3 ||
      //   (myAccount &&
      //     myAccount.activeSubscription &&
      //     myAccount.activeSubscription.planId)
      // );
    }
  ),

  listMyChops: listMyChopsSelector,

  isListMyChopsPending: state => state.isListMyChopsPending,
  isListPublicChopsPending: state => state.isListPublicChopsPending,

  listChopsByPersonFromMatch: createSelector(
    people.personIdFromMatchSelector,
    chopsSelector,
    commentsWithPeopleSelector,
    getMyClapsMap,
    people.select.listPeople,
    (personId, chops, commentsMap, myClapsMap, peopleMap) => {
      return Object.values(chops)
        .filter(c => c.composedBy === personId)
        .sort((c1, c2) => c2.createdAt - c1.createdAt)
        .map(chop => {
          return mapSocialToChop({
            chop,
            commentsMap,
            myClapsMap,
            chops,
            peopleMap,
          });
        });
    }
  ),

  getBeatsAudioMap: createSelector(beatsAudioSelector, beatsAudio =>
    beatsAudio.toJS()
  ),

  getRecordingsAudioMap: createSelector(
    recordingsAudioSelector,
    recordingsAudio => recordingsAudio.toJS()
  ),
};
