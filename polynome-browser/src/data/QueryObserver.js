import { Component } from 'react';
import shouldComponentUpdate from '../shouldComponentUpdate';

export default class QueryObserver extends Component {
  constructor() {
    super();
    this.state = {
      loading: true,
      error: null,
      data: null,
    };

    this.shouldComponentUpdate = shouldComponentUpdate;

    this._runQuery = async () => {
      this.setState({
        loading: true,
        error: null,
        data: null,
      });

      try {
        let result = await this.props.client.query({
          query: this.props.query,
        });

        if (!this._mounted) {
          return;
        }

        this.setState({
          loading: false,
          error: null,
          data: result.data,
        });
      } catch (e) {
        if (!this._mounted) {
          return;
        }

        this.setState({
          loading: false,
          error: e,
          data: null,
        });
      }
    };

    this._startWatchingQuery = () => {
      this._watcher = this.props.client
        .watchQuery({
          query: this.props.query,
          fetchPolicy: 'cache-only',
        })
        .subscribe(result =>
          this.setState({
            data: result.data,
          })
        );
    };

    this._stopWatchingQuery = () => {
      this._watcher.unsubscribe();
    };
  }

  componentDidMount() {
    this._mounted = true;
    this._startWatchingQuery();
    this._runQuery();
  }

  componentWillUnmount() {
    this._mounted = false;
    this._stopWatchingQuery();
  }

  render() {
    return this.props.children(this.state);
  }
}
