import AudioContext from '../AudioContext';
import getFullUrl from '../getFullUrl';
import Emitter from 'tiny-emitter';
import nanoid from 'nanoid';
require('formdata-polyfill');

const emitter = new Emitter();
const audioContext = new AudioContext();

const uploadWavWorker = new window.Worker(
  `${process.env.PUBLIC_URL}/fileStorageWorker.js`
);

uploadWavWorker.onmessage = e => {
  emitter.emit('result', {
    ...e.data,
  });
};

const CURRENT_TOKEN = 'poly/token';
function getCurrentToken() {
  return localStorage.getItem(CURRENT_TOKEN) || null;
}

function getHeaders() {
  let headers = {};
  let token = getCurrentToken();

  if (token) {
    headers = {
      ...headers,
      Authorization: token,
    };
  }

  return headers;
}

const FileStorage = {
  uploadRecording: ({ recording, audioData }) => {
    return new Promise((resolve, reject) => {
      const requestId = nanoid();
      const filename = `${recording.recordingId}.wav`;
      const url = getFullUrl(`/api/recordings/${filename}`);

      const onResult = data => {
        if (data.requestId !== requestId) {
          return;
        }

        emitter.off('result', onResult);
        recording = {
          ...recording,
          sound: {
            ...recording.sound,
            url: data.response.data.url,
          },
        };

        resolve(recording);
      };

      emitter.on('result', onResult);

      uploadWavWorker.postMessage({
        samples: audioData.getChannelData(0),
        sampleRate: audioData.sampleRate,
        url,
        filename,
        requestId,
        token: getCurrentToken(),
      });
    });
  },

  async changeUserpic({ personId, file }) {
    const formData = new FormData();
    formData.append('userpic', file);
    const result = await fetch(`/api/people/${personId}/userpic`, {
      method: 'POST',
      body: formData,
      headers: getHeaders(),
    });

    const json = await result.json();

    if (!json.data.ok) {
      let errorMessage = 'Server error';
      const { errors } = json.data;
      if (errors && errors[0] && errors[0].message) {
        errorMessage = errors[0].message;
      }
      throw new Error(errorMessage);
    }

    return json.data.person;
  },

  loadBeatAudio({ soundId, url }) {
    // TODO: when to URL, when to Path ?
    const fullUrl = getFullUrl(
      `${process.env.REACT_APP_POLYNOME_API_PATH}${url}`
    );
    return fetch(fullUrl)
      .then(response => {
        return response.arrayBuffer();
      })
      .then(rawData => {
        return new Promise((resolve, reject) => {
          audioContext.decodeAudioData(rawData, resolve, reject);
        });
      })
      .then(audioData => {
        return {
          soundId,
          audioData,
        };
      });
  },

  loadRecordingAudio({ recordingId, url }) {
    return fetch(url)
      .then(response => {
        return response.arrayBuffer();
      })
      .then(rawData => {
        return new Promise((resolve, reject) => {
          audioContext.decodeAudioData(rawData, resolve, reject);
        });
      })
      .then(audioData => {
        return {
          recordingId,
          audioData,
        };
      });
  },
};

export default FileStorage;
