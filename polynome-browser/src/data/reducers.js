import { Map, Set } from 'immutable';
import * as trackUtils from '../trackUtils';
import nanoid from 'nanoid';

import isEqual from 'lodash/isEqual';
import isMatch from 'lodash/isMatch';
import pick from 'lodash/pick';
import cloneDeep from 'lodash/cloneDeep';

import * as chops from './chops';
import * as mediaDevices from './mediaDevices';
import * as kits from './kits';
import * as people from './people';
import * as alerts from './alerts';

export const DEFAULT_STATE = {
  me: null,
  chops: {},
  lastChopIdComposed: null,
  selectedInputDeviceId: null,
  mediaDevices: [],
  kits: [],
  publicChopsIds: new Set(),
  beatsAudio: new Map(),
  recordingsAudio: new Map(),
  commentsByChopId: new Map(),
  myClapsByChopId: new Map(),
  people: new Map(),
  alerts: [],
  defaultLatencyByDeviceId: new Map(),
  isListMyChopsPending: false,
  listMyChopsError: null,
  isListPublicChopsPending: false,
  listPublicChopsError: null,
};

function getDefaultKitId({ kits }) {
  if (!kits || !kits.length) {
    throw new Error('No kits fetched');
  }

  let kitId = kits[0].id;

  return kits.reduce((a, kit) => {
    if (kit.isDefault) {
      return kit.id;
    }

    return a;
  }, kitId);
}

function updateElements(arr, query, updateFn) {
  return arr.map(el => {
    if (isMatch(el, query)) {
      return updateFn(el);
    }

    return el;
  });
}

function getChopById(state, chopId) {
  return state.chops[chopId];
}

function updateChopById(state, chopId, updateFn) {
  const chop = state.chops[chopId];

  if (!chop) {
    throw new Error(
      `Can't update chop, it doesn't exist, chopId: ${chopId}`
    );
  }

  return {
    ...state,
    chops: {
      ...state.chops,
      [chopId]: updateFn(chop),
    },
  };
}

function addChop(state, chop) {
  if (chop.comments) {
    for (let comment of chop.comments) {
      state = addComment(state, comment);
    }
  }

  if (chop.remixOfChop && !getChopById(state, chop.remixOf)) {
    state = addChop(state, chop.remixOfChop);
  }

  return {
    ...state,
    lastChopIdComposed: chop.chopId,
    chops: {
      ...state.chops,
      [chop.chopId]: chop,
    },
  };
}

function addBeatAudio(state, audioObj) {
  const { beatsAudio } = state;
  return {
    ...state,
    beatsAudio: beatsAudio.set(audioObj.soundId, audioObj),
  };
}

function getBeatAudioById(state, soundId) {
  return state.beatsAudio.get(soundId);
}

function updateBeatAudio(state, soundId, updateFn) {
  const { beatsAudio } = state;
  return {
    ...state,
    beatsAudio: beatsAudio.update(soundId, updateFn),
  };
}

function addRecordingAudio(state, audioObj) {
  const { recordingsAudio } = state;
  return {
    ...state,
    recordingsAudio: recordingsAudio.set(
      audioObj.recordingId,
      audioObj
    ),
  };
}

function getRecordingAudioById(state, recordingId) {
  return state.recordingsAudio.get(recordingId);
}

function updateRecordingAudio(state, recordingId, updateFn) {
  const { recordingsAudio } = state;
  return {
    ...state,
    recordingsAudio: recordingsAudio.update(recordingId, updateFn),
  };
}

function addComment(state, comment) {
  if (!comment || !comment.chopId) {
    throw new Error(`Comment is invalid`, comment);
  }

  state = {
    ...state,
    commentsByChopId: state.commentsByChopId.update(
      comment.chopId,
      Map(),
      comments => comments.set(comment.commentId, comment)
    ),
  };

  if (comment.person) {
    state = addPerson(state, comment.person);
  }

  return state;
}

function addClap(state, clapEvent) {
  return {
    ...state,
    myClapsByChopId: state.myClapsByChopId.update(
      clapEvent.chopId,
      0,
      c => c + clapEvent.clapsCount
    ),
  };
}

function addPerson(state, person) {
  return {
    ...state,
    people: state.people.set(person.personId, person),
  };
}

export default function root(state, action) {
  if (chops.match.isCreateNewChop(action)) {
    const { chop } = action;

    if (!chop.chopId) {
      throw new Error('Chop with no id');
    }

    return addChop(state, chop);
  }

  if (chops.match.isRemixChop(action)) {
    const { chopId, remixOf, composedBy } = action;
    const chop = getChopById(state, remixOf);
    const chopBasicInfo = cloneDeep(
      pick(chop, [
        'beats',
        'lines',
        'commonBeatsCount',
        'commonBeatsTempo',
        'name',
        'description',
        'isDiscoverable',
        'recordings',
        'activeSoundType',
        'activeKitId',
        'soloItem',
      ])
    );

    chopBasicInfo.beats = chopBasicInfo.beats.map(b => {
      return {
        ...b,
        id: nanoid(),
      };
    });

    chopBasicInfo.lines = chopBasicInfo.lines.map(l => {
      return {
        ...l,
        lineId: nanoid(),
      };
    });

    const newChop = {
      ...chopBasicInfo,
      chopId,
      remixOf,
      composedBy,
      createdAt: Date.now(),
    };

    return addChop(state, newChop);
  }

  if (chops.match.isGetChopByIdFulfilled(action)) {
    if (!action.payload) {
      throw new Error(`Empty chop ${JSON.stringify(action)}`);
    }

    return addChop(state, action.payload);
  }

  if (chops.match.isChangeName(action)) {
    const { chopId, name } = action;

    return updateChopById(state, chopId, c => {
      return {
        ...c,
        name,
      };
    });
  }

  if (chops.match.isChangeOffset(action)) {
    const { chopId, beatId, offset } = action;
    const chop = getChopById(state, chopId);
    const beats = updateElements(chop.beats, { id: beatId }, b => {
      return {
        ...b,
        offset,
      };
    });

    return updateChopById(state, chopId, c => {
      return {
        ...c,
        beats,
      };
    });
  }

  if (chops.match.isChangeBeatsCount(action)) {
    const { chopId, beatId, beatsCount } = action;
    const chop = getChopById(state, chopId);
    const { commonBeatsCount, commonBeatsTempo, beats } = chop;

    const trackBeatsCount = trackUtils.getTrackBeatsCount({
      commonBeatsCount,
      beats,
    });
    const cyclesNum = Math.ceil(trackBeatsCount / commonBeatsCount);

    let newBeats = updateElements(beats, { id: beatId }, b => {
      return {
        ...b,
        beatsCount,
        lineDuration: beatsCount * cyclesNum,
      };
    });

    const newCommonBeatsCount = trackUtils.calculateCommonBeatsCount(
      newBeats
    );
    const maxOffset = newCommonBeatsCount / beatsCount - 1;
    const newCommonBeatsTempo = trackUtils.calculateNewTempo({
      oldTempo: commonBeatsTempo,
      oldBeatsCount: commonBeatsCount,
      oldBeats: beats,
      newBeatsCount: newCommonBeatsCount,
      newBeats,
    });

    newBeats = updateElements(newBeats, { id: beatId }, b => {
      return {
        ...b,
        offset: Math.min(b.offset || 0, maxOffset),
      };
    });

    return updateChopById(state, chopId, c => {
      return {
        ...c,
        beats: newBeats.sort(trackUtils.beatsSortFn),
        commonBeatsCount: newCommonBeatsCount,
        commonBeatsTempo: newCommonBeatsTempo,
      };
    });
  }

  if (chops.match.isToggleMuteSection(action)) {
    const { chopId, beatId, sectionNumber } = action;
    return updateChopById(state, chopId, c => {
      return {
        ...c,
        beats: updateElements(c.beats, { id: beatId }, b => {
          let muted = [];

          if (b.mutedBeats) {
            muted = b.mutedBeats.slice();
          }

          const index = muted.indexOf(sectionNumber);

          if (index !== -1) {
            muted.splice(index, 1);
          }

          if (index === -1) {
            muted.push(sectionNumber);
          }

          return {
            ...b,
            mutedBeats: muted,
          };
        }),
      };
    });
  }

  if (chops.match.isToggleMuteBeat(action)) {
    const { chopId, beatId } = action;
    return updateChopById(state, chopId, c => {
      return {
        ...c,
        beats: updateElements(c.beats, { id: beatId }, b => {
          return {
            ...b,
            isMute: !b.isMute,
          };
        }),
      };
    });
  }

  if (chops.match.isToggleMuteLine(action)) {
    const { chopId, lineId } = action;
    return updateChopById(state, chopId, c => {
      return {
        ...c,
        lines: updateElements(c.lines, { lineId: lineId }, l => ({
          ...l,
          isMute: !l.isMute,
        })),
      };
    });
  }

  if (chops.match.isToggleSoloItem(action)) {
    const { chopId, lineId, beatId, itemType } = action;
    const chop = getChopById(state, chopId);

    let soloItem;

    if (itemType === 'beat') {
      soloItem = {
        itemType,
        beatId,
      };
    }

    if (itemType === 'line') {
      soloItem = {
        itemType,
        lineId,
      };
    }

    let matchesCurrent = isEqual(chop.soloItem, soloItem);

    if (matchesCurrent) {
      return updateChopById(state, chopId, c => {
        return {
          ...c,
          soloItem: null,
        };
      });
    }

    return updateChopById(state, chopId, c => {
      return {
        ...c,
        soloItem,
      };
    });
  }

  if (chops.match.isAddBeat(action)) {
    let { beat, chopId } = action;
    const { kits } = state;
    const chop = getChopById(state, chopId);
    let {
      beats,
      commonBeatsCount,
      commonBeatsTempo,
      activeSoundType,
      activeKitId,
    } = chop;

    const trackBeatsCount = trackUtils.getTrackBeatsCount({
      commonBeatsCount,
      beats,
    });

    beat = {
      ...beat,
      sound: {
        isAuto: true,
      },
      lineStart: 0,
      lineDuration:
        beat.beatsCount * (trackBeatsCount / (commonBeatsCount || 1)),
    };

    let newBeats = beats
      .concat([beat])
      .sort(trackUtils.beatsSortFn)
      .map(b => {
        return {
          ...b,
        };
      });

    newBeats = trackUtils.mapSoundToBeats({
      soundType: activeSoundType,
      kitId: activeKitId,
      beats: newBeats,
      kits,
    });

    const newCommonBeatsCount = trackUtils.calculateCommonBeatsCount(
      newBeats
    );

    const newTempo = trackUtils.calculateNewTempo({
      oldTempo: commonBeatsTempo,
      oldBeatsCount: commonBeatsCount,
      oldBeats: beats,
      newBeatsCount: newCommonBeatsCount,
      newBeats: newBeats,
    });

    return updateChopById(state, chopId, c => {
      return {
        ...c,
        commonBeatsCount: newCommonBeatsCount,
        commonBeatsTempo: newTempo,
        beats: newBeats,
      };
    });
  }

  if (chops.match.isAddLine(action)) {
    const { chopId, line, writingRecordingId } = action;
    const chop = getChopById(state, chopId);
    const { recordings } = chop;

    let newRecordings = recordings;

    if (writingRecordingId) {
      newRecordings = updateElements(
        recordings,
        { recordingId: writingRecordingId },
        r => ({ ...r, lineId: line.lineId })
      );
    }

    return updateChopById(state, chopId, c => {
      return {
        ...c,
        lines: c.lines.concat([line]),
        recordings: newRecordings,
      };
    });
  }

  if (chops.match.isStartRecording(action)) {
    const { chopId, recording, lineId, audioData } = action;
    const chop = getChopById(state, chopId);
    let { recordingsAudio } = state;

    const lines = updateElements(chop.lines, { lineId }, l => {
      return {
        ...l,
        writingRecordingId: recording.recordingId,
      };
    });

    const activeRecordings = lines.map(l => {
      return l.playingRecordingId;
    });

    let recordings = chop.recordings.concat([
      {
        ...recording,
        recordingStatus: 'recording',
      },
    ]);

    const RECORDINGS_SOFT_LIMIT = 20;
    if (recordingsAudio.size > RECORDINGS_SOFT_LIMIT) {
      let purged = 0;

      for (let recordingId of recordingsAudio.keys()) {
        if (purged >= 5) {
          break;
        }

        if (
          recordingId === recording.recordingId ||
          activeRecordings.indexOf(recordingId) !== -1
        ) {
          continue;
        }

        recordingsAudio = recordingsAudio.delete(recordingId);
        purged += 1;
      }
    }

    state = updateChopById(state, chopId, c => ({
      ...c,
      recordings,
      lines,
    }));

    state = {
      ...state,
      recordingsAudio,
    };

    return addRecordingAudio(state, {
      recordingId: recording.recordingId,
      audioData,
      recordingStatus: 'recording',
    });
  }

  if (chops.match.isFinishRecordingPending(action)) {
    const { chopId, lineId, recordingId } = action.payload;

    state = updateChopById(state, chopId, c => {
      return {
        ...c,
        lines: updateElements(c.lines, {}, l => {
          let { playingRecordingId } = l;
          if (l.lineId === lineId) {
            playingRecordingId = recordingId;
          }

          return {
            ...l,
            writingRecordingId: null,
            playingRecordingId,
          };
        }),
        recordings: updateElements(c.recordings, { recordingId }, r => {
          return {
            ...r,
            recordingStatus: 'ready',
          };
        }),
      };
    });

    return updateRecordingAudio(state, recordingId, a => {
      return {
        ...a,
        recordingStatus: 'ready',
      };
    });
  }

  if (chops.match.isFinishRecordingFulfilled(action)) {
    const { chopId, recordingId, recording } = action.payload;
    state = updateChopById(state, chopId, c => {
      return {
        ...c,
        recordings: updateElements(c.recordings, { recordingId }, r => {
          return {
            ...r,
            sound: {
              ...r.sound,
              url: recording.sound.url,
            },
          };
        }),
      };
    });
  }

  if (chops.match.isDeleteBeat(action)) {
    const { chopId, beatId } = action;
    const chop = getChopById(state, chopId);
    const { beats, commonBeatsCount, commonBeatsTempo } = chop;
    let { soloItem } = chop;

    let newBeats = beats.filter(b => b.id !== beatId);

    if (soloItem && soloItem.beatId === beatId) {
      soloItem = null;
    }

    if (newBeats.length === 0) {
      return updateChopById(state, chopId, c => ({
        ...c,
        beats: newBeats,
        commonBeatsCount: 1,
        commonBeatsTempo: 120,
        soloItem,
      }));
    }

    let newCommonBeatsCount = trackUtils.calculateCommonBeatsCount(
      newBeats
    );

    let newCommonBeatsTempo = trackUtils.calculateNewTempo({
      oldTempo: commonBeatsTempo,
      oldBeatsCount: commonBeatsCount,
      oldBeats: beats,
      newBeatsCount: newCommonBeatsCount,
      newBeats,
    });

    return updateChopById(state, chopId, c => ({
      ...c,
      beats: newBeats,
      commonBeatsCount: newCommonBeatsCount,
      commonBeatsTempo: newCommonBeatsTempo,
      soloItem,
    }));
  }

  if (chops.match.isChangeBarsCount(action)) {
    const { chopId, barsCount } = action;
    if (barsCount <= 0) {
      return state;
    }

    const chop = getChopById(state, chopId);

    const { beats, lines, commonBeatsCount } = chop;

    let newBeats = updateElements(beats, {}, b => {
      return {
        ...b,
        lineDuration: barsCount * b.beatsCount,
      };
    });

    const trackBeatsCount = trackUtils.getTrackBeatsCount({
      commonBeatsCount,
      beats: newBeats,
    });

    let newLines = updateElements(lines, {}, l => {
      const lineDuration = trackBeatsCount / commonBeatsCount;
      return {
        ...l,
        lineDuration,
      };
    });

    return updateChopById(state, chopId, c => {
      return {
        ...c,
        beats: newBeats,
        lines: newLines,
      };
    });
  }

  if (chops.match.isDeleteLine(action)) {
    const chopId = action.chopId;
    const lineId = action.lineId;
    let { soloItem } = getChopById(state, chopId);

    if (soloItem && soloItem.lineId === lineId) {
      soloItem = null;
    }

    return updateChopById(state, chopId, c => ({
      ...c,
      lines: c.lines.filter(l => l.lineId !== lineId),
      soloItem,
    }));
  }

  if (chops.match.isUpdateChop(action)) {
    const { chopId, update } = action;
    const updateSanitized = pick(update, [
      'isDiscoverable',
      'isRemoved',
    ]);

    return updateChopById(state, chopId, c => ({
      ...c,
      ...updateSanitized,
    }));
  }

  if (chops.match.isUpdateLine(action)) {
    const { chopId, lineId, update } = action;
    const updateSanitized = pick(update, ['playingRecordingId']);

    return updateChopById(state, chopId, c => {
      return {
        ...c,
        lines: updateElements(c.lines, { lineId }, l => {
          return {
            ...l,
            ...updateSanitized,
          };
        }),
      };
    });
  }

  if (chops.match.isChangeTempo(action)) {
    const { chopId, beatId, tempo } = action;
    const chop = getChopById(state, chopId);
    const { commonBeatsCount, beats } = chop;
    const beat = beats.find(b => b.id === beatId);
    const newTempo = commonBeatsCount * tempo / beat.beatsCount;
    return updateChopById(state, chopId, c => ({
      ...c,
      commonBeatsTempo: newTempo,
    }));
  }

  if (chops.match.isChangeRecordingLatency(action)) {
    const { chopId, recordingId, latency } = action;

    return updateChopById(state, chopId, c => {
      return {
        ...c,
        recordings: updateElements(c.recordings, { recordingId }, r => {
          return {
            ...r,
            sound: { ...r.sound, latency },
          };
        }),
      };
    });
  }

  if (chops.match.isChangeSound(action)) {
    const { sound, chopId, beatId } = action;
    const chop = getChopById(state, chopId);
    const { kits } = state;
    const { beats, activeSoundType, activeKitId } = chop;

    let newBeats = updateElements(beats, { id: beatId }, b => {
      return {
        ...b,
        sound,
      };
    });

    if (sound.isAuto) {
      newBeats = trackUtils.mapSoundToBeats({
        beats: newBeats,
        kits,
        soundType: activeSoundType,
        kitId: activeKitId,
      });
    }

    return updateChopById(state, chopId, c => ({
      ...c,
      beats: newBeats,
    }));
  }

  if (chops.match.isChangeSoundType(action)) {
    const { soundType, kitId, chopId } = action;
    const chop = getChopById(state, chopId);
    const { kits } = state;
    const { beats } = chop;

    const newBeats = trackUtils.mapSoundToBeats({
      kits,
      beats,
      soundType,
      kitId,
      allAuto: true,
    });

    return updateChopById(state, chopId, c => {
      return {
        ...c,
        beats: newBeats,
        activeSoundType: soundType,
        activeKitId: kitId || getDefaultKitId(state),
      };
    });
  }

  if (chops.match.isChooseTemplate(action)) {
    const { chopId } = action;
    const {
      beats,
      commonBeatsCount,
      commonBeatsTempo,
      activeSoundType,
      activeKitId,
    } = action.template;

    return updateChopById(state, chopId, c => {
      return {
        ...c,
        beats: beats.map(b => ({
          ...b,
          id: nanoid(),
          isMute: false,
          mutedBeats: b.mutedBeats || [],
          lineStart: 0,
          lineDuration: b.beatsCount,
          personId: state.me.personId,
        })),
        soloItem: null,
        activeSoundType,
        activeKitId,
        commonBeatsCount,
        commonBeatsTempo,
      };
    });
  }

  if (mediaDevices.match.isListMediaDevicesFulfilled(action)) {
    const { mediaDevices, defaultLatencyMap } = action.payload;
    return {
      ...state,
      mediaDevices: mediaDevices,
      defaultLatencyByDeviceId: state.defaultLatencyByDeviceId.merge(
        Map(defaultLatencyMap)
      ),
    };
  }

  if (mediaDevices.match.isChangeSelectedInputDeviceId(action)) {
    return {
      ...state,
      selectedInputDeviceId: action.deviceId,
    };
  }

  if (mediaDevices.match.isSetDefaultLatencyPending(action)) {
    const { deviceId, latency } = action.payload;
    let { defaultLatencyByDeviceId } = state;
    return {
      ...state,
      defaultLatencyByDeviceId: defaultLatencyByDeviceId.set(
        deviceId,
        latency
      ),
    };
  }

  if (kits.match.isListKitsFulfilled(action)) {
    return {
      ...state,
      kits: action.payload,
    };
  }

  if (chops.match.isListMyChopsPending(action)) {
    return {
      ...state,
      isListMyChopsPending: true,
      listMyChopsError: null,
    };
  }

  if (chops.match.isListMyChopsRejected(action)) {
    return {
      ...state,
      isListMyChopsPending: false,
      listMyChopsError: action.payload,
    };
  }

  if (chops.match.isListMyChopsFulfilled(action)) {
    for (let chop of action.payload) {
      state = addChop(state, chop);
    }

    return {
      ...state,
      isListMyChopsPending: false,
      listMyChopsError: null,
    };
  }

  if (chops.match.isListChopsByPersonFulfilled(action)) {
    for (let chop of action.payload) {
      state = addChop(state, chop);
    }

    return state;
  }

  if (chops.match.isListPublicChopsPending(action)) {
    return {
      ...state,
      isListPublicChopsPending: true,
      listPublicChopsError: null,
    };
  }

  if (chops.match.isListPublicChopsRejected(action)) {
    return {
      ...state,
      isListPublicChopsPending: false,
      listPublicChopsError: action.payload,
    };
  }

  if (chops.match.isListPublicChopsFulfilled(action)) {
    let publicChopsIds = state.publicChopsIds;

    for (let chop of action.payload) {
      state = addChop(state, chop);
      publicChopsIds = publicChopsIds.add(chop.chopId);
    }

    return {
      ...state,
      publicChopsIds,
      isListPublicChopsPending: false,
      listPublicChopsError: null,
    };
  }

  if (chops.match.isLoadBeatAudioPending(action)) {
    const { soundId } = action.payload;
    const beatsAudio = getBeatAudioById(state, soundId);

    if (beatsAudio) {
      console.warn(
        'Audio load started for the second time',
        soundId,
        new Error().stack
      );
    }

    return addBeatAudio(state, {
      soundId,
      loadStatus: 'pending',
    });
  }

  if (chops.match.isLoadBeatAudioFulfilled(action)) {
    const { soundId, audioData } = action.payload;
    return updateBeatAudio(state, soundId, a => ({
      ...a,
      loadStatus: 'ready',
      audioData,
    }));
  }

  if (chops.match.isLoadRecordingAudioPending(action)) {
    const { recordingId } = action.payload;
    const recordingAudio = getRecordingAudioById(state, recordingId);

    if (recordingAudio) {
      console.warn(
        'Audio load started for the second time, recording',
        recordingId,
        new Error().stack
      );
    }

    return addRecordingAudio(state, {
      recordingId,
      loadStatus: 'pending',
    });
  }

  if (chops.match.isLoadRecordingAudioFulfilled(action)) {
    const { recordingId, audioData } = action.payload;
    return updateRecordingAudio(state, recordingId, a => ({
      ...a,
      loadStatus: 'ready',
      audioData,
    }));
  }

  if (chops.match.isSendCommentPending(action)) {
    return addComment(state, action.payload);
  }

  if (chops.match.isClapPending(action)) {
    const clapEvent = action.payload;
    return addClap(state, clapEvent);
  }

  if (
    people.match.isGetOrCreatePersonFulfilled(action) ||
    people.match.isGetMyselfFulfilled(action)
  ) {
    const me = action.payload;
    return {
      ...state,
      me,
    };
  }

  if (people.match.isGetPersonFulfilled(action)) {
    state = addPerson(state, action.payload);
  }

  if (people.match.isChangeHandlePending(action)) {
    const { handle } = action.payload;
    const { me } = state;
    return {
      ...state,
      me: {
        ...me,
        handle,
      },
    };
  }

  if (people.match.isChangeUserpicPending(action)) {
    const { dataUrl } = action.payload;
    const { me } = state;
    return {
      ...state,
      me: {
        ...me,
        userpic: dataUrl,
      },
    };
  }

  if (people.match.isChangeUserpicFulfilled(action)) {
    const { userpic } = action.payload;
    const { me } = state;
    return {
      ...state,
      me: {
        ...me,
        userpic,
      },
    };
  }

  if (alerts.match.isAlertUpdatesAvailable(action)) {
    return {
      ...state,
      alerts: [...alerts, action.alert],
    };
  }

  if (alerts.match.isDismissAlert(action)) {
    return {
      ...state,
      alerts: state.alerts.map(n => {
        if (n.alertId === action.alertId) {
          return {
            ...n,
            dismissedAt: action.dismissedAt,
          };
        }

        return n;
      }),
    };
  }

  return state;
}
