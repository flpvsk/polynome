import React from 'react';
import { Query, Mutation } from 'react-apollo';

import {
  GET_MY_ACCOUNT,
  CHANGE_EMAIL,
  CHANGE_PASSWORD,
} from './GqlServer';

function toErrorMessage(e) {
  if (!e) {
    return '';
  }

  console.error(e);
  const msg = e.message || '';
  return msg.replace('GraphQL error: ', '');
}

export default function wrapWithAccountProvider(Wrapped) {
  return props => {
    return (
      <Query query={GET_MY_ACCOUNT}>
        {getMyAccount => (
          <Mutation mutation={CHANGE_EMAIL}>
            {(changeEmail, changeEmailInfo) => (
              <Mutation mutation={CHANGE_PASSWORD}>
                {(changePassword, changePasswordInfo) => {
                  const { myAccount } = getMyAccount.data;
                  const isMyAccountLoading = getMyAccount.loading;

                  const isChangeEmailLoading = changeEmailInfo.loading;
                  const changeEmailError = toErrorMessage(
                    changeEmailInfo.error
                  );
                  const onChangeEmail = ({ email }) => {
                    if (!myAccount) {
                      return;
                    }

                    if (myAccount.email === email) {
                      return;
                    }

                    changeEmail({
                      variables: {
                        email,
                        personId: myAccount.personId,
                      },
                    });
                  };

                  const isChangePasswordLoading =
                    changePasswordInfo.loading;
                  const changePasswordError = toErrorMessage(
                    changePasswordInfo.error
                  );
                  const onChangePassword = ({
                    oldPassword,
                    newPassword,
                  }) => {
                    if (!myAccount) {
                      return;
                    }

                    changePassword({
                      variables: {
                        oldPassword,
                        newPassword,
                        personId: myAccount.personId,
                      },
                    });
                  };

                  return (
                    <Wrapped
                      {...props}
                      myAccount={myAccount}
                      isMyAccountLoading={isMyAccountLoading}
                      onChangeEmail={onChangeEmail}
                      isChangeEmailLoading={isChangeEmailLoading}
                      changeEmailError={changeEmailError}
                      onChangePassword={onChangePassword}
                      isChangePasswordLoading={isChangePasswordLoading}
                      changePasswordError={changePasswordError}
                    />
                  );
                }}
              </Mutation>
            )}
          </Mutation>
        )}
      </Query>
    );
  };
}
