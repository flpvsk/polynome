const subtle = window.crypto.subtle;

export async function generateKeyPair() {
  const keys = await subtle.generateKey(
    {
      name: 'RSASSA-PKCS1-v1_5',
      modulusLength: 2048, //can be 1024, 2048, or 4096
      publicExponent: new Uint8Array([0x01, 0x00, 0x01]),
      hash: { name: 'SHA-512' },
    },
    true,
    ['sign', 'verify']
  );

  const publicKey = await subtle.exportKey('jwk', keys.publicKey);
  const privateKey = await subtle.exportKey('jwk', keys.privateKey);

  return {
    publicKey,
    privateKey,
  };
}

export async function sign(privateKeyJwk, data) {
  const key = await window.crypto.subtle.importKey(
    'jwk',
    privateKeyJwk,
    false,
    ['sign']
  );

  return window.crypto.subtle.sign(
    { name: 'RSASSA-PKCS1-v1_5' },
    key,
    data
  );
}

export async function verify(publicKeyJwk, data, signature) {
  const key = await window.crypto.subtle.importKey(
    'jwk',
    'publicKeyJwk',
    false,
    ['verify']
  );

  return window.crypto.subtle.verify(
    { name: 'RSASSA-PKCS1-v1_5' },
    key,
    signature,
    data
  );
}
