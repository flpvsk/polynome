import React from 'react';
import { Mutation } from 'react-apollo';

import { GET_MY_ACCOUNT, CANCEL_SUBSCRIPTION } from './GqlServer';

function toErrorMessage(e) {
  if (!e) {
    return '';
  }

  console.error(e);
  const msg = e.message || '';
  return msg.replace('GraphQL error: ', '');
}

function updateCache(cache, { error, data }) {
  if (error) {
    return;
  }

  const account = cache.readQuery({ query: GET_MY_ACCOUNT });
  cache.writeQuery({
    query: GET_MY_ACCOUNT,
    data: {
      ...account,
      activeSubscription: data.cancelSubscription,
    },
  });
}

export default function wrapWithCancelSubscription(Wrapped) {
  return props => (
    <Mutation mutation={CANCEL_SUBSCRIPTION} update={updateCache}>
      {(cancelSubscription, cancelSubscriptionInfo) => {
        const { loading, error, called } = cancelSubscriptionInfo;

        return (
          <Wrapped
            {...props}
            isCancelSubscriptionLoading={loading}
            cancelSubscriptionError={toErrorMessage(error)}
            cancelSubscriptionSuccess={called}
            onCancelSubscription={cancelSubscription}
          />
        );
      }}
    </Mutation>
  );
}
