// import Firebase from './Firebase';
import GqlServer from './GqlServer';
import LocalStorage from './LocalStorage';

const Api = {
  getOrCreatePerson: GqlServer.getOrCreatePerson,
  getPerson: GqlServer.getPerson,
  changeHandle: GqlServer.changeHandle,
  listPublicChops: GqlServer.listPublicChops,
  listChopsByPerson: GqlServer.listChopsByPerson,
  putChop: GqlServer.putChop,
  listMyChops: GqlServer.listMyChops,
  getChopById: GqlServer.getChopById,
  saveComment: GqlServer.saveComment,
  clap: GqlServer.clap,
  getDefaultLatency: LocalStorage.getDefaultLatency,
  setDefaultLatency: LocalStorage.setDefaultLatency,
  createSubscription: GqlServer.createSubscription,
  listKits: GqlServer.listKits,
};

export default Api;
