import React, { Component } from 'react';
import { Query, Mutation } from 'react-apollo';

import {
  LIST_NOTIFICATIONS_GROUPS,
  MARK_NOTIFICATIONS_AS_READ,
} from './GqlServer';

class WithNotificationsWrapper extends Component {
  render() {
    const { Wrapped } = this.props;
    const { ownProps } = this.props;

    if (!ownProps.me) {
      return 'Loading...';
    }

    return (
      <Query
        query={LIST_NOTIFICATIONS_GROUPS}
        variables={{ forPersonId: ownProps.me.personId }}
      >
        {queryResult => {
          // has `error` as well
          const {
            loading,
            data,
            startPolling,
            stopPolling,
            fetchMore,
          } = queryResult;

          const loadMore = () => {
            if (!data.notificationsGroups.pageInfo.hasNextPage) {
              return;
            }

            fetchMore({
              variables: {
                after: data.notificationsGroups.pageInfo.endCursor,
              },

              updateQuery: (prev, { fetchMoreResult }) => {
                const oldGroups = prev.notificationsGroups;
                const newGroups = fetchMoreResult.notificationsGroups;

                const newResult = {
                  ...prev,
                  notificationsGroups: {
                    ...prev.notificationsGroups,
                    edges: oldGroups.edges.concat(newGroups.edges),
                    pageInfo: newGroups.pageInfo,
                  },
                };

                return newResult;
              },
            });
          };

          const personId = ownProps.me.personId;

          const updateCache = (cache, { data: { before } }) => {
            const r = cache.readQuery({
              query: LIST_NOTIFICATIONS_GROUPS,
              variables: {
                forPersonId: personId,
              },
            });

            cache.writeQuery({
              query: LIST_NOTIFICATIONS_GROUPS,
              variables: {
                forPersonId: personId,
              },
              data: {
                ...r,
                notificationsGroups: {
                  ...r.notificationsGroups,
                  edges: r.notificationsGroups.edges.map(e => {
                    return {
                      ...e,
                      node: {
                        ...e.node,
                        isRead: true,
                      },
                    };
                  }),
                },
              },
            });
          };

          return (
            <WithNotificationsPollingWrapper
              startPolling={startPolling}
              stopPolling={stopPolling}
            >
              <Mutation
                mutation={MARK_NOTIFICATIONS_AS_READ}
                update={updateCache}
              >
                {markNotificationsAsRead => {
                  const markRead = ({ cursor }) =>
                    markNotificationsAsRead({
                      variables: { before: cursor, personId },
                    });

                  return (
                    <Wrapped
                      {...this.props.ownProps}
                      notificationsGroups={data.notificationsGroups}
                      markRead={markRead}
                      loadMore={loadMore}
                      isLoading={loading}
                    />
                  );
                }}
              </Mutation>
            </WithNotificationsPollingWrapper>
          );
        }}
      </Query>
    );
  }
}

class WithNotificationsPollingWrapper extends Component {
  componentDidMount() {
    this.props.startPolling(5000);
  }

  componentWillUnmount() {
    this.props.stopPolling();
  }

  render() {
    return this.props.children;
  }
}

export default function(Wrapped) {
  return props => {
    if (!props.me || !props.me.personId) {
      return null;
    }

    return (
      <WithNotificationsWrapper Wrapped={Wrapped} ownProps={props} />
    );
  };
}
