export const defaultEnvelopConfigByType = {
  time: {
    attackTime: 0.001,
    attackLevel: 1,
    decayTime: 0.005,
    sustainLevel: 0.8,
    sustainTime: 0.05,
    releaseTime: 0.01,
  },

  beat: {
    attackTime: 0,
    attackLevel: 0,
    decayTime: 0.1,
    sustainLevel: 0.8,
    sustainTime: 0.2,
    releaseTime: 0,
  },
};

export const defaultEnvelopTypeBySoundType = {
  kit: 'disabled',
  oscilator: 'time',
};

export const getDefaultEnvelopBySoundType = soundType => {
  const envelopType = defaultEnvelopTypeBySoundType[soundType];
  return {
    ...defaultEnvelopConfigByType,
    type: envelopType,
  };
};
