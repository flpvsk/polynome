function getStorage() {
  return window.localStorage;
}

function getFloat(key, defaultValue) {
  const storageValue = getStorage().getItem(key);
  if (storageValue === null) {
    return defaultValue;
  }

  return parseFloat(storageValue, 10);
}

function setValue(key, value) {
  return getStorage().setItem(key, JSON.stringify(value));
}

const LocalStorage = {
  async getDefaultLatency({ deviceId }) {
    return getFloat(`latency/${deviceId}`, 0.05);
  },

  async setDefaultLatency({ deviceId, latency }) {
    setValue(`latency/${deviceId}`, latency);
  },
};

export default LocalStorage;
