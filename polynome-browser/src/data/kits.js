// import { createSelector } from 'reselect';

import Storage from './storage';

const LIST_KITS = 'LIST_KITS';
// const LIST_KITS_PENDING = 'LIST_KITS_PENDING';
const LIST_KITS_FULFILLED = 'LIST_KITS_FULFILLED';
// const LIST_KITS_REJECTED = 'LIST_KITS_REJECTED';

// Action matchers
export const match = {
  isListKitsFulfilled: ({ type }) => type === LIST_KITS_FULFILLED,
};

// Action creators
export const run = {
  listKits: (_, dispatch) => {
    dispatch({
      type: LIST_KITS,
      payload: Storage.listKits(),
    });
  },
};

// Selectors
export const select = {
  listKits: state => state.kits,
};
