import ApolloClient from 'apollo-client';
import {
  InMemoryCache,
  IntrospectionFragmentMatcher,
  defaultDataIdFromObject,
} from 'apollo-cache-inmemory';
import gqlSchema from './gqlSchema.json';
import { HttpLink } from 'apollo-link-http';
import { onError } from 'apollo-link-error';
import { ApolloLink } from 'apollo-link';
import { setContext } from 'apollo-link-context';

import gql from 'graphql-tag';

import omit from 'lodash/omit';
import isObject from 'lodash/isObject';
import isArray from 'lodash/isArray';

const CHOP_BASIC_FRAGMENT = gql`
  fragment BeatBasic on Beat {
    id
    personId
    beatsCount
    lineStart
    lineDuration

    isMute
    mutedBeats

    offset
    sound {
      type
      isAuto
      pitch
      soundId
      envelop {
        type

        time {
          attackTime
          attackLevel
          decayTime
          sustainLevel
          sustainTime
          releaseTime
        }

        beat {
          attackTime
          attackLevel
          decayTime
          sustainLevel
          sustainTime
          releaseTime
        }
      }
    }
  }

  fragment LineBasic on Line {
    lineId
    lineName

    beatsCount
    lineStart
    lineDuration

    isMute

    playingRecordingId
  }

  fragment RecordingBasic on Recording {
    recordingId
    personId
    lineId
    name
    beatsCount
    beatsTempo
    sound {
      type
      sampleRate
      latency
      deviceId
      url
    }
  }

  fragment SoloItemBasic on SoloItem {
    itemType
    beatId
    lineId
  }

  fragment ChopBasic on Chop {
    chopId
    remixOf

    beats {
      ...BeatBasic
    }
    lines {
      ...LineBasic
    }
    commonBeatsCount
    commonBeatsTempo

    composedBy
    createdAt

    name
    description
    isDiscoverable

    recordings {
      ...RecordingBasic
    }

    activeSoundType
    activeKitId
    soloItem {
      ...SoloItemBasic
    }
  }
`;

const PERSON_BASIC_FRAGMENT = gql`
  fragment PersonBasic on Person {
    personId
    handle
    userpic
  }
`;

const COMMENT_BASIC_FRAGMENT = gql`
  fragment CommentBasic on Comment {
    commentId
    chopId
    personId
    createdAt
    text
  }
`;

const CHOP_SOCIAL_FRAGMENT = gql`
  fragment ChopSocial on Chop {
    ...ChopBasic

    comments {
      ...CommentBasic
      person {
        ...PersonBasic
      }
    }

    clapsCount
  }

  ${CHOP_BASIC_FRAGMENT}
  ${PERSON_BASIC_FRAGMENT}
  ${COMMENT_BASIC_FRAGMENT}
`;

const TOKEN_BASIC_FRAGMENT = gql`
  fragment TokenBasic on Token {
    token
    expiresAt
  }
`;

const SUBSCRIPTION_BASIC_FRAGMENT = gql`
  fragment SubscriptionBasic on Subscription {
    subscriptionId
    planId
    plan {
      planId
      name
    }
    isCancelled
    createdAt
    cancelledAt
    validUntil
  }
`;

const PUT_CHOP = gql`
  mutation putChop($chop: ChopInput!) {
    putChop(chop: $chop) {
      chopId
    }
  }
`;

const CHANGE_HANDLE = gql`
  mutation changeHandle($personId: ID!, $handle: String!) {
    changeHandle(personId: $personId, handle: $handle) {
      ...PersonBasic
    }
  }
  ${PERSON_BASIC_FRAGMENT}
`;

const SAVE_COMMENT = gql`
  mutation saveComment($comment: CommentInput!) {
    saveComment(comment: $comment) {
      ...CommentBasic
    }
  }
  ${COMMENT_BASIC_FRAGMENT}
`;

const SAVE_CLAP = gql`
  mutation saveClap($clap: ClapInput!) {
    saveClap(clap: $clap) {
      clapId
    }
  }
`;

export const CANCEL_SUBSCRIPTION = gql`
  mutation cancelSubscription {
    cancelSubscription {
      ...SubscriptionBasic
    }
  }

  ${SUBSCRIPTION_BASIC_FRAGMENT}
`;

const CREATE_SUBSCRIPTION = gql`
  mutation createSubscription(
    $planId: ID!
    $stripeTokenId: ID!
    $personId: ID!
    $email: String
    $password: String
    $nameOnCard: String
    $billingAddress: AddressInput
    $discountCode: String
  ) {
    createSubscription(
      planId: $planId
      stripeTokenId: $stripeTokenId
      personId: $personId
      email: $email
      password: $password
      billingAddress: $billingAddress
      nameOnCard: $nameOnCard
      discountCode: $discountCode
    ) {
      ...SubscriptionBasic
    }
  }
  ${SUBSCRIPTION_BASIC_FRAGMENT}
`;

const LIST_PUBLIC_CHOPS = gql`
  query listPublicChops($notComposedBy: ID, $personId: ID) {
    chops(notComposedBy: $notComposedBy, personId: $personId) {
      ...ChopSocial
      remixOfChop {
        ...ChopSocial
        composer {
          ...PersonBasic
        }
      }
      hasMyClaps(personId: $personId)
      composer {
        ...PersonBasic
      }
    }
  }
  ${CHOP_SOCIAL_FRAGMENT}
`;

const LIST_MY_CHOPS = gql`
  query listMyChops($composedBy: ID, $personId: ID) {
    chops(composedBy: $composedBy, personId: $personId) {
      ...ChopSocial
      remixOfChop {
        ...ChopBasic
        composer {
          ...PersonBasic
        }
      }
      hasMyClaps(personId: $personId)
    }
  }
  ${CHOP_SOCIAL_FRAGMENT}
`;

const KIT_BASIC_FRAGMENT = gql`
  fragment KitSoundBasic on KitSound {
    id
    name
    url
  }

  fragment KitBasic on Kit {
    id
    name
    isAvailable
    sounds {
      ...KitSoundBasic
    }
  }
`;

const LIST_KITS = gql`
  query kits {
    kits {
      ...KitBasic
    }
  }

  ${KIT_BASIC_FRAGMENT}
`;

const LIST_CHOPS_BY_PERSON = gql`
  query listChopsByPerson($composedBy: ID, $personId: ID) {
    chops(composedBy: $composedBy, personId: $personId) {
      ...ChopSocial
      remixOfChop {
        ...ChopBasic
        composer {
          ...PersonBasic
        }
      }
      hasMyClaps(personId: $personId)
    }
  }
  ${CHOP_SOCIAL_FRAGMENT}
`;

const GET_CHOP_BY_ID = gql`
  query getChopById($chopId: ID) {
    chop(chopId: $chopId) {
      ...ChopSocial
      remixOfChop {
        ...ChopBasic
        composer {
          ...PersonBasic
        }
      }
      composer {
        ...PersonBasic
      }
    }
  }
  ${CHOP_SOCIAL_FRAGMENT}
`;

const GET_PERSON = gql`
  query getPersonById($personId: ID) {
    person(personId: $personId) {
      ...PersonBasic
    }
  }
  ${PERSON_BASIC_FRAGMENT}
`;

const ACCOUNT_BASIC_FRAGMENT = gql`
  fragment AccountBasic on Account {
    accountId
    personId
    email
    hasPassword
    activeSubscription {
      subscriptionId
      planId
      plan {
        planId
        name
      }
      isCancelled
      validUntil
    }
  }
`;

const DISCOUNT_BASIC_FRAGMENT = gql`
  fragment DiscountBasic on Discount {
    discountId
    discountCode
    description
  }
`;

export const GET_MY_ACCOUNT = gql`
  query getMyAccount {
    myAccount {
      ...AccountBasic
    }
  }

  ${ACCOUNT_BASIC_FRAGMENT}
`;

export const GET_MYSELF = gql`
  query getMyself {
    myself {
      ...PersonBasic
    }
  }

  ${PERSON_BASIC_FRAGMENT}
`;

const PLAN_BASIC_FRAGMENT = gql`
  fragment PlanBasic on Plan {
    planId
    name
    description
    price {
      amount
      currencyCode
      currencySymbol
    }
    strikeOutPrice {
      amount
      currencyCode
      currencySymbol
    }
    frequencyAdverb
    frequencyNoun
    features
  }
`;

export const LIST_PLANS = gql`
  query listPlans {
    plans {
      ...PlanBasic
    }
  }

  ${PLAN_BASIC_FRAGMENT}
`;

const AUTHENTICATED_PERSON_FRAGMENT = gql`
  fragment AuthenticatedPersonBasic on AuthenticatedPerson {
    person {
      ...PersonBasic
    }

    token {
      ...TokenBasic
    }
  }

  ${PERSON_BASIC_FRAGMENT}
  ${TOKEN_BASIC_FRAGMENT}
`;

export const LOGIN = gql`
  mutation login($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      ...AuthenticatedPersonBasic
    }
  }

  ${AUTHENTICATED_PERSON_FRAGMENT}
`;

export const LOGIN_ANONYMOUSLY = gql`
  mutation loginAnonymously {
    loginAnonymously {
      ...AuthenticatedPersonBasic
    }
  }

  ${AUTHENTICATED_PERSON_FRAGMENT}
`;

export const LOGOUT = gql`
  mutation logout {
    logout
  }
`;

export const SIGNUP = gql`
  mutation signup($email: String!, $password: String!) {
    signup(email: $email, password: $password) {
      ...AuthenticatedPersonBasic
    }
  }

  ${AUTHENTICATED_PERSON_FRAGMENT}
`;

export const EMAIL_LOGIN_LINK = gql`
  mutation emailLoginLink($email: String!) {
    emailLoginLink(email: $email)
  }
`;

export const GET_DISCOUNT = gql`
  query getDiscount($personId: ID, $planId: ID, $discountCode: ID!) {
    discount(
      personId: $personId
      planId: $planId
      discountCode: $discountCode
    ) {
      ...DiscountBasic
    }
  }

  ${DISCOUNT_BASIC_FRAGMENT}
`;

export const CHANGE_EMAIL = gql`
  mutation changeEmail($personId: ID!, $email: String!) {
    changeEmail(personId: $personId, email: $email) {
      ...AccountBasic
    }
  }

  ${ACCOUNT_BASIC_FRAGMENT}
`;

export const CHANGE_PASSWORD = gql`
  mutation changePassword(
    $personId: ID!
    $oldPassword: String
    $newPassword: String!
  ) {
    changePassword(
      personId: $personId
      oldPassword: $oldPassword
      newPassword: $newPassword
    ) {
      ...AccountBasic
    }
  }

  ${ACCOUNT_BASIC_FRAGMENT}
`;

export const LIST_NOTIFICATIONS_GROUPS = gql`
  query listNotificationsGroups(
    $forPersonId: ID!
    $first: Int
    $after: ID
  ) {
    notificationsGroups(
      forPersonId: $forPersonId
      first: $first
      after: $after
    ) {
      edges {
        cursor
        node {
          notificationGroupId
          date
          type
          chopId
          isRead
          chop {
            chopId
            name
          }
          notifications {
            notificationId
            commentId
            createdAt
            forPersonId
            fromPersonId
            fromPerson {
              personId
              userpic
              handle
            }
          }
        }
      }
      pageInfo {
        endCursor
        hasNextPage
      }
    }
  }
`;

export const MARK_NOTIFICATIONS_AS_READ = gql`
  mutation markNotificationsAsRead($before: ID!, $personId: ID!) {
    markNotificationsAsRead(before: $before, personId: $personId)
  }
`;

// const DEPRECATED_LOGIN = gql`
//   mutation deprecatedLogin($personId: ID!) {
//     deprecatedLogin(personId: $personId) {
//       ...TokenBasic
//     }
//   }
//   ${TOKEN_BASIC_FRAGMENT}
// `;

const fragmentMatcher = new IntrospectionFragmentMatcher({
  introspectionQueryResultData: gqlSchema,
});

const cache = new InMemoryCache({
  fragmentMatcher,
  dataIdFromObject: obj => {
    switch (obj.__typename) {
      case 'Person':
        return obj.personId;
      case 'Chop':
        return obj.chopId;
      case 'Beat':
        return obj.id;
      case 'Line':
        return obj.lineId;
      case 'BeatSound':
        return obj.soundId;
      case 'NotificationGroup':
        return obj.notificationGroupId;
      case 'NotificationGroupEdge':
        return obj.cursor;
      case 'Notification':
        return obj.notificationId;
      case 'Recording':
        return obj.recordingId;
      case 'Clap':
        return obj.clapId;
      case 'Comment':
        return obj.commentId;
      case 'Subscription':
        return obj.subscriptionId;
      case 'Account':
        return obj.accountId;
      default: {
        return defaultDataIdFromObject(obj);
      }
    }
  },
});

const authLink = setContext((_, { headers }) => {
  // get the authentication token from local storage if it exists
  const token = getCurrentToken();
  return {
    headers: {
      ...headers,
      Authorization: token || '',
    },
  };
});

const LOGIN_OPS = ['login', 'signup', 'loginAnonymously'];

const localAuthLink = new ApolloLink((operation, forward) => {
  return forward(operation).map(result => {
    if (LOGIN_OPS.indexOf(operation.operationName) > -1) {
      if (result.errors && result.errors.length) {
        return result;
      }

      for (let opName of LOGIN_OPS) {
        let data = result.data[opName];
        if (!data) {
          continue;
        }

        saveCurrentPersonId(data.person.personId);
        saveCurrentToken(data.token.token);

        client.writeQuery({
          query: GET_MYSELF,
          data: {
            myself: data.person,
          },
        });
      }
    }

    if (operation.operationName === 'logout') {
      client.resetStore();
      saveCurrentPersonId(null);
      saveCurrentToken(null);
    }

    return result;
  });
});

export const client = new ApolloClient({
  link: ApolloLink.from([
    localAuthLink,
    onError(({ graphQLErrors, networkError }) => {
      if (graphQLErrors) {
        graphQLErrors.map(({ message, locations, path }) =>
          console.log(
            `[GraphQL error]: ` +
              `Message: ${message}, Location: ${locations}, Path: ${path}`
          )
        );
      }
      if (networkError) {
        console.log(`[Network error]: ${networkError}`);
      }
    }),
    authLink,
    new HttpLink({
      uri: '/graphql',
      credentials: 'same-origin',
    }),
  ]),
  cache,
});

window.client = client;

const CURRENT_PERSON_ID = 'poly/personId';
const CURRENT_TOKEN = 'poly/token';

function getCurrentPersonId() {
  const personIdStr = localStorage.getItem(CURRENT_PERSON_ID);
  if (!personIdStr) {
    return null;
  }

  if (personIdStr === 'null') {
    return null;
  }

  return personIdStr;
}

function saveCurrentPersonId(personId) {
  if (personId === null) {
    localStorage.removeItem(CURRENT_PERSON_ID);
    return;
  }

  localStorage.setItem(CURRENT_PERSON_ID, personId);
}

function saveCurrentToken(token) {
  if (token === null) {
    localStorage.removeItem(CURRENT_TOKEN);
    return;
  }

  localStorage.setItem(CURRENT_TOKEN, token);
}

function getCurrentToken() {
  const item = localStorage.getItem(CURRENT_TOKEN);

  if (item === 'null') {
    return null;
  }

  return item || null;
}

export function hasCredentials() {
  return !!(getCurrentToken() && getCurrentPersonId());
}

function dropServiceInfo(doc) {
  return omit(doc, ['__typename']);
}

function dropServiceInfoRecur(doc) {
  if (isObject(doc)) {
    doc = dropServiceInfo(doc);
  }

  if (isArray(doc)) {
    doc = doc.map(dropServiceInfoRecur);
  }

  for (let key of Object.keys(doc)) {
    const value = doc[key];

    if (isObject(value)) {
      doc = {
        ...doc,
        [key]: dropServiceInfoRecur(value),
      };
    }

    if (isArray(value)) {
      doc = {
        ...doc,
        [key]: value.map(dropServiceInfoRecur),
      };
    }
  }

  return doc;
}

function toChop(gqlChop) {
  if (!gqlChop) {
    return null;
  }

  return {
    ...gqlChop,
    beats: [...(gqlChop.beats || [])],
    lines: [...(gqlChop.lines || [])],
  };
}

function toPerson(gqlPerson) {
  return gqlPerson;
}

function toAccount(gqlAccount) {
  return gqlAccount;
}

function toComment(gqlComment) {
  return gqlComment;
}

function toSubscription(gqlSubscription) {
  return gqlSubscription;
}

function setTypename(obj, typename) {
  if (!obj) {
    return obj;
  }

  return {
    ...obj,
    __typename: typename,
  };
}

const Api = {
  async getMyAccount() {
    let accountQuery = await client.query({ query: GET_MY_ACCOUNT });
    return toAccount(accountQuery.data.myAccount);
  },

  async getPerson({ personId }) {
    let personQuery = await client.query({
      query: GET_PERSON,
      variables: {
        personId,
      },
    });

    return toPerson(personQuery.data.person);
  },

  async listPublicChops() {
    const personId = getCurrentPersonId();

    const queryResult = await client.query({
      query: LIST_PUBLIC_CHOPS,
      variables: {
        notComposedBy: personId,
        personId: personId,
      },
    });

    return queryResult.data.chops.map(toChop);
  },

  async listChopsByPerson({ composedBy }) {
    const personId = getCurrentPersonId();

    const queryResult = await client.query({
      query: LIST_CHOPS_BY_PERSON,
      variables: {
        composedBy,
        personId,
      },
    });

    return queryResult.data.chops.map(toChop);
  },

  async putChop(chop) {
    chop = omit(chop, ['comments', 'hasMyClaps', 'clapsCount']);

    client.writeFragment({
      id: chop.chopId,
      fragment: gql`
        fragment ChopIsRemoved on Chop {
          isRemoved
        }
      `,
      data: {
        isRemoved: chop.isRemoved || false,
        __typename: 'Chop',
      },
    });

    const result = await client.mutate({
      mutation: PUT_CHOP,
      variables: {
        chop: dropServiceInfoRecur(chop),
      },
    });

    return result;
  },

  async listMyChops() {
    let personId = getCurrentPersonId();
    const queryResult = await client.query({
      query: LIST_MY_CHOPS,
      variables: {
        composedBy: personId,
        personId: personId,
      },
    });

    return queryResult.data.chops.map(toChop);
  },

  async getChopById({ chopId }) {
    const queryResult = await client.query({
      query: GET_CHOP_BY_ID,
      variables: {
        chopId: chopId,
      },
    });

    return toChop(queryResult.data.chop);
  },

  async changeHandle({ personId, handle }) {
    const result = await client.mutate({
      mutation: CHANGE_HANDLE,
      variables: {
        personId,
        handle,
      },
    });

    return toPerson(result.data.changeHandle);
  },

  async saveComment(comment) {
    const result = await client.mutate({
      mutation: SAVE_COMMENT,
      variables: { comment },
    });

    return toComment(result.data.saveComment);
  },

  async clap({ clapId, chopId, personId, createdAt, clapsCount }) {
    const clap = {
      clapId,
      chopId,
      personId,
      createdAt,
      clapsCount,
    };

    await client.mutate({
      mutation: SAVE_CLAP,
      variables: { clap },
    });

    return;
  },

  async createSubscription({
    personId,
    planId,
    email,
    password,
    stripeTokenId,
    billingAddress,
    nameOnCard,
    discountCode,
  }) {
    const result = await client.mutate({
      mutation: CREATE_SUBSCRIPTION,
      variables: {
        personId,
        planId,
        email,
        password,
        stripeTokenId,
        nameOnCard,
        billingAddress,
        discountCode,
      },
    });

    const subscription = result.data.createSubscription;
    const account = cache.readQuery({ query: GET_MY_ACCOUNT });
    cache.writeQuery({
      query: GET_MY_ACCOUNT,
      data: {
        ...account,
        activeSubscription: subscription,
      },
    });

    return toSubscription(subscription);
  },

  async listKits() {
    const result = await client.query({
      query: LIST_KITS,
    });

    return result.data.kits;
  },

  async updateChopCache(chop) {
    const gqlChop = {
      ...omit(chop, 'comments', 'clapsCount'),
      soloItem: setTypename(chop.soloItem, 'SoloItem'),
      beats: chop.beats.map(b => {
        const result = setTypename(b, 'Beat');
        result.sound = setTypename(b.sound, 'BeatSound');

        if (result.sound && result.sound.envelop) {
          const env = result.sound.envelop;
          result.sound.envelop = {
            ...env,

            time: {
              ...env.time,
              __typename: 'AdsrConfig',
            },

            beat: {
              ...env.beat,
              __typename: 'AdsrConfig',
            },

            __typename: 'BeatSoundEnvelop',
          };
        }

        return result;
      }),
      lines: chop.lines.map(l => {
        const result = setTypename(l, 'Line');
        return result;
      }),
      recordings: chop.recordings.map(r => {
        const result = setTypename(r, 'Recording');
        result.sound = setTypename(r.sound, 'RecordingSound');
        return result;
      }),
      __typename: 'Chop',
    };

    client.writeFragment({
      id: chop.chopId,
      fragment: gql`
        ${CHOP_BASIC_FRAGMENT}
      `,
      fragmentName: 'ChopBasic',
      data: gqlChop,
    });
  },
};

export default Api;
