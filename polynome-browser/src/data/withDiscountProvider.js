import React, { Component } from 'react';
import { Query } from 'react-apollo';

import { GET_DISCOUNT } from './GqlServer';

function toErrorMessage(e) {
  if (!e) {
    return '';
  }

  console.error(e);
  const msg = e.message || '';
  return msg.replace('GraphQL error: ', '');
}

class WithDiscountProvider extends Component {
  constructor() {
    super();
    this.state = {
      skip: true,
      variables: {},
    };
  }

  render() {
    const { Wrapped, parentProps } = this.props;
    const { skip, variables } = this.state;

    return (
      <Query query={GET_DISCOUNT} skip={skip} variables={variables}>
        {getDiscount => {
          const isDiscountLoading = getDiscount.loading && !skip;
          let discountError = toErrorMessage(getDiscount.error);
          const refetchDiscount = async ({
            personId,
            discountCode,
            planId,
          }) => {
            this.setState({
              skip: false,
              variables: { personId, planId, discountCode },
            });
          };

          let discount;
          if (getDiscount.data) {
            discount = getDiscount.data.discount;
          }

          if (
            !discountError &&
            !getDiscount.loading &&
            !discount &&
            !skip
          ) {
            discountError = 'Discount code is not valid';
          }

          return (
            <Wrapped
              {...parentProps}
              getDiscount={refetchDiscount}
              discountError={discountError}
              isDiscountLoading={isDiscountLoading}
              discount={discount}
            />
          );
        }}
      </Query>
    );
  }
}

export default function wrapWithDiscountProvider(Wrapped) {
  return props => {
    return (
      <WithDiscountProvider Wrapped={Wrapped} parentProps={props} />
    );
  };
}
