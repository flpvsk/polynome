import React, { Component } from 'react';
import { ApolloConsumer } from 'react-apollo';
import shouldComponentUpdate from '../shouldComponentUpdate';

import { GET_MYSELF, hasCredentials } from './GqlServer';

class QueryObserver extends Component {
  constructor() {
    super();
    this.state = {
      loading: true,
      error: null,
      data: null,
    };

    this.shouldComponentUpdate = shouldComponentUpdate;

    this._runQuery = async () => {
      this.setState({
        loading: true,
        error: null,
        data: null,
      });

      if (!hasCredentials()) {
        this.setState({
          loading: false,
          error: null,
          data: null,
        });

        return;
      }

      try {
        let result = await this.props.client.query({
          query: this.props.query,
        });

        if (!this._mounted) {
          return;
        }

        this.setState({
          loading: false,
          error: null,
          data: result.data,
        });
      } catch (e) {
        if (!this._mounted) {
          return;
        }

        this.setState({
          loading: false,
          error: e,
          data: null,
        });
      }
    };

    this._startWatchingQuery = () => {
      this._watcher = this.props.client
        .watchQuery({
          query: this.props.query,
          fetchPolicy: 'cache-only',
        })
        .subscribe(result =>
          this.setState({
            data: result.data,
          })
        );
    };

    this._stopWatchingQuery = () => {
      this._watcher.unsubscribe();
    };
  }

  componentDidMount() {
    this._mounted = true;
    this._startWatchingQuery();
    this._runQuery();
  }

  componentWillUnmount() {
    this._mounted = false;
    this._stopWatchingQuery();
  }

  render() {
    return this.props.children(this.state);
  }
}

export default function wrapWithMyselfProvider(Wrapped) {
  return props => {
    return (
      <ApolloConsumer>
        {client => (
          <QueryObserver client={client} query={GET_MYSELF}>
            {getMyself => {
              const isGetMyselfLoading = getMyself.loading;

              let me = null;
              if (getMyself.data && getMyself.data.myself) {
                me = getMyself.data.myself;
              }

              return (
                <Wrapped
                  {...props}
                  me={me}
                  isGetMyselfLoading={isGetMyselfLoading}
                  getMyselfError={getMyself.error}
                />
              );
            }}
          </QueryObserver>
        )}
      </ApolloConsumer>
    );
  };
}
