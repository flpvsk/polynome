import React from 'react';
import { ApolloConsumer } from 'react-apollo';
import QueryObserver from './QueryObserver';

import { GET_MY_ACCOUNT } from './GqlServer';

export default function wrapWithMyAccountProvider(Wrapped) {
  return props => {
    return (
      <ApolloConsumer>
        {client => (
          <QueryObserver client={client} query={GET_MY_ACCOUNT}>
            {getMyAccount => {
              const isMyAccountLoading = getMyAccount.loading;

              let myAccount;
              if (getMyAccount.data) {
                myAccount = getMyAccount.data.myAccount;
              }

              return (
                <Wrapped
                  {...props}
                  myAccount={myAccount}
                  isMyAccountLoading={isMyAccountLoading}
                  myAccountErrors={getMyAccount.errors}
                />
              );
            }}
          </QueryObserver>
        )}
      </ApolloConsumer>
    );
  };
}
