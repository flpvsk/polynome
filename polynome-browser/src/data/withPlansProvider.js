import React from 'react';
import { Query } from 'react-apollo';

import { LIST_PLANS } from './GqlServer';

export default function wrapWithPlansProvider(Wrapped) {
  return props => {
    return (
      <Query query={LIST_PLANS}>
        {listPlans => {
          return (
            <Wrapped
              {...props}
              plans={listPlans.data.plans}
              arePlansLoading={listPlans.loading}
            />
          );
        }}
      </Query>
    );
  };
}
