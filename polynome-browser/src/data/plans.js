export const FREE_PLAN = {
  planId: null,
  name: 'Undecided',
  description:
    `Three chops and ten minutes of recording time.\n\n` +
    `Hey, you're just looking around, we get it.`,
  features: [
    `Up to 3 chops`,
    `Up to 30min of recording time`,
    `Cross-device sync`,
  ],
};

export function getPlanFromSubscription(subscription) {
  let activePlan = FREE_PLAN;

  if (subscription && subscription.plan) {
    activePlan = subscription.plan;
  }

  return activePlan;
}
