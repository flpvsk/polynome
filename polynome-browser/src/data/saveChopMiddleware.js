import omit from 'lodash/omit';
import * as chops from './chops';
import * as people from './people';
import GqlServer from './GqlServer';

function extractChopData(chop) {
  chop = omit(chop, ['composer', 'remixOfChop', 'hasMyClaps']);
  chop = {
    ...chop,
    lines: chop.lines.map(l => omit(l, ['writingRecordingId'])),
    recordings: chop.recordings
      .filter(r => r.recordingStatus !== 'recording')
      .map(r => omit(r, 'recordingStatus')),
  };
  return chop;
}

const delay = ms =>
  new Promise(resolve => setTimeout(resolve, Math.max(ms, 0) || 0));

export default function createMiddleware(updateFn) {
  return store => {
    let toSave = new Set();
    let toSkip = new Set();

    const runSave = async () => {
      const leftToSave = new Set();
      const state = store.getState();

      for (let chopId of toSave) {
        if (toSkip.has(chopId)) {
          continue;
        }

        let me = people.select.getMyself(state);

        const chop = chops.select.getChopById(state, { chopId });

        if (!me || me.personId !== chop.composedBy) {
          console.log(`[saveChopMiddleware] Won't save the chop.`);
          toSkip.add(chopId);
          return;
        }

        const chopData = extractChopData(chop);

        try {
          await updateFn(chopData);
        } catch (e) {
          console.error('Error saving a chop, will retry', e);
          leftToSave.add(chopId);
        }
      }

      toSave = leftToSave;
    };

    const runSaveLoop = async () => {
      try {
        await delay(4000);
        await runSave();
      } finally {
        runSaveLoop();
      }
    };

    runSaveLoop();

    return next => {
      return async action => {
        next(action);
        if (chops.CHOP_UPDATE_ACTIONS.indexOf(action.type) !== -1) {
          const state = store.getState();
          const chopId = action.chopId || action.payload.chopId;
          const chop = chops.select.getChopById(state, { chopId });
          GqlServer.updateChopCache(chop);
          toSave.add(chopId);
        }
      };
    };
  };
}
