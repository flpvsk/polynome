import React from 'react';

import { Mutation } from 'react-apollo';
import {
  LOGIN,
  SIGNUP,
  EMAIL_LOGIN_LINK,
  LOGIN_ANONYMOUSLY,
} from './GqlServer';

function toErrorMessage(e) {
  if (!e) {
    return '';
  }

  console.error(e);
  const msg = e.message || '';
  return msg.replace('GraphQL error: ', '');
}

function withInitialScreenProvider(Wrapped) {
  return props => (
    <Mutation mutation={LOGIN}>
      {(login, loginInfo) => (
        <Mutation mutation={LOGIN_ANONYMOUSLY}>
          {(loginAnonymously, loginAnonymouslyInfo) => (
            <Mutation mutation={SIGNUP}>
              {(signup, signupInfo) => (
                <Mutation mutation={EMAIL_LOGIN_LINK}>
                  {(emailLink, emailLinkInfo) => {
                    const onLogin = ({ email, password }) =>
                      login({
                        variables: {
                          email,
                          password,
                        },
                      });

                    const onSignup = ({ email, password }) =>
                      signup({
                        variables: {
                          email,
                          password,
                        },
                      });

                    const onEmailLink = ({ email }) =>
                      emailLink({
                        variables: {
                          email,
                        },
                      });

                    const onLoginAnonymously = () => loginAnonymously();

                    const emailSent =
                      emailLinkInfo.data &&
                      emailLinkInfo.data.emailLoginLink &&
                      !emailLinkInfo.loading;

                    return (
                      <Wrapped
                        {...props}
                        onLogin={onLogin}
                        loginLoading={loginInfo.loading}
                        loginError={toErrorMessage(loginInfo.error)}
                        onSignup={onSignup}
                        signupLoading={signupInfo.loading}
                        signupError={toErrorMessage(signupInfo.error)}
                        onEmailLink={onEmailLink}
                        emailLinkSent={emailSent}
                        emailLinkError={toErrorMessage(
                          emailLinkInfo.error
                        )}
                        emailLinkLoading={emailLinkInfo.loading}
                        onLoginAnonymously={onLoginAnonymously}
                        loginAnonymouslyLoading={
                          loginAnonymouslyInfo.loading
                        }
                        loginAnonymouslyError={toErrorMessage(
                          loginAnonymously.error
                        )}
                      />
                    );
                  }}
                </Mutation>
              )}
            </Mutation>
          )}
        </Mutation>
      )}
    </Mutation>
  );
}

export default withInitialScreenProvider;
