import { createSelector } from 'reselect';
import storage from './storage';
import FileStorage from './FileStorage';

const GET_OR_CREATE_PERSON = 'GET_OR_CREATE_PERSON';
const GET_OR_CREATE_PERSON_FULFILLED = 'GET_OR_CREATE_PERSON_FULFILLED';

const GET_PERSON = 'GET_PERSON';
const GET_PERSON_FULFILLED = 'GET_PERSON_FULFILLED';

const GET_MYSELF_FULFILLED = 'GET_MYSELF_FULFILLED';

const GET_MY_ACCOUNT = 'GET_MY_ACCOUNT';
const GET_MY_ACCOUNT_PENDING = 'GET_MY_ACCOUNT_PENDING';
const GET_MY_ACCOUNT_FULFILLED = 'GET_MY_ACCOUNT_FULFILLED';
const GET_MY_ACCOUNT_REJECTED = 'GET_MY_ACCOUNT_REJECTED';

const CHANGE_HANDLE = 'CHANGE_HANDLE';
const CHANGE_HANDLE_PENDING = 'CHANGE_HANDLE_PENDING';
// const CHANGE_HANDLE_REJECTED = 'CHANGE_HANDLE_REJECTED';
const CHANGE_HANDLE_FULFILLED = 'CHANGE_HANDLE_FULFILLED';

const CHANGE_USERPIC = 'CHANGE_USERPIC';
const CHANGE_USERPIC_PENDING = 'CHANGE_USERPIC_PENDING';
const CHANGE_USERPIC_FULFILLED = 'CHANGE_USERPIC_FULFILLED';

export const match = {
  isGetOrCreatePerson: ({ type }) => type === GET_OR_CREATE_PERSON,
  isGetOrCreatePersonFulfilled: ({ type }) =>
    type === GET_OR_CREATE_PERSON_FULFILLED,

  isGetPersonFulfilled: ({ type }) => type === GET_PERSON_FULFILLED,

  isGetMyselfFulfilled: ({ type }) => type === GET_MYSELF_FULFILLED,

  isGetMyAccountPending: ({ type }) => type === GET_MY_ACCOUNT_PENDING,
  isGetMyAccountFulfilled: ({ type }) =>
    type === GET_MY_ACCOUNT_FULFILLED,
  isGetMyAccountRejected: ({ type }) =>
    type === GET_MY_ACCOUNT_REJECTED,

  isChangeHandlePending: ({ type }) => type === CHANGE_HANDLE_PENDING,
  isChangeHandleFulfilled: ({ type }) =>
    type === CHANGE_HANDLE_FULFILLED,

  isChangeUserpicPending: ({ type }) => type === CHANGE_USERPIC_PENDING,
  isChangeUserpicFulfilled: ({ type }) =>
    type === CHANGE_USERPIC_FULFILLED,
};

export const run = {
  getOrCreatePerson: (_, dispatch) => {
    dispatch({
      type: GET_OR_CREATE_PERSON,
      payload: storage.getOrCreatePerson(),
    });
  },

  getMyAccount: (_, dispatch) => {
    dispatch({
      type: GET_MY_ACCOUNT,
      payload: storage.getMyAccount(),
    });
  },

  getMyselfFulfilled: (person, dispatch) => {
    dispatch({
      type: GET_MYSELF_FULFILLED,
      payload: person,
    });
  },

  changeHandle: ({ personId, handle }, dispatch) => {
    dispatch({
      type: CHANGE_HANDLE,
      payload: {
        promise: storage.changeHandle({ personId, handle }),
        data: {
          handle,
          personId,
        },
      },
    });
  },

  changeUserpic: ({ personId, dataUrl, file }, dispatch) => {
    dispatch({
      type: CHANGE_USERPIC,
      payload: {
        promise: FileStorage.changeUserpic({ personId, file }),
        data: {
          dataUrl,
          personId,
        },
      },
    });
  },

  getPersonById: ({ personId }, dispatch) => {
    dispatch({
      type: GET_PERSON,
      payload: storage.getPerson({ personId }),
    });
  },
};

const peopleSelector = state => state.people;
const myselfSelector = state => state.me;
export const personIdFromMatchSelector = (_, props) =>
  props.match.params.personId;

export const select = {
  getMyself: myselfSelector,

  getPersonFromMatch: createSelector(
    peopleSelector,
    myselfSelector,
    personIdFromMatchSelector,
    (people, me, personId) => {
      if (me && me.personId === personId) {
        return me;
      }
      return people.get(personId);
    }
  ),

  listPeople: createSelector(
    myselfSelector,
    peopleSelector,
    (me, people) => {
      if (!me) {
        return people;
      }

      return people.set(me.personId, me);
    }
  ),
};
