import nanoid from 'nanoid';

const ALERT_UPDATES_AVAILABLE = 'ALERT_UPDATES_AVAILABLE';
const DISMISS_ALERT = 'DISMISS_ALERT';

export const UPDATES_AVAILABLE = 'UPDATES_AVAILABLE';

export const match = {
  isAlertUpdatesAvailable: ({ type }) =>
    type === ALERT_UPDATES_AVAILABLE,
  isDismissAlert: ({ type }) => type === DISMISS_ALERT,
};

export const run = {
  alertUpdatesAvailable: (_, dispatch) => {
    const createdAt = Date.now();
    const alertId = nanoid();

    dispatch({
      type: ALERT_UPDATES_AVAILABLE,
      alert: {
        type: UPDATES_AVAILABLE,
        createdAt,
        alertId,
      },
    });
  },

  dismissAlert: ({ alertId }, dispatch) => {
    const dismissedAt = Date.now();
    dispatch({
      alertId,
      dismissedAt,
      type: DISMISS_ALERT,
    });
  },
};

export const select = {
  listActiveAlerts: state => {
    return state.alerts.filter(n => !n.dismissedAt);
  },
};
