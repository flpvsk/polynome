import { createSelector } from 'reselect';
import closeMediaStream from '../closeMediaStream';
import storage from './storage';

const LIST_MEDIA_DEVICES = 'LIST_MEDIA_DEVICES';
const LIST_MEDIA_DEVICES_PENDING = 'LIST_MEDIA_DEVICES_PENDING';
const LIST_MEDIA_DEVICES_FULFILLED = 'LIST_MEDIA_DEVICES_FULFILLED';
const LIST_MEDIA_DEVICES_REJECTED = 'LIST_MEDIA_DEVICES_REJECTED';

const CHANGE_SELECTED_INPUT_DEVICE_ID =
  'CHANGE_SELECTED_INPUT_DEVICE_ID';

const GET_DEFAULT_LATENCY = 'GET_DEFAULT_LATENCY';
const GET_DEFAULT_LATENCY_FULFILLED = 'GET_DEFAULT_LATENCY_FULFILLED';

const SET_DEFAULT_LATENCY = 'SET_DEFAULT_LATENCY';
const SET_DEFAULT_LATENCY_PENDING = 'SET_DEFAULT_LATENCY_PENDING';

// Action matchers
export const match = {
  isListMediaDevicesPending: ({ type }) => {
    return type === LIST_MEDIA_DEVICES_PENDING;
  },

  isListMediaDevicesRejected: ({ type }) => {
    return type === LIST_MEDIA_DEVICES_REJECTED;
  },

  isListMediaDevicesFulfilled: ({ type }) => {
    return type === LIST_MEDIA_DEVICES_FULFILLED;
  },

  isChangeSelectedInputDeviceId: ({ type }) => {
    return type === CHANGE_SELECTED_INPUT_DEVICE_ID;
  },

  isGetDefaultLatencyFulfilled: ({ type }) => {
    return type === GET_DEFAULT_LATENCY_FULFILLED;
  },

  isSetDefaultLatencyPending: ({ type }) => {
    return type === SET_DEFAULT_LATENCY_PENDING;
  },
};

// Action creators
async function getMediaDevicesAsync() {
  const devicesApi = navigator.mediaDevices;
  const stream = await devicesApi.getUserMedia({ audio: true });
  let mediaDevices = await devicesApi.enumerateDevices();
  mediaDevices = mediaDevices.map(m => m.toJSON());

  closeMediaStream(stream);

  const defaultLatencyMap = {};
  for (let mediaDevice of mediaDevices) {
    const { deviceId } = mediaDevice;
    const latency = await storage.getDefaultLatency({ deviceId });
    defaultLatencyMap[deviceId] = latency;
  }

  return {
    mediaDevices,
    defaultLatencyMap,
  };
}

export const run = {
  listMediaDevices: (_, dispatch) => {
    return dispatch({
      type: LIST_MEDIA_DEVICES,
      payload: getMediaDevicesAsync(),
    });
  },

  changeSelectedInputDeviceId: ({ deviceId }, dispatch) => {
    return dispatch({
      type: CHANGE_SELECTED_INPUT_DEVICE_ID,
      deviceId,
    });
  },

  getDefaultLatency: ({ deviceId }, dispatch) => {
    return dispatch({
      type: GET_DEFAULT_LATENCY,
      payload: storage.getDefaultLatency({ deviceId }),
    });
  },

  setDefaultLatency({ deviceId, latency }, dispatch) {
    return dispatch({
      type: SET_DEFAULT_LATENCY,
      payload: {
        promise: storage.setDefaultLatency({ deviceId, latency }),
        data: { deviceId, latency },
      },
    });
  },
};

// Selectors
function selectedInputDeviceIdSelector(state) {
  if (state.selectedInputDeviceId) {
    return state.selectedInputDeviceId;
  }

  for (let device of state.mediaDevices || []) {
    if (device.kind === 'audioinput') {
      return device.deviceId;
    }
  }

  return null;
}

function defaultLatencyMapSelector(state) {
  return state.defaultLatencyByDeviceId;
}

export const select = {
  getSelectedInputDeviceId: createSelector(
    selectedInputDeviceIdSelector,
    result => result
  ),

  getDefaultLatency: createSelector(
    selectedInputDeviceIdSelector,
    defaultLatencyMapSelector,
    (deviceId, latencyMap) => latencyMap.get(deviceId, 0)
  ),

  listMediaDevices: state => state.mediaDevices,
};
