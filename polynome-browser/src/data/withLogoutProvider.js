import React from 'react';

import { Mutation } from 'react-apollo';
import { LOGOUT } from './GqlServer';

function withLogoutProvider(Wrapped) {
  return props => (
    <Mutation mutation={LOGOUT}>
      {logout => {
        return <Wrapped {...props} logout={logout} />;
      }}
    </Mutation>
  );
}

export default withLogoutProvider;
