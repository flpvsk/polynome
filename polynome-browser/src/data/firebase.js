import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const config = {
  apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
  authDomain: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_FIREBASE_DATABASE_URL,
  projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
  storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_FIREBASE_MESSAGING_SENDER_ID,
};

const app = firebase.initializeApp(config);
let db;
let dbQuery;

async function getDb() {
  if (db) {
    return db;
  }

  if (dbQuery) {
    await dbQuery;
    return firebase.firestore();
  }

  const fireStoreSettings = { timestampsInSnapshots: true };
  firebase.firestore().settings(fireStoreSettings);

  dbQuery = firebase.firestore().enablePersistence();
  try {
    await dbQuery;
  } catch (e) {
    console.error('Firebase: failed to enable persistence', e);
  } finally {
    return firebase.firestore();
  }
}

function mapUserToPerson(firebaseUser) {
  return {
    personId: firebaseUser.uid,
    handle: firebaseUser.displayName,
    userpic: firebaseUser.photoURL,
  };
}

function mapChopsToChops(querySnapshot) {
  const result = [];
  querySnapshot.forEach(doc => {
    console.log(`${doc.id} => ${doc.data()}`);
    result.push(doc.data());
  });

  return result;
}

function randomPic() {
  const id = Math.floor(Math.random() * 100);
  const portraits = ['men', 'women', 'lego'];
  const portraitOf = portraits[id % 3];
  const portraitId = id % 8;

  return `https://randomuser.me/api/portraits/${portraitOf}/${portraitId}.jpg`;
}

function mapChopsToPublicChops(chops) {
  return mapChopsToChops(chops).map(chop => {
    return {
      ...chop,
      composer: {
        personId: chop.composerId,
        userpic: randomPic(),
        handle: Math.floor(Math.random() * 100),
      },
    };
  });
}

const Api = {
  async getOrCreatePerson() {
    const { currentUser } = firebase.auth();
    let firebaseUser = currentUser;

    if (!firebaseUser) {
      const firebaseAcc = await app.auth().signInAnonymously();
      firebaseUser = firebaseAcc.user;
    }

    await firebaseUser.updateProfile({
      displayName: 'flpvsk',
      photoURL:
        'https://pbs.twimg.com/profile_images/639514028256165888/XF1nGHAF_400x400.png',
    });

    return mapUserToPerson(firebaseUser);
  },

  async listPublicChops() {
    const db = await getDb();
    const me = await Api.getOrCreatePerson();
    const recentChops1 = await db
      .collection('chops')
      .where('composedBy', '>', me.personId)
      .limit(25)
      .get();

    const recentChops2 = await db
      .collection('chops')
      .where('composedBy', '<', me.personId)
      .limit(25)
      .get();

    const result = []
      .concat(mapChopsToPublicChops(recentChops1))
      .concat(mapChopsToPublicChops(recentChops2))
      .sort((c1, c2) => c2.createdAt - c1.createdAt);

    return result;
  },

  async createChop(chop) {
    const db = await getDb();
    const chopRef = db.collection('chops').doc(chop.chopId);
    return chopRef.set(chop);
  },

  async updateChop(chop) {
    const db = await getDb();
    return db
      .collection('chops')
      .doc(chop.chopId)
      .set(chop);
  },

  async listMyChops() {
    const db = await getDb();
    const me = await Api.getOrCreatePerson();
    const myChops = await db
      .collection('chops')
      .where('composedBy', '==', me.personId)
      .get();

    return mapChopsToChops(myChops);
  },

  async getChopById(chopId) {
    const db = await getDb();
    return db
      .collection('chops')
      .doc(chopId)
      .get();
  },
};

export default Api;
