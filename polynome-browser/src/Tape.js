import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import AudioContext from './AudioContext';
import getFullUrl from './getFullUrl';
import connectChopEditor from './connectChopEditor';

import nanoid from 'nanoid';

import * as trackUtils from './trackUtils';
import Recorder from './Recorder';

import shouldComponentUpdate from './shouldComponentUpdate';

import theme from './theme';
import viewport from './viewport';
import {
  colorStrByBeat,
  colorStrByLine,
  colorStrForRecording,
} from './color';

import TopBarContainer from './AppBarTopContainer';
import FloatingActionButton from './ButtonFloatingAction';

import TextWrapper from './TextWrapper';

import MenuItem from './MenuItem';
import MenuItemBarsCountControl from './MenuItemBarsCountControl';
import Divider from './Divider';
import MenuHeader from './MenuHeader';
import LoadingIndicatorCircular from './LoadingIndicatorCircular';

import TempoControl from './TempoControl';
import Track from './Track';
import AudioPlayer from './AudioPlayer';
import AudioLoader from './AudioLoader';
import IconPlay from './IconPlay';
import IconStop from './IconStop';
import IconRecord from './IconRecord';
import IconShare from './IconShare';
import IconRemix from './IconRemix';
import IconFeel from './IconFeel';
import IconHome from './IconHome';
import ButtonContained from './ButtonContained';
import ButtonText from './ButtonText';
import ButtonIcon from './ButtonIcon';
import Switch from './Switch';
import ListItem from './ListItem';
import InputText from './InputText';

import BeatBottomSheetContent from './BeatBottomSheetContent';
import LineBottomSheetContent from './LineBottomSheetContent';
import SnackbarRecording from './SnackbarRecording';
import ChopFooter from './ChopFooter';

const DEFAULT_KIT_ID = 'bluebird';
// const DEFAULT_SOUND_ID = 'bluebird-kit-hi-hat-foot-close-2';

const KEY_WHITESPACE = 32;
const KEY_ENTER = 13;

const DEFAULT_TEMPO = 120;
const DELAY = 0.05;

const styles = {
  block: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },

  __topBarButton: {
    appearance: 'none',
    background: 'none',
    border: 'none',
  },

  __trackHeader: {
    width: '100%',
    paddingLeft: 10,
    paddingRight: 10,
    display: 'flex',
    marginBottom: '14px',
    marginTop: '14px',
  },

  __tempoControlWrapper: {
    marginTop: 24,
    marginBottom: 8,
    width: '100%',
    display: 'flex',
    paddingLeft: 16,
    paddingRight: 16,
    justifyContent: 'space-between',
  },

  __tempoControlIcon: {
    width: 48,
    height: 48,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },

  __emptyStateContainer: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 24,
  },
};

function createNewLine({
  lineId,
  commonBeatsCount,
  trackBeatsCount,
  lineName,
  personId,
}) {
  return {
    lineId,
    personId,
    lineName,
    lineStart: 0,
    lineDuration: trackBeatsCount / commonBeatsCount,
    beatsCount: 1,
    writingRecordingId: null,
    playingRecordingId: null,
    isMute: false,
  };
}

// TODO: migrate "beats" and like to specific ids, e.g. beatId

class Tape extends Component {
  static getDerivedStateFromProps(props, state) {
    if (props.chop) {
      const isPaused = props.chop.beats.length === 0 || state.isPaused;
      return {
        ...state,
        ...props.chop,
        isChopFetched: true,
        kits: props.kits,
        recordingsAudioMap: props.recordingsAudioMap,
        isPaused,
        beats: props.chop.beats.slice().sort(trackUtils.beatsSortFn),
      };
    }

    if (!props.chop) {
      return {
        isChopFetched: false,
      };
    }

    return state;
  }

  constructor() {
    super();
    let windowWidth = viewport.getWidth();

    this._audioContext = new AudioContext();

    this.state = {
      isInitialized: false,
      startTime: 0,
      commonBeatsCount: 1,
      commonBeatsTempo: DEFAULT_TEMPO,
      beats: [],
      lines: [],
      selectedItem: null,
      soloItem: null,
      recordings: [],
      recordingForLineId: null,
      writingRecordingId: null,
      windowWidth,
      isPaused: true,
      isRecording: false,
      pauseTime: null,
      activeSoundType: 'kit',
      activeKitId: DEFAULT_KIT_ID,
      kits: [],
      savedBeats: [],
      showShareDialog: false,
      showChangeNameDialog: false,
      isDiscoverable: false,
    };

    this.shouldComponentUpdate = shouldComponentUpdate;

    this._toggleIsDiscoverable = () => {
      this.props.onUpdateChop({
        update: {
          isDiscoverable: !this.state.isDiscoverable,
        },
      });
    };

    this._share = () => {
      this.setState({
        showShareDialog: true,
      });
    };

    this._remix = () => {
      if (!this.props.me) {
        return;
      }

      this.props.onRemix({
        remixOf: this.props.chop.chopId,
        personId: this.props.me.personId,
        type: 'form',
      });
    };

    this._showChangeNameDialog = () => {
      this.setState({ showChangeNameDialog: true });
    };

    this._hideChangeNameDialog = e => {
      e.preventDefault();
      e.stopPropagation();

      this.setState({ showChangeNameDialog: false });
    };

    this._changeName = value => {
      this.props.onChangeName({ name: value });
    };

    this._hideDialogs = e => {
      e.preventDefault();
      e.stopPropagation();

      this.setState({
        showShareDialog: false,
        showChangeNameDialog: false,
      });
    };

    this._hideShareDialog = e => {
      e.preventDefault();
      e.stopPropagation();

      this.setState({
        showShareDialog: false,
      });
    };

    this._getCurrentTime = () => {
      if (!this._audioContext) {
        return 0;
      }

      return this._audioContext.currentTime;
    };

    this._toggleBeat = beatId => {
      if (
        this.state.selectedItem &&
        this.state.selectedItem.itemType === 'beat' &&
        this.state.selectedItem.beatId === beatId
      ) {
        this.setState({ selectedItem: null });
        return;
      }

      this.setState({
        selectedItem: {
          itemType: 'beat',
          beatId,
        },
      });
    };

    this._toggleLine = lineId => {
      const { selectedItem } = this.state;

      if (
        selectedItem &&
        selectedItem.itemType === 'line' &&
        selectedItem.lineId === lineId
      ) {
        this.setState({ selectedItem: null });
        return;
      }

      this.setState({
        selectedItem: {
          itemType: 'line',
          lineId,
        },
      });
    };

    this._onBeatAdded = beat => {
      if (!this.props.me) {
        return;
      }

      const { beats } = this.state;
      beat = {
        id: nanoid(),
        personId: this.props.me.personId,
        ...beat,
      };

      let { startTime, isPaused } = this.state;

      if (!beats.length) {
        startTime = this._audioContext.currentTime + DELAY;
        isPaused = false;
      }

      const updateStateAndRhythm = () => {
        this.props.onAddBeat({ beat });
        this.setState({ startTime, isPaused });
      };

      this._runWhenAudioIsReady(updateStateAndRhythm);
    };

    this._addDefaultBeat = () => {
      this._onBeatAdded({
        beatsCount: 1,
        tempo: 120,
        lineStart: 0,
        lineDuration: 1,
      });
    };

    this._changeTempo = (beatId, tempo) => {
      this.setState({
        startTime: this._audioContext.currentTime + DELAY,
      });

      this.props.onChangeTempo({ beatId, tempo });
    };

    this._changeSound = (beatId, sound) => {
      this.props.onChangeSound({ beatId, sound });
    };

    this._changeOffset = (beatId, offset) => {
      this.props.onChangeOffset({ beatId, offset });
    };

    this._deselectAll = () => {
      this.setState({ selectedItem: null });
    };

    this._toggleMuteSection = (beatId, sectionNumber) => {
      this.props.onToggleMuteSection({ beatId, sectionNumber });
    };

    this._toggleMuteBeat = beatId => {
      this.props.onToggleMuteBeat({ beatId });
    };

    this._toggleSoloBeat = beatId => {
      this.props.onToggleSoloItem({ itemType: 'beat', beatId });
    };

    this._toggleMuteLine = lineId => {
      this.props.onToggleMuteLine({ lineId });
    };

    this._toggleSoloLine = lineId => {
      this.props.onToggleSoloItem({ itemType: 'line', lineId });
    };

    this._toggleRecordLine = lineId => {
      if (this.state.recordingForLineId === lineId) {
        this.setState({
          recordingForLineId: null,
        });
        return;
      }

      this.setState({
        recordingForLineId: lineId,
      });
    };

    this._togglePause = () => {
      const { isPaused, isRecording } = this.state;

      if (isRecording) {
        this._recorder.stopRecordLoop();
      }

      if (!isPaused) {
        this.setState({
          isPaused: true,
          isRecording: false,
          startTime: null,
        });

        return;
      }

      const play = () => {
        this.setState({
          startTime: this._audioContext.currentTime + DELAY,
          isPaused: false,
        });
      };

      this._runWhenAudioIsReady(play);
    };

    this._runWhenAudioIsReady = async fn => {
      try {
        await this._prepareAudio();
        fn();
      } catch (e) {
        console.warn('Error starting AudioContext', e);
      }
    };

    this._prepareAudio = () => {
      return new Promise((resolve, reject) => {
        if (this._audioContext.state === 'running') {
          resolve();
          return;
        }

        if (this._audioContext.state === 'suspended') {
          this._audioContext
            .resume()
            .then(resolve)
            .catch(reject);
          return;
        }

        reject(
          new Error(
            `Unknown AudioContext state ${this._audioContext.state}`
          )
        );
      });
    };

    this._togglePauseAndTrack = () => {
      this._togglePause();
    };

    this._globalKeyEventHandler = e => {
      const { keyCode } = e;

      if ([KEY_WHITESPACE, KEY_ENTER].indexOf(keyCode) === -1) {
        return true;
      }

      const tagName = e.target.tagName;
      if (tagName === 'INPUT' || tagName === 'TEXTAREA') {
        return true;
      }

      if (keyCode === KEY_ENTER) {
        if (this.state.isRecording) {
          this._addLine();
          e.cancelBubble = true;
          e.stopPropagation();
          e.stopImmediatePropagation();
          e.preventDefault();

          return false;
        }

        return true;
      }

      if (!this.state.beats || !this.state.beats.length) {
        return true;
      }

      e.cancelBubble = true;
      e.stopPropagation();
      e.stopImmediatePropagation();
      e.preventDefault();

      this._togglePause();

      return false;
    };

    this._deleteBeat = beatId => {
      this.setState({ selectedItem: null });
      this.props.onDeleteBeat({ beatId });
    };

    this._deleteLine = lineId => {
      let { recordingForLineId } = this.state;
      if (lineId === recordingForLineId) {
        recordingForLineId = null;
      }

      this.setState({ selectedItem: null, recordingForLineId });
      this.props.onDeleteLine({ lineId });
    };

    this._changeBeatsCount = (beatId, beatsCount) => {
      this.props.onChangeBeatsCount({ beatId, beatsCount });
    };

    this._recordingStarted = ({
      lineId,
      audioData,
      commonBeatsCount,
      commonBeatsTempo,
      trackBeatsCount,
      latency,
      sampleRate,
    }) => {
      if (!this.props.me) {
        return;
      }

      const recordingId = nanoid();
      const writingRecordingId = recordingId;
      const date = new Date();
      const dateStr = date
        .toISOString()
        .slice(0, -5)
        .replace('T', ' ');
      const line = this.state.lines.find(l => l.lineId === lineId);

      const recording = {
        lineId,
        recordingId,
        personId: this.props.me.personId,
        name: `${line.lineName} ${dateStr}`,
        beatsCount: trackBeatsCount,
        beatsTempo: commonBeatsTempo,
        sound: {
          type: 'recording',
          sampleRate,
          latency: latency,
          deviceId: this.state.selectedInputDeviceId,
        },
      };

      this.props.onStartRecording({ recording, audioData, lineId });
      this.setState({ writingRecordingId });
    };

    this._startPlaying = () => {
      if (this.state.isPaused) {
        this.setState({
          startTime: this._audioContext.currentTime + DELAY,
          isPaused: false,
        });

        return;
      }

      if (this.state.isRecording) {
        this._recorder.stopRecordLoop();
        this.setState({
          isRecording: false,
          startTime: this._audioContext.currentTime + DELAY,
          isPaused: false,
        });
      }
    };

    this._startRecording = async () => {
      if (!this.props.me) {
        return;
      }

      this._listMediaDevices();
      await this._prepareAudio();

      const { commonBeatsCount, commonBeatsTempo, beats } = this.state;
      const { selectedInputDeviceId } = this.props;
      let { recordingForLineId, lines } = this.state;

      const trackBeatsCount = trackUtils.getTrackBeatsCount({
        commonBeatsCount,
        beats,
      });

      if (!recordingForLineId) {
        const lineId = nanoid();
        const lineName = String.fromCharCode(65 + lines.length);
        const line = createNewLine({
          lineId,
          lineName,
          commonBeatsCount,
          trackBeatsCount,
          personId: this.props.me.personId,
        });

        recordingForLineId = lineId;
        this.props.onAddLine({ line });
        await this.setState({ recordingForLineId });
      }

      const soundDuration = trackBeatsCount * 60 / commonBeatsTempo;

      const { currentTime } = this._audioContext;

      await this._recorder.startRecordLoop({
        deviceId: selectedInputDeviceId,
        soundDuration,
        skipDuration: soundDuration,
        startTime: currentTime,
      });

      this.setState({
        startTime: currentTime,
        isRecording: true,
        isPaused: false,
      });
    };

    this._changeInputDeviceId = deviceId => {
      this.props.onChangeSelectedInputDeviceId({ deviceId });
    };

    this._addLine = () => {
      if (!this.props.me) {
        return;
      }

      const id = nanoid();
      const {
        commonBeatsCount,
        beats,
        isRecording,
        writingRecordingId,
      } = this.state;
      let { lines } = this.state;
      let currentlyWritingRecordingId = null;

      const lineName = String.fromCharCode(65 + lines.length);

      const trackBeatsCount = trackUtils.getTrackBeatsCount({
        commonBeatsCount,
        beats,
      });

      const line = createNewLine({
        lineId: id,
        lineName,
        commonBeatsCount,
        trackBeatsCount,
        personId: this.props.me.personId,
      });

      if (isRecording && writingRecordingId) {
        currentlyWritingRecordingId = writingRecordingId;
        line.writingRecordingId = writingRecordingId;
      }

      this.props.onAddLine({
        line,
        writingRecordingId: currentlyWritingRecordingId,
      });

      this.setState({ recordingForLineId: id });
    };

    this._changeCyclesCount = newCount => {
      this.props.onChangeBarsCount({ barsCount: newCount });
    };

    this._changePlayingRecording = (lineId, playingRecordingId) => {
      this.props.onUpdateLine({
        lineId,
        update: { playingRecordingId },
      });
    };

    this._changeRecordingLatency = (recordingId, latency) => {
      this.props.onChangeRecordingLatency({
        recordingId,
        latency,
      });
    };

    this._changeDefaultLatency = latency => {
      this.props.onChangeDefaultLatency({
        deviceId: this.props.selectedInputDeviceId,
        latency,
      });
    };

    this._listMediaDevices = () => {
      if (!this.props.mediaDevices.length) {
        this.props.listMediaDevices();
      }
    };
  }

  componentWillUnmount() {
    window.document.removeEventListener(
      'keydown',
      this._globalKeyEventHandler,
      true
    );

    this._recorder.off(Recorder.RECORDING_STARTED_EVENT);
    this._recorder.off(Recorder.RECORDING_FINISHED_EVENT);
  }

  componentDidMount() {
    if (!this.props.kits.length) {
      this.props.listKits();
    }

    if (!this.state.isChopFetched) {
      this.props.getChop();
    }

    window.document.addEventListener(
      'keydown',
      this._globalKeyEventHandler,
      true
    );

    Promise.all([
      Recorder.create({
        audioContext: this._audioContext,
      }),
    ]).then(([recorder]) => {
      this._recorder = recorder;

      this._recorder.on(
        Recorder.RECORDING_STARTED_EVENT,
        ({ audioData, sampleRate }) => {
          const {
            recordingForLineId,
            commonBeatsCount,
            commonBeatsTempo,
            beats,
          } = this.state;

          const trackBeatsCount = trackUtils.getTrackBeatsCount({
            commonBeatsCount,
            beats,
          });

          this._recordingStarted({
            lineId: recordingForLineId,
            audioData,
            latency: this.props.defaultLatency,
            sampleRate,
            commonBeatsCount,
            commonBeatsTempo,
            trackBeatsCount,
          });
        }
      );

      this._recorder.on(
        Recorder.RECORDING_FINISHED_EVENT,
        ({ audioData, sampleRate }) => {
          const { recordingForLineId, writingRecordingId } = this.state;

          const recording = this.state.recordings.find(
            r => r.recordingId === writingRecordingId
          );

          this.props.onFinishRecording({
            lineId: recordingForLineId,
            recordingId: writingRecordingId,
            recording,
            audioData,
          });

          this.setState({
            writingRecordingId: null,
          });
        }
      );

      this.setState({
        isInitialized: true,
      });
    });
  }

  _getCyclesCount() {
    const { commonBeatsCount, beats } = this.state;

    const trackBeatsCount = trackUtils.getTrackBeatsCount({
      commonBeatsCount,
      beats,
    });

    return trackBeatsCount / commonBeatsCount || 1;
  }

  render() {
    const {
      isInitialized,
      isChopFetched,
      isRecording,
      showShareDialog,
      showChangeNameDialog,
      startTime,
      commonBeatsCount,
      commonBeatsTempo,
      writingRecordingId,
      recordingForLineId,
      recordings,
      lines,
      beats,
      kits,
      isPaused,
      selectedItem,
      soloItem,
      composedBy,
    } = this.state;
    const { me } = this.props;
    let isMyChop;
    let chopFooter;

    if (isInitialized && isChopFetched) {
      isMyChop = me && me.personId === composedBy;
    }

    if (isInitialized && isChopFetched && beats.length && me) {
      chopFooter = (
        <ChopFooter
          me={me}
          myAccount={this.props.myAccount}
          onClap={this.props.onClap}
          onSendComment={this.props.onSendComment}
          chop={this.props.chop}
        />
      );
    }

    let fabs = [];

    let fabRecord = (
      <FloatingActionButton
        backgroundColor={'#fff'}
        key="fab-record"
        onTouchTap={this._startRecording}
      >
        <IconRecord
          color={colorStrForRecording()}
          width={24}
          height={24}
        />
      </FloatingActionButton>
    );
    let fabStopRecord = (
      <FloatingActionButton
        key="fab-stop"
        backgroundColor={'#fff'}
        onTouchTap={this._togglePause}
      >
        <IconStop
          color={colorStrForRecording()}
          width={24}
          height={24}
        />
      </FloatingActionButton>
    );
    let fabPlay = (
      <FloatingActionButton
        key="fab-play"
        onTouchTap={this._startPlaying}
      >
        <IconPlay
          color={theme.color.onPrimary}
          width={24}
          height={24}
        />
      </FloatingActionButton>
    );

    let fabStopPlay = (
      <FloatingActionButton
        key="fab-stop"
        onTouchTap={this._togglePause}
      >
        <IconStop
          color={theme.color.onPrimary}
          width={24}
          height={24}
        />
      </FloatingActionButton>
    );

    if (!isPaused && !isRecording) {
      if (isMyChop) {
        fabs.push(fabRecord);
      }
      fabs.push(fabStopPlay);
    }

    if (isPaused && beats.length) {
      if (isMyChop) {
        fabs.push(fabRecord);
      }
      fabs.push(fabPlay);
    }

    if (isRecording) {
      if (isMyChop) {
        fabs.push(fabStopRecord);
      }
      fabs.push(fabPlay);
    }

    let loadingIndicator;
    if (!isInitialized || !isChopFetched) {
      fabs = [];
      loadingIndicator = <LoadingIndicatorCircular />;
    }

    let selectedBeat;
    let selectedLine;
    let bottomSheetIcon;
    let bottomSheetIconBackgroundColor;
    let bottomSheetContent = null;
    let bottomSheetTitle;

    if (selectedItem && selectedItem.itemType === 'beat') {
      const isMuteDisabled = !!soloItem;
      const isMute =
        soloItem && soloItem.beatId !== selectedItem.beatId;
      const isSolo =
        soloItem && soloItem.beatId === selectedItem.beatId;

      selectedBeat = beats.find(b => b.id === selectedItem.beatId);
      selectedBeat = {
        ...selectedBeat,
        tempo:
          selectedBeat.beatsCount * commonBeatsTempo / commonBeatsCount,
      };

      bottomSheetIcon = selectedBeat.beatsCount;
      bottomSheetIconBackgroundColor = colorStrByBeat({
        beat: selectedBeat,
        isMute,
        isSolo,
      });

      bottomSheetTitle = `Subdivision`;
      bottomSheetContent = () => (
        <BeatBottomSheetContent
          key="selected-beat-controls"
          title={bottomSheetTitle}
          icon={bottomSheetIcon}
          iconBackgroundColor={bottomSheetIconBackgroundColor}
          width={this.state.windowWidth}
          onDismiss={() => this._toggleBeat(selectedItem.beatId)}
          onChangeTempo={this._changeTempo}
          onChangeOffset={this._changeOffset}
          onChangeBeatsCount={this._changeBeatsCount}
          onDeleteBeat={this._deleteBeat}
          onMuteSection={this._toggleMuteSection}
          onToggleSoloBeat={this._toggleSoloBeat}
          onToggleMuteBeat={this._toggleMuteBeat}
          onChangeSound={this._changeSound}
          commonBeatsCount={commonBeatsCount}
          activeSoundType={this.state.activeSoundType}
          activeKit={activeKit}
          isMuteDisabled={isMuteDisabled}
          isMute={isMute}
          isSolo={isSolo}
          selectedBeat={selectedBeat}
          isMyChop={isMyChop}
        />
      );
    }

    if (selectedItem && selectedItem.itemType === 'line') {
      const isMuteDisabled = !!soloItem;
      const isMute =
        soloItem && soloItem.lineId !== selectedItem.lineId;
      const isSolo =
        soloItem && soloItem.lineId === selectedItem.lineId;
      const isRecordingForLine =
        recordingForLineId === selectedItem.lineId;

      selectedLine = lines.find(l => l.lineId === selectedItem.lineId);

      const lineRecordings = recordings.filter(r => {
        return (
          r.lineId === selectedLine.lineId &&
          r.lineId !== selectedLine.writingRecordingId
        );
      });

      bottomSheetIcon = selectedLine.lineName;
      bottomSheetIconBackgroundColor = colorStrByLine({
        line: selectedLine,
        isMute,
        isSolo,
        isRecording: isRecordingForLine,
      });

      bottomSheetTitle = `Riff`;
      bottomSheetContent = () => (
        <LineBottomSheetContent
          key="selected-line-controls"
          title={bottomSheetTitle}
          icon={bottomSheetIcon}
          iconBackgroundColor={bottomSheetIconBackgroundColor}
          width={this.state.windowWidth}
          onDismiss={() => this._toggleLine(selectedItem.lineId)}
          onDeleteLine={this._deleteLine}
          onToggleSoloLine={this._toggleSoloLine}
          onToggleMuteLine={this._toggleMuteLine}
          onToggleRecordLine={this._toggleRecordLine}
          onChangePlayingRecording={this._changePlayingRecording}
          onChangeRecordingLatency={this._changeRecordingLatency}
          onChangeDefaultLatency={this._changeDefaultLatency}
          isMuteDisabled={isMuteDisabled}
          isMute={isMute}
          isSolo={isSolo}
          isRecording={isRecordingForLine}
          selectedLine={selectedLine}
          recordings={lineRecordings}
          isMyChop={isMyChop}
          defaultLatency={this.props.defaultLatency}
        />
      );
    }

    let tempoValue;
    let tempoBeatId;
    let activeKit;

    let tempoBeat = selectedBeat || beats[beats.length - 1];

    if (tempoBeat) {
      tempoBeatId = tempoBeat.id;
      tempoValue =
        tempoBeat.beatsCount * commonBeatsTempo / commonBeatsCount;
    }

    tempoValue = tempoValue || commonBeatsTempo;

    if (isInitialized) {
      activeKit = kits.find(k => k.id === this.state.activeKitId);
    }

    const audioBeats = trackUtils.beatsToAudio(this.state);

    let menuFn;

    if (isMyChop && isInitialized && isChopFetched) {
      const isPitchActive = this.state.activeSoundType === 'oscilator';
      const pitchIcon = isPitchActive ? 'done' : null;

      const soundChoices = [
        <MenuItem
          key="menu-item-sound-type-pitch"
          text={'Pitch'}
          iconName={pitchIcon}
          onTouchTap={() => {
            this.props.onChangeSoundType({ soundType: 'oscilator' });
          }}
        >
          Pitch
        </MenuItem>,
      ];

      for (let kit of kits) {
        if (!kit.isAvailable) {
          continue;
        }

        let kitIconName = null;
        if (
          this.state.activeSoundType === 'kit' &&
          this.state.activeKitId === kit.id
        ) {
          kitIconName = 'done';
        }
        soundChoices.push(
          <MenuItem
            key={`menu-item-sound-type-kit-${kit.id}`}
            text={kit.name}
            iconName={kitIconName}
            onTouchTap={() => {
              this.props.onChangeSoundType({
                soundType: 'kit',
                kitId: kit.id,
              });
            }}
          >
            {kit.name}
          </MenuItem>
        );
      }

      menuFn = dismissMenu => {
        const addBeat = () => {
          this._addDefaultBeat();
          dismissMenu();
        };

        const addLine = () => {
          this._addLine();
          dismissMenu();
        };

        return [
          <MenuItemBarsCountControl
            key="menu-item-bars-control"
            onChangeBarsCount={this._changeCyclesCount}
            barsCount={this._getCyclesCount()}
          />,
          <MenuItem
            key="menu-item-add-beat"
            iconName="plus"
            onTouchTap={addBeat}
            text={'Add beat'}
          />,
          <MenuItem
            key="menu-item-add-line"
            iconName="add-line"
            onTouchTap={addLine}
            text={'Add riff'}
          />,
          <Divider key="menu-divider-1" />,
          <MenuHeader
            key="menu-header-sound"
            text="Sound of the beat"
          />,
          ...soundChoices,

          <Divider key="menu-divider-2" />,
          <MenuHeader
            key="menu-header-input"
            text="Record sound from"
          />,
          ...this.props.mediaDevices
            .filter(d => d.kind === 'audioinput')
            .map(d => {
              let iconName = '';
              if (this.props.selectedInputDeviceId === d.deviceId) {
                iconName = 'done';
              }

              return (
                <MenuItem
                  key={`menu-input-device-${d.deviceId}`}
                  text={d.label}
                  iconName={iconName}
                  onTouchTap={() => {
                    this._changeInputDeviceId(d.deviceId);
                    dismissMenu();
                  }}
                />
              );
            }),
        ];
      };
    }

    let emptyState = [];
    if (isMyChop && isChopFetched && !beats.length) {
      emptyState = [
        <div key="empty-state" style={styles.__emptyStateContainer}>
          <TextWrapper
            key="empty-state-text"
            styleName="h5"
            color={theme.color.onBackground}
          >
            No beats here yet
          </TextWrapper>

          <div style={{ marginTop: 16 }}>
            <ButtonContained
              key="add-beat"
              onTouchTap={this._addDefaultBeat}
            >
              Add beat
            </ButtonContained>
          </div>
        </div>,
      ];
    }

    let readyState = [];
    if (isChopFetched && isInitialized && this.state.beats.length > 0) {
      let addLine;

      if (isMyChop) {
        addLine = this._addLine;
      }

      readyState = [
        <div
          key="tempo-control-wrap"
          style={styles.__tempoControlWrapper}
        >
          <TempoControl
            key="tempo-control"
            width={this.state.windowWidth - 32}
            tempo={tempoValue}
            beatId={tempoBeatId}
            onTempoChange={this._changeTempo}
          />
        </div>,

        <Track
          key="beats-track"
          track={{
            beats,
            lines: lines,
            recordings: recordings,
            recordingsAudioMap: this.state.recordingsAudioMap,
            commonBeatsCount,
            commonBeatsTempo,
            soloItem,
          }}
          selectedItem={selectedItem}
          startTime={this.state.startTime}
          getCurrentTime={this._getCurrentTime}
          width={this.state.windowWidth - 32}
          recordingForLineId={this.state.recordingForLineId}
          writingRecordingId={writingRecordingId}
          isPaused={this.state.isPaused}
          isRecording={this.state.isRecording}
          onBeatSelected={this._toggleBeat}
          onLineSelected={this._toggleLine}
          onAddLine={addLine}
        />,
      ];
    }

    let dialogContentFn = null;
    let dialogHeader = null;
    let dialogActions = [];

    if (showChangeNameDialog && isMyChop) {
      dialogHeader = 'Change name';
      dialogContentFn = () => {
        return (
          <ListItem key="dialog-item-name" noPadding>
            <div style={{ marginRight: 12 }}>
              <TextWrapper
                styleName="subtitle1"
                color={theme.color.onSurfaceMediumEmphasis}
              >
                {`Name`}
              </TextWrapper>
            </div>

            <InputText
              maxLength={16}
              onChange={this._changeName}
              value={this.state.name}
            />
          </ListItem>
        );
      };

      dialogActions = [
        <ButtonText
          onTouchTap={this._hideChangeNameDialog}
          color={theme.color.primary}
          key="dialog-done-button"
        >
          Done
        </ButtonText>,
      ];
    }

    if (showShareDialog && isMyChop) {
      dialogHeader = 'Sharing options';

      dialogContentFn = () => {
        let sharingDesc = `
          This chop is not listed on the home page.
          Others can only access it via its URL.
        `;

        if (this.state.isDiscoverable) {
          sharingDesc = `
          This chop is public and is listed on the home page.
          You can also share it via its URL.
          `;
        }

        const url = getFullUrl(this.props.location.pathname);

        return [
          <div key="dialog-item-text" style={{ marginBottom: 28 }}>
            <TextWrapper
              styleName={'body1'}
              color={theme.color.onSurfaceDisabled}
            >
              {sharingDesc}
            </TextWrapper>
          </div>,

          <ListItem key="dialog-item-name" noPadding>
            <div style={{ marginRight: 12 }}>
              <TextWrapper
                styleName="subtitle1"
                color={theme.color.onSurfaceMediumEmphasis}
              >
                {`Name`}
              </TextWrapper>
            </div>

            <InputText
              maxLength={16}
              onChange={this._changeName}
              value={this.state.name}
            />
          </ListItem>,

          <ListItem key="dialog-item-url" noPadding>
            <div style={{ marginRight: 12 }}>
              <TextWrapper
                styleName="subtitle1"
                color={theme.color.onSurfaceMediumEmphasis}
              >
                {`URL`}
              </TextWrapper>
            </div>
            <InputText readOnly value={url} />
          </ListItem>,

          <ListItem
            key="dialog-item-is-discoverable"
            onTouchTap={this._toggleIsDiscoverable}
            noPadding
          >
            <TextWrapper
              styleName="subtitle1"
              color={theme.color.onSurfaceMediumEmphasis}
            >
              {`Make public`}
            </TextWrapper>

            <Switch isOn={this.state.isDiscoverable} />
          </ListItem>,
        ];
      };

      dialogActions = [
        <ButtonText
          onTouchTap={this._hideShareDialog}
          color={theme.color.primary}
          key="dialog-done-button"
        >
          Done
        </ButtonText>,
      ];
    }

    let appBarActions = [];
    if (isInitialized && isChopFetched && this.props.myAccount) {
      let shareOrRemix;

      if (isMyChop) {
        shareOrRemix = (
          <ButtonIcon
            key="topbar-action-share"
            width={24}
            onTouchTap={this._share}
          >
            <IconShare
              width={24}
              height={24}
              color={theme.color.onSurfaceMediumEmphasis}
            />
          </ButtonIcon>
        );
      }

      if (!isMyChop) {
        shareOrRemix = (
          <ButtonIcon
            key="topbar-action-remix"
            width={24}
            onTouchTap={this._remix}
          >
            <IconRemix
              width={24}
              height={24}
              color={theme.color.onSurfaceMediumEmphasis}
            />
          </ButtonIcon>
        );
      }

      appBarActions.push(shareOrRemix);
    }

    appBarActions.push(
      <Link
        key="app-bar-action-feel-link"
        to={`/feel/${this.props.match.params.chopId}`}
        title="Switch to circle view"
      >
        <ButtonIcon key="topbar-action-rhythm" width={24}>
          <IconFeel
            width={24}
            height={24}
            color={theme.color.onSurfaceMediumEmphasis}
          />
        </ButtonIcon>
      </Link>
    );

    return (
      <TopBarContainer
        menuContent={menuFn}
        onShowMenu={this._listMediaDevices}
        dialogHeader={dialogHeader}
        dialogContent={dialogContentFn}
        dialogActions={dialogActions}
        bottomSheetIcon={bottomSheetIcon}
        bottomSheetIconBackgroundColor={bottomSheetIconBackgroundColor}
        bottomSheetTitle={bottomSheetTitle}
        onDismissBottomSheet={this._deselectAll}
        appBarActions={appBarActions}
        appBarTitle={
          <button
            style={styles.__topBarButton}
            onClick={this._showChangeNameDialog}
          >
            <TextWrapper
              styleName={'topBarTitle'}
              color={theme.color.onSurfaceMediumEmphasis}
            >
              {this.state.name}
            </TextWrapper>
          </button>
        }
        appBarLeftIcon={
          <Link to={`/`} style={{ height: '100%', width: '100%' }}>
            <IconHome
              width={24}
              color={theme.color.onSurfaceMediumEmphasis}
            />
          </Link>
        }
        fabs={fabs}
      >
        <AudioLoader
          recordings={recordings}
          kits={kits}
          beats={beats}
          lines={lines}
        >
          {({ recordingByLineId, beatsAudio, recordingsAudio }) => [
            ...emptyState,
            ...readyState,

            <AudioPlayer
              key="sounds-audio-player"
              startTime={this.state.startTime}
              beats={audioBeats}
              lines={[]}
              commonBeatsCount={commonBeatsCount}
              commonBeatsTempo={commonBeatsTempo}
              recordingByLineId={recordingByLineId}
              beatsAudio={beatsAudio}
              recordingsAudio={recordingsAudio}
              audioContext={this._audioContext}
            />,

            ...trackUtils
              .linesToAudio(this.state)
              .map(l => (
                <AudioPlayer
                  key={`lines-audio-player-${l.lineId}`}
                  startTime={this.state.startTime}
                  beats={[]}
                  lines={[l]}
                  commonBeatsCount={commonBeatsCount}
                  commonBeatsTempo={commonBeatsTempo}
                  recordingByLineId={recordingByLineId}
                  beatsAudio={beatsAudio}
                  recordingsAudio={recordingsAudio}
                  audioContext={this._audioContext}
                />
              )),
          ]}
        </AudioLoader>
        {bottomSheetContent ? bottomSheetContent() : chopFooter}

        {isRecording ? (
          <SnackbarRecording
            beats={beats}
            startTime={startTime}
            getCurrentTime={this._getCurrentTime}
            commonBeatsCount={commonBeatsCount}
            commonBeatsTempo={commonBeatsTempo}
          />
        ) : null}

        {loadingIndicator}
      </TopBarContainer>
    );
  }
}
export default connectChopEditor(Tape);
