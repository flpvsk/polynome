import React, { Component } from 'react';

import theme from './theme';
import viewport from './viewport';
import { elevationToStyle } from './elevation';

import ButtonIcon from './ButtonIcon';
import TextWrapper from './TextWrapper';
import InputTextAutosize from './InputTextAutosize';

import IconSend from './IconSend';

const styles = {
  block: {
    position: 'fixed',
    bottom: 0,
    left: 0,
    right: 0,
    minHeight: 56,
    backgroundColor: theme.color.primary,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',

    ...elevationToStyle(7),
  },

  __contentArea: {
    flexGrow: 1,
    display: 'flex',
    paddingLeft: 16,
    paddingRight: 16,
    alignItems: 'stretch',
    flexDirection: 'column',
    maxWidth: viewport.getWidth(),
    paddingTop: 8,
    paddingBottom: 8,
  },

  __commentAreaContent: {
    flexGrow: 1,
    display: 'flex',
    alignItems: 'center',
  },

  __charCounter: {
    marginTop: 4,
  },
};

class InputComment extends Component {
  constructor() {
    super();

    this.state = {
      value: '',
    };

    this._changeValue = value => {
      this.setState({ value });
    };

    this._send = e => {
      this.props.onSend(this.state.value);

      if (e) {
        e.preventDefault();
        e.stopPropagation();
      }
    };
  }

  render() {
    const { value } = this.state;
    let charTextColor = theme.color.onPrimaryDisabled;
    let buttonColor = theme.color.onPrimary;
    let isButtonDisabled = false;

    if (value.length > 320) {
      charTextColor = theme.color.error;
    }

    if (value.length > 320 || value.length === 0) {
      isButtonDisabled = true;
      buttonColor = theme.color.onPrimaryDisabled;
    }

    return (
      <div style={styles.block}>
        <div style={styles.__contentArea}>
          <form
            style={styles.__commentAreaContent}
            onSubmit={this._send}
          >
            <InputTextAutosize
              value={value}
              autoFocus
              onChange={this._changeValue}
              onClick={e => e.stopPropagation()}
            />

            <div style={{ marginLeft: 8, alignSelf: 'flex-end' }}>
              <ButtonIcon
                isDisabled={isButtonDisabled}
                onTouchTap={this._send}
              >
                <IconSend color={buttonColor} width={24} />
              </ButtonIcon>
            </div>

            <button style={{ display: 'none' }} />
          </form>

          <div title="Character counter" style={styles.__charCounter}>
            <TextWrapper styleName={'subtitle2'} color={charTextColor}>
              {value.length} / 320
            </TextWrapper>
          </div>
        </div>
      </div>
    );
  }
}

export default InputComment;
