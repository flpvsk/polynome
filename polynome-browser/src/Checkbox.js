import React from 'react';
import theme from './theme';

const styles = {
  block: {
    border: `2px solid ${theme.color.outline}`,
  },
};

class Checkbox extends React.Component {
  render() {
    return <div style={styles.block} />;
  }
}

export default Checkbox;
