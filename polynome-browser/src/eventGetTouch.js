const getEventY = e => {
  if (e.touches && !e.touches.length) {
    throw new Error('No touch event detected');
  }

  if (e.touches) {
    return [e.touches[0].clientX, e.touches[0].clientY];
  }

  return [e.clientX, e.clientY];
};

export default getEventY;
