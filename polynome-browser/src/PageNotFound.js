import React from 'react';

import theme from './theme';

import AppBarTopContainer from './AppBarTopContainer';
import TextWrapper from './TextWrapper';
import ButtonIcon from './ButtonIcon';

import withNavigation from './withNavigation';

import logo from './logo.svg';

const PageNotFound = props => {
  return (
    <AppBarTopContainer
      showNavigation={props.showNavigation}
      navigationContent={props.navigationContent}
      appBarTitle={
        <TextWrapper
          styleName={'topBarTitle'}
          color={theme.color.onSurfaceMediumEmphasis}
        >
          {'Page not found'}
        </TextWrapper>
      }
      appBarLeftIcon={
        <ButtonIcon onTouchTap={props.onShowNavigation}>
          <img width="100%" height="100%" src={logo} alt="Logo" />
        </ButtonIcon>
      }
      appBarActions={[]}
    >
      <div
        style={{
          display: 'flex',
          width: '100%',
          paddingTop: 32,
          paddingLeft: 16,
        }}
      >
        <TextWrapper
          styleName={'h4'}
          color={theme.color.onBackgroundMediumEmphasis}
        >
          {`There's no groove here.`}
        </TextWrapper>
      </div>
    </AppBarTopContainer>
  );
};

export default withNavigation(PageNotFound);
