import Emitter from 'tiny-emitter';
import closeMediaStream from './closeMediaStream';

function copyToChannel({ audioData, channelData, channelNum, offset }) {
  if (audioData.copyToChannel) {
    audioData.copyToChannel(channelData, channelNum, offset);
    return;
  }

  if (audioData.getChannelData) {
    let arrayToFill = audioData.getChannelData(channelNum);
    arrayToFill.set(channelData, offset);
    return;
  }

  throw new Error(`AudioBuffer doesn't support copy`);
}

class RecorderScriptProcessor {
  static get RECORDING_FINISHED_EVENT() {
    return 'recording-finished';
  }

  static get RECORDING_STARTED_EVENT() {
    return 'recording-started';
  }

  static async create({ audioContext }) {
    return new RecorderScriptProcessor({ audioContext });
  }

  constructor({ audioContext }) {
    this._emitter = new Emitter();
    this._audioContext = audioContext;

    this._stream = null;
    this._streamSource = null;
    this._processor = null;
    this._audioData = null;

    this._isRecordLoopInProgress = false;

    this.on = this._emitter.on.bind(this._emitter);
    this.off = this._emitter.off.bind(this._emitter);
  }

  async startRecordLoop({ soundDuration, skipDuration, deviceId }) {
    const audioContext = this._audioContext;

    const constraint = {
      echoCancellation: true,
      latency: 0,
    };

    if (deviceId) {
      constraint.deviceId = {
        exact: deviceId,
      };
    }

    const stream = await navigator.mediaDevices.getUserMedia({
      audio: constraint,
    });

    const streamSource = audioContext.createMediaStreamSource(stream);
    const processor = audioContext.createScriptProcessor(
      2048,
      streamSource.numberOfOutputs,
      1
    );

    const audioBufferSize = Math.ceil(
      soundDuration * audioContext.sampleRate
    );
    const skipBufferSize = Math.ceil(
      skipDuration * audioContext.sampleRate
    );
    const loopBufferSize = skipBufferSize + audioBufferSize;

    let audioData = audioContext.createBuffer(
      streamSource.numberOfOutputs,
      audioBufferSize,
      audioContext.sampleRate
    );

    this._emitter.emit(
      RecorderScriptProcessor.RECORDING_STARTED_EVENT,
      {
        audioData: audioData,
        sampleRate: audioContext.sampleRate,
      }
    );

    let bytesReceived = 0;
    processor.onaudioprocess = e => {
      const loopsBeforeData = Math.floor(
        bytesReceived / loopBufferSize
      );
      const eventDataSize = e.inputBuffer.getChannelData(0).length;
      const loopsAfterData = Math.floor(
        (bytesReceived + eventDataSize) / loopBufferSize
      );
      const loopPositionBeforeData =
        bytesReceived - loopsBeforeData * loopBufferSize;
      const loopPositionAfterData =
        bytesReceived + eventDataSize - loopsAfterData * loopBufferSize;

      if (
        loopsBeforeData === loopsAfterData &&
        loopPositionBeforeData <= skipBufferSize &&
        loopPositionAfterData <= skipBufferSize
      ) {
        // same loop, we're in skip part
        bytesReceived += eventDataSize;
        return;
      }

      let lineBytesWritten = 0;
      let lineBytesToWrite = eventDataSize;
      let startWritingFrom = 0;

      if (loopsBeforeData === loopsAfterData) {
        if (loopPositionBeforeData <= skipBufferSize) {
          lineBytesWritten = 0;
          startWritingFrom = skipBufferSize - loopPositionBeforeData;
        }

        if (loopPositionBeforeData > skipBufferSize) {
          lineBytesWritten = loopPositionBeforeData - skipBufferSize;
        }
      }

      if (loopsBeforeData !== loopsAfterData) {
        if (loopPositionBeforeData > skipBufferSize) {
          lineBytesWritten = loopPositionBeforeData - skipBufferSize;
          lineBytesToWrite = audioBufferSize - lineBytesWritten;
        }

        if (loopPositionAfterData > skipBufferSize) {
          console.warn(
            'loop buffer size is too large, this should not happen'
          );
          bytesReceived += eventDataSize;
          return;
        }
      }

      for (
        let channelNum = 0;
        channelNum < streamSource.numberOfOutputs;
        channelNum++
      ) {
        let channelData = e.inputBuffer.getChannelData(channelNum);
        if (channelData.length !== lineBytesToWrite) {
          channelData = channelData.slice(
            startWritingFrom + 1,
            lineBytesToWrite
          );
        }

        let offset = 0;
        if (lineBytesWritten > 0) {
          offset = lineBytesWritten + 1;
        }

        copyToChannel({ audioData, channelData, channelNum, offset });

        // this audioData will be emitted on stop
        this._audioData = audioData;
      }

      if (lineBytesWritten + lineBytesToWrite === audioBufferSize) {
        let completeAudioData = audioData;
        audioData = audioContext.createBuffer(
          streamSource.numberOfOutputs,
          audioBufferSize,
          audioContext.sampleRate
        );

        // wait until first bytes are written to emit
        this._audioData = null;

        // off the processing "thread"
        Promise.resolve()
          .then(() => {
            this._emitter.emit(
              RecorderScriptProcessor.RECORDING_FINISHED_EVENT,
              {
                audioData: completeAudioData,
                sampleRate: audioContext.sampleRate,
              }
            );
          })
          .then(() => {
            this._emitter.emit(
              RecorderScriptProcessor.RECORDING_STARTED_EVENT,
              {
                audioData: audioData,
                sampleRate: audioContext.sampleRate,
              }
            );
          });
      }

      bytesReceived += eventDataSize;
    };

    streamSource.connect(processor);
    processor.connect(audioContext.destination);

    this._stream = stream;
    this._streamSource = streamSource;
    this._processor = processor;
    this._isRecordLoopInProgress = true;
  }

  async stopRecordLoop() {
    if (this._streamSource) {
      this._streamSource.disconnect();
      this._streamSource = null;
    }

    if (this._processor) {
      this._processor.disconnect();
      this._processor = null;
    }

    if (this._stream) {
      closeMediaStream(this._stream);
      this._stream = null;
    }

    if (this._audioData) {
      this._emitter.emit(
        RecorderScriptProcessor.RECORDING_FINISHED_EVENT,
        {
          audioData: this._audioData,
          sampleRate: this._audioContext.sampleRate,
        }
      );
      this._audioData = null;
    }
  }
}

export default RecorderScriptProcessor;
