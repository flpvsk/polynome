import React from 'react';
import theme from './theme';

import ListItem from './ListItem';
import TextButton from './ButtonText';
import TextWrapper from './TextWrapper';
import Divider from './Divider';
import Switch from './Switch';
import DropdownListItem from './DropdownListItem';
import InputNumberListItem from './InputNumberListItem';
import ButtonContained from './ButtonContained';

const styles = {
  __buttonWrapper: {
    marginLeft: -16,
  },
};

class LineBottomSheetContent extends React.Component {
  constructor() {
    super();

    this._deleteLine = () => {
      this.props.onDeleteLine(this.props.selectedLine.lineId);
    };

    this._changeRecordingLatency = latency => {
      const currentRecording = this._getCurrentRecording();
      this.props.onChangeRecordingLatency(
        currentRecording.recordingId,
        latency
      );
    };

    this._changeDefaultLatency = latency => {
      this.props.onChangeDefaultLatency(latency);
    };

    this._changePlayingRecording = recordingId => {
      this.props.onChangePlayingRecording(
        this.props.selectedLine.lineId,
        recordingId
      );
    };

    this._downloadCurrentRecording = () => {
      const recording = this._getCurrentRecording();
      if (recording.sound.url) {
        window.open(recording.sound.url);
      }
    };
  }

  _getCurrentRecording() {
    return this.props.recordings.find(r => {
      return (
        r.recordingId === this.props.selectedLine.playingRecordingId
      );
    });
  }

  render() {
    const { selectedLine } = this.props;
    const {
      recordings,
      isMuteDisabled,
      onToggleMuteLine,
      onToggleSoloLine,
      onToggleRecordLine,
      isSolo,
      isRecording,
      isMyChop,
    } = this.props;
    const lineId = selectedLine.lineId;
    let recordingsDropdown;
    let recordingLatencyItem;
    let downloadRecordingItem;

    if (recordings && recordings.length) {
      recordingsDropdown = (
        <DropdownListItem
          onChange={this._changePlayingRecording}
          value={selectedLine.playingRecordingId}
          isDisabled={!isMyChop}
          options={recordings
            .filter(r => r.recordingStatus !== 'recording')
            .map(r => {
              return {
                key: `option-recording-${r.recordingId}`,
                text: r.name,
                value: r.recordingId,
              };
            })}
        />
      );

      const currentRecording = this._getCurrentRecording();

      if (currentRecording && currentRecording.sound.url) {
        downloadRecordingItem = (
          <ListItem
            onTouchTap={this._downloadCurrentRecording}
            key="item-recording-download"
          >
            <div style={styles.__buttonWrapper}>
              <TextButton>Open Recording</TextButton>
            </div>
          </ListItem>
        );
      }

      recordingLatencyItem = (
        <ListItem key="item-recording-latency">
          <TextWrapper
            color={theme.color.onSurfaceMediumEmphasis}
            styleName="subtitle1"
          >
            {`Rec latency, sec`}
          </TextWrapper>
          <InputNumberListItem
            onChange={this._changeRecordingLatency}
            value={currentRecording.sound.latency}
            isDisabled={!isMyChop}
          />
        </ListItem>
      );
    }

    if (!recordings || !recordings.length) {
      recordingsDropdown = (
        <TextWrapper
          color={theme.color.onSurfaceMediumEmphasis}
          styleName="body1"
        >
          {`no recordings yet`}
        </TextWrapper>
      );
    }

    let removeItem;
    if (isMyChop) {
      removeItem = (
        <ListItem key="item-remove">
          <div style={styles.__buttonWrapper}>
            <TextButton onTouchTap={this._deleteLine}>
              remove riff
            </TextButton>
          </div>
        </ListItem>
      );
    }

    return [
      <ListItem key="item-header" height={72}>
        <TextWrapper
          styleName={'h4'}
          color={theme.color.onBackgroundMediumEmphasis}
        >
          {'Riff'}
        </TextWrapper>
        <ButtonContained
          elevation={0}
          background={this.props.iconBackgroundColor}
          textColor={theme.color.onSecondaryMediumEmphasis}
          onTouchTap={this.props.onDismiss}
        >
          {this.props.icon}
        </ButtonContained>
      </ListItem>,
      <ListItem
        key="item-record"
        onTouchTap={isMyChop ? () => onToggleRecordLine(lineId) : null}
      >
        <TextWrapper
          styleName="subtitle1"
          color={theme.color.onSurfaceMediumEmphasis}
        >
          Record
        </TextWrapper>

        <Switch isOn={isRecording} isDisabled={!isMyChop} />
      </ListItem>,

      <Divider key="item-record-divider" />,

      <ListItem
        key="item-mute"
        onTouchTap={() => !isMuteDisabled && onToggleMuteLine(lineId)}
      >
        <TextWrapper
          styleName="subtitle1"
          color={theme.color.onSurfaceMediumEmphasis}
        >
          Mute
        </TextWrapper>

        <Switch
          isOn={selectedLine.isMute}
          isDisabled={isMuteDisabled}
        />
      </ListItem>,

      <ListItem
        key="item-solo"
        onTouchTap={() => onToggleSoloLine(lineId)}
      >
        <TextWrapper
          styleName="subtitle1"
          color={theme.color.onSurfaceMediumEmphasis}
        >
          Solo
        </TextWrapper>

        <Switch isOn={isSolo} />
      </ListItem>,

      <Divider key="mute-solo-divider" />,

      <ListItem key="item-recording">
        <TextWrapper
          styleName="subtitle1"
          color={theme.color.onSurfaceMediumEmphasis}
        >
          Current recording
        </TextWrapper>

        {recordingsDropdown}
      </ListItem>,

      recordingLatencyItem,
      <ListItem key="item-recording-latency-default">
        <TextWrapper
          color={theme.color.onSurfaceMediumEmphasis}
          styleName="subtitle1"
        >
          {`Default latency, sec`}
        </TextWrapper>
        <InputNumberListItem
          onChange={this._changeDefaultLatency}
          value={this.props.defaultLatency}
          isDisabled={!isMyChop}
        />
      </ListItem>,

      downloadRecordingItem,

      <Divider key={`remove-divider`} />,
      removeItem,
    ];
  }
}

export default LineBottomSheetContent;
