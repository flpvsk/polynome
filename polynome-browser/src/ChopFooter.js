import React, { Component } from 'react';
import { withRouter } from 'react-router';

import debounce from 'lodash/debounce';

import theme from './theme';
import viewport from './viewport';
import { elevationToStyle } from './elevation';
import toPrettyNumber from './toPrettyNumber';

import ButtonIcon from './ButtonIcon';
import TextWrapper from './TextWrapper';
import InputComment from './InputComment';
import Userpic from './Userpic';
import Comment from './Comment';

import IconClap from './IconClap';
import IconCommentAdd from './IconCommentAdd';

import Divider from './Divider';

const styles = {
  block: {
    width: viewport.getWidth(),
  },

  __actions: {
    height: 72,
    paddingLeft: 16,
    paddingRight: 16,
    display: 'flex',
    alignItems: 'center',
  },

  __actionsClap: {
    display: 'flex',
    alignItems: 'center',
    flexGrow: 0,
  },

  __actionsOther: {
    display: 'flex',
    alignItems: 'center',
    flexGrow: 0,
    justifyContent: 'flex-end',
    marginLeft: 24,
  },

  __composer: {
    height: 72,
    paddingLeft: 16,
    paddingRight: 16,
    display: 'flex',
    alignItems: 'center',
    cursor: 'pointer',
  },

  __composerUserpic: {
    display: 'flex',
    alignItems: 'center',
    width: 56,
    flexGrow: 0,
  },

  __newComment: {
    display: 'flex',
    alignItems: 'center',

    position: 'fixed',
    bottom: 8,

    paddingLeft: 16,
    paddingRight: 16,
    height: 72,
    width: viewport.getWidth(),

    backgroundColor: theme.color.surface,
    ...elevationToStyle(2),
    zIndex: 10,
  },

  __newCommentText: {
    paddingLeft: 16,
    flexGrow: 1,
  },

  __newCommentActions: {
    display: 'flex',
  },

  __actionWrapper: {
    display: 'flex',
    alignItems: 'center',
    minWidth: 56,
  },
};

class ChopFooter extends Component {
  constructor() {
    super();
    this.state = {
      clapsCount: 0,
      isCommenting: false,
    };

    this._sendClaps = debounce(() => {
      if (!this.props.me) {
        return;
      }

      const { chopId } = this.props.chop;
      const { personId } = this.props.me;
      const clapsCount = this.state.clapsCount;

      this.setState({ clapsCount: 0 });

      this.props.onClap({ chopId, clapsCount, personId });
    }, 500);

    this._clap = () => {
      if (!this.props.myAccount) {
        return;
      }

      this.setState({
        clapsCount: this.state.clapsCount + 1,
      });
      this._sendClaps();
    };

    this._startCommenting = () => {
      if (!this.props.myAccount) {
        return;
      }

      this.setState({
        isCommenting: true,
      });
    };

    this._stopCommenting = () => {
      this.setState({
        isCommenting: false,
      });
    };

    this._sendComment = text => {
      if (!this.props.me) {
        return;
      }

      const chopId = this.props.chop.chopId;
      const personId = this.props.me.personId;

      this.setState({
        isCommenting: false,
        commentText: '',
      });

      this.props.onSendComment({
        chopId,
        text,
        personId,
      });

      // TODO: get rid of this hack
      setTimeout(() => {
        if (window) {
          window.scrollTo(0, document.body.scrollHeight);
        }
      }, 0);
    };

    this._goToComposer = () => {
      this.props.history.push(`/people/${this.props.chop.composedBy}`);
    };
  }

  render() {
    const { chop } = this.props;
    const clapsCount = (chop.clapsCount || 0) + this.state.clapsCount;
    let clapColor = theme.color.onBackgroundMediumEmphasis;

    if (chop.hasMyClaps || this.state.clapsCount > 0) {
      clapColor = theme.color.primaryLight;
    }

    const visibleComments = chop.comments.map(c => (
      <Comment key={`comment-${c.commentId}`} comment={c} />
    ));

    let inputComment;
    if (this.state.isCommenting) {
      inputComment = <InputComment onSend={this._sendComment} />;
    }

    return (
      <div style={styles.block}>
        <div style={styles.__composer} onClick={this._goToComposer}>
          <div style={styles.__composerUserpic}>
            <Userpic size={48} person={chop.composer} />
          </div>
          <div style={styles.__composerText}>
            <TextWrapper
              styleName={'h4'}
              color={theme.color.onBackground}
            >
              {chop.composer.handle || 'anonymous'}
            </TextWrapper>
          </div>
        </div>

        <Divider />

        <div style={styles.__actions}>
          <div style={styles.__actionsClap}>
            <ButtonIcon
              onTouchTap={this._clap}
              title="Clap"
              key={`action-clap`}
            >
              <IconClap width={40} height={40} color={clapColor} />
            </ButtonIcon>
            <span style={{ marginLeft: 8 }}>
              <TextWrapper styleName={'h6'} color={clapColor}>
                {clapsCount}
              </TextWrapper>
            </span>
          </div>

          <div style={styles.__actionsOther}>
            <div style={styles.__actionWrapper}>
              <ButtonIcon
                key={`action-comment`}
                onTouchTap={this._startCommenting}
                title="Comment"
              >
                <IconCommentAdd
                  width={24}
                  color={theme.color.onBackgroundMediumEmphasis}
                />
              </ButtonIcon>

              <TextWrapper
                styleName={'subtitle2'}
                color={theme.color.onSurfaceMediumEmphasis}
              >
                <strong>
                  {chop.comments
                    ? toPrettyNumber(chop.comments.length)
                    : ` `}
                </strong>
              </TextWrapper>
            </div>
          </div>
        </div>

        <Divider />
        {visibleComments}
        {inputComment}
      </div>
    );
  }
}

export default withRouter(ChopFooter);
