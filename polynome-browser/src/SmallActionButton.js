import React from 'react';

const styles = {
  block: {
    fontSize: '0.8em',
    fontWeight: '300',
    color: '#eee',
    textAlign: 'center',
    lineHeight: '1em',
    height: '32px',
    width: '32px',
    background: 'none',
  },

  _isPressed: {
    backgroundColor: '#1d29b9',
    border: 'none',
  },

  _isDisabled: {
    border: '1px solid #777',
    color: '#ababab',
  },
};

const SmallActionButton = props => {
  let buttonStyle = {
    ...styles.block,
  };

  if (props.isPressed) {
    buttonStyle = {
      ...buttonStyle,
      ...styles._isPressed,
    };
  }

  if (props.isPressed && props.pressedColor) {
    buttonStyle = {
      ...buttonStyle,
      backgroundColor: props.pressedColor,
      border: 'none',
    };
  }

  if (props.isDisabled) {
    buttonStyle = {
      ...buttonStyle,
      ...styles._isDisabled,
    };
  }

  return (
    <button
      style={buttonStyle}
      title={props.title}
      onClick={props.onTouchTap}
    >
      {props.children}
    </button>
  );
};

export default SmallActionButton;
