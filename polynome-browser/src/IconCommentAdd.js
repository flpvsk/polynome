import React from 'react';

const IconCommentAdd = ({ width, height, color, flip }) => {
  let style = {};

  if (!flip) {
    style = { transform: 'scale(-1, 1)' };
  }

  return (
    <svg
      style={style}
      width={width}
      height={height}
      fill={color}
      viewBox="0 0 24 24"
    >
      <path fill="none" d="M0 0h24v24H0V0z" />
      <path d="M21.99 4c0-1.1-.89-2-1.99-2H4c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h14l4 4-.01-18zM17 11h-4v4h-2v-4H7V9h4V5h2v4h4v2z" />
    </svg>
  );
};

export default IconCommentAdd;
