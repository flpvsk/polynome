import React, { Component } from 'react';
import InputPlain from './InputPlain';

const SCRIPT_ID = '_googleMaps';

class InputAddress extends Component {
  constructor() {
    super();

    this.state = { value: '' };

    this._scriptReady = () => {
      let autocomplete = new window.google.maps.places.Autocomplete(
        this._inputRef,
        { types: ['geocode'] }
      );

      autocomplete.addListener('place_changed', () => {
        const place = autocomplete.getPlace();
        const addressString = place.formatted_address;

        this.setState({ value: addressString });
        this.props.onChange({
          addressString,
          addressComponents: (place.address_components || []).map(
            c => ({
              longName: c.long_name,
              shortName: c.short_name,
              types: c.types,
            })
          ),
        });
      });
    };

    this._setAddressString = value => {
      if (this.state.value === value) {
        return;
      }

      this.setState({ value });

      this.props.onChange({ addressString: value });
    };
  }

  componentDidMount() {
    const windowScript = window[SCRIPT_ID];

    if (!windowScript) {
      const script = document.createElement('script');
      script.src =
        `https://maps.googleapis.com/maps/api/js?` +
        `key=${
          process.env.REACT_APP_GOOGLE_MAPS_API_KEY
        }&libraries=places`;
      script.id = SCRIPT_ID;
      script.async = true;
      script.defer = true;
      script.onload = this._scriptReady;
      window.document.body.appendChild(script);
    }

    if (windowScript && !window.google) {
      windowScript.onload = this._scriptReady;
    }

    if (windowScript && window.google) {
      this._scriptReady();
    }
  }

  componentWillUnmount() {
    if (window[SCRIPT_ID]) {
      window[SCRIPT_ID].onload = null;
    }
  }

  render() {
    return (
      <InputPlain
        type="text"
        autoComplete="off"
        readOnly={this.props.readOnly}
        disabled={this.props.readOnly}
        required={true}
        inputRef={r => (this._inputRef = r)}
        placeholder={this.props.placeholder}
        value={this.state.value}
        onChange={this._setAddressString}
      />
    );
  }
}

export default InputAddress;
