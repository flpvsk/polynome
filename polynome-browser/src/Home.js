import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

import nanoid from 'nanoid';

import { connect } from 'react-redux';
import * as people from './data/people';
import * as chops from './data/chops';

import theme from './theme';
import viewport from './viewport';

import shouldComponentUpdate from './shouldComponentUpdate';

import PolyAudioContext, { prepareContext } from './AudioContext';

import AppBarTopContainer from './AppBarTopContainer';
import TextWrapper from './TextWrapper';
import ButtonIcon from './ButtonIcon';
import ButtonContained from './ButtonContained';
import InputComment from './InputComment';
import CardFeed from './CardFeed';

import LoadingIndicatorCircular from './LoadingIndicatorCircular';

import IconPlus from './IconPlus';
import IconNotifications from './IconNotifications';
import IconMenu from './IconMenu';

import ChopPlayer from './ChopPlayer';

import withNavigation from './withNavigation';
import withMyAccountProvider from './data/withMyAccountProvider';

import LogoText from './LogoText';

const styles = {
  __content: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    width: '100%',
    paddingLeft: 16,
    paddingRight: 16,
  },

  __subheader: {
    width: '100%',
    height: 48,
    display: 'flex',
    alignItems: 'center',
  },

  _bumpUp: {
    marginTop: -16,
  },

  __commentArea: {
    position: 'fixed',
    bottom: 0,
    minHeight: 56,
    width: '100%',
    backgroundColor: theme.color.primary,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
  },

  __commentAreaContent: {
    flexGrow: 1,
    display: 'flex',
    paddingLeft: 16,
    paddingRight: 16,
    alignItems: 'center',
    maxWidth: viewport.getWidth(),
    paddingTop: 8,
    paddingBottom: 8,
  },

  __loadingWrapper: {
    paddingTop: 8,
    paddingBottom: 16,
  },
};

class Home extends Component {
  constructor() {
    super();

    this.state = {
      plaingChop: null,
      commentingOn: null,
      commentsToShowByChopId: {},
      loadingAudioFor: [],
    };

    this._audioContext = new PolyAudioContext();

    this.shouldComponentUpdate = shouldComponentUpdate;

    this._playChop = async chop => {
      await prepareContext(this._audioContext);
      this.setState({
        playingChop: chop,
      });
    };

    this._stop = () => {
      this.setState({
        playingChop: null,
      });
    };

    this._commentOn = chopId => {
      this.setState({
        commentingOn: chopId,
      });
    };

    this._stopCommenting = e => {
      e.preventDefault();
      e.stopPropagation();

      this.setState({
        commentingOn: null,
      });
    };

    this._sendComment = text => {
      const chopId = this.state.commentingOn;
      const personId = this.props.me.personId;

      this.setState({
        commentingOn: null,
      });

      const chop = this._findChop(chopId);

      this._changeCommentsCountToShow(chopId, chop.comments.length + 1);

      this.props.onSendComment({
        chopId,
        text,
        personId,
      });
    };

    this._getCommentsToShowByChopId = chopId => {
      const commentsToShow = this.state.commentsToShowByChopId[chopId];
      if (!commentsToShow) {
        return 2;
      }
    };

    this._changeCommentsCountToShow = (chopId, count) => {
      this.setState({
        commentsToShowByChopId: {
          ...this.state.commentsToShowByChopId,
          [chopId]: count,
        },
      });
    };

    this._findChop = chopId => {
      const { myRecentChops, publicChops } = this.props;

      return myRecentChops
        .concat(publicChops)
        .find(c => c.chopId === chopId);
    };

    this._onStartAudioLoad = chopId => {
      const { loadingAudioFor } = this.state;
      const loadingAudioSet = new Set(loadingAudioFor);
      loadingAudioSet.add(chopId);

      this.setState({
        loadingAudioFor: [...loadingAudioSet],
      });
    };

    this._onFinishAudioLoad = chopId => {
      const { loadingAudioFor } = this.state;
      this.setState({
        loadingAudioFor: loadingAudioFor.filter(c => c !== chopId),
      });
    };
  }

  componentDidMount() {
    this.props.listPublicChops();
    this.props.listMyChops();
  }

  render() {
    const {
      me,
      myRecentChops,
      publicChops,
      isListMyChopsPending,
      isListPublicChopsPending,
    } = this.props;
    const { playingChop, commentingOn, loadingAudioFor } = this.state;

    let myChopsFeed = [];
    let publicChopsFeed = [];
    let commentingOnChop;
    let inputComment;
    let dismissInputComment;
    let audio = null;

    if (playingChop) {
      audio = (
        <ChopPlayer
          audioContext={this._audioContext}
          chop={playingChop}
          startTime={0}
          onStartLoad={this._onStartAudioLoad}
          onFinishLoad={this._onFinishAudioLoad}
        />
      );
    }

    commentingOnChop = this._findChop(commentingOn);

    if (commentingOnChop) {
      inputComment = (
        <InputComment
          key={`${commentingOnChop.chopId}-new-comment`}
          onSend={this._sendComment}
        />
      );
      dismissInputComment = this._stopCommenting;
    }

    myChopsFeed.push(
      <div key="my-chops-header" style={styles.__subheader}>
        <TextWrapper
          styleName={'subtitle2'}
          color={theme.color.onBackgroundDisabled}
        >
          {`My recent chops`}
        </TextWrapper>
      </div>
    );

    if (isListMyChopsPending) {
      myChopsFeed.push(
        <div
          style={styles.__loadingWrapper}
          key="list-my-chops-loading"
        >
          <LoadingIndicatorCircular />
        </div>
      );
    }

    myChopsFeed = myChopsFeed.concat(
      myRecentChops
        .filter(c => !c.isRemoved)
        .map(chop => (
          <CardFeed
            key={`my-chop-${chop.chopId}`}
            me={me}
            myAccount={this.props.myAccount}
            chop={chop}
            commentsToShow={this._getCommentsToShowByChopId(
              chop.chopId
            )}
            isPlaying={
              playingChop && playingChop.chopId === chop.chopId
            }
            isLoading={loadingAudioFor.indexOf(chop.chopId) > -1}
            onPlay={this._playChop}
            onStop={this._stop}
            onRemix={this.props.onRemix}
            onClap={this.props.onClap}
            onComment={this._commentOn}
            onChangeCommentsCountToShow={
              this._changeCommentsCountToShow
            }
          />
        ))
    );

    myChopsFeed.push(
      <div key={'create-chop'}>
        <ButtonContained onTouchTap={this.props.onCreateFeel}>
          Create chop
        </ButtonContained>
      </div>
    );

    if (
      isListPublicChopsPending ||
      (publicChops && publicChops.length)
    ) {
      publicChopsFeed.push(
        <div key="public-chops-header" style={styles.__subheader}>
          <TextWrapper
            styleName={'subtitle2'}
            color={theme.color.onBackgroundDisabled}
          >
            {`Public feed`}
          </TextWrapper>
        </div>
      );
    }

    if (isListPublicChopsPending) {
      publicChopsFeed.push(
        <div
          style={styles.__loadingWrapper}
          key="list-public-chops-loading"
        >
          <LoadingIndicatorCircular />
        </div>
      );
    }

    if (publicChops && publicChops.length) {
      publicChopsFeed = publicChopsFeed.concat(
        publicChops.map(chop => (
          <CardFeed
            key={`public-chop-${chop.chopId}`}
            me={me}
            myAccount={this.props.myAccount}
            chop={chop}
            commentsToShow={this._getCommentsToShowByChopId(
              chop.chopId
            )}
            isPlaying={
              playingChop && playingChop.chopId === chop.chopId
            }
            isLoading={loadingAudioFor.indexOf(chop.chopId) > -1}
            onClap={this.props.onClap}
            onPlay={this._playChop}
            onStop={this._stop}
            onRemix={this.props.onRemix}
            onComment={this._commentOn}
            onChangeCommentsCountToShow={
              this._changeCommentsCountToShow
            }
          />
        ))
      );
    }

    return (
      <AppBarTopContainer
        showNavigation={this.props.showNavigation}
        navigationContent={this.props.navigationContent}
        appBarTitle={
          <LogoText
            height={32}
            color={theme.color.onBackgroundMediumEmphasis}
          />
        }
        appBarLeftIcon={
          <ButtonIcon onTouchTap={this.props.onShowNavigation}>
            <IconMenu
              width="24"
              height="24"
              color={theme.color.onBackgroundMediumEmphasis}
            />
          </ButtonIcon>
        }
        appBarActions={[
          <ButtonIcon
            key="appbar-action-rhythm"
            onTouchTap={this.props.onCreateFeel}
          >
            <IconPlus
              width={24}
              color={theme.color.onBackgroundMediumEmphasis}
            />
          </ButtonIcon>,
          <ButtonIcon
            key="appbar-action-notifications"
            onTouchTap={() => this.props.history.push('/notifications')}
          >
            <IconNotifications
              width={24}
              color={theme.color.onBackgroundMediumEmphasis}
            />
          </ButtonIcon>,
        ]}
      >
        <div style={styles.__content} onClick={dismissInputComment}>
          {myChopsFeed}
          {publicChopsFeed}
        </div>

        {inputComment}
        {audio}
      </AppBarTopContainer>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    myRecentChops: chops.select.listMyRecentChops(state),
    publicChops: chops.select.listPublicChops(state),
    isListMyChopsPending: chops.select.isListMyChopsPending(state),
    isListPublicChopsPending: chops.select.isListPublicChopsPending(
      state
    ),
    me: people.select.getMyself(state),
    canCreateChop: chops.select.canCreateChop(state, props),
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onCreateFeel: () => {
      if (!ownProps.me) {
        return;
      }

      if (!ownProps.canCreateChop) {
        ownProps.history.push(`/upgrade`);
        return;
      }

      const chopId = nanoid();

      chops.run.createNewChop(
        {
          composedBy: ownProps.me.personId,
          chopId,
          type: 'feel',
        },
        dispatch
      );

      ownProps.history.push(`/feel/${chopId}`);
    },

    onCreateForm: () => {
      if (!ownProps.me) {
        return;
      }

      if (!ownProps.canCreateChop) {
        ownProps.history.push(`/upgrade`);
        return;
      }

      const chopId = nanoid();

      chops.run.createNewChop(
        {
          composedBy: ownProps.me.personId,
          chopId,
          type: 'form',
        },
        dispatch
      );

      ownProps.history.push(`/form/${chopId}`);
    },

    listPublicChops: () => {
      chops.run.listPublicChops({}, dispatch);
    },

    listMyChops: () => {
      if (!ownProps.me) {
        return;
      }

      chops.run.listMyChops({}, dispatch);
    },

    onRemix: ({ remixOf, remixOfChop, personId }) => {
      if (!ownProps.canCreateChop) {
        ownProps.history.push(`/upgrade`);
        return;
      }

      const chopId = nanoid();
      chops.run.remixChop(
        {
          composedBy: personId,
          chopId,
          remixOf,
          type: 'form',
        },
        dispatch
      );

      if (remixOfChop.lines && remixOfChop.lines.length > 0) {
        ownProps.history.push(`/form/${chopId}`);
        return;
      }

      ownProps.history.push(`/feel/${chopId}`);
    },

    onClap: ({ chopId, personId, clapsCount }) => {
      chops.run.clap({ chopId, personId, clapsCount }, dispatch);
    },

    onSendComment: ({ text, chopId, personId }) => {
      chops.run.sendComment(
        {
          text,
          chopId,
          personId,
        },
        dispatch
      );
    },
  };
};

export default withRouter(
  withMyAccountProvider(
    connect(mapStateToProps)(
      connect(null, mapDispatchToProps)(withNavigation(Home))
    )
  )
);
