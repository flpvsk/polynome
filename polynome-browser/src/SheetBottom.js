import React from 'react';

import {
  Spring,
  animated,
  interpolate,
  config as springConfig,
} from 'react-spring';

import theme from './theme';
import { elevationToStyle } from './elevation';
import eventGetTouch from './eventGetTouch';
import viewport from './viewport';

import TextWrapper from './TextWrapper';
import IconButton from './ButtonIcon';
import IconBack from './IconBack';
import TopAppBar from './AppBarTop';

const preventDismiss = e => {
  e.preventDefault();
  e.stopPropagation();
};

const styles = {
  block: {
    height: '100%',
    width: '100%',

    position: 'relative',
    backgroundColor: theme.color.surface,
    transform: `translateY(${viewport.getHeight()}px)`,
  },

  _isVisible: {
    transform: `translateY(${viewport.getHeight() / 2}px)`,
    ...elevationToStyle(8),
  },

  __content: {
    width: '100%',
    maxWidth: viewport.getWidth(),
    marginLeft: 'auto',
    marginRight: 'auto',
    paddingTop: theme.size.appBarTopHeight,
  },

  __appBar: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    height: theme.size.appBarTopHeight,
    opacity: 0,
  },

  __icon: {
    backgroundColor: theme.color.primary,
    borderRadius: '50%',
    width: 56,
    height: 56,
    position: 'absolute',
    top: -28,
    left: `calc(50% + ${viewport.getWidth() / 2}px - 56px - 16px)`,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    ...elevationToStyle(6),
  },

  __header: {
    paddingTop: 24,
    paddingBottom: 24,
    height: 80,
  },

  __headerContent: {
    paddingLeft: 16,
    paddingRight: 16,
    maxWidth: viewport.getWidth(),
    margin: 'auto',
  },
};

class BottomSheet extends React.Component {
  constructor() {
    super();

    const viewportHeight = viewport.getHeight();
    this.state = {
      touchStartY: null,
      prevOffsetY: viewportHeight / 2,
      offsetY: 0,
      viewportHeight,
    };

    this._expand = e => {
      e.preventDefault();
      e.stopPropagation();

      if (this.state.offsetY === 0) {
        this.setState({
          offsetY: -0.5 * viewportHeight,
          prevOffsetY: this.state.offsetY,
        });
      }

      return false;
    };

    this._touchStart = e => {
      let y = eventGetTouch(e)[1];

      this.setState({
        offsetAtTouchStartY: this.state.offsetY,
        touchStartY: y,
      });

      e.preventDefault();
      e.stopPropagation();
      return false;
    };

    this._touchMove = e => {
      const {
        touchStartY,
        viewportHeight,
        offsetAtTouchStartY,
        offsetY,
      } = this.state;
      let y = eventGetTouch(e)[1];

      if (touchStartY === null) {
        return;
      }

      let newOffsetY = offsetAtTouchStartY + (y - touchStartY);

      if (newOffsetY < -viewportHeight / 2) {
        newOffsetY = -viewportHeight / 2;
      }

      this.setState({
        prevOffsetY: offsetY,
        offsetY: newOffsetY,
      });

      e.preventDefault();
      e.stopPropagation();
      return false;
    };

    this._touchEnd = e => {
      const {
        offsetY,
        viewportHeight,
        touchStartY,
        offsetAtTouchStartY,
      } = this.state;
      let newOffsetY = offsetY;

      if (touchStartY === null) {
        return;
      }

      const movement = Math.abs(
        Math.abs(offsetAtTouchStartY) - Math.abs(offsetY)
      );

      if (movement < 10) {
        return;
      }

      if (offsetY > 0.25 * viewportHeight) {
        this._dismiss();
        return;
      }

      if (offsetY < -0.25 * viewportHeight) {
        newOffsetY = -0.5 * viewportHeight;
      }

      if (offsetAtTouchStartY === -viewportHeight / 2) {
        const moveLength = Math.abs(viewportHeight / 2 + offsetY);
        if (moveLength > theme.size.appBarTopHeight) {
          this._dismiss();
          return;
        }

        if (moveLength < theme.size.appBarTopHeight) {
          // snap back
          newOffsetY = -viewportHeight / 2;
        }
      }

      this.setState({
        offsetY: newOffsetY,
        prevOffsetY: offsetY,
        touchStartY: null,
      });

      e.preventDefault();
      e.stopPropagation();
      return false;
    };

    this._touchCancel = e => {
      this.setState({
        touchY: null,
        touchStartY: null,
      });
    };

    this._dismiss = () => {
      const newOffsetY = 0.5 * viewportHeight + 56 / 2;

      this.setState({
        touchStartY: null,
        offsetY: newOffsetY,
        prevOffsetY: this.state.offsetY,
      });

      this.props.onDismiss();
    };
  }

  componentDidMount() {}

  componentWillUnmount() {}

  render() {
    let { props } = this;
    const { viewportHeight, prevOffsetY, offsetY } = this.state;

    let header = null;
    let iconStyle = {
      ...styles.__icon,
    };

    let contentStyle = {
      ...styles.__content,
    };

    if (props.iconBackgroundColor) {
      iconStyle.backgroundColor = props.iconBackgroundColor;
    }

    if (props.headerText) {
      contentStyle.paddingTop = 0;
      header = (
        <div style={styles.__header}>
          <div style={styles.__headerContent}>
            <TextWrapper styleName={'h5'} color={theme.color.onPrimary}>
              {props.headerText}
            </TextWrapper>
          </div>
        </div>
      );
    }

    const springFrom = {
      y: viewportHeight / 2 + prevOffsetY,
    };

    const springTo = {
      y: viewportHeight / 2 + offsetY,
    };

    const showAppBarY = theme.size.appBarTopHeight;

    return (
      <Spring
        native
        config={springConfig.stiff}
        from={springFrom}
        to={springTo}
      >
        {values => {
          let icon;

          iconStyle = {
            ...iconStyle,
            opacity: interpolate(
              values.y,
              y => (y >= showAppBarY ? 1 : y / showAppBarY)
            ),
          };

          if (props.icon) {
            icon = (
              <animated.div style={iconStyle}>
                {props.icon}
              </animated.div>
            );
          }

          const appBarStyle = {
            ...styles.__appBar,
            opacity: interpolate(
              values.y,
              y => (y < showAppBarY ? 1 - y / showAppBarY : 0)
            ),
          };

          return (
            <animated.div
              ref={ref => (this._container = ref)}
              onClick={preventDismiss}
              onTouchStart={this._touchStart}
              onTouchMove={this._touchMove}
              onTouchEnd={this._touchEnd}
              onTouchCancel={this._touchCancel}
              style={{
                ...styles.block,
                transform: interpolate(
                  values.y,
                  y => `translateY(${y}px)`
                ),
              }}
            >
              <animated.div
                onMouseUp={this._expand}
                style={appBarStyle}
              >
                <TopAppBar
                  backgroundColor={theme.color.primary}
                  elevation={2}
                  title={
                    <TextWrapper
                      styleName="h6"
                      color={theme.color.onPrimary}
                    >
                      {props.title}
                    </TextWrapper>
                  }
                  leftIcon={
                    <IconButton
                      styleName="appBar"
                      onTouchTap={this._dismiss}
                    >
                      <IconBack
                        color={theme.color.onPrimary}
                        width="100%"
                        height="100%"
                      />
                    </IconButton>
                  }
                />
              </animated.div>

              {icon}
              {header}
              <div style={contentStyle}>{props.children}</div>
            </animated.div>
          );
        }}
      </Spring>
    );
  }
}

export default BottomSheet;
