import React from 'react';
import { DebounceInput } from 'react-debounce-input';
import pick from 'lodash/pick';

import theme from './theme';

import { styles as textStyles } from './TextWrapper';

const styles = {
  block: {
    height: 40,
    background: 'none',
    appearance: 'none',
    border: 'none',
    backgroundColor: theme.color.containerOnSurface,
    borderBottom: `1px solid ${theme.color.outline}`,
    borderTopLeftRadius: theme.shape.borderRadius,
    borderTopRightRadius: theme.shape.borderRadius,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    paddingLeft: 12,
    paddingRight: 12,
    color: theme.color.onSurface,
    textAlign: 'left',
    ...textStyles.subtitle1,
    width: '100%',
  },
};

class InputEmail extends React.Component {
  constructor() {
    super();
    this.state = {
      value: '',
    };

    this._onChange = e => {
      const value = e.target.value;
      this.setState({ value });
      this.props.onChange(value);
    };
  }

  componentDidMount() {
    this.setState({ value: this.props.value });
  }

  render() {
    let propsStyle = pick(this.props, ['backgroundColor']);

    let style = {
      ...styles.block,
      ...propsStyle,
    };

    if (this.props.noPadding) {
      style = {
        ...style,
        paddingLeft: 0,
        paddingRight: 0,
      };
    }

    return (
      <DebounceInput
        type="email"
        debounceTimeout={500}
        style={style}
        onChange={this._onChange}
        value={this.state.value}
        placeholder={this.props.placeholder}
        maxLength={this.props.maxLength}
        disabled={this.props.isDisabled}
        readOnly={this.props.readOnly}
        required={this.props.required}
        autoFocus={this.props.isAutoFocus}
        onClick={e => e.stopPropagation()}
        name={this.props.name}
        id={this.props.id}
      />
    );
  }
}

export default InputEmail;
