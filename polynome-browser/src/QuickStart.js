import React, { Component } from 'react';

import shouldComponentUpdate from './shouldComponentUpdate';

import viewport from './viewport';
import theme from './theme';

import TextWrapper from './TextWrapper';
import ButtonText from './ButtonText';
import ButtonContained from './ButtonContained';

import AudioPlayer from './AudioPlayer';
import AudioLoader from './AudioLoader';

import * as trackUtils from './trackUtils';

const buttonStyles = {
  block: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    height: 128,
    width: 128,
    padding: 16,
    border: `1px solid ${theme.color.outline}`,
    borderRadius: theme.shape.borderRadius,
    background: 'none',
    boxShadow: 'none',
  },

  _selected: {
    backgroundColor: theme.color.primary,
    background: `linear-gradient(HSL(255, 61%, 39%), HSL(255, 61%, 37%))`,
    border: `none`,
  },
};

function ButtonQuickStart({ text, isSelected, onTouchTap }) {
  let textColor = theme.color.onBackground;
  let blockStyle = buttonStyles.block;

  if (isSelected) {
    blockStyle = { ...blockStyle, ...buttonStyles._selected };
    textColor = theme.color.onPrimary;
  }

  return (
    <button onClick={onTouchTap} style={blockStyle}>
      <TextWrapper styleName="button" align="center" color={textColor}>
        {text}
      </TextWrapper>
    </button>
  );
}

const QUICK_START_CONFIGS = [
  {
    configId: '4/4',
    name: '4/4',
    beats: [
      { id: '1', beatsCount: 1, offset: 0 },
      { id: '4', beatsCount: 4, offset: 0 },
    ],
    commonBeatsCount: 4,
    commonBeatsTempo: 120,
  },
  {
    configId: '3/4',
    name: '3/4',
    beats: [
      { id: '1', beatsCount: 1, offset: 0 },
      { id: '3', beatsCount: 3, offset: 0 },
    ],
    commonBeatsCount: 3,
    commonBeatsTempo: 120,
  },
  {
    configId: '2-3-clave',
    name: '2-3 clave',
    beats: [
      { id: '1', beatsCount: 1, offset: 0 },
      {
        id: '16',
        beatsCount: 16,
        offset: 0,
        mutedBeats: [
          1,
          2,
          // 3,
          4,
          // 5,
          6,
          7,
          8,
          // 9,
          10,
          11,
          // 12,
          13,
          14,
          // 15,
          16,
        ],
      },
    ],
    commonBeatsCount: 16,
    commonBeatsTempo: 3 * 120,
  },
  {
    configId: '2-overy-3-poly',
    name: '2 over 3 poly',
    beats: [
      {
        id: '2',
        beatsCount: 2,
        offset: 0,
        mutedBeats: [],
      },
      {
        id: '3',
        beatsCount: 3,
        offset: 0,
        mutedBeats: [],
      },
    ],
    commonBeatsCount: 6,
    commonBeatsTempo: 3 * 120,
  },
  {
    configId: '5-swing',
    name: '5-tuplet swing',
    beats: [
      {
        id: '1',
        beatsCount: 1,
        offset: 10,
        mutedBeats: [],
      },
      {
        id: '2',
        beatsCount: 2,
        offset: 0,
        mutedBeats: [],
      },
      {
        id: '20',
        beatsCount: 20,
        offset: 0,
        mutedBeats: [
          1,
          2,
          3,
          4 /* 5, */,
          6,
          7,
          8,
          9 /* 10, */,
          11,
          12,
          13,
          14 /* 15, */,
          16,
          17,
          18,
          19 /* 20, */,
        ],
      },
    ],
    commonBeatsCount: 20,
    commonBeatsTempo: 4 * 120,
  },
];

const styles = {
  block: {
    display: 'flex',
    alignSelf: 'stretch',
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
    paddingLeft: 16,
    paddingRight: 16,
    maxWidth: viewport.getWidth(),
  },

  __choices: {
    display: 'grid',
    gridColumnGap: 16,
    gridRowGap: 16,
    gridTemplateColumns: 'repeat(auto-fill, 128px)',
    gridTemplateRows: 128,
  },

  __header: {
    marginTop: 16,
  },

  __subheader: {
    marginTop: 32,
    marginBottom: 8,
  },

  __paragraph: {
    marginTop: 8,
    marginBottom: 0,
  },

  __actions: {
    marginTop: 40,
    display: 'flex',
  },
};

export default class QuickStart extends Component {
  constructor() {
    super();

    this.shouldComponentUpdate = shouldComponentUpdate;

    this.state = {
      beatsConfigId: null,
      kitId: null,
      soundType: null,
      startTime: null,
    };

    this._setBeatsConfig = configId => {
      return () => {
        if (configId === this.state.beatsConfigId) {
          this.setState({
            beatsConfigId: null,
            startTime: null,
          });

          return;
        }

        const update = { beatsConfigId: configId };
        if (this._isSoundDefined) {
          update.startTime = this.props.audioContext.currentTime;
        }

        this.props.runWhenAudioIsReady(() => this.setState(update));
      };
    };

    this._isSoundDefined = () =>
      this.state.soundType === 'oscilator' || this.state.kitId;

    this._setSound = ({ kitId, soundType }) => {
      return () => {
        if (
          kitId === this.state.kitId &&
          soundType === this.state.soundType
        ) {
          this.setState({
            kitId: null,
            soundType: null,
            startTime: null,
          });
          return;
        }
        const update = { kitId, soundType };

        if (this.state.beatsConfigId) {
          update.startTime = this.props.audioContext.currentTime;
        }

        this.props.runWhenAudioIsReady(() => this.setState(update));
      };
    };

    this._chooseTemplate = () => {
      const { beatsConfigId, soundType, kitId } = this.state;
      const { kits } = this.props;

      if (!soundType || !beatsConfigId) {
        return;
      }

      let config = QUICK_START_CONFIGS.find(
        c => c.configId === beatsConfigId
      );

      this.props.onChooseTemplate({
        beats: trackUtils.mapSoundToBeats({
          beats: config.beats,
          kits: kits,
          soundType,
          kitId,
          allAuto: true,
        }),
        commonBeatsCount: config.commonBeatsCount,
        commonBeatsTempo: config.commonBeatsTempo,
        activeSoundType: soundType,
        activeKitId: kitId,
      });
    };
  }

  render() {
    const { audioContext, kits, onSkipQuickStart } = this.props;

    const { beatsConfigId, soundType, kitId, startTime } = this.state;

    let config = QUICK_START_CONFIGS.find(
      c => c.configId === beatsConfigId
    );

    let isSoundDefined = this._isSoundDefined();

    let playQuickStart = beatsConfigId && isSoundDefined;

    let audio;
    if (playQuickStart) {
      let beatsToPlay = trackUtils.mapSoundToBeats({
        beats: config.beats,
        kits: kits,
        soundType,
        kitId,
        allAuto: true,
      });

      let audioBeats = trackUtils.beatsToAudio({
        beats: beatsToPlay,
        commonBeatsCount: config.commonBeatsCount,
        isPaused: false,
      });

      audio = (
        <AudioLoader
          recordings={[]}
          beats={beatsToPlay}
          lines={[]}
          kits={kits}
          audioContext={audioContext}
        >
          {({ recordingByLineId, beatsAudio, recordingsAudio }) => (
            <AudioPlayer
              key="quick-start-sounds-audio-player"
              startTime={startTime}
              beats={audioBeats}
              lines={[]}
              commonBeatsCount={config.commonBeatsCount}
              commonBeatsTempo={config.commonBeatsTempo}
              recordingByLineId={recordingByLineId}
              beatsAudio={beatsAudio}
              recordingsAudio={recordingsAudio}
              audioContext={audioContext}
            />
          )}
        </AudioLoader>
      );
    }

    const isActionDisabled = !beatsConfigId || !this._isSoundDefined();

    return (
      <div style={styles.block}>
        <header style={styles.__header}>
          <TextWrapper styleName="h4" color={theme.color.onBackground}>
            Quick start
          </TextWrapper>
        </header>

        <section>
          <p style={styles.__paragraph}>
            {TextWrapper.body1OnBackground(
              `Start with one of our beat templates or `
            )}
            <TextWrapper
              styleName="link"
              color={theme.color.primaryLight}
              onTouchTap={onSkipQuickStart}
            >
              {`build it from scratch`}
            </TextWrapper>
            {TextWrapper.body1OnBackground(
              `. You can always edit the chop later.`
            )}
          </p>
        </section>

        <header style={styles.__subheader}>
          <TextWrapper styleName="h5" color={theme.color.onBackground}>
            Beat
          </TextWrapper>
        </header>

        <section style={styles.__choices}>
          {QUICK_START_CONFIGS.map(config => (
            <ButtonQuickStart
              key={`beats-config-${config.configId}`}
              onTouchTap={this._setBeatsConfig(config.configId)}
              text={config.name}
              isSelected={beatsConfigId === config.configId}
            />
          ))}
        </section>

        <header style={styles.__subheader}>
          <TextWrapper styleName="h5" color={theme.color.onBackground}>
            Sound
          </TextWrapper>
        </header>

        <section style={styles.__choices}>
          <ButtonQuickStart
            onTouchTap={this._setSound({
              soundType: 'oscilator',
              kitId: null,
            })}
            text="Pitch"
            isSelected={soundType === 'oscilator'}
          />
          {kits.map(kit => (
            <ButtonQuickStart
              key={`sound-kit-${kit.id}`}
              onTouchTap={this._setSound({
                soundType: 'kit',
                kitId: kit.id,
              })}
              text={kit.name}
              isSelected={soundType === 'kit' && kitId === kit.id}
            />
          ))}
        </section>

        <section style={styles.__actions}>
          <ButtonContained
            onTouchTap={this._chooseTemplate}
            isDisabled={isActionDisabled}
            background={
              isActionDisabled
                ? theme.color.onBackgroundDisabled
                : theme.color.primary
            }
          >
            Choose template
          </ButtonContained>

          <span style={{ marginLeft: 16 }}>
            <ButtonText onTouchTap={onSkipQuickStart}>
              Start from scratch
            </ButtonText>
          </span>
        </section>

        {audio}
      </div>
    );
  }
}
