import React, { Component } from 'react';
import NavigationContent from './NavigationContent';
import omit from 'lodash/omit';

class WithNavigation extends Component {
  constructor() {
    super();

    this.state = {
      showNavigation: false,
    };

    this._showNavigation = () => {
      this.setState({ showNavigation: true });
    };

    this._hideNavigation = () => {
      this.setState({ showNavigation: false });
    };
  }

  render() {
    const navigationContent = (
      <NavigationContent hideNavigation={this._hideNavigation} />
    );

    const Wrapped = this.props.Wrapped;
    const wrappedProps = {
      ...omit(this.props, 'Wrapped'),
      showNavigation: this.state.showNavigation,
      onShowNavigation: this._showNavigation,
      onHideNavigation: this._hideNavigation,
      navigationContent,
    };

    return <Wrapped {...wrappedProps} />;
  }
}

function withNavigation(Wrapped) {
  return props => <WithNavigation {...props} Wrapped={Wrapped} />;
}

export default withNavigation;
