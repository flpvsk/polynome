import { DEFAULT_TEMPO } from './data/chops';
import sort from './sort';
import computeLcm from 'compute-lcm';
import { getDefaultEnvelopBySoundType } from './data/envelops';

const SECONDS_IN_MINUTE = 60;
const PITCH_BY_SUBDIV = ['A', 'C', 'E', 'G', 'B', 'D', 'F'];

function getBeatLineStart({ beat, commonBeatsCount }) {
  const beatsCount = beat.beatsCount || 1;
  return (beat.lineStart || 0) * commonBeatsCount / beatsCount;
}

function getBeatLineStop({ beat, commonBeatsCount }) {
  const beatsCount = beat.beatsCount || 1;

  return (
    ((beat.lineStart || 0) + (beat.lineDuration || beatsCount)) *
    commonBeatsCount /
    beatsCount
  );
}

function getSoundIdByIndex(kits, kitId, index) {
  const kit = kits.reduce((acc, k) => {
    if (acc) {
      return acc;
    }

    if (k.id === kitId) {
      return k;
    }

    return acc;
  }, null);

  if (!kit) {
    return null;
  }

  return kit.sounds[index % kit.sounds.length].id;
}

export function getTrackStartStopBeats({
  commonBeatsCount,
  beats = [],
  lines = [],
}) {
  let trackStart = 0;
  let trackStop = 0;

  for (let beat of beats.concat(lines)) {
    const commonBeatStart = getBeatLineStart({
      beat,
      commonBeatsCount,
    });
    const commonBeatStop = getBeatLineStop({ beat, commonBeatsCount });

    trackStart = Math.min(trackStart, commonBeatStart);
    trackStop = Math.max(trackStop, commonBeatStop);
  }

  return [trackStart, trackStop];
}

export function getTrackBeatsCount({ commonBeatsCount, beats, lines }) {
  const [start, stop] = getTrackStartStopBeats({
    commonBeatsCount,
    beats,
    lines,
  });
  return stop - start;
}

export function getBeatsStartStopMap({ commonBeatsCount, beats }) {
  const beatsStartStopById = {};
  for (let beat of beats) {
    const commonBeatStart = getBeatLineStart({
      beat,
      commonBeatsCount,
    });
    const commonBeatStop = getBeatLineStop({ beat, commonBeatsCount });

    beatsStartStopById[beat.id] = [commonBeatStart, commonBeatStop];
  }

  return beatsStartStopById;
}

export function getTrackDuration({
  commonBeatsTempo,
  commonBeatsCount,
  beats,
  lines,
}) {
  return (
    getTrackBeatsCount({
      commonBeatsCount,
      beats,
      lines,
    }) *
    SECONDS_IN_MINUTE /
    commonBeatsTempo
  );
}

function mostFrequentBeatCount(acc, beat) {
  if (!acc) {
    return beat.beatsCount;
  }
  return Math.max(acc, beat.beatsCount);
}

export function calculateNewTempo({
  oldTempo,
  oldBeatsCount,
  oldBeats,
  newBeatsCount,
  newBeats,
}) {
  const oldMostFrequentBeatCount = oldBeats.reduce(
    mostFrequentBeatCount,
    0
  );
  const newMostFrequentBeatCount = newBeats.reduce(
    mostFrequentBeatCount,
    0
  );

  if (!oldBeats.length) {
    return DEFAULT_TEMPO;
  }

  const oldBeatTempo =
    oldMostFrequentBeatCount * oldTempo / oldBeatsCount;
  const newTempo =
    oldBeatTempo * newBeatsCount / newMostFrequentBeatCount;

  return newTempo;
}

export function calculateCommonBeatsCount(beats) {
  if (!beats || !beats.length) {
    return 0;
  }

  if (beats.length === 1) {
    return beats[0].beatsCount;
  }

  if (beats.length > 1) {
    const beatsNumbers = beats.map(b => b.beatsCount);
    return computeLcm(beatsNumbers);
  }
}

export function beatsSortFn(b1, b2) {
  let result = b1.beatsCount - b2.beatsCount;
  if (result === 0) {
    let b1Muted = b1.mutedBeats || [];
    let b2Muted = b2.mutedBeats || [];

    return b2Muted.length - b1Muted.length;
  }

  return result;
}

export function reverseBeatsSortFn(b1, b2) {
  return -beatsSortFn(b1, b2);
}

export function beatsToAudio({
  beats,
  soloItem,
  commonBeatsCount,
  isPaused,
}) {
  if (isPaused) {
    return [];
  }

  return beats.reduce((acc, b) => {
    if (soloItem && soloItem.beatId === b.id) {
      acc.push(b);
      return acc;
    }

    if (soloItem && soloItem.beatId !== b.id) {
      return acc;
    }

    if (b.isMute) {
      return acc;
    }

    acc.push(b);
    return acc;
  }, []);
}

export function linesToAudio({
  lines,
  soloItem,
  commonBeatsCount,
  isPaused,
  isRecording,
  recordingForLineId,
}) {
  if (isPaused) {
    return [];
  }

  // When recording, armed line is 2 times longer because of the silence
  // part.
  const result = lines
    .map(l => {
      if (isRecording && l.lineId === recordingForLineId) {
        return {
          ...l,
          lineDuration: 2 * l.lineDuration,
        };
      }

      return l;
    })
    .filter(l => {
      if (soloItem && soloItem.lineId === l.lineId) {
        return true;
      }

      if (soloItem && soloItem.lineId !== l.lineId) {
        return false;
      }

      if (l.isMute) {
        return false;
      }

      return true;
    });

  return result;
}

export function mapSoundToBeats({
  beats,
  kits,
  soundType,
  kitId,
  allAuto,
}) {
  return sort(beats, beatsSortFn).map((b, i) => {
    const beatSound = b.sound || {};
    if (!beatSound.isAuto && !allAuto) {
      return b;
    }

    let sound = {
      ...beatSound,
      type: soundType,
      pitch: PITCH_BY_SUBDIV[i % PITCH_BY_SUBDIV.length],
      isAuto: true,
    };

    if (allAuto) {
      sound.envelop = getDefaultEnvelopBySoundType(soundType);
    }

    if (soundType === 'kit') {
      sound.soundId = getSoundIdByIndex(
        kits,
        kitId,
        beats.length - i - 1
      );
    }

    return {
      ...b,
      sound,
    };
  });
}
