import React, { Component } from 'react';
import theme from './theme';
import * as trackUtils from './trackUtils';

import TextWrapper from './TextWrapper';
import Snackbar from './Snackbar';

class SnackbarRecording extends Component {
  constructor() {
    super();

    this.state = {
      tip: '',
      isVisible: false,
    };

    this._stop = false;

    this._loop = () => {
      if (this._stop) {
        return;
      }

      const { getCurrentTime, startTime } = this.props;
      const {
        trackDuration,
        cycleDuration,
        mostFrequentBeatDuration,
        mostFrequentBeatCount,
      } = this.getTrackData();

      let tip = '';
      let isVisible = false;

      if (!startTime) {
        const nextFrame =
          window.requestAnimationFrame ||
          window.webkitRequestAnimationFrame;
        nextFrame(this._loop);
        return;
      }

      const currentTime = getCurrentTime();
      const timePassed = currentTime - startTime;
      const currentTrackNumber = Math.floor(timePassed / trackDuration);
      const currentTrackProgress =
        timePassed - currentTrackNumber * trackDuration;

      const cyclesCount = Math.round(trackDuration / cycleDuration);
      const currentCycle = Math.floor(
        currentTrackProgress / cycleDuration
      );
      const currentCycleProgress =
        currentTrackProgress - currentCycle * cycleDuration;
      const isLastCycle = currentCycle === cyclesCount - 1;
      const isFirstCycle = currentCycle === 0;

      if (currentTrackNumber % 2 === 1) {
        tip = 'Record.';
      }

      if (currentTrackNumber % 2 === 1 && isFirstCycle) {
        isVisible = true;
      }

      if (currentTrackNumber % 2 === 0) {
        tip = 'Listen.';
      }

      if (currentTrackNumber % 2 === 0 && isFirstCycle) {
        isVisible = true;
      }

      if (currentTrackNumber % 2 === 0 && isLastCycle) {
        const count =
          mostFrequentBeatCount -
          Math.floor(currentCycleProgress / mostFrequentBeatDuration);

        tip = `Recording will start in ${count}.`;
        isVisible = true;
      }

      this.setState({
        tip,
        isVisible,
      });

      const nextFrame =
        window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame;
      nextFrame(this._loop);
    };
  }

  componentDidMount() {
    this._loop();
  }

  componentWillUnmount() {
    this._stop = true;
  }

  getTrackData() {
    const { commonBeatsTempo, commonBeatsCount, beats } = this.props;

    const trackDuration = trackUtils.getTrackDuration({
      commonBeatsTempo,
      commonBeatsCount,
      beats,
    });

    const mostFrequentBeat = beats.reduce((acc, b) => {
      if (acc === null) {
        return b;
      }

      if (acc.beatsCount <= b.beatsCount) {
        return b;
      }

      return acc;
    }, null);

    const mostFrequentBeatDuration =
      commonBeatsCount *
      60 /
      (mostFrequentBeat.beatsCount * commonBeatsTempo);

    return {
      cycleDuration: commonBeatsCount * 60 / commonBeatsTempo,
      trackDuration,
      mostFrequentBeatDuration,
      mostFrequentBeatCount: mostFrequentBeat.beatsCount,
    };
  }

  render() {
    const { tip, isVisible } = this.state;
    return (
      <Snackbar isVisible={isVisible}>
        <TextWrapper styleName={'body2'} color={theme.color.onSnackbar}>
          {tip}
        </TextWrapper>
      </Snackbar>
    );
  }
}

export default SnackbarRecording;
