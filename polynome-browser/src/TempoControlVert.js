import React, { Component } from 'react';
import pad from 'lodash/pad';
import padStart from 'lodash/padStart';

import theme from './theme';

const PATTERN_SIZE = 62;
const CROSS_SIZE = 72;

const styles = {
  block: {
    width: CROSS_SIZE,
    position: 'relative',
    visibility: 'visible',
  },

  _isHidden: {
    visibility: 'hidden',
  },

  __scale: {},

  __valuesBlock: {
    position: 'absolute',
    bottom: '-0.5em',
    height: '0.8em',
    width: '100%',
    overflow: 'hidden',
  },

  __currentBpmBlock: {
    color: '#eee',
    textAlign: 'center',
    marginBottom: '0.4em',
    fontSize: '0.8em',
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
  },

  __currentBpmWrapper: {
    position: 'relative',
  },

  __currentBpmInput: {
    position: 'relative',
    background: 'none',
    border: 'none',
    color: '#eee',
    width: '6em',
    marginLeft: '-2em',
    textAlign: 'right',
    paddingRight: '2.2em',
  },

  __currentBpmLabel: {
    position: 'absolute',
    right: 0,
    bottom: '0.04em',
  },

  __currentBpmText: {
    userSelect: 'none',
    fontSize: '0.8em',
    letterSpacing: 0.5,
    textAlign: 'center',
    color: theme.color.onSurface,
  },

  __scaleValueText: {
    userSelect: 'none',
    fontSize: '0.65em',
    letterSpacing: 0.4,
    color: theme.color.onSurfaceMediumEmphasis,
    textAlign: 'center',
    background: 'none',
    border: 'none',
  },
};

class TempoControl extends Component {
  constructor() {
    super();
    this._changeTempo = e => {
      let value = e.target.value;
      if (value === '') {
        this.setState({ tempo: value });
        return;
      }

      value = value.trim();
      value = value.replace(/,/g, '.');

      let tempo = parseFloat(value);

      if (isNaN(tempo)) {
        this.setState({ tempo: value });
        return;
      }

      tempo = Math.round(tempo * 10) / 10;

      this.setState({ tempo: value });
      this.props.onTempoChange(this.props.beatId, tempo);
    };

    this._setTempoTo = tempo => {
      if (tempo === 0) {
        tempo = 1;
      }

      return () => {
        this.props.onTempoChange(this.props.beatId, tempo);
      };
    };

    this._startTrackingTouch = e => {
      let y;

      if (e.touches) {
        y = e.touches[0].clientY;
      }

      if (!e.touches) {
        y = e.pageY;
      }

      const tempoNumber = this._getCurrentTempoNumber();

      this.setState({
        touchStartTempo: tempoNumber,
        touchStartY: y,
      });
    };

    this._trackTouchMove = e => {
      if (!this.state.touchStartY) {
        return;
      }

      let y;

      if (e.touches) {
        y = e.touches[0].clientY;
      }

      if (!e.touches) {
        y = e.pageY;
      }

      const diff = this.state.touchStartY - y;
      const tempoDiff = diff / PATTERN_SIZE * 10;
      let newTempo =
        Math.round((this.state.touchStartTempo + tempoDiff) * 10) / 10;

      if (newTempo < 1) {
        newTempo = 1;
      }

      this.setState({
        tempo: newTempo,
      });
    };

    this._commitChangeTempo = () => {
      if (!this.state.touchStartY) {
        return;
      }

      this.setState({
        touchStartY: null,
        touchStartTempo: null,
      });
      this.props.onTempoChange(this.props.beatId, this.state.tempo);
    };

    this._setTempoTo = tempoValue => {
      return () => {
        this.props.onTempoChange(this.props.beatId, tempoValue);
      };
    };

    this.state = {
      tempo: null,
      touchStartY: null,
      touchStartTempo: null,
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (parseFloat(props.tempo) === parseFloat(state.tempo)) {
      return state;
    }

    return {
      ...state,
      tempo: Math.round(props.tempo * 10) / 10,
    };
  }

  componentDidMount() {
    window.document.addEventListener(
      'mouseup',
      this._commitChangeTempo,
      true
    );
  }

  componentWillUnMount() {
    window.document.removeEventListener(
      'mouseup',
      this._commitChangeTempo,
      true
    );
  }

  _renderScale() {
    const currentTempo = this._getCurrentTempoNumber();
    let { height } = this.props;
    const scaleSize = height;

    const tempoStep = 10 / 4;
    const scaleStep = PATTERN_SIZE / 4;
    const scaleStepsCount = scaleSize / scaleStep;

    let currentTempoStr = padStart(String(currentTempo), 5, '\u00a0');
    const currentTempoText = currentTempoStr + ' bpm';

    const items = [];
    let halfScale = scaleSize / 2;
    let scaleTempoDiff =
      Math.floor((currentTempo * 10) % (tempoStep * 10)) / 10;
    let scaleMarksOffset = scaleStep * scaleTempoDiff / tempoStep;
    let lowerScaleValueTempo = currentTempo - scaleTempoDiff;
    let minTempoValue =
      lowerScaleValueTempo - Math.ceil(scaleStepsCount / 2) * tempoStep;

    items.push(
      <text
        style={styles.__currentBpmText}
        fill={theme.color.onSurface}
        key="scale-current"
        x="0"
        y={halfScale}
      >
        {currentTempoText}
      </text>
    );

    let alphaRange = 2;
    for (
      let stepNumber = 0;
      stepNumber < scaleStepsCount;
      stepNumber++
    ) {
      const scaleValue =
        stepNumber * scaleStep - scaleMarksOffset - scaleStep * 0.4;
      const tempoValue = minTempoValue + stepNumber * tempoStep;
      let alpha = 1;

      const stepsFromCenter =
        Math.abs(scaleValue - halfScale) / scaleStep;

      if (stepsFromCenter < alphaRange) {
        alpha = stepsFromCenter / alphaRange;
      }

      if (alpha < 0.2) {
        alpha = 0;
      }

      if (tempoValue < 0) {
        continue;
      }

      if (tempoValue % 10 === 0) {
        const style = {
          ...styles.__scaleValueText,
          opacity: alpha,
        };
        items.push(
          <text
            onClick={this._setTempoTo(tempoValue)}
            style={style}
            fill={theme.color.onSurfaceMediumEmphasis}
            key={`scale-${stepNumber}`}
            x={26}
            y={scaleValue}
          >
            {pad(tempoValue, 5, '\u00a0')}
          </text>
        );
      }

      if (tempoValue % 10 !== 0) {
        items.push(
          <circle
            style={{ opacity: alpha }}
            fill={theme.color.onSurfaceMediumEmphasis}
            key={`scale-${stepNumber}`}
            cy={scaleValue}
            cx="36"
            r="1"
          />
        );
      }

      items.push(
        <rect
          key={`scale-click-handler-${stepNumber}`}
          onClick={this._setTempoTo(tempoValue)}
          x="0"
          y={scaleValue + scaleStep * 0.5}
          width={CROSS_SIZE}
          height={scaleStep}
          fill="rgba(255, 255, 255, 0)"
          strokeWidth={0}
          stroke="none"
        />
      );
    }

    return (
      <svg
        height={this.props.height}
        width={CROSS_SIZE}
        style={styles.__scale}
        onTouchStart={this._startTrackingTouch}
        onTouchEnd={this._commitChangeTempo}
        onTouchMove={this._trackTouchMove}
        onMouseDown={this._startTrackingTouch}
        onMouseMove={this._trackTouchMove}
        onMouseUp={this._commitChangeTempo}
      >
        {items}
      </svg>
    );
  }

  _getCurrentTempoNumber() {
    const { tempo } = this.state;
    let tempoNumber = parseFloat(tempo);

    if (isNaN(tempoNumber)) {
      return this.props.tempo;
    }

    return tempoNumber;
  }

  render() {
    const tempo = this._getCurrentTempoNumber();

    let blockStyle = {
      ...styles.block,
      width: CROSS_SIZE,
    };

    if (isNaN(tempo)) {
      blockStyle = {
        ...blockStyle,
        ...styles._isHidden,
      };

      return <div style={blockStyle} />;
    }

    return <div style={blockStyle}>{this._renderScale()}</div>;
  }
}

export default TempoControl;
