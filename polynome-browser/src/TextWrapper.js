import React from 'react';
import isNumber from 'lodash/isNumber';

import theme from './theme';
import { elevationToTextStyle } from './elevation';

const body1 = {
  fontSize: '0.982em',
  letterSpacing: '0.5px',
  fontWeight: 400,
  hyphens: 'auto',
  wordBreak: 'break-word',
};

const styles = {
  block: {
    textAlign: 'left',
  },

  body1,

  body2: {
    fontSize: '0.859em',
    letterSpacing: 0.25,
    fontWeight: 400,
    hyphens: 'auto',
    wordBreak: 'break-word',
  },

  subtitle1: {
    fontSize: '0.982em',
    letterSpacing: 0.15,
    fontWeight: 400,
  },

  subtitle2: {
    fontSize: '0.859em',
    letterSpacing: 0.1,
    fontWeight: 400,
  },

  h1: {
    fontSize: '5.891em',
    letterSpacing: -1.5,
    fontWeight: 200,
  },

  h2: {
    fontSize: '3.682em',
    letterSpacing: -0.5,
    fontWeight: 200,
  },

  h3: {
    fontSize: '2.946em',
    letterSpacing: 0,
    fontWeight: 500,
    display: 'inline',
    padding: 0,
    margin: 0,
  },

  h4: {
    fontSize: '2.086em',
    letterSpacing: 0.25,
    fontWeight: 400,
    display: 'inline',
    padding: 0,
    margin: 0,
  },

  h5: {
    fontSize: '1.472em',
    letterSpacing: 0,
    fontWeight: 400,
    display: 'inline',
    padding: 0,
    margin: 0,
  },

  h6: {
    fontSize: '1.228em',
    letterSpacing: 0.25,
    fontWeight: 500,
    display: 'inline',
    padding: 0,
    margin: 0,
  },

  button: {
    fontWeight: 500,
    fontSize: '0.859em',
    letterSpacing: 1.25,
    textTransform: 'uppercase',
  },

  topBarTitle: {
    fontSize: '1.228em',
    letterSpacing: 0.25,
    fontWeight: 500,
    display: 'inline',
    padding: 0,
    margin: 0,
  },

  overline: {
    fontSize: '0.625em',
    letterSpacing: 1.5,
    textTransform: 'uppercase',
    fontWeight: 400,
  },

  link: {
    ...body1,
    textDecoration: 'underline',
    cursor: 'pointer',
  },

  _noWrap: {
    whiteSpace: 'nowrap',
  },
};

const tagByStyleName = {
  link: 'a',
  h3: 'h3',
  h4: 'h4',
  h5: 'h5',
  h6: 'h6',
  topBarTitle: 'h1',
};

const TextWrapper = props => {
  const concreteStyle = styles[props.styleName] || {};
  const noWrapStyle = props.noWrap ? styles._noWrap : {};
  let elevationStyle = {};
  let alignStyle = {};

  if (isNumber(props.elevation)) {
    elevationStyle = elevationToTextStyle(props.elevation);
  }

  if (props.align) {
    alignStyle = { textAlign: props.align };
  }

  const blockStyle = {
    ...styles.block,
    ...concreteStyle,
    ...noWrapStyle,
    ...elevationStyle,
    ...alignStyle,
    color: props.color,
  };

  const tag = tagByStyleName[props.styleName] || 'span';

  return React.createElement(
    tag,
    {
      onClick: props.onTouchTap,
      style: blockStyle,
      href: props.href,
      title: props.title,
    },
    props.children
  );
};

TextWrapper.body1OnBackground = function body1OnBackground(text) {
  return (
    <TextWrapper styleName="body1" color={theme.color.onBackground}>
      {text}
    </TextWrapper>
  );
};

TextWrapper.link = function link(text, href) {
  return (
    <TextWrapper
      styleName="link"
      color={theme.color.primaryLight}
      href={href}
    >
      {text}
    </TextWrapper>
  );
};

export { styles };

export default TextWrapper;
