import AudioContext from './AudioContext';

let Recorder;

Recorder = require('./RecorderScriptProcessor').default;

if ('audioWorklet' in AudioContext.prototype) {
  Recorder = require('./RecorderWorklet').default;
}

export default Recorder;
