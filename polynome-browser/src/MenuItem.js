import React from 'react';

import theme from './theme';
import iconsByName from './iconsByName';
import TextWrapper from './TextWrapper';

const styles = {
  block: {
    height: 48,
    width: '100%',
    paddingLeft: 16,
    paddingRight: 16,
    display: 'flex',
    alignItems: 'center',
    alignContent: 'center',

    background: 'none',
    border: 'none',
    borderRadius: theme.shape.borderRadius,
  },

  __icon: {
    width: 24,
    height: 24,
    marginRight: 20,
  },
};

const MenuItem = props => {
  const Icon = iconsByName[props.iconName];
  let icon;

  if (Icon) {
    icon = (
      <Icon
        color={theme.color.onSurfaceMediumEmphasis}
        width={24}
        height={24}
      />
    );
  }

  let blockStyle = {
    ...styles.block,
  };

  if (props.background) {
    blockStyle = {
      ...blockStyle,
      background: props.background,
    };
  }

  if (props.onTouchTap) {
    blockStyle = {
      ...blockStyle,
      cursor: 'pointer',
    };
  }

  return (
    <button style={blockStyle} onClick={props.onTouchTap}>
      <div style={styles.__icon}>{icon}</div>
      <TextWrapper
        styleName="body1"
        color={theme.color.onSurfaceMediumEmphasis}
      >
        {props.text}
      </TextWrapper>
    </button>
  );
};

export default MenuItem;
