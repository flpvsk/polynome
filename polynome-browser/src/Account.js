import React, { Component } from 'react';

import AppBarTopContainer from './AppBarTopContainer';
import TextWrapper from './TextWrapper';
import ButtonIcon from './ButtonIcon';
import Divider from './Divider';
import ListItem from './ListItem';
import InputEmail from './InputEmail';
import InputPassword from './InputPassword';
import ButtonText from './ButtonText';
import LoadingIndicatorCircular from './LoadingIndicatorCircular';
import SnackbarWithTimer from './SnackbarWithTimer';

import IconMenu from './IconMenu';

import theme from './theme';

import withNavigation from './withNavigation';
import withAccountProvider from './data/withAccountProvider';

const styles = {
  block: {
    width: '100%',
  },

  __subheader: {
    width: '100%',
    height: 48,
    paddingLeft: 16,
    paddingRight: 16,
    display: 'flex',
    alignItems: 'center',
  },

  __info: {
    paddingLeft: 16,
    paddingRight: 16,
    width: '100%',
  },
};

const TwoLineListItem = props => {
  const styles = {
    block: {
      height: 64,
      paddingLeft: 16,
      paddingRight: 16,
      display: 'flex',
      justifyContent: 'flex-start',
      flexDirection: 'column',
      width: '100%',
      cursor: 'pointer',
    },

    __overline: {
      height: 24,
      display: 'flex',
      alignItems: 'flex-end',
    },

    __content: {
      height: 20,
      display: 'flex',
      alignItems: 'flex-end',
    },
  };

  return (
    <div style={styles.block} onClick={props.onTouchTap}>
      <div style={styles.__overline}>
        <TextWrapper
          styleName="overline"
          color={theme.color.onBackgroundDisabled}
        >
          {props.overline}
        </TextWrapper>
      </div>

      <div style={styles.__content}>{props.children}</div>
    </div>
  );
};

class Account extends Component {
  constructor() {
    super();

    this.state = {
      showDialogFor: null,
      emailValue: '',
      newPasswordValue: '',
      oldPasswordValue: '',
      snackbarMessage: null,
      showSnackbarUntil: 0,
    };

    this._showChangeEmailDialog = () => {
      this.setState({
        showDialogFor: 'email',
        emailValue: this.props.myAccount.email || '',
      });
    };

    this._showChangePasswordDialog = () => {
      this.setState({
        showDialogFor: 'password',
        oldPasswordValue: '',
        newPasswordValue: '',
        changePasswordError: '',
      });
    };

    this._changeEmailValue = value =>
      this.setState({
        emailValue: value,
      });

    this._changeOldPasswordValue = value =>
      this.setState({
        oldPasswordValue: value,
      });

    this._changePasswordValue = value =>
      this.setState({
        newPasswordValue: value,
      });

    this._confirmChangeEmail = e => {
      e.preventDefault();

      this.props.onChangeEmail({
        email: this.state.emailValue,
      });
    };

    this._confirmChangePassword = e => {
      e.preventDefault();
      const { myAccount } = this.props;
      const newPassword = this.state.newPasswordValue;
      const oldPassword = this.state.oldPasswordValue;

      if (!myAccount) {
        return;
      }

      if (myAccount.hasPassword && oldPassword === newPassword) {
        this.setState({
          changePasswordError: 'Passwords can not be the same.',
        });
        return;
      }

      if (!newPassword || newPassword.length < 4) {
        this.setState({
          changePasswordError:
            'Password should be longer than 4 charecters.',
        });
        return;
      }

      this.setState({ changePasswordError: '' });
      this.props.onChangePassword({ oldPassword, newPassword });
    };

    this._submitEmailForm = () => {
      this._emailForm.submit();
    };

    this._submitPasswordForm = () => {
      this._passwordForm.submit();
    };

    this._changeEmailDialog = () => {
      const error = this.props.changeEmailError;

      return (
        <form
          ref={r => (this._emailForm = r)}
          onSubmit={this._confirmChangeEmail}
        >
          <ListItem key="dialog-item-email" noPadding>
            <InputEmail
              onChange={this._changeEmailValue}
              value={this.state.emailValue}
              placeholder="Email"
            />
          </ListItem>

          <ListItem key="dialog-item-errors" noPadding>
            <TextWrapper textStyle="body1" color={theme.color.error}>
              {error}
            </TextWrapper>
          </ListItem>

          <button style={{ display: 'none' }} />
        </form>
      );
    };

    this._changePasswordDialog = () => {
      let oldPasswordListItem;

      if (this.props.myAccount.hasPassword) {
        oldPasswordListItem = (
          <ListItem key="dialog-item-old-password" noPadding>
            <InputPassword
              placeholder="Old password"
              onChange={this._changeOldPasswordValue}
              value={this.state.oldPasswordValue}
            />
          </ListItem>
        );
      }

      return (
        <form
          ref={r => (this._passwordForm = r)}
          onSubmit={this._preventDefault}
        >
          {oldPasswordListItem}

          <ListItem key="dialog-item-password" noPadding>
            <InputPassword
              placeholder="New password"
              onChange={this._changePasswordValue}
              value={this.state.newPasswordValue}
            />
          </ListItem>

          <ListItem key="dialog-item-errors" noPadding>
            <TextWrapper textStyle="body1" color={theme.color.error}>
              {this.state.changePasswordError ||
                this.props.changePasswordError}
            </TextWrapper>
          </ListItem>

          <button style={{ display: 'none' }} />
        </form>
      );
    };

    this._hideDialog = () => {
      this.setState({
        showDialogFor: null,
        emailValue: '',
        oldPasswordValue: '',
        newPasswordValue: '',
        changePasswordError: '',
      });
    };
  }

  componentDidUpdate(oldProps) {
    const { props } = this;

    if (
      oldProps.isChangeEmailLoading &&
      !props.isChangeEmailLoading &&
      props.myAccount.email === this.state.emailValue &&
      this.state.showDialogFor === 'email'
    ) {
      this.setState({
        showDialogFor: null,
        snackbarMessage: 'Got it! Email changed 🙌 ',
        showSnackbarUntil: Date.now() + 3000,
      });
    }

    if (
      oldProps.isChangePasswordLoading &&
      !props.isChangePasswordLoading &&
      !this.props.changePasswordError &&
      this.state.showDialogFor === 'password'
    ) {
      this.setState({
        showDialogFor: null,
        snackbarMessage: 'Ok, password changed 👍',
        showSnackbarUntil: Date.now() + 1000,
      });
    }
  }

  _preventDefault(e) {
    e.preventDefault();
  }

  render() {
    const {
      myAccount,
      isMyAccountLoading,
      isChangeEmailLoading,
    } = this.props;

    if (isMyAccountLoading) {
      return (
        <AppBarTopContainer
          showNavigation={this.props.showNavigation}
          navigationContent={this.props.navigationContent}
          appBarLeftIcon={
            <ButtonIcon onTouchTap={this.props.onShowNavigation}>
              <IconMenu
                width="24"
                height="24"
                color={theme.color.onBackgroundMediumEmphasis}
              />
            </ButtonIcon>
          }
          appBarTitle={
            <TextWrapper
              styleName={'topBarTitle'}
              color={theme.color.onSurfaceMediumEmphasis}
            >
              My account
            </TextWrapper>
          }
        >
          <LoadingIndicatorCircular />
        </AppBarTopContainer>
      );
    }

    let dialogHeader;
    let dialogContentFn;
    let dialogActions;

    if (this.state.showDialogFor === 'email') {
      let actionColor = theme.color.onSurfaceDisabled;

      if (this.state.emailValue !== this.props.myAccount.email) {
        actionColor = theme.color.primaryLight;
      }

      dialogHeader = 'Change email';
      dialogContentFn = this._changeEmailDialog;
      dialogActions = [
        <ButtonText
          onTouchTap={this._hideDialog}
          textColor={theme.color.onSurfaceMediumEmphasis}
          key="dialog-cancel-button"
        >
          Cancel
        </ButtonText>,
        <ButtonText
          onTouchTap={this._confirmChangeEmail}
          textColor={actionColor}
          key="dialog-confirm-email-change-button"
        >
          Change
        </ButtonText>,
      ];

      if (isChangeEmailLoading) {
        dialogActions = [<LoadingIndicatorCircular />];
      }
    }

    if (this.state.showDialogFor === 'password') {
      const { isChangePasswordLoading } = this.props;

      dialogHeader = myAccount.hasPassword
        ? 'Change password'
        : 'Set password';

      dialogContentFn = this._changePasswordDialog;
      dialogActions = [
        <ButtonText
          onTouchTap={this._hideDialog}
          textColor={theme.color.onSurfaceMediumEmphasis}
          key="dialog-cancel-button"
        >
          Cancel
        </ButtonText>,
        <ButtonText
          onTouchTap={this._confirmChangePassword}
          textColor={theme.color.primaryLight}
          key="dialog-confirm-password-change-button"
        >
          Change
        </ButtonText>,
      ];

      if (isChangePasswordLoading) {
        dialogActions = [<LoadingIndicatorCircular />];
      }
    }

    let passwordText;
    let passwordAction;
    const emailText = myAccount.email || 'Not set';

    if (!myAccount.email) {
      passwordText = `Please set your email to set the password`;
    }

    if (myAccount.email && myAccount.hasPassword) {
      passwordText = `****`;
      passwordAction = this._showChangePasswordDialog;
    }

    if (myAccount.email && !myAccount.hasPassword) {
      passwordText = `Not set`;
      passwordAction = this._showChangePasswordDialog;
    }

    let passwordRow = (
      <TwoLineListItem overline="password" onTouchTap={passwordAction}>
        <TextWrapper styleName="body1" color={theme.color.onBackground}>
          {passwordText}
        </TextWrapper>
      </TwoLineListItem>
    );

    return (
      <AppBarTopContainer
        showNavigation={this.props.showNavigation}
        navigationContent={this.props.navigationContent}
        appBarTitle={
          <TextWrapper
            styleName={'topBarTitle'}
            color={theme.color.onSurfaceMediumEmphasis}
          >
            My account
          </TextWrapper>
        }
        appBarLeftIcon={
          <ButtonIcon onTouchTap={this.props.onShowNavigation}>
            <IconMenu
              width="24"
              height="24"
              color={theme.color.onBackgroundMediumEmphasis}
            />
          </ButtonIcon>
        }
        appBarActions={[]}
        dialogHeader={dialogHeader}
        dialogContent={dialogContentFn}
        dialogActions={dialogActions}
      >
        <div style={styles.block}>
          <TwoLineListItem
            onTouchTap={this._showChangeEmailDialog}
            overline="email"
          >
            <TextWrapper
              styleName="body1"
              color={theme.color.onBackground}
            >
              {emailText}
            </TextWrapper>
          </TwoLineListItem>

          {passwordRow}

          <Divider />

          {/*
            <div key="choose-plan-header" style={styles.__subheader}>
              <TextWrapper
                styleName={'subtitle2'}
                color={theme.color.onBackgroundDisabled}
              >
                {`Subscription`}
              </TextWrapper>
            </div>

            <TwoLineListItem
              onTouchTap={() => this.props.history.push('/subscription')}
              overline="current plan"
            >
              <TextWrapper
                styleName="body1"
                color={theme.color.onBackground}
              >
                {
                  getPlanFromSubscription(myAccount.activeSubscription)
                    .name
                }
              </TextWrapper>
            </TwoLineListItem>
          */}
        </div>
        <SnackbarWithTimer
          message={this.state.snackbarMessage}
          showUntil={this.state.showSnackbarUntil}
        />
      </AppBarTopContainer>
    );
  }
}

export default withNavigation(withAccountProvider(Account));
