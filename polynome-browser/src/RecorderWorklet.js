import Emitter from 'tiny-emitter';
import closeMediaStream from './closeMediaStream';

const PATH_TO_WORKLET = '/recorderWorklet.js';

class Recorder {
  static get RECORDING_FINISHED_EVENT() {
    return 'recording-finished';
  }

  static get RECORDING_STARTED_EVENT() {
    return 'recording-started';
  }

  static async create({ audioContext }) {
    await audioContext.audioWorklet.addModule(PATH_TO_WORKLET);
    const recorderNode = new window.AudioWorkletNode(
      audioContext,
      'recorder-worklet'
    );

    return new Recorder({
      recorderNode,
      audioContext,
    });
  }

  constructor({ recorderNode, audioContext }) {
    this._recorderNode = recorderNode;
    this._audioContext = audioContext;

    this._emitter = new Emitter();
    this.on = this._emitter.on.bind(this._emitter);
    this.off = this._emitter.off.bind(this._emitter);

    this._stream = undefined;
    this._streamSource = undefined;

    this._timeout = undefined;

    this._isRecordingInProgress = false;
  }

  async startRecordLoop({
    soundDuration,
    skipDuration,
    deviceId,
    startTime,
  }) {
    if (this._isRecordingInProgress) {
      throw new Error(
        `Can't start a recording, it's already in progress`
      );
    }

    const loopDuration = soundDuration + skipDuration;
    const lookAhead = loopDuration;
    const audioContext = this._audioContext;
    const isRecordingParameter = this._recorderNode.parameters.get(
      'isRecording'
    );

    const constraint = {
      echoCancellation: true,
      latency: 0,
    };

    if (deviceId) {
      constraint.deviceId = {
        exact: deviceId,
      };
    }

    const stream = await navigator.mediaDevices.getUserMedia({
      audio: constraint,
    });

    const streamSource = audioContext.createMediaStreamSource(stream);

    streamSource.connect(this._recorderNode);
    this._recorderNode.connect(audioContext.destination);
    const startRecording = startTime + skipDuration;
    const stopRecording = startRecording + soundDuration;
    isRecordingParameter.setValueAtTime(1, startRecording);
    isRecordingParameter.setValueAtTime(0, stopRecording);
    this._scheduledUntil = stopRecording;

    const audioBufferSize = Math.ceil(
      soundDuration * audioContext.sampleRate
    );
    let bytesWritten = 0;
    let currentAudioData = audioContext.createBuffer(
      1,
      audioBufferSize,
      audioContext.sampleRate
    );

    this._recorderNode.port.onmessage = ({ data }) => {
      const { eventType } = data;

      if (eventType === 'stop') {
        bytesWritten = 0;

        let nextAudioData = audioContext.createBuffer(
          1,
          audioBufferSize,
          audioContext.sampleRate
        );

        this._emitter.emit(Recorder.RECORDING_FINISHED_EVENT, {
          audioData: currentAudioData,
          sampleRate: audioContext.sampleRate,
        });

        if (this._isRecordingInProgress) {
          this._emitter.emit(Recorder.RECORDING_STARTED_EVENT, {
            audioData: nextAudioData,
            sampleRate: audioContext.sampleRate,
          });
        }

        if (!this._isRecordingInProgress) {
          // cleanup
          this._recorderNode.port.onmessage = null;
          this._recorderNode.disconnect();
          currentAudioData = null;
          nextAudioData = null;
          return;
        }

        currentAudioData = nextAudioData;

        return;
      }

      if (eventType === 'data') {
        const { audioBuffer } = data;
        let offset = 0;
        if (bytesWritten > 0) {
          offset = bytesWritten + 1;
        }

        currentAudioData.copyToChannel(audioBuffer, 0, offset);
        bytesWritten += audioBuffer.length;
        return;
      }
    };

    this._stream = stream;
    this._streamSource = streamSource;

    const scheduleNext = () => {
      try {
        const { currentTime } = audioContext;
        let scheduledUntil = this._scheduledUntil;

        if (currentTime > scheduledUntil) {
          scheduledUntil = currentTime;
        }

        if (scheduledUntil > currentTime + lookAhead) {
          return;
        }

        const toScheduleUntil = scheduledUntil + lookAhead;
        let loopsScheduled =
          Math.floor((scheduledUntil - startTime) / loopDuration) - 1;
        let loopsToSchedule =
          Math.ceil((toScheduleUntil - startTime) / loopDuration) -
          loopsScheduled;

        while (loopsToSchedule !== 0) {
          const nextLoopStart =
            startTime + (loopsScheduled + 1) * loopDuration;
          const startRecording = nextLoopStart + skipDuration;
          const stopRecording = startRecording + soundDuration;

          isRecordingParameter.setValueAtTime(1, startRecording);
          isRecordingParameter.setValueAtTime(0, stopRecording);

          this._scheduledUntil = stopRecording;
          loopsToSchedule--;
          loopsScheduled++;
        }
      } finally {
        this._timeout = setTimeout(scheduleNext, lookAhead / 2);
      }
    };
    this._timeout = setTimeout(scheduleNext, lookAhead / 2);

    this._isRecordingInProgress = true;

    this._emitter.emit(Recorder.RECORDING_STARTED_EVENT, {
      audioData: currentAudioData,
      sampleRate: audioContext.sampleRate,
    });
  }

  async stopRecordLoop() {
    this._isRecordingInProgress = false;

    if (this._streamSource) {
      this._streamSource.disconnect();
      this._streamSource = null;
    }

    if (this._recorderNode) {
      this._recorderNode.parameters
        .get('isRecording')
        .cancelScheduledValues(0);
      this._recorderNode.parameters
        .get('isRecording')
        .setValueAtTime(0, 0);
    }

    if (this._stream) {
      closeMediaStream(this._stream);
      this._stream = null;
    }

    clearTimeout(this._timeout);
    this._scheduledUntil = 0;
  }
}

export default Recorder;
