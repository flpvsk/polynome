import React from 'react';

const styles = {
  block: {},
};

const IconRecordOutline = ({ width, height, color }) => {
  let blockStyle = styles.block;

  return (
    <svg
      style={blockStyle}
      width={width}
      height={height}
      fill={color}
      viewBox="0 0 24 24"
    >
      <g id="Bounding_Boxes">
        <path fill="none" d="M0,0h24v24H0V0z" />
      </g>
      <g id="Outline">
        <g id="ui_x5F_spec_x5F_header" />
        <path
          d={
            'M12,6c3.31,0,6,2.69,6,6s-2.69,6-6,6s-6-2.69' +
            '-6-6S8.69,6,12,6 M12,4c-4.42,0-8,3.58-8,8s3.58,8,8,8s8' +
            '-3.58,8-8 S16.42,4,12,4L12,4z'
          }
        />
      </g>
    </svg>
  );
};

export default IconRecordOutline;
