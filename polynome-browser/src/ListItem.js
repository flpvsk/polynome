import React from 'react';
import pick from 'lodash/pick';

const styles = {
  block: {
    width: '100%',
    minHeight: 48,
    paddingLeft: 16,
    paddingRight: 16,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },

  _noPadding: {
    paddingLeft: 0,
    paddingRight: 0,
  },
};

const ListItem = props => {
  const styleProps = pick(props, ['height', 'backgroundColor']);
  let blockStyle = {
    ...styles.block,
    ...styleProps,
  };

  if (props.noPadding) {
    blockStyle = {
      ...blockStyle,
      ...styles._noPadding,
    };
  }

  return (
    <div style={blockStyle} onClick={props.onTouchTap}>
      {props.children}
    </div>
  );
};

export default ListItem;
