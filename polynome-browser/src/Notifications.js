import React, { Component } from 'react';
import timeago from 'timeago.js';

import { connect } from 'react-redux';
import * as people from './data/people';

import withNotificationsProvider from './data/withNotificationsProvider';

import theme from './theme';
import viewport from './viewport';

import shouldComponentUpdate from './shouldComponentUpdate';

import AppBarTopContainer from './AppBarTopContainer';
import TextWrapper from './TextWrapper';
import ButtonIcon from './ButtonIcon';
import Userpic from './Userpic';
import LoadingIndicatorCircular from './LoadingIndicatorCircular';

import IconHeartFilled from './IconHeartFilled';
import IconComment from './IconComment';
import IconRemix from './IconRemix';
import IconSmile from './IconSmile';
import IconMenu from './IconMenu';

import withNavigation from './withNavigation';

const _t = timeago();
const toRelativeTimeText = _t.format.bind(_t);

const styles = {
  block: {
    width: viewport.getWidth(),
    display: 'flex',
    flexDirection: 'column',
    margin: '0 auto',
    alignItems: 'stretch',
  },

  __notificationGroup: {
    display: 'flex',
    flexDirection: 'row',
    paddingLeft: 16,
    paddingTop: 16,
  },

  __notificationGroupPicture: {
    marginRight: 16,
    position: 'relative',
  },

  __notificationGroupText: {
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1,
    borderBottom: `1px solid ${theme.color.outline}`,
    paddingRight: 16,
    paddingBottom: 16,
    position: 'relative',
  },

  __notificationUnreadIndicator: {
    position: 'absolute',
    top: 8,
    right: 8,
    borderRadius: '50%',
    backgroundColor: theme.color.primaryLight,
    height: 8,
    width: 8,
  },

  __loadingIndicator: {
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'center',
    width: '100%',
    height: 72,
  },
};

const IconWrapperNotification = props => {
  const style = {
    width: 20,
    height: 20,
    border: `1px solid ${theme.color.outline}`,
    borderRadius: '50%',
    position: 'absolute',
    top: 28,
    right: 6,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    background: props.background,
  };

  return <div style={style}>{props.children}</div>;
};

class Notifications extends Component {
  constructor() {
    super();
    this.state = {};
    this.shouldComponentUpdate = shouldComponentUpdate;

    this._stop = false;
    this._viewed = new Map();

    this._loop = () => {
      if (this._stop) {
        return;
      }

      const time = Date.now();
      let notifyId;

      for (let el of this._contentNode.children) {
        const classList = el.classList;
        let id;

        if (!classList.contains('_unread')) {
          continue;
        }

        for (let className of classList) {
          if (className.indexOf('e-') !== 0) {
            continue;
          }

          id = className.replace(/^e-/, '');
        }

        const timeStart = this._viewed.get(id);

        if (timeStart && time - timeStart > 1000) {
          notifyId = id;
          this._viewed.delete(id);
          continue;
        }

        if (!timeStart) {
          this._viewed.set(id, time);
        }
      }

      if (notifyId && this.props.markRead) {
        this.props.markRead({ cursor: notifyId });
      }

      setTimeout(this._loop, 1000);
    };

    this._checkAndLoadMore = () => {
      const { body } = window.document;
      const { notificationsGroups, loadMore } = this.props;

      let isNearTheBottom =
        body.scrollHeight - body.scrollTop - body.clientHeight <
        window.scrollY + 200;

      isNearTheBottom =
        isNearTheBottom || body.scrollHeight === body.clientHeight;

      if (!notificationsGroups || !notificationsGroups.pageInfo) {
        return;
      }

      const hasNextPage = notificationsGroups.pageInfo.hasNextPage;
      if (isNearTheBottom && hasNextPage) {
        loadMore();
      }
    };

    this._goToPerson = person => {
      return e => {
        e.stopPropagation();
        e.preventDefault();
        this.props.history.push(`/people/${person.personId}`);
        return false;
      };
    };
  }

  componentDidMount() {
    this._loop();

    window.document.addEventListener('scroll', this._checkAndLoadMore);
    this._checkAndLoadMore();
  }

  componentDidUpdate() {
    this._checkAndLoadMore();
  }

  componentWillUnmount() {
    this._stop = true;
    this._viewed = null;

    window.document.removeEventListener(
      'scroll',
      this._checkAndLoadMore
    );
  }

  _renderNotificationGroup(edge) {
    const { node } = edge;
    const notificationsGroup = node;
    const { type, notifications, chop, isRead } = notificationsGroup;
    const lastNotification = notifications[notifications.length - 1];
    const person = lastNotification.fromPerson || {};

    let eventTypeText = '';
    let EventIcon;
    let eventIconBackground = theme.color.primaryLight;
    let secondLineText;
    let action;

    const peopleIdSet = new Set();
    for (let n of notifications) {
      peopleIdSet.add(n.fromPersonId);
    }

    if (peopleIdSet.size === 2) {
      eventTypeText += 'and 1 other ';
    }

    if (peopleIdSet.size > 2) {
      eventTypeText += `and ${notifications.length - 1} others `;
    }

    if (type === 'clap') {
      eventTypeText += 'clapped for';
      EventIcon = IconHeartFilled;
    }

    if (type === 'comment') {
      eventTypeText += 'commented on';
      EventIcon = IconComment;
    }

    if (type === 'remix') {
      eventTypeText += 'remixed';
      EventIcon = IconRemix;
    }

    if (type === 'welcome') {
      let name = '';
      if (this.props.me && this.props.me.handle) {
        name = `, ${this.props.me.handle}`;
      }

      eventTypeText += `Welcome to Polychops${name}`;
      EventIcon = IconSmile;
      secondLineText = (
        <div>
          <TextWrapper
            styleName={'body1'}
            color={theme.color.onBackground}
          >
            {`Stoked to have you here! ` +
              `Here's some info about the project.`}
          </TextWrapper>
        </div>
      );

      action = () =>
        window.open(
          'https://medium.com/@flpvsk/polychops-655f9347dd4b',
          true
        );
    }

    let unreadIndicator;
    if (!isRead) {
      unreadIndicator = (
        <div style={styles.__notificationUnreadIndicator} />
      );
    }

    let unreadClass;
    if (!isRead) {
      unreadClass = ' _unread';
    }

    if (chop) {
      secondLineText = (
        <TextWrapper
          styleName={'body1'}
          color={theme.color.onBackground}
        >
          <strong>{chop ? chop.name : ''}</strong>
        </TextWrapper>
      );

      action = () => {
        this.props.history.push(`/form/${chop.chopId}`);
      };
    }

    return (
      <div
        key={`notification-${edge.cursor}`}
        style={styles.__notificationGroup}
        className={`e-${edge.cursor} ${unreadClass}`}
      >
        <div
          onClick={this._goToPerson(person)}
          style={styles.__notificationGroupPicture}
        >
          <Userpic size={52} person={person} />
          <IconWrapperNotification background={eventIconBackground}>
            <EventIcon
              color={theme.color.onSecondaryMediumEmphasis}
              width={12}
            />
          </IconWrapperNotification>
        </div>

        <div onClick={action} style={styles.__notificationGroupText}>
          <div>
            <TextWrapper
              styleName={'body1'}
              color={theme.color.onBackground}
              onTouchTap={this._goToPerson(person)}
            >
              <strong>{person.handle || 'anonymous'}</strong>{' '}
            </TextWrapper>
            <TextWrapper
              styleName={'body1'}
              color={theme.color.onBackgroundDisabled}
            >
              {eventTypeText}
            </TextWrapper>
          </div>
          <div>
            {secondLineText}
            {` `}
            <TextWrapper
              styleName={'body1'}
              color={theme.color.onBackgroundDisabled}
            >
              {toRelativeTimeText(lastNotification.createdAt)}
            </TextWrapper>
          </div>
          {unreadIndicator}
        </div>
      </div>
    );
  }

  render() {
    const { notificationsGroups, isLoading } = this.props;

    let notificationsList;
    if (notificationsGroups) {
      notificationsList = notificationsGroups.edges.map(edge => {
        return this._renderNotificationGroup(edge);
      });
    }

    let loadingIndicator;
    if (isLoading) {
      loadingIndicator = (
        <div key="loading-indicator" style={styles.__loadingIndicator}>
          <LoadingIndicatorCircular />
        </div>
      );
    }

    return (
      <AppBarTopContainer
        showNavigation={this.props.showNavigation}
        navigationContent={this.props.navigationContent}
        appBarTitle={
          <TextWrapper
            styleName={'topBarTitle'}
            color={theme.color.onSurfaceMediumEmphasis}
          >
            {'Notifications'}
          </TextWrapper>
        }
        appBarLeftIcon={
          <ButtonIcon onTouchTap={this.props.onShowNavigation}>
            <IconMenu
              width="24"
              height="24"
              color={theme.color.onBackgroundMediumEmphasis}
            />
          </ButtonIcon>
        }
        appBarActions={[]}
      >
        <div ref={c => (this._contentNode = c)} style={styles.block}>
          {notificationsList}
          {loadingIndicator}
        </div>
      </AppBarTopContainer>
    );
  }
}

function mapStateToProps(state) {
  return {
    me: people.select.getMyself(state),
  };
}

export default withNavigation(
  connect(mapStateToProps)(withNotificationsProvider(Notifications))
);
