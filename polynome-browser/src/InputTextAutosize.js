import React from 'react';
import { DebounceInput } from 'react-debounce-input';
import Textarea from 'react-textarea-autosize';

import theme from './theme';

import { styles as textStyles } from './TextWrapper';

const styles = {
  block: {
    minHeight: 40,
    background: 'none',
    appearance: 'none',
    border: 'none',
    backgroundColor: theme.color.containerOnSurface,
    borderBottom: `1px solid ${theme.color.outline}`,
    borderTopLeftRadius: theme.shape.borderRadius,
    borderTopRightRadius: theme.shape.borderRadius,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    paddingTop: 12,
    paddingLeft: 12,
    paddingRight: 12,
    color: theme.color.onSurface,
    textAlign: 'left',
    ...textStyles.subtitle1,
    width: '100%',
  },
};

class InputTextAutosize extends React.Component {
  constructor() {
    super();

    this.state = {
      value: '',
    };

    this._onChange = e => {
      const value = e.target.value;
      this.setState({ value });
      this.props.onChange(value);
    };
  }

  render() {
    const { value } = this.state;
    return (
      <DebounceInput
        style={styles.block}
        element={Textarea}
        onChange={this._onChange}
        value={value}
        autoFocus={this.props.autoFocus}
        maxLength={this.props.maxLength}
      />
    );
  }
}

export default InputTextAutosize;
