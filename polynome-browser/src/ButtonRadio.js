import React, { Component } from 'react';

const styles = {
  block: {
    height: 20,
    width: 20,
    display: 'flex',
    background: 'none',
    border: 'none',
  },

  __radio: {
    height: 20,
    width: 20,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
    borderRadius: '50%',
    borderWidth: 2,
    borderStyle: 'solid',
  },

  __needle: {
    height: 8,
    width: 8,
    borderRadius: '50%',
  },

  __radioInput: {
    display: 'none',
  },
};

export default class RadioButton extends Component {
  constructor() {
    super();

    this._onChange = () => {
      this.props.onChange(!this.props.isSelected);
    };

    this._select = () => {
      if (this.props.isDisabled) {
        return;
      }

      this._inputRef.click();
    };
  }

  render() {
    const {
      isSelected,
      isDisabled,
      enabledColor,
      selectedColor,
      disabledColor,
      name,
      id,
    } = this.props;

    let color = isSelected ? selectedColor : enabledColor;

    if (isDisabled) {
      color = disabledColor;
    }

    let action = isDisabled ? null : this._onChange;

    let blockStyle = {
      ...styles.block,
    };

    let radioStyle = {
      ...styles.__radio,
      borderColor: color,
    };

    let needleStyle = {
      ...styles.__needle,
      backgroundColor: color,
    };

    let needle;

    if (isSelected) {
      needle = <div style={needleStyle} />;
    }

    return (
      <button style={blockStyle} onClick={this._select}>
        <input
          ref={r => (this._inputRef = r)}
          type="radio"
          style={styles.__radioInput}
          id={id}
          name={name}
          checked={isSelected}
          disabled={isDisabled}
          onChange={action}
        />

        <div style={radioStyle}>{needle}</div>
      </button>
    );
  }
}
