import React from 'react';

const IconMenu = ({ width, height, color }) => {
  return (
    <svg width={width} height={height} fill={color} viewBox="0 0 24 24">
      <path d="M0 0h24v24H0z" fill="none" />
      <path d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z" />
    </svg>
  );
};

export default IconMenu;
