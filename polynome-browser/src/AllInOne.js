import React, { Component } from 'react';
import isMatch from 'lodash';
import computeLcm from 'compute-lcm';
import nanoid from 'nanoid';

import Api from './Api';

import beatsSortFn from './beatsSortFn';
import trackUtils from './trackUtils';
import Recorder from './RecorderWorklet';

import shouldComponentUpdate from './shouldComponentUpdate';
import BeatsControls from './BeatsControls';
import Track from './Track';
import TempoControl from './TempoControl';
import CyclesCountControl from './CyclesCountControl';
import Player from './Player';
import AudioPlayer from './AudioPlayer';
import AudioLoader from './AudioLoader';
import CircleControls from './CircleControls';
import SelectedBeatControls from './SelectedBeatControls';
import Settings from './Settings';
import LoadIndicator from './LoadIndicator';

import './App.css';

const DEFAULT_KIT_ID = 'bluebird';

const KEY_WHITESPACE = 32;
const KEY_ENTER = 13;

const PITCH_BY_SUBDIV = ['A', 'C', 'E', 'G', 'B', 'D', 'F'];

const DEFAULT_TEMPO = 120;
const DELAY = 0.05;

function closeMediaStream(stream) {
  if (stream.getTracks) {
    stream.getTracks().forEach(track => track.stop());
  }

  if (!stream.getTracks) {
    stream.stop();
  }
}

function createNewLine({
  lineId,
  commonBeatsCount,
  trackBeatsCount,
  lineName,
}) {
  return {
    lineId,
    lineName,
    lineStart: 0,
    lineDuration: trackBeatsCount / commonBeatsCount,
    beatsCount: 1,
    writingRecordingId: undefined,
    playingRecordingId: undefined,
  };
}

function getSoundIdByIndex(kits, kitId, index) {
  const kit = kits.reduce((acc, k) => {
    if (acc) {
      return acc;
    }

    if (k.id === kitId) {
      return k;
    }

    return acc;
  }, null);

  if (!kit) {
    return null;
  }

  return kit.sounds[index % kit.sounds.length].id;
}

function beatsToAudio({ beats, commonBeatsCount, isPaused }) {
  if (isPaused) {
    return [];
  }

  const hasSoloBeat = beats.reduce((acc, b) => {
    return b.isSolo || acc;
  }, false);

  return beats.reduce((acc, b) => {
    if (hasSoloBeat && !b.isSolo) {
      return acc;
    }

    if (b.isMute) {
      return acc;
    }

    acc.push(b);

    return acc;
  }, []);
}

function linesToAudio({
  lines,
  commonBeatsCount,
  isPaused,
  isRecording,
  recordingForLineId,
}) {
  if (isPaused) {
    return [];
  }

  if (!isRecording) {
    return lines;
  }

  // When recording, armed line is 2 times longer because of the silence
  // part.
  const result = lines.map(l => {
    if (l.lineId === recordingForLineId) {
      return {
        ...l,
        lineDuration: 2 * l.lineDuration,
      };
    }

    return l;
  });

  return result;
}

function mapSoundToBeats({ beats, kits, soundType, kitId, allAuto }) {
  return beats.map((b, i) => {
    if (!b.sound.isAuto && !allAuto) {
      return b;
    }

    let sound = {
      type: soundType,
      pitch: PITCH_BY_SUBDIV[i % PITCH_BY_SUBDIV.length],
      isAuto: true,
    };

    if (soundType === 'kit') {
      sound.soundId = getSoundIdByIndex(
        kits,
        kitId,
        beats.length - i - 1
      );
    }

    return {
      ...b,
      sound,
    };
  });
}

function calculateNewTempo({ oldTempo, oldBeatsCount, newBeatsCount }) {
  const cycleLength = oldTempo / oldBeatsCount;
  return newBeatsCount * cycleLength;
}

function calculateCommonBeatsCount(beats) {
  if (!beats || !beats.length) {
    return 0;
  }

  if (beats.length === 1) {
    return beats[0].beatsCount;
  }

  if (beats.length > 1) {
    const beatsNumbers = beats.map(b => b.beatsCount);
    return computeLcm(beatsNumbers);
  }
}

function updateElements(arr, query, updateFn) {
  return arr.map(el => {
    if (isMatch(el, query)) {
      return updateFn(el);
    }

    return el;
  });
}

function updateBeat(beats, id, updateFn) {
  return beats
    .map(b => {
      if (b.id !== id) {
        return b;
      }

      return updateFn(b);
    })
    .sort(beatsSortFn);
}

function findBeat(beats, id) {
  return beats.reduce((a, b) => {
    if (b.id === id) {
      return b;
    }

    return a;
  }, undefined);
}

function deselectBeatFn(b) {
  return {
    ...b,
    isSelected: false,
  };
}

// TODO: migrate "beats" and like to specific ids, e.g. beatId

class App extends Component {
  constructor() {
    super();
    let windowWidth = window.innerWidth;

    if (windowWidth > 420) {
      windowWidth = 420;
    }

    this.state = {
      isInitialized: false,
      startTime: 0,
      countDown: 0,
      commonBeatsCount: 0,
      commonBeatsTempo: DEFAULT_TEMPO,
      beats: [],
      lines: [],
      recordings: [],
      recordingForLineId: undefined,
      writingRecordingId: undefined,
      windowWidth,
      isPaused: false,
      isRecording: false,
      pauseTime: undefined,
      activeSoundType: 'kit',
      activeKitId: DEFAULT_KIT_ID,
      kits: [],
      savedBeats: [],
      loadingState: {
        isLoading: false,
        progress: 0,
      },
    };

    this.shouldComponentUpdate = shouldComponentUpdate;

    this._getCurrentTime = () => {
      if (!this._audioContext) {
        return 0;
      }

      return this._audioContext.currentTime;
    };

    this._toggleBeat = beatId => {
      const { beats } = this.state;
      const selectedBeat = beats.reduce((a, b) => {
        if (b.isSelected) {
          return b.id;
        }

        return a;
      }, undefined);

      if (selectedBeat === beatId) {
        this.setState({
          beats: beats.map(b => {
            return {
              ...b,
              isSelected: false,
            };
          }),
        });

        return;
      }

      this.setState({
        beats: beats.map(b => {
          return {
            ...b,
            isSelected: b.id === beatId,
          };
        }),
      });
    };

    this._onBeatAdded = beat => {
      Api.track({
        category: 'Controls',
        action: 'click',
        label: 'add-beat',
        value: beat.beatsCount,
      });

      let {
        beats,
        startTime,
        commonBeatsCount,
        commonBeatsTempo,
        lastTempoChange,
        activeSoundType,
        activeKitId,
        kits,
      } = this.state;

      let newTempo = commonBeatsTempo;

      beat = {
        id: nanoid(),
        sound: {
          isAuto: true,
        },
        lineStart: 0,
        lineDuration: beat.beatsCount,
        ...beat,
      };

      let newBeats = beats
        .concat([beat])
        .sort(beatsSortFn)
        .map(b => {
          return {
            ...b,
            isSelected: false,
          };
        });

      newBeats = mapSoundToBeats({
        soundType: activeSoundType,
        kitId: activeKitId,
        beats: newBeats,
        kits,
      });

      commonBeatsCount = calculateCommonBeatsCount(newBeats);

      if (!beats.length) {
        startTime = this._audioContext.currentTime + DELAY;
      }

      if (!lastTempoChange) {
        lastTempoChange = {
          beatsCount: beat.beatsCount,
          tempo: beat.tempo || commonBeatsTempo,
        };
      }

      newTempo = calculateNewTempo({
        oldTempo: lastTempoChange.tempo,
        oldBeatsCount: lastTempoChange.beatsCount,
        newBeatsCount: commonBeatsCount,
      });

      const updateStateAndRhythm = () => {
        this.setState({
          startTime,
          lastTempoChange,
        });

        this._setRhythm({
          commonBeatsCount,
          commonBeatsTempo: newTempo,
          beats: newBeats,
        });
      };

      this._runWhenAudioIsReady(updateStateAndRhythm);
    };

    this._clear = () => {
      Api.track({
        category: 'Controls',
        action: 'click',
        label: 'clear-rhythm',
      });
      this.setState({
        startTime: 0,
        lastTempoChange: undefined,
        isPaused: false,
      });

      this._setRhythm({
        commonBeatsCount: 0,
        commonBeatsTempo: 120,
        beats: [],
      });
    };

    this._changeTempo = (beatId, tempo) => {
      Api.track({
        category: 'Controls',
        action: 'click',
        label: 'change-tempo',
      });

      const { commonBeatsCount, beats } = this.state;
      const beat = findBeat(beats, beatId);
      const newTempo = commonBeatsCount * tempo / beat.beatsCount;
      this.setState({
        startTime: this._audioContext.currentTime + DELAY,
        lastTempoChange: {
          beatsCount: beat.beatsCount,
          tempo,
        },
      });

      this._setRhythm({
        commonBeatsTempo: newTempo,
      });
    };

    this._changeOffset = (beatId, offset) => {
      Api.track({
        category: 'Controls',
        action: 'click',
        label: 'change-offset',
        value: offset,
      });

      this._setRhythm({
        beats: updateBeat(this.state.beats, beatId, b => {
          return {
            ...b,
            offset,
          };
        }),
      });
    };

    this._deselectAll = () => {
      Api.track({
        category: 'Controls',
        action: 'click',
        label: 'deselect-all',
      });

      this.setState({
        beats: this.state.beats.map(b => {
          return {
            ...b,
            isSelected: false,
          };
        }),
      });
    };

    this._toggleMuteSection = (beatId, beatNumber) => {
      Api.track({
        category: 'Controls',
        action: 'click',
        label: 'toggle-mute-section',
      });

      this._setRhythm({
        beats: updateBeat(this.state.beats, beatId, b => {
          let muted = [];

          if (b.mutedBeats) {
            muted = b.mutedBeats.slice();
          }

          const index = muted.indexOf(beatNumber);

          if (index !== -1) {
            muted.splice(index, 1);
          }

          if (index === -1) {
            muted.push(beatNumber);
          }

          return {
            ...b,
            mutedBeats: muted,
          };
        }),
      });
    };

    this._toggleMuteCircle = beatId => {
      Api.track({
        category: 'Controls',
        action: 'click',
        label: 'toggle-mute-circle',
      });

      this._setRhythm({
        beats: updateBeat(this.state.beats, beatId, b => {
          return {
            ...b,
            isMute: !b.isMute,
          };
        }),
      });
    };

    this._toggleSoloCircle = beatId => {
      Api.track({
        category: 'Controls',
        action: 'click',
        label: 'toggle-solo-circle',
      });

      this._setRhythm({
        beats: this.state.beats.map(b => {
          return {
            ...b,
            isSolo: b.id === beatId ? !b.isSolo : false,
          };
        }),
      });
    };

    this._togglePause = () => {
      const { isPaused, isRecording } = this.state;

      if (isRecording) {
        this._recorder.stopRecordLoop();
        this._recorder.off(Recorder.RECORDING_STARTED_EVENT);
        this._recorder.off(Recorder.RECORDING_FINISHED_EVENT);
      }

      if (!isPaused) {
        this.setState({
          isPaused: true,
          isRecording: false,
          startTime: undefined,
        });

        return;
      }

      const play = () => {
        this.setState({
          startTime: this._audioContext.currentTime + DELAY,
          isPaused: false,
        });
      };

      this._runWhenAudioIsReady(play);
    };

    this._runWhenAudioIsReady = async fn => {
      try {
        await this._prepareAudio();
        fn();
      } catch (e) {
        console.warn('Error starting AudioContext', e);
      }
    };

    this._prepareAudio = () => {
      return new Promise((resolve, reject) => {
        if (this._audioContext.state === 'running') {
          resolve();
          return;
        }

        if (this._audioContext.state === 'suspended') {
          this._audioContext
            .resume()
            .then(resolve)
            .catch(reject);
          return;
        }

        reject(
          new Error(
            `Unknown AudioContext state ${this._audioContext.state}`
          )
        );
      });
    };

    this._togglePauseAndTrack = () => {
      let label = 'toggle-pause';

      if (this.state.isRecording) {
        label = 'toggle-pause-stop-recording';
      }

      Api.track({
        category: 'Controls',
        action: 'click',
        label,
      });

      this._togglePause();
    };

    this._globalKeyEventHandler = e => {
      const { keyCode } = e;

      if ([KEY_WHITESPACE, KEY_ENTER].indexOf(keyCode) === -1) {
        return true;
      }

      if (e.target.tagName === 'INPUT') {
        return true;
      }

      if (keyCode === KEY_ENTER) {
        if (this.state.isRecording) {
          this._addLine();
          e.cancelBubble = true;
          e.stopPropagation();
          e.stopImmediatePropagation();
          e.preventDefault();

          Api.track({
            category: 'Controls',
            action: 'keypress',
            label: 'add-line',
          });

          return false;
        }

        return true;
      }

      if (!this.state.beats || !this.state.beats.length) {
        return true;
      }

      e.cancelBubble = true;
      e.stopPropagation();
      e.stopImmediatePropagation();
      e.preventDefault();

      const label = this.state.isRecording
        ? 'toggle-pause-stop-recording'
        : 'toggle-pause';

      Api.track({
        category: 'Controls',
        action: 'keypress',
        label,
      });

      this._togglePause();

      return false;
    };

    this._deleteBeat = beatId => {
      Api.track({
        category: 'Controls',
        action: 'click',
        label: 'delete-beat',
      });

      let {
        beats,
        commonBeatsCount,
        commonBeatsTempo,
        startTime,
      } = this.state;

      beats = beats.filter(b => b.id !== beatId);

      if (beats.length === 0) {
        this._setRhythm({
          beats,
          startTime: 0,
          commonBeatsCount: 0,
          commonBeatsTempo: 120,
        });

        return;
      }

      let newCommonBeatsCount = calculateCommonBeatsCount(beats);

      let newCommonBeatsTempo = calculateNewTempo({
        oldTempo: commonBeatsTempo,
        oldBeatsCount: commonBeatsCount,
        newBeatsCount: newCommonBeatsCount,
      });

      this.setState({
        startTime,
      });

      this._setRhythm({
        beats,
        commonBeatsCount: newCommonBeatsCount,
        commonBeatsTempo: newCommonBeatsTempo,
      });
    };

    this._changeBeatsCount = (beatId, beatsCount) => {
      Api.track({
        category: 'Controls',
        action: 'click',
        label: 'change-beat-count',
        value: beatsCount,
      });

      const beats = updateBeat(this.state.beats, beatId, b => {
        return {
          ...b,
          beatsCount,
        };
      });

      const { commonBeatsCount, commonBeatsTempo } = this.state;

      const newCommonBeatsCount = calculateCommonBeatsCount(beats);
      const newCommonBeatsTempo = calculateNewTempo({
        oldTempo: commonBeatsTempo,
        oldBeatsCount: commonBeatsCount,
        newBeatsCount: newCommonBeatsCount,
      });

      this._setRhythm({
        beats: beats.sort(beatsSortFn),
        commonBeatsCount: newCommonBeatsCount,
        commonBeatsTempo: newCommonBeatsTempo,
      });
    };

    this._showSettings = () => {
      Api.track({
        category: 'Controls',
        action: 'click',
        label: 'show-settings',
      });

      this.setState({
        shouldShowSettings: true,
      });
    };

    this._hideSettings = () => {
      Api.track({
        category: 'Controls',
        action: 'click',
        label: 'hide-settings',
      });

      this.setState({
        shouldShowSettings: false,
      });
    };

    this._changeSoundType = (soundType, kitId) => {
      Api.track({
        category: 'Controls',
        action: 'click',
        label: `change-active-sound-type-${soundType}`,
      });

      const { kits, beats } = this.state;

      const newBeats = mapSoundToBeats({
        kits,
        beats,
        soundType,
        kitId,
        allAuto: true,
      });

      this._setRhythm({
        beats: newBeats,
        activeSoundType: soundType,
        activeKitId: kitId || DEFAULT_KIT_ID,
      });
    };

    this._changeSound = (beatId, sound) => {
      Api.track({
        category: 'Controls',
        action: 'click',
        label: `change-beat-sound`,
      });

      const { kits, beats, activeSoundType, activeKitId } = this.state;

      let newBeats = updateBeat(beats, beatId, b => {
        return {
          ...b,
          sound,
        };
      });

      if (sound.isAuto) {
        newBeats = mapSoundToBeats({
          beats: newBeats,
          kits,
          soundType: activeSoundType,
          kitId: activeKitId,
        });
      }

      this._setRhythm({
        beats: newBeats,
      });
    };

    this._startLoad = idsToLoad => {
      if (this.state.loadingState.isLoading) {
        return;
      }

      this.setState({
        loadingState: {
          isLoading: true,
          progress: 0,
        },
      });
    };

    this._updateLoadProgress = progress => {
      this.setState({
        loadingState: {
          isLoading: true,
          progress,
        },
      });
    };

    this._finishLoad = () => {
      this.setState({
        loadingState: {
          isLoading: false,
          progress: 1,
        },
      });
    };

    this._saveCurrentBeat = name => {
      Api.track({
        category: 'Controls',
        action: 'click',
        label: `save-current-beat`,
      });

      const {
        beats,
        commonBeatsCount,
        commonBeatsTempo,
        activeKitId,
        activeSoundType,
      } = this.state;

      const beatInfo = {
        id: nanoid(),
        name,
        beats,
        commonBeatsCount,
        commonBeatsTempo,
        activeKitId,
        activeSoundType,
        createdAt: Date.now(),
      };

      this.setState({
        savedBeats: [beatInfo].concat(this.state.savedBeats),
      });

      Api.saveBeat(beatInfo);
    };

    this._removeBeat = beatId => {
      Api.track({
        category: 'Controls',
        action: 'click',
        label: `remove-beat`,
      });

      this.setState({
        savedBeats: this.state.savedBeats.filter(b => b.id !== beatId),
      });

      Api.removeBeat(beatId);
    };

    this._loadBeat = beatInfoId => {
      Api.track({
        category: 'Controls',
        action: 'click',
        label: `load-beat`,
      });

      const { savedBeats } = this.state;
      const beatInfo = savedBeats.reduce((acc, b) => {
        if (b.id === beatInfoId) {
          return b;
        }

        return acc;
      }, undefined);

      const {
        commonBeatsCount,
        commonBeatsTempo,
        beats,
        activeKitId,
        activeSoundType,
      } = beatInfo;

      if (this.state.isPaused) {
        this._togglePause();
      }

      this._setRhythm({
        commonBeatsCount,
        commonBeatsTempo,
        activeKitId,
        activeSoundType,
        beats,
      });
    };

    this._setRhythm = rhythm => {
      const {
        commonBeatsCount,
        commonBeatsTempo,
        activeKitId,
        activeSoundType,
      } = this.state;

      const beats = rhythm.beats || this.state.beats;

      if (!beats.length) {
        Api.saveLastPlayedBeat(null);
      }

      if (beats.length) {
        Api.saveLastPlayedBeat({
          commonBeatsCount,
          commonBeatsTempo,
          ...rhythm,
          beats: beats.map(deselectBeatFn),
          activeKitId,
          activeSoundType,
        });
      }

      this.setState(rhythm);
    };

    this._recordingStarted = ({
      lineId,
      audioData,
      commonBeatsCount,
      commonBeatsTempo,
      trackBeatsCount,
      latency,
      sampleRate,
    }) => {
      let { lines, recordings } = this.state;
      const recordingId = nanoid();
      const writingRecordingId = recordingId;

      recordings = recordings.concat([
        {
          lineId,
          recordingId,
          name: `Recording #${recordings.length + 1}`,
          beatsCount: trackBeatsCount,
          beatsTempo: commonBeatsTempo,
          sound: {
            type: 'recording',
            audioData: audioData,
            sampleRate,
            latency: latency,
          },
        },
      ]);

      lines = updateElements(lines, { lineId }, l => {
        return {
          ...l,
          writingRecordingId,
        };
      });

      const activeRecordings = lines.map(l => {
        return l.playingRecordingId;
      });

      const RECORDINGS_SOFT_LIMIT = 5;
      recordings = recordings
        .slice(0, RECORDINGS_SOFT_LIMIT)
        .filter(r => {
          return (
            r.recordingId === recordingId ||
            activeRecordings.indexOf(r.recordingId) !== -1
          );
        });

      this.setState({
        lines,
        recordings,
        writingRecordingId,
      });
    };

    this._startRecording = async () => {
      await this._prepareAudio();

      const {
        commonBeatsCount,
        commonBeatsTempo,
        selectedInputDeviceId,
        beats,
      } = this.state;

      let { recordingForLineId, lines } = this.state;

      const trackBeatsCount = trackUtils.getTrackBeatsCount({
        commonBeatsCount,
        beats,
      });

      if (!recordingForLineId) {
        const lineId = nanoid();
        const lineName = String.fromCharCode(97 + lines.length);
        const line = createNewLine({
          lineId,
          lineName,
          commonBeatsCount,
          trackBeatsCount,
        });

        lines = lines.concat([line]);
        recordingForLineId = lineId;

        await this.setState({
          lines,
          recordingForLineId,
        });
      }

      const countDown = trackBeatsCount;
      const soundDuration = trackBeatsCount * 60 / commonBeatsTempo;

      this._recorder.on(
        Recorder.RECORDING_STARTED_EVENT,
        ({ audioData, sampleRate }) => {
          const { recordingForLineId } = this.state;
          this._recordingStarted({
            lineId: recordingForLineId,
            audioData,
            latency: 0.068, // TODO: a non-hardcoded way
            sampleRate,
            commonBeatsCount,
            commonBeatsTempo,
            trackBeatsCount,
          });
        }
      );

      this._recorder.on(
        Recorder.RECORDING_FINISHED_EVENT,
        ({ audioData, sampleRate }) => {
          const { recordingForLineId, writingRecordingId } = this.state;
          let { lines } = this.state;

          lines = updateElements(lines, {}, l => {
            let { playingRecordingId } = l;
            if (l.lineId === recordingForLineId) {
              playingRecordingId = writingRecordingId;
            }

            return {
              ...l,
              writingRecordingId: undefined,
              playingRecordingId,
            };
          });

          this.setState({
            lines,
            writingRecordingId: undefined,
          });
        }
      );

      const { currentTime } = this._audioContext;

      await this._recorder.startRecordLoop({
        deviceId: selectedInputDeviceId,
        soundDuration,
        skipDuration: soundDuration,
        startTime: currentTime,
      });

      this.setState({
        startTime: currentTime,
        countDown: countDown,
        isRecording: true,
        isPaused: false,
      });
    };

    this._changeInputDeviceId = deviceId => {
      this.setState({
        selectedInputDeviceId: deviceId,
      });
    };

    this._addLine = () => {
      const id = nanoid();
      const {
        commonBeatsCount,
        beats,
        isRecording,
        writingRecordingId,
      } = this.state;
      let { lines, recordings } = this.state;
      const lineName = String.fromCharCode(97 + lines.length);

      const trackBeatsCount = trackUtils.getTrackBeatsCount({
        commonBeatsCount,
        beats,
      });

      const line = createNewLine({
        lineId: id,
        lineName,
        commonBeatsCount,
        trackBeatsCount,
      });

      if (isRecording && writingRecordingId) {
        line.writingRecordingId = writingRecordingId;
        recordings = updateElements(
          recordings,
          { recordingId: writingRecordingId },
          r => {
            return {
              ...r,
              lineId: id,
            };
          }
        );
      }

      this.setState({
        recordingForLineId: id,
        lines: lines.concat([line]),
        recordings,
      });
    };

    this._changeCyclesCount = newCount => {
      if (newCount <= 0) {
        return;
      }

      const { beats } = this.state;
      this.setState({
        beats: updateElements(beats, {}, b => {
          return {
            ...b,
            lineDuration: newCount * b.beatsCount,
          };
        }),
      });
    };
  }

  componentWillMount() {
    const Context = window.AudioContext || window.webkitAudioContext;
    this._audioContext = new Context();
  }

  componentWillUnmount() {
    window.document.removeEventListener(
      'keydown',
      this._globalKeyEventHandler,
      true
    );
  }

  componentDidMount() {
    window.document.addEventListener(
      'keydown',
      this._globalKeyEventHandler,
      true
    );

    Promise.all([
      Recorder.create({
        audioContext: this._audioContext,
      }),

      Api.listKits().then(kits =>
        this.setState({
          kits,
          loadingState: {
            isLoading: true,
            progress: this.state.loadingState.progress + 0.33,
          },
        })
      ),
      Api.listSavedBeats().then(savedBeats =>
        this.setState({
          savedBeats,
          loadingState: {
            isLoading: true,
            progress: this.state.loadingState.progress + 0.33,
          },
        })
      ),
      Api.getLastPlayedBeat().then(savedBeat => {
        const loadingState = {
          isLoading: true,
          progress: this.state.loadingState.progress + 0.33,
        };

        if (!savedBeat) {
          this.setState({ loadingState });
          return;
        }

        const {
          commonBeatsCount,
          commonBeatsTempo,
          activeKitId,
          activeSoundType,
          beats,
        } = savedBeat;

        this.setState({
          commonBeatsCount,
          commonBeatsTempo,
          activeKitId,
          activeSoundType,
          beats,
          loadingState,
          startTime: 0,
          isPaused: true,
        });
      }),

      (async () => {
        const devicesApi = navigator.mediaDevices;
        const stream = await devicesApi.getUserMedia({ audio: true });
        const mediaDevices = await devicesApi.enumerateDevices();
        let selectedInputDeviceId = null;

        for (let device of mediaDevices) {
          if (device.kind === 'audioinput') {
            selectedInputDeviceId = device.deviceId;
            break;
          }
        }

        closeMediaStream(stream);

        this.setState({
          mediaDevices: mediaDevices.map(m => m.toJSON()),
          selectedInputDeviceId,
        });
      })(),
    ]).then(([recorder]) => {
      this._recorder = recorder;
      this.setState({
        isInitialized: true,
        loadingState: {
          isLoading: false,
          progress: 1,
        },
      });
    });
  }

  _getCyclesCount() {
    const { commonBeatsCount, beats } = this.state;

    const trackBeatsCount = trackUtils.getTrackBeatsCount({
      commonBeatsCount,
      beats,
    });

    return trackBeatsCount / commonBeatsCount;
  }

  render() {
    const {
      commonBeatsCount,
      commonBeatsTempo,
      writingRecordingId,
      recordings,
      lines,
      beats,
    } = this.state;

    const selectedBeat = beats.reduce((a, b) => {
      if (a) {
        return a;
      }

      if (b.isSelected) {
        return {
          ...b,
          tempo: b.beatsCount * commonBeatsTempo / commonBeatsCount,
        };
      }

      return a;
    }, undefined);

    let tempoValue;
    let tempoBeatId;

    let tempoBeat = selectedBeat || beats[beats.length - 1];

    if (tempoBeat) {
      tempoBeatId = tempoBeat.id;
      tempoValue =
        tempoBeat.beatsCount * commonBeatsTempo / commonBeatsCount;
    }

    let content;

    if (this.state.shouldShowSettings) {
      content = (
        <Settings
          key="settings"
          activeSoundType={this.state.activeSoundType}
          activeKitId={this.state.activeKitId}
          beats={beats}
          commonBeatsTempo={this.state.commonBeatsTempo}
          savedBeats={this.state.savedBeats}
          kits={this.state.kits}
          width={this.state.windowWidth}
          onCloseSettings={this._hideSettings}
          onChangeSoundType={this._changeSoundType}
          onSaveBeat={this._saveCurrentBeat}
          onRemoveBeat={this._removeBeat}
          onLoadBeat={this._loadBeat}
        />
      );
    }

    if (!this.state.shouldShowSettings && this.state.isInitialized) {
      const activeKit = this.state.kits.reduce((acc, k) => {
        if (k.id === this.state.activeKitId) {
          return k;
        }

        return acc;
      }, undefined);

      content = [
        <BeatsControls
          key="header"
          width={this.state.windowWidth - 20}
          beats={beats}
          commonBeatsCount={commonBeatsCount}
          commonBeatsTempo={commonBeatsTempo}
          onMute={this._toggleMuteCircle}
          onSolo={this._toggleSoloCircle}
          onBeatClick={this._toggleBeat}
        />,

        <TempoControl
          key="tempo-control"
          width={this.state.windowWidth - 20}
          tempo={tempoValue}
          beatId={tempoBeatId}
          onTempoChange={this._changeTempo}
        />,

        <CyclesCountControl
          key="cycles-control"
          onChangeCyclesCount={this._changeCyclesCount}
          cyclesCount={this._getCyclesCount()}
        />,

        <Track
          key="track"
          track={{
            beats,
            lines,
            recordings,
            commonBeatsCount,
            commonBeatsTempo,
          }}
          startTime={this.state.startTime}
          getCurrentTime={this._getCurrentTime}
          width={this.state.windowWidth - 20}
          recordingForLineId={this.state.recordingForLineId}
          writingRecordingId={writingRecordingId}
          isPaused={this.state.isPaused}
          isRecording={this.state.isRecording}
        />,

        <div
          key="relative-container"
          className="App__RelativeContainer"
        >
          <Player
            width={this.state.windowWidth * 0.8}
            commonBeatsTempo={commonBeatsTempo}
            commonBeatsCount={commonBeatsCount}
            audioContext={this._audioContext}
            startTime={this.state.startTime}
            beats={beats}
            isPaused={this.state.isPaused}
            pauseTime={this.state.pauseTime}
          />

          <CircleControls
            width={this.state.windowWidth * 0.8}
            defaultTempo={DEFAULT_TEMPO}
            onBeatAdded={this._onBeatAdded}
            onPauseToggle={this._togglePauseAndTrack}
            onShowSettings={this._showSettings}
            canClear={beats.length > 0 && !selectedBeat}
            onToggleRecord={this._startRecording}
            onChangeInputDeviceId={this._changeInputDeviceId}
            selectedInputDeviceId={this.state.selectedInputDeviceId}
            inputDevices={this.state.mediaDevices.filter(
              d => d.kind === 'audioinput'
            )}
            shouldShowPausePlay={beats.length > 0 && !selectedBeat}
            shouldShowEmptyScreenBlock={beats.length === 0}
            isPaused={this.state.isPaused}
            onClear={this._clear}
            isEmptyState={!beats || !beats.length}
          />
        </div>,

        <SelectedBeatControls
          key="selected-beat-controls"
          width={this.state.windowWidth}
          onChangeTempo={this._changeTempo}
          onChangeOffset={this._changeOffset}
          onChangeBeatsCount={this._changeBeatsCount}
          onDeleteBeat={this._deleteBeat}
          onMuteBeat={this._toggleMuteSection}
          onDeselectBeat={this._deselectAll}
          onChangeSound={this._changeSound}
          commonBeatsCount={commonBeatsCount}
          activeSoundType={this.state.activeSoundType}
          activeKit={activeKit}
          selectedBeat={selectedBeat}
        />,

        <button key="add-line" onClick={this._addLine}>
          Add Line
        </button>,
      ];
    }

    const audioBeats = beatsToAudio(this.state);
    const soundsIds = audioBeats.reduce((acc, beat) => {
      if (beat.sound.type === 'kit') {
        acc.add(beat.sound.soundId);
      }

      return acc;
    }, new Set());

    return (
      <div className="App">
        <AudioLoader
          onLoadStarted={this._startLoad}
          onLoadCompleted={this._finishLoad}
          onLoadFailed={console.log.bind(console, 'loadfailed')}
          onLoadProgressed={this._updateLoadProgress}
          soundsIds={[...soundsIds]}
          recordings={recordings}
          lines={lines}
          audioContext={this._audioContext}
        >
          {sounds => [
            <AudioPlayer
              key="sounds-audio-player"
              startTime={this.state.startTime}
              countDown={this.state.countDown}
              beats={audioBeats}
              lines={[]}
              commonBeatsCount={commonBeatsCount}
              commonBeatsTempo={commonBeatsTempo}
              sounds={sounds}
              audioContext={this._audioContext}
            />,

            ...linesToAudio(this.state).map(l => (
              <AudioPlayer
                key={`lines-audio-player-${l.lineId}`}
                startTime={this.state.startTime}
                countDown={0}
                beats={[]}
                lines={[l]}
                commonBeatsCount={commonBeatsCount}
                commonBeatsTempo={commonBeatsTempo}
                sounds={sounds}
                audioContext={this._audioContext}
              />
            )),
          ]}
        </AudioLoader>

        {content}

        <LoadIndicator
          width={this.state.windowWidth}
          loadingState={this.state.loadingState}
        />
      </div>
    );
  }
}

export default App;
