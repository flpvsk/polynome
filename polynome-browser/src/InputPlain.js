import React from 'react';
import theme from './theme';
import { styles as textStyles } from './TextWrapper';

const styles = {
  block: {
    height: 40,
    background: 'none',
    appearance: 'none',
    border: 'none',
    backgroundColor: theme.color.containerOnSurface,
    borderBottom: `1px solid ${theme.color.outline}`,
    borderTopLeftRadius: theme.shape.borderRadius,
    borderTopRightRadius: theme.shape.borderRadius,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    paddingLeft: 12,
    paddingRight: 12,
    color: theme.color.onSurface,
    textAlign: 'left',
    ...textStyles.subtitle1,
    width: '100%',
  },
};

class InputPlain extends React.Component {
  constructor() {
    super();
    this._onChange = e => {
      const value = e.target.value;
      this.props.onChange(value);
    };
  }

  render() {
    let blockStyle = styles.block;

    return (
      <input
        type={this.props.type || 'text'}
        autoComplete={this.props.autocomplete}
        style={blockStyle}
        onChange={this._onChange}
        value={this.props.value}
        placeholder={this.props.placeholder}
        maxLength={this.props.maxLength}
        disabled={this.props.isDisabled}
        readOnly={this.props.readOnly}
        ref={this.props.inputRef}
        onClick={e => e.stopPropagation()}
      />
    );
  }
}

export default InputPlain;
