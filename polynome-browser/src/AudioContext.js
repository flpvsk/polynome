const Context = window.AudioContext || window.webkitAudioContext;

export async function prepareContext(audioContext) {
  return new Promise((resolve, reject) => {
    if (audioContext.state === 'running') {
      resolve();
      return;
    }

    if (audioContext.state === 'suspended') {
      audioContext
        .resume()
        .then(resolve)
        .catch(reject);
      return;
    }

    reject(
      new Error(`Unknown AudioContext state ${audioContext.state}`)
    );
  });
}

export default Context;
