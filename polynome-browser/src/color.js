const DELTA = 8;

const SOLO_COLOR = [27, 100, 60];
// const MUTE_COLOR = [0, 0, 60];

// const BEAT_COLORS_BY_PRIME_ORIGINAL = [
//   [[300, 60, 42]],
//   [[53, 93, 53 - DELTA], [53, 93, 53], [53, 93, 53 + DELTA]],
//   [[326, 78, 52 - DELTA], [326, 78, 52], [326, 78, 52 + DELTA]],
//   [[338, 93, 53 - DELTA], [338, 93, 53], [338, 93, 53 + DELTA]],
// ];

const BEAT_COLORS_BY_PRIME = [
  [[5, 96, 61]],
  [[47, 81, 56 - DELTA], [47, 81, 56], [47, 81, 56 + DELTA]],
  [[168, 97, 41 - DELTA], [168, 97, 41], [168, 97, 41 + DELTA]],
  [[217, 48, 39 - DELTA], [217, 48, 39], [217, 48, 39 + DELTA]],
];

const LINE_COLORS = [[225, 100, 63], [195, 100, 64]];

const RECORDING_COLOR = [333, 100, 45];

export function colorStrByBeat({ beat, isMute, isSolo }) {
  if (isMute === undefined) {
    isMute = beat.isMute;
  }

  const baseColor = baseColorByBeat(beat);
  let c = baseColor;

  if (isMute) {
    c = muteColor(c);
  }

  if (isSolo) {
    c = soloColor();
  }

  return toHslString(c);
}

export function delimiterColorStrByBeat({ beat, isMute, isSolo }) {
  isMute = isMute || beat.isMute;

  const baseColor = baseColorByBeat(beat);
  let c = baseColor;

  if (isMute) {
    c = muteColor(c);
  }

  if (isSolo) {
    c = soloColor();
  }

  return toHslString(delimiterColorByBase(c));
}

export function delimiterColorStrByLine({
  line,
  isMute,
  isSolo,
  isRecording,
}) {
  isMute = isMute || line.isMute;

  const baseColor = baseColorByLine(line);
  let c = baseColor;

  if (isMute) {
    c = muteColor(c);
  }

  if (isSolo) {
    c = soloColor();
  }

  if (isRecording) {
    c = RECORDING_COLOR;
  }

  return toHslString(delimiterColorByBase(c));
}

export function colorStrByLine({ line, isMute, isSolo, isRecording }) {
  isMute = isMute || line.isMute;

  const baseColor = baseColorByLine(line);
  let c = baseColor;

  if (isMute) {
    c = muteColor(c);
  }

  if (isSolo) {
    c = soloColor();
  }

  if (isRecording) {
    c = RECORDING_COLOR;
  }

  return toHslString(c);
}

export function colorStrForRecording() {
  return toHslString(RECORDING_COLOR);
}

export function baseColorByBeat(beat) {
  return baseColorByBeatsCount(beat.beatsCount);
}

const PRIME_NUMBERS = [
  2,
  3,
  5,
  7,
  11,
  13,
  17,
  19,
  23,
  29,
  31,
  37,
  41,
  43,
  47,
  53,
].reverse();

export function baseColorByBeatsCount(beatsCount) {
  if (beatsCount === 1) {
    return BEAT_COLORS_BY_PRIME[0][0];
  }

  let prime = 0;
  let mult = 0;
  for (let i = 0; i < PRIME_NUMBERS.length; i++) {
    const n = PRIME_NUMBERS[i];

    if (n > beatsCount) {
      continue;
    }

    if (beatsCount % n === 0) {
      prime = i;
      mult = beatsCount / n;
      break;
    }
  }

  const primeArray =
    BEAT_COLORS_BY_PRIME[1 + prime % (BEAT_COLORS_BY_PRIME.length - 1)];

  return [...primeArray[mult % primeArray.length]];
}

export function delimiterColorByBeat(beat) {
  return delimiterColorByBeatsCount(beat.beatsCount);
}

export function delimiterColorByBeatsCount(beatsCount) {
  const baseColor = baseColorByBeatsCount(beatsCount);
  return delimiterColorByBase(baseColor);
}

export function delimiterColorByBase(baseColor) {
  const lightnessUpdate = baseColor[2] > 60 ? 30 : 20;
  return updateLightness(baseColor, -lightnessUpdate);
}

export function soloColor() {
  return SOLO_COLOR;
}

export function muteColor(baseColor) {
  // return MUTE_COLOR;
  return updateHsl(baseColor, 0, -100, -12);
}

export function updateLightness(color, lightness) {
  return updateHsl(color, 0, 0, lightness);
}

export function updateHsl(color, hue, sat, lightness) {
  const c = [...color];
  c[0] += hue;
  c[1] += sat;
  c[2] += lightness;

  if (c[0] > 360) {
    c[0] = 360;
    // console.warn(
    //   'Making the color hue larger than possible',
    //   color,
    //   hue
    // );
  }

  if (c[0] < 0) {
    c[0] = 0;
    // console.warn(
    //   'Making the color hue smaller than possible',
    //   color,
    //   hue
    // );
  }

  if (c[1] > 100) {
    c[1] = 100;
    // console.warn(
    //   'Making the color saturation larger than possible',
    //   color,
    //   sat
    // );
  }

  if (c[1] < 0) {
    c[1] = 0;
    // console.warn(
    //   'Making the color saturation smaller than possible',
    //   color,
    //   sat
    // );
  }

  if (c[2] > 100) {
    c[2] = 100;
    // console.warn(
    //   'Making the color lightness larger than possible',
    //   color,
    //   lightness
    // );
  }

  if (c[1] < 0) {
    c[1] = 0;
    // console.warn(
    //   'Making the color lightness smaller than possible',
    //   color,
    //   lightness
    // );
  }

  return c;
}

export function baseColorByLine(line) {
  const n = line.lineName.charCodeAt(0);
  return LINE_COLORS[n % LINE_COLORS.length];
}

export function baseColorForRecording() {
  return RECORDING_COLOR;
}

export function toHslString(color) {
  const [hue, sat, light] = color;
  return `hsl(${hue}, ${sat}%, ${light}%)`;
}

export function toHslaString(color) {
  const [hue, sat, light, opacity] = color;
  return `hsla(${hue}, ${sat}%, ${light}%, ${opacity})`;
}

const all = {
  colorStrByBeat,
  baseColorByBeat,
  baseColorByBeatsCount,
  delimiterColorByBeat,
  delimiterColorByBeatsCount,
  delimiterColorByBase,
  delimiterColorStrByBeat,
  delimiterColorStrByLine,
  baseColorByLine,
  colorStrByLine,
  baseColorForRecording,
  colorStrForRecording,
  soloColor,
  muteColor,
  updateLightness,
  updateHsl,
  toHslString,
  toHslaString,
};

export default all;
