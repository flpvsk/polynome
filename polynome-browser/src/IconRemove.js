import React from 'react';

const RemoveIcon = ({ width, height, color }) => {
  return (
    <svg width={width} height={height} fill={color} viewBox="0 0 24 24">
      <path d="M19 13H5v-2h14v2z" />
      <path d="M0 0h24v24H0z" fill="none" />
    </svg>
  );
};

export default RemoveIcon;
