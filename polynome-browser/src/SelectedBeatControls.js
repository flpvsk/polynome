import React, { Component } from 'react';
import sortBy from 'lodash/sortBy';

import shouldComponentUpdate from './shouldComponentUpdate';
import color from './color';

import IconDelete from './IconDelete';
import IconDone from './IconDone';
import './SelectedBeatControls.css';

const PITCH_LIST = ['A', 'B', 'C', 'D', 'E', 'F', 'G'];

class SelectedBeatControls extends Component {
  static getDerivedStateFromProps(nextProps, prevState) {
    const { selectedBeat } = nextProps;

    if (!selectedBeat) {
      return {
        beatId: null,
        beatsCount: null,
        offset: null,
        tempo: null,
      };
    }

    if (prevState.beatId === selectedBeat.id) {
      return prevState;
    }

    return {
      beatId: selectedBeat.id,
      beatsCount: selectedBeat.beatsCount || 1,
      offset: selectedBeat.offset || 0,
      tempo: selectedBeat.tempo || 1,
    };
  }

  constructor() {
    super();

    this.state = {
      beatId: null,
      beatsCount: null,
      offset: null,
      tempo: null,
    };

    this.shouldComponentUpdate = shouldComponentUpdate;

    this._changeTempo = e => {
      const selectedBeat = this.props.selectedBeat || {};
      let { value } = e.target;

      if (value === '') {
        this.setState({
          tempo: '',
        });
        return;
      }

      let tempo = parseFloat(value, 10);

      if (isNaN(tempo) || tempo < 1) {
        tempo = 1;
      }

      if (tempo !== selectedBeat.tempo) {
        this.props.onChangeTempo(this.props.selectedBeat.id, tempo);
      }

      this.setState({ tempo: tempo || selectedBeat.tempo });
    };

    this._changeOffset = e => {
      let offset = parseInt(e.target.value, 10);

      if (isNaN(offset) || offset < 0) {
        offset = 0;
      }

      const { commonBeatsCount, selectedBeat } = this.props;

      if (!selectedBeat) {
        return;
      }

      const max = commonBeatsCount / selectedBeat.beatsCount - 1;
      if (offset > max) {
        offset = max;
      }

      if (offset !== selectedBeat.offset) {
        this.props.onChangeOffset(this.props.selectedBeat.id, offset);
      }

      this.setState({
        offset: offset || e.target.value,
      });
    };

    this._changeBeatsCount = e => {
      this.setState({ beatsCount: e.target.value });

      const { selectedBeat } = this.props;
      let beatsCount = parseInt(e.target.value, 10);

      if (
        !isNaN(beatsCount) &&
        beatsCount > 0 &&
        beatsCount !== selectedBeat.beatsCount
      ) {
        this.props.onChangeBeatsCount(
          this.props.selectedBeat.id,
          beatsCount
        );
      }
    };

    this._changeSound = e => {
      const valueObj = {
        isAuto: false,
      };
      const {
        activeSoundType,
        onChangeSound,
        selectedBeat,
      } = this.props;

      if (activeSoundType === 'oscilator') {
        valueObj.pitch = e.target.value;
      }

      if (activeSoundType === 'kit') {
        valueObj.soundId = e.target.value;
      }

      onChangeSound(selectedBeat.id, {
        type: activeSoundType,
        ...valueObj,
      });
    };

    this._toggleAutoSound = () => {
      const { selectedBeat, onChangeSound } = this.props;
      const isAuto = !selectedBeat.sound.isAuto;

      onChangeSound(selectedBeat.id, {
        ...selectedBeat.sound,
        isAuto,
      });
    };
  }

  _renderTimeControls() {
    const { selectedBeat, commonBeatsCount, width } = this.props;

    const svgWidth = width - 40;

    if (!selectedBeat) {
      return;
    }

    const { beatsCount, mutedBeats = [], offset = 0 } = selectedBeat;

    const every = commonBeatsCount / beatsCount;
    const inc = svgWidth / commonBeatsCount;
    const elements = [];
    const buttons = [];
    let texts = [];

    for (let i = 0; i < commonBeatsCount; i++) {
      let x;

      if (i === 0) {
        x = inc / 2;
      }

      if (i > 0) {
        x = inc / 2 + i * inc;
      }

      const isBeat = (i - offset) % every === 0;
      // const isSubBeat = (
      //   (i - offset) % (every / 2) === 0 ||
      //   (i - offset) % (every / 3) === 0
      // );
      const isSuperBeat = (i - offset) % (every * 2) === 0;

      elements.push(
        <rect
          key={`commonbeat-rect-${i + 1}`}
          x={x}
          y="-4px"
          width="4"
          height="16"
        />
      );

      let showText =
        i + 1 === 1 ||
        isSuperBeat ||
        commonBeatsCount <= 15 ||
        (beatsCount <= 10 && isBeat) ||
        i + 1 === commonBeatsCount;

      if (showText) {
        texts.push(
          <tspan key={`commonbeat-text-${i + 1}`} x={x + 2} y="38">
            {i + 1}
          </tspan>
        );
      }

      if (isBeat) {
        const beatNumber = (i - offset) / every + 1;
        const isMute = mutedBeats.indexOf(beatNumber) !== -1;
        let className = 'SelectedBeatsControls__BeatSliderButton';
        let circleColor = color.baseColorByBeat(selectedBeat);

        if (isMute) {
          className += ' _isMute';
          circleColor = color.muteColor(circleColor);
        }

        const style = {
          left: x + 7,
          backgroundColor: color.toHslString(circleColor),
        };

        const onClick = () => {
          this.props.onMuteBeat(selectedBeat.id, beatNumber);
        };

        buttons.push(
          <button
            key={`beat-slider-${selectedBeat.id}-${beatNumber}`}
            onClick={onClick}
            className={className}
            style={style}
          >
            {beatNumber}
          </button>
        );
      }
    }

    return (
      <div
        className="SelectedBeatControls__TimeControls"
        style={{ width: this.props.width }}
      >
        <div
          style={{ width: this.props.width }}
          className="SelectedBeatsControls__BeatSlider"
        >
          {buttons}
        </div>
        <svg
          width={svgWidth}
          height="40px"
          viewBox={`0 0 ${svgWidth} 40`}
        >
          <g fill="#777">
            <rect x="0" y="12" width={svgWidth} height="4" />
            {elements}
            <text
              fontFamily="Futura, Roboto-Medium, Roboto, sans-serif"
              fontSize="0.8em"
              fontWeight="400"
              textAnchor="middle"
            >
              {texts}
            </text>
          </g>
        </svg>
      </div>
    );
  }

  render() {
    const {
      activeSoundType,
      activeKit,
      selectedBeat,
      commonBeatsCount,
      width,
    } = this.props;

    let className = 'SelectedBeatControls';

    if (!selectedBeat) {
      return <div className={className} />;
    }

    let beatsCountValue = this.state.beatsCount;
    let offsetValue = this.state.offset;
    let tempoValue = this.state.tempo;

    if (beatsCountValue === null) {
      beatsCountValue = selectedBeat.beatsCount;
    }

    if (offsetValue === null) {
      offsetValue = selectedBeat.offset || 0;
    }

    if (tempoValue === null) {
      tempoValue = selectedBeat.tempo;
    }

    if (tempoValue !== '') {
      tempoValue = Math.round(tempoValue * 10) / 10;
    }

    className += ' _isVisible';

    offsetValue = '' + offsetValue;
    tempoValue = '' + tempoValue;
    beatsCountValue = '' + beatsCountValue;

    const sound = selectedBeat.sound;
    let soundSelect;
    let soundLabel;
    let soundSelectClassName = 'SelectedBeatControls__Input';
    let soundAutoButtonClassName = 'SelectedBeatControls__AutoButton';
    let soundAutoTitle = 'Press to choose sound automatically';

    if (sound.isAuto) {
      soundSelectClassName += ' _isDisabled';
      soundAutoButtonClassName += ' _isActive';
      soundAutoTitle = 'Press to choose sound manually';
    }

    const soundAutoButton = (
      <button
        title={soundAutoTitle}
        className={soundAutoButtonClassName}
        onClick={this._toggleAutoSound}
      >
        <span>auto</span>
      </button>
    );

    if (activeSoundType === 'oscilator') {
      soundLabel = 'Pitch';

      const soundOptions = PITCH_LIST.map(pitch => {
        return (
          <option key={`pitch-${pitch}`} value={pitch}>
            {pitch}
          </option>
        );
      });

      soundSelect = (
        <select
          className={soundSelectClassName}
          onChange={this._changeSound}
          disabled={sound.isAuto}
          value={sound.pitch}
        >
          {soundOptions}
        </select>
      );
    }

    if (activeSoundType === 'kit' && activeKit) {
      soundLabel = 'Sound';

      const soundName = activeKit.sounds.reduce((acc, s) => {
        if (acc.length > 0) return acc;
        if (s.id === sound.soundId) return s.name;
        return acc;
      }, '');

      const sounds = sortBy(activeKit.sounds, s => s.name);
      const soundOptions = sounds.map(sound => {
        return (
          <option key={`kit-${sound.id}`} value={sound.id}>
            {sound.name}
          </option>
        );
      });

      soundSelect = (
        <select
          className={soundSelectClassName}
          title={soundName}
          onChange={this._changeSound}
          disabled={sound.isAuto}
          value={sound.soundId}
        >
          {soundOptions}
        </select>
      );
    }

    return (
      <div className={className}>
        {this._renderTimeControls()}

        <div className="SelectedBeatControls__Info" style={{ width }}>
          <div className="SelectedBeatControls__InfoBlock">
            <div className="SelectedBeatControls__InfoBlockLeft">
              Beats
            </div>
            <div className="SelectedBeatControls__InfoBlockRight">
              <input
                className="SelectedBeatControls__Input"
                type="number"
                min={1}
                max={50}
                step={1}
                value={beatsCountValue}
                onKeyUp={this._changeBeatsCount}
                onChange={this._changeBeatsCount}
              />
            </div>
          </div>

          <div className="SelectedBeatControls__InfoBlock">
            <div className="SelectedBeatControls__InfoBlockLeft">
              Tempo
            </div>
            <div className="SelectedBeatControls__InfoBlockRight">
              <input
                className="SelectedBeatControls__Input"
                type="number"
                min={1}
                max={1000}
                step={1}
                value={tempoValue}
                onKeyUp={this._changeTempo}
                onChange={this._changeTempo}
              />
            </div>
          </div>

          <div className="SelectedBeatControls__InfoBlock">
            <div className="SelectedBeatControls__InfoBlockLeft">
              Offset
            </div>
            <div className="SelectedBeatControls__InfoBlockRight">
              <input
                className="SelectedBeatControls__Input"
                type="number"
                step={1}
                min={0}
                max={commonBeatsCount / selectedBeat.beatsCount - 1}
                value={offsetValue}
                onKeyUp={this._changeOffset}
                onChange={this._changeOffset}
              />
            </div>
          </div>

          <div className="SelectedBeatControls__InfoBlock">
            <label className="SelectedBeatControls__InfoBlockLeft">
              {soundLabel}
            </label>
            <div className="SelectedBeatControls__InfoBlockRight">
              {soundSelect}
              {soundAutoButton}
            </div>
          </div>
        </div>

        <div
          className="SelectedBeatControls__Actions"
          style={{ width }}
        >
          <button
            className="SelectedBeatControls__ActionButton"
            onClick={() => this.props.onDeleteBeat(selectedBeat.id)}
          >
            <IconDelete color="#777" width="40%" height="100%" />
          </button>

          <button
            className="SelectedBeatControls__ActionButton"
            onClick={this.props.onDeselectBeat}
          >
            <IconDone color="#777" width="50%" height="100%" />
          </button>
        </div>
      </div>
    );
  }
}

export default SelectedBeatControls;
