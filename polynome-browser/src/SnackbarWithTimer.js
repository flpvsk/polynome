import React, { Component } from 'react';
import Snackbar from './Snackbar';
import ButtonText from './ButtonText';
import TextWrapper from './TextWrapper';

import shouldComponentUpdate from './shouldComponentUpdate';
import theme from './theme';

class SnackbarWithTimer extends Component {
  constructor() {
    super();

    this.state = {
      isVisible: false,
      isDismissed: false,
    };

    this.shouldComponentUpdate = shouldComponentUpdate;

    this._isMounted = false;

    this._loop = () => {
      if (!this._isMounted) {
        return;
      }

      const isVisible =
        !this.state.isDismissed && Date.now() < this.props.showUntil;

      this.setState({ isVisible });
      requestAnimationFrame(this._loop);
    };
  }

  componentDidMount() {
    this._isMounted = true;
    this._loop();
  }

  componentDidUpdate(oldProps) {
    if (
      this.props.message !== oldProps.message ||
      this.props.showUntil !== oldProps.showUntil
    ) {
      this.setState({ isDismissed: false });
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    return (
      <Snackbar isVisible={this.state.isVisible}>
        <div
          style={{ flexGrow: 1 }}
          key={`${alert.alertId}-alert-wrap`}
        >
          <TextWrapper
            key={`${alert.alertId}-alert-text`}
            color={theme.color.onSnackbar}
            styleName={'body2'}
          >
            {this.props.message}
          </TextWrapper>
        </div>,
        <ButtonText
          key={`${alert.alertId}-dismiss`}
          onTouchTap={() => this.setState({ isDismissed: true })}
        >
          hide
        </ButtonText>
      </Snackbar>
    );
  }
}

export default SnackbarWithTimer;
