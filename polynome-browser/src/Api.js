import EventEmitter from 'event-emitter';
import allOff from 'event-emitter/all-off';

const localStorage = window.localStorage;

function createResourceRequest(load, cancel) {
  const emitter = new EventEmitter();
  const progress = percent => {
    emitter.emit('progress', percent);
  };

  const promise = new Promise((resolve, reject) => {
    load(resolve, reject, progress);
  });

  return {
    then: promise.then.bind(promise),
    on: emitter.on.bind(emitter),
    allOff: () => allOff(emitter),
    cancel,
  };
}

const STATIC_KITS = [
  {
    id: 'bluebird',
    name: 'Drum Kit',
    sounds: [
      {
        id: 'bluebird-kit-cowbell',
        name: 'Cowbell',
        url: '/kits/bluebird/cowbell.wav',
      },
      {
        id: 'bluebird-kit-crash-left-stop',
        name: 'Crash Left Stop',
        url: '/kits/bluebird/crashLeftStop.wav',
      },
      {
        id: 'bluebird-kit-crash-left',
        name: 'Crash Left',
        url: '/kits/bluebird/crashLeft.wav',
        period: 3,
      },
      {
        id: 'bluebird-kit-crash-right-stop',
        name: 'Crash Right Stop',
        url: '/kits/bluebird/crashRightStop.wav',
        period: 5,
      },
      {
        id: 'bluebird-kit-crash-right',
        name: 'Crash Right',
        url: '/kits/bluebird/crashRight.wav',
      },
      {
        id: 'bluebird-kit-hand-claps',
        name: 'Hand Claps',
        url: '/kits/bluebird/handClaps.wav',
      },
      {
        id: 'bluebird-kit-hi-hat-foot-close-1',
        name: 'Hi-hat Foot Close 1',
        url: '/kits/bluebird/hiHatFootClose.wav',
      },
      {
        id: 'bluebird-kit-hi-hat-foot-close-2',
        name: 'Hi-hat Foot Close 2',
        url: '/kits/bluebird/hiHatFootClose1.wav',
        period: 0,
      },
      {
        id: 'bluebird-kit-hi-hat-foot-close-3',
        name: 'Hi-hat Foot Close 3',
        url: '/kits/bluebird/hiHatFootClose2.wav',
      },
      {
        id: 'bluebird-kit-hi-tom-1',
        name: 'Hi Tom',
        url: '/kits/bluebird/hiTom.wav',
      },
      {
        id: 'bluebird-kit-hi-tom-2',
        name: 'Hi Tom 2',
        url: '/kits/bluebird/hiTom2.wav',
      },
      {
        id: 'bluebird-kit-kick',
        name: 'Kick',
        url: '/kits/bluebird/kick.wav',
        period: 1,
      },
      {
        id: 'bluebird-kit-low-tom',
        name: 'Low Tom',
        url: '/kits/bluebird/lowTom.wav',
      },
      {
        id: 'bluebird-kit-mid-tom-1',
        name: 'Mid Tom 1',
        url: '/kits/bluebird/midTom1.wav',
      },
      {
        id: 'bluebird-kit-mid-tom-2',
        name: 'Mid Tom 2',
        url: '/kits/bluebird/midTom-2.wav',
      },
      {
        id: 'bluebird-kit-mid-tom-3',
        name: 'Mid Tom 3',
        url: '/kits/bluebird/midTom-3.wav',
      },
      {
        id: 'bluebird-kit-ride-bell',
        name: 'Ride Bell',
        url: '/kits/bluebird/rideBell.wav',
      },
      {
        id: 'bluebird-kit-ride-edge',
        name: 'Ride Edge',
        url: '/kits/bluebird/rideEdge.wav',
      },
      {
        id: 'bluebird-kit-ride-in',
        name: 'Ride In',
        url: '/kits/bluebird/rideIn.wav',
      },
      {
        id: 'bluebird-kit-ride-out',
        name: 'Ride Out',
        url: '/kits/bluebird/rideOut.wav',
      },
      {
        id: 'bluebird-kit-shaker',
        name: 'Shaker',
        url: '/kits/bluebird/shaker.wav',
        period: 4,
      },
      {
        id: 'bluebird-kit-snare-center',
        name: 'Snare Center',
        url: '/kits/bluebird/snareCenter.wav',
        period: 2,
      },
      {
        id: 'bluebird-kit-snare-edge',
        name: 'Snare Edge',
        url: '/kits/bluebird/snareEdge.wav',
      },
      {
        id: 'bluebird-kit-snare-rimshot-edge',
        name: 'Snare Rimshot Edge',
        url: '/kits/bluebird/snareRimshotEdge.wav',
      },
      {
        id: 'bluebird-kit-snare-rimshot',
        name: 'Snare Rimshot',
        url: '/kits/bluebird/snareRimshot.wav',
      },
      {
        id: 'bluebird-kit-snare-sidestick',
        name: 'Snare Sidestick',
        url: '/kits/bluebird/snareSidestick.wav',
      },
      {
        id: 'bluebird-kit-tambourine',
        name: 'Tambourine',
        url: '/kits/bluebird/tambourine.wav',
      },
    ]
      .map(s => {
        if (s.period === undefined) {
          return {
            ...s,
            period: 1000,
          };
        }
        return s;
      })
      .sort((a1, a2) => a1.period - a2.period),
  },
  {
    id: 'test-kit',
    name: 'Electronic Drum Kit',
    sounds: [
      {
        id: 'test-kit-hat',
        name: 'Hat',
        url: '/kits/testKit/hat.wav',
      },
      {
        id: 'test-kit-kick',
        name: 'Kick',
        url: '/kits/testKit/kick.wav',
      },
      {
        id: 'test-kit-snare',
        name: 'Snare',
        url: '/kits/testKit/snare.wav',
      },
      {
        id: 'test-kit-shaker',
        name: 'Shaker',
        url: '/kits/testKit/shaker.wav',
      },
      {
        id: 'test-kit-crash',
        name: 'Crash',
        url: '/kits/testKit/crash.wav',
      },
    ],
  },
];

const Api = {
  listKits: () => {
    return new Promise((resolve, reject) => {
      resolve(STATIC_KITS);
    });
  },

  loadKit: id => {
    return new Promise((resolve, reject) => {
      const kit = STATIC_KITS.reduce((acc, kit) => {
        if (kit.id === id) {
          return kit;
        }

        return acc;
      }, undefined);

      if (!kit) {
        reject(new Error(`No kit with id '${id}'`));
      }

      const fetchCalls = kit.sounds.map(sound => {
        return fetch(
          `${process.env.PUBLIC_URL}` +
            `${process.env.REACT_APP_POLYNOME_API_PATH}` +
            `${sound.url}`
        )
          .then(response => {
            return response.arrayBuffer();
          })
          .then(rawData => {
            return {
              ...sound,
              rawData,
            };
          });
      });

      return Promise.all(fetchCalls)
        .then(result => resolve(result))
        .catch(err => reject(err));
    });
  },

  loadSoundsByIds: ids => {
    let cancelled = false;
    const cancel = () => {
      cancelled = true;
    };

    const load = (resolve, reject, progress) => {
      if (cancelled) {
        reject();
        return;
      }

      const sounds = [];
      for (let id of ids) {
        let sound;
        for (let kit of STATIC_KITS) {
          for (let kitSound of kit.sounds) {
            if (kitSound.id === id) {
              sound = kitSound;
            }
          }
        }

        if (!sound) {
          reject(new Error(`Sound not found: ${id}`));
          return;
        }

        sounds.push(sound);
      }

      let loaded = 0;
      Promise.all(
        sounds.map(sound => {
          return fetch(
            `${process.env.PUBLIC_URL}` +
              `${process.env.REACT_APP_POLYNOME_API_PATH}` +
              `${sound.url}`
          )
            .then(response => {
              return response.arrayBuffer();
            })
            .then(rawData => {
              if (cancelled) {
                return;
              }

              progress(++loaded / sounds.length);
              return {
                ...sound,
                rawData,
              };
            });
        })
      )
        .then(sounds => {
          if (cancelled) {
            reject();
            return;
          }

          resolve(sounds);
        })
        .catch(reject);
    };

    return createResourceRequest(load, cancel);
  },

  listSavedBeats: () => {
    return new Promise((resolve, reject) => {
      const res = [];
      for (let i = 0; i < localStorage.length; i++) {
        const key = localStorage.key(i);

        if (key.indexOf('beats/') === 0) {
          res.push(JSON.parse(localStorage.getItem(key)));
        }
      }

      resolve(res.sort((b1, b2) => b1.createdAt - b2.createdAt));
    });
  },

  saveBeat: beatInfo => {
    return new Promise((resolve, reject) => {
      const beatStr = JSON.stringify(beatInfo);
      localStorage.setItem(`beats/${beatInfo.id}`, beatStr);
      resolve();
    });
  },

  removeBeat: id => {
    return new Promise((resolve, reject) => {
      localStorage.removeItem(`beats/${id}`);
      resolve();
    });
  },

  getLastPlayedBeat: () => {
    return new Promise((resolve, reject) => {
      const lastPlayedStr = localStorage.getItem('lastPlayedBeat');

      if (lastPlayedStr) {
        resolve(JSON.parse(lastPlayedStr));
      }

      resolve(null);
    });
  },

  saveLastPlayedBeat: beat => {
    return new Promise((resolve, reject) => {
      const lastPlayedStr = JSON.stringify(beat);
      localStorage.setItem('lastPlayedBeat', lastPlayedStr);
      resolve();
    });
  },

  track: event => {
    try {
      if (!window.gtag) {
        return;
      }

      window.gtag('event', event.action, {
        event_category: event.category,
        event_label: event.label,
        event_value: event.value,
      });
    } catch (e) {
      console.warn('Error tracking event', event, e);
    }
  },
};

export default Api;
