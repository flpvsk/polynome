import React from 'react';
import theme from './theme';
import { styles as textStyles } from './TextWrapper';

const styles = {
  block: {
    height: 40,
    background: 'none',
    appearance: 'none',
    border: 'none',
    backgroundColor: theme.color.containerOnSurface,
    borderBottom: `1px solid ${theme.color.outline}`,
    borderTopLeftRadius: theme.shape.borderRadius,
    borderTopRightRadius: theme.shape.borderRadius,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    color: theme.color.onSurface,
    ...textStyles.subtitle1,
  },

  __option: {
    backgroundColor: theme.color.surface,
    color: theme.color.onSurface,
  },

  _isDisabled: {
    color: theme.color.onSurfaceDisabled,
  },
};

class DropdownListItem extends React.Component {
  constructor() {
    super();
    this._onChange = e => {
      this.props.onChange(e.target.value);
    };
  }

  render() {
    const options = this.props.options.map(o => {
      return (
        <option style={styles.__option} key={o.key} value={o.value}>
          {o.text}
        </option>
      );
    });

    let blockStyle = styles.block;

    if (this.props.isDisabled) {
      blockStyle = {
        ...blockStyle,
        ...styles._isDisabled,
      };
    }

    return (
      <select
        style={blockStyle}
        onChange={this._onChange}
        onClick={e => e.stopPropagation()}
        value={this.props.value}
        disabled={this.props.isDisabled}
      >
        {options}
      </select>
    );
  }
}

export default DropdownListItem;
