# Polychops

Deliberate practice for musicians.

## Project dependencies

* [Node](https://nodejs.org/) `>= v10.3.0`
* [MongoDB](https://www.mongodb.com/download-center?jmp=nav#community) `>= v3.4.3`
* [Sound packs (aka kits)](https://drive.google.com/drive/folders/1lIW7Mz_6oxjzP3KeLOooHAf3ium7J9ct?usp=sharing)


## Running Polychops

#### 1. [Start MongoDB](https://docs.mongodb.com/manual/administration/install-community/)


#### 2. Make sure `.env` files are in place

Make sure `.env` files exist in both `polynome-browser` and
`polynome-server` and all the required variables are set.

An easy way to get started is to copy `env.example`:

```sh
# Run in the root of the project
cp polynome-browser/env.example polynome-browser/.env
cp polynome-server/env.example polynome-server/.env
```

And set the `POLYNOME_KITS_DIR` in `polynome-server` to the directory
where you downloaded the [sound packs](https://drive.google.com/drive/folders/1lIW7Mz_6oxjzP3KeLOooHAf3ium7J9ct?usp=sharing)

<details>
<summary>*polynome-browser environment varialbes*</summary>

`REACT_APP_STRIPE_API_KEY`<br/>
Example value: `"pk_test_tTzJRCpGJolwPfISseVaffgh"`<br/>
Public Stripe API key, required for payments.

`REACT_APP_GOOGLE_MAPS_API_KEY`<br/>
Example value: `"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"`<br/>
Google Maps API key. Geocoding API is used on the
Checkout screen.

`REACT_APP_POLYNOME_API_PATH`<br/>
Example value: `"/api"`<br/>
URL path to API. API is used for files like userpics, recordings, kits.

</details>

<details>
<summary>*polynome-server environment varialbes*</summary>

`POLYNOME_SERVER_URL`<br />
Example value: `"http://localhost:3000"`<br/>
Public server URL.

`POLYNOME_API_PATH`<br />
Example value: `"/api"`<br />
REST API path.

`POLYNOME_RECORDINGS_DIR`<br />
Example value: `"~/tmp/recordings/"`<br/>
Path to recordings folder.<br />

`POLYNOME_KITS_DIR`<br />
Example value: `"~/tmp/kits"`<br/>
Path to kits folder. **Set this value** <br />

`POLYNOME_USERPICS_DIR`<br/>
Example value: `"/Users/filipovskii/tmp/userpics/"`<br/>
Path to userpics folder.<br/>

`POLYNOME_WELCOMING_PERSON_ID`<br />
Example value: `"bIQJ9krpOBcvbIWU4vk~_"`<br/>
Person id to send "Welcome to Polychops" message
from.<br />

`POLYNOME_STRIPE_API_KEY`<br />
Example value: `"rk_test_aaaaaaaaaaaaaaaaaaaaaaaa"`<br/>
Secret Stripe api key, required for payments.

</details>


#### 3. Start the server and the client

```sh
# Starting the server
# Run in the root of the project

cd polynome-server
npm install
npm start
```

```sh
# Strating the browser app
# Run in the root of the project

cd polynome-browser
npm install
npm start
```

## Commands

#### Creating a discount

1. Create a coupon in [Stripe](https://dashboard.stripe.com/coupons);
2. Create a discount entry in the app's database:

    ```sh
    cd polynome-server
    npm run create-discount -- \
      -s stripe-coupon-id \
      -d "Discount description" \
      -c discount-code
    ```

