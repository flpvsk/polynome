const SECONDS_IN_ONE_MINUTE = 60;

const DELIMITER_WIDTH = 4;
const DELIMITER_COLOR = '#676767';
const TRACKER_COLOR = '#E3E1B9';
const CENTER_COLOR = TRACKER_COLOR;
const BACKGROUND_COLOR = '#2C2E2C';
const ACCENT_COLORS = [
  '#484A45',
  '#637877',
  '#EBC95C',
  '#BAC0A2',
  '#5E757F',
  /*
  '#B5873C',
  '#3A52AD',
  '#B76533',
  '#3F2F40',
  '#323F45',
  '#3A52AD',
  */
];


const TMP_TEMPO = 540;
const TMP_BEATS_1 = 5;
const TMP_BEATS_2 = 7;



function findAllNumbersInRangeDivisibleBy(divisor, from, to) {
  if (from === 0 && to === 0) {
    return [0];
  }

  const result = [];

  let first;
  if (from === 0) {
    first = 0;
  }

  if (from !== 0) {
    let multiplier = Math.ceil(from / divisor);
    let candidate = divisor * multiplier;

    if (candidate > to) {
      return [];
    }

    first = candidate;
  }

  let current = first;

  // Change to `<=` if inclusive is needed
  while (current < to) {
    result.push(current);
    current += divisor;
  }

  return result;
}


// to, from and start are in seconds
function polytime(config, from, to) {
  if (!config.beats || !config.beats.length) {
    return [];
  }

  if (!config.tempo) {
    return [];
  }

  const {tempo, beats} = config;
  const start = config.start || 0;
  from = from || start;
  to = to || undefined;

  const interval = SECONDS_IN_ONE_MINUTE / tempo;
  const baseBeats = findAllNumbersInRangeDivisibleBy(
    interval,
    from - start,
    to - start
  );
  const baseBeatsFromStart = baseBeats.map(b => b + start);

  return baseBeatsFromStart.reduce((acc, time, index) => {
    const beatsIds = beats.reduce((ids, beat) => {
      const offset = beat.offset || 0;
      const every = beat.every || 1;
      const shouldSound = (index + offset) % every === 0;

      if (shouldSound) {
        ids.push(beat.id);
      }

      return ids;
    }, []);

    if (beatsIds.length > 0) {
      acc.push({
        time,
        beatsIds
      });
    }

    return acc;
  }, []);
}

function startAudio() {
  let context = new AudioContext();
  let currentTime = context.currentTime;
  let osc1 = context.createOscillator();
  let osc2 = context.createOscillator();
  let gain1 = context.createGain();
  let gain2 = context.createGain();

  osc1.type = 'triangle';
  osc1.frequency.setValueAtTime(440, currentTime);
  osc1.connect(gain1);

  osc2.type = 'triangle';
  osc2.frequency.setValueAtTime(261.63, currentTime);
  osc2.connect(gain2);

  gain1.gain.setValueAtTime(0, 0);
  gain1.connect(context.destination);

  gain2.gain.setValueAtTime(0, 0);
  gain2.connect(context.destination);

  osc1.start();
  osc2.start();

  const tempo = TMP_TEMPO;
  const config = {
    start: currentTime,
    tempo,
    beats: [{
      id: 'a', // two
      every: TMP_BEATS_1,
    }, {
      id: 'b', // triplet
      every: TMP_BEATS_2,
    }]
  };

  const beatTimes = polytime(config, currentTime, currentTime + 10);

  for (t of beatTimes) {
    const {time} = t;
    for (beatId of t.beatsIds) {
      if (beatId === 'a') {
        gain1.gain.setValueAtTime(0.5, time);
        gain1.gain.setValueAtTime(0, time + 0.05);
      }

      if (beatId === 'b') {
        gain2.gain.setValueAtTime(0.5, time);
        gain2.gain.setValueAtTime(0, time + 0.05);
      }
    }
  }

  // TODO: not always the case, e.g. 2 and 4
  const beatsInCycle = TMP_BEATS_1 * TMP_BEATS_2;

  return {
    context,
    cycleDuration: beatsInCycle * (SECONDS_IN_ONE_MINUTE / tempo),
    startTime: currentTime,
    beatTimes
  };
};


function drawArc(context, {
  centerX,
  centerY,
  radius,
  startAngle,
  endAngle,
  width,
  color
}) {
  context.beginPath();
  context.arc(centerX, centerY, radius, startAngle, endAngle, false);
  context.lineWidth = width;
  context.strokeStyle = color;
  context.stroke();
}


function pointOnCirlce({
  centerX,
  centerY,
  radius,
  angle
}) {
  return {
    x: centerX + Math.cos(angle) * radius,
    y: centerY + Math.sin(angle) * radius
  };
}

function findPerpendicularWithWidth(point1, point2, width) {
  const slope1 = (point1.y - point2.y) / (point1.x - point2.x);
  const slope2 = - 1 / slope1;

  // slope2 = (point3.y - point2.y) / (point3.x - point2.x) =>
  //   (point3.y - point2.y) = slope2 * (point3.x - point2.x)
  // width ^ 2 = (point3.y - point2.y) ^ 2 + (point3.x - point2.x) ^ 2 =>
  //   width ^ 2 = (slope2 ^ 2) * (point3.x - point2.x) ^ 2 +
  //     (point3.x - point2.x) ^ 2 =>
  //   (point3.x - point2.x) ^ 2 = (width ^ 2) / ((slope2 ^ 2) + 1) =>
  //
  //   (point3.x - point2.x) = ((width ^ 2) / ((slope2 ^ 2) + 1)) ^ (-2),
  //   (point4.x - point2.x) = -((width ^ 2) / ((slope2 ^ 2) + 1)) ^ (-2)


  const point3 = {};
  const point4 = {};

  point3.x = point2.x + ((width ^ 2) / ((slope2 ^ 2) + 1)) ^ (-2);
  point4.x = point2.x - ((width ^ 2) / ((slope2 ^ 2) + 1)) ^ (-2);

  point3.y = slope2 * (point3.x - point2.x) + point2.y;
  point4.y = slope2 * (point4.x - point2.x) + point2.y;

  return [point3, point4];
}

function findAnglesForWidth({
  centerX,
  centerY,
  baseAngle,
  radius,
  width
}) {
  const alpha = Math.acos(width / (2 * radius));
  const beta = Math.PI - 2 * alpha;
  return [
    baseAngle - beta / 2,
    baseAngle + beta / 2
  ];
}


function drawDelimiter(context, {
  centerX,
  centerY,
  innerRadius,
  outerRadius,
  angle,
  width,
  color
}) {
  context.beginPath();

  const [startInner, endInner] = findAnglesForWidth({
    centerX,
    centerY,
    baseAngle: angle,
    radius: innerRadius,
    width
  });

  const [startOuter, endOuter] = findAnglesForWidth({
    centerX,
    centerY,
    baseAngle: angle,
    radius: outerRadius,
    width
  });

  context.arc(
    centerX,
    centerY,
    innerRadius,
    startInner,
    endInner,
    false
  );

  context.arc(
    centerX,
    centerY,
    outerRadius,
    endOuter,
    startOuter,
    true
  );
  context.fillStyle = color;
  context.fill();
}


function drawCircle(context, {
  centerX,
  centerY,
  radius,
  width,
  parts,
  offset = 0
}) {
  const angleStep = Math.PI * 2 / parts;
  const colors = ACCENT_COLORS;

  const partAngles = [];
  for (let part = 0; part < parts; part++) {
    const startAngle = offset + part * angleStep - Math.PI / 2;
    const endAngle = offset + (part + 1) * angleStep - Math.PI / 2;
    partAngles.push(startAngle);
    drawArc(context, {
      centerX,
      centerY,
      radius,
      startAngle,
      endAngle,
      width,
      color: colors[parts % colors.length]
    });
  }

  partAngles.forEach((startAngle) => {
    drawDelimiter(context, {
      centerX,
      centerY,
      angle: startAngle,
      outerRadius: radius + width / 2,
      innerRadius: radius - width / 2,
      width: DELIMITER_WIDTH,
      color: DELIMITER_COLOR
    });
  });
}


function drawTracker(context, {
  centerX,
  centerY,
  innerRadius,
  outerRadius,
  angle,
  width
}) {
  drawDelimiter(context, {
    centerX,
    centerY,
    innerRadius,
    outerRadius,
    width,
    angle,
    color: TRACKER_COLOR
  });
}

(function() {
  const canvas = window.canvas;
  const context = canvas.getContext('2d');
  const centerX = canvas.width / 2;
  const centerY = canvas.height / 2;

  const audio = startAudio();

  const startTime = audio.startTime;
  const cycleDuration = audio.cycleDuration;
  const beatTimes = audio.beatTimes;
  const startPosition = -Math.PI / 2;

  function draw() {
    const currentTime = audio.context.currentTime;
    const cyclePositionAbsolute = (
      (currentTime - startTime) /
      cycleDuration
    );
    const cyclePositionPercent = (
      cyclePositionAbsolute - Math.trunc(cyclePositionAbsolute)
    );
    const cyclePosition = (
      startPosition + 2 * Math.PI * cyclePositionPercent
    );

    context.fillStyle = BACKGROUND_COLOR;
    context.fillRect(0, 0, 400, 400);

    const commonCircle = {
      centerX,
      centerY
    };

    const smallRadius = 60;
    const smallWidth = 40;
    const largeRadius = 100;
    const largeWidth = 60;


    // drawing center circle
    context.beginPath();
    context.arc(
      centerX,
      centerY,
      smallRadius,
      0,
      2 * Math.PI,
      false
    );
    context.lineWidth = 0;
    context.fillStyle = tinycolor(CENTER_COLOR)
      .setAlpha(0.4)
      .toRgbString();
    context.fill();

    drawCircle(context, {
      ...commonCircle,
      radius: largeRadius,
      width: largeWidth,
      parts: TMP_BEATS_2,
    });

    drawCircle(context, {
      ...commonCircle,
      radius: smallRadius,
      width: smallWidth,
      parts: TMP_BEATS_1,
    });

    drawTracker(context, {
      ...commonCircle,
      innerRadius: smallRadius - smallWidth / 2,
      outerRadius: largeRadius + largeWidth / 2,
      width: 4,
      angle: cyclePosition
    });

    /*
    drawTracker(context, {
      ...commonCircle,
      radius: 150,
      width: 40,
      position: trackerPosition
    });
    */

    window.requestAnimationFrame(draw);
  }

  draw();
})();
