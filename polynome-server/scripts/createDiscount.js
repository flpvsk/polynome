const args = require('command-line-args');
const usage = require('command-line-usage');
const storage = require(`${__dirname}/../src/storage`);

const optionsDefs = [
  {
    name: 'stripe',
    alias: 's',
    type: String,
    multiple: false,
    typeLabel: '{underline coupon-id}',
    description: `Stripe coupon id`,
  },

  {
    name: 'description',
    alias: 'd',
    type: String,
    multiple: false,
    description: `Discount description`,
  },

  {
    name: 'code',
    alias: 'c',
    type: String,
    multiple: false,
    description: `Discount code`,
  },

  {
    name: 'help',
    type: Boolean,
  },
];

const usageSections = [
  {
    header: 'Creates a Polychops discount',
    content: (
      `Creates a discount in the current database.\n` +
      `Requires an active Stripe coupon ` +
      `https://dashboard.stripe.com/test/coupons`
    ),
  },
  {
    header: 'Options',
    optionList: optionsDefs,
  }
];

const options = args(optionsDefs);
const usageStr = usage(usageSections);

if (options.help) {
  console.log(usageStr);
  process.exit(0);
}

const run = async () => {
  try {
    const discount = await storage.createDiscount({
      stripeCouponId: options.stripe,
      discountCode: options.code,
      description: options.description,
    });

    console.log(
      `Discount created:\n${JSON.stringify(discount, null, 2)}`
    );
    process.exit(0);
  } catch (e) {
    console.error(`Error creating discount`, e);
    process.exit(1);
  }
};


run();

