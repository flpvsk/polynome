const fs = require('fs-extra');
const path = require('path');
require('dotenv').config()

const { ApolloServer } = require('apollo-server');
const { registerServer } = require('apollo-server-express');
const express = require('express');

const { typeDefs, resolvers } = require('./src/graphql');
const auth = require('./src/auth');

const configureRecordingsRoute = require('./src/recordings');
const configureKitsRoute = require('./src/kits');
const configurePeopleRoute = require('./src/people');

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: async ({ req }) => {
    const token = req.headers.authorization || '';
    const personId = await auth.checkToken({ token, });
    return { personId, token, };
  },
});

const app = express();

configureRecordingsRoute({
  app,
  rootPath: `${process.env.POLYNOME_API_PATH}/recordings`
});

configurePeopleRoute({
  app,
  rootPath: `${process.env.POLYNOME_API_PATH}/people`
});

configureKitsRoute({
  app,
  rootPath: `${process.env.POLYNOME_API_PATH}/kits`,
});

registerServer({ server, app });

const listenOn = process.env.POLYNOME_SOCKET || 4000;

(async function main() {
  let listenOn = 4000;

  if (process.env.POLYNOME_SOCKET) {
    listenOn = process.env.POLYNOME_SOCKET;
    await fs.ensureDir(path.dirname(listenOn));

    try {
      await fs.unlink(listenOn);
      console.log('Previous socket file removed', listenOn);
    } catch (e) {
    }
  }

  app.listen(listenOn, (data) => {
    console.log(`🚀  Server ready at ${listenOn}`);
  });

})();

