const PolyError = require('./PolyError');
const storage = require('./storage');
const messaging = require('./messaging');

let migrationInProgress = new Map();

const TIME_20_MINUTES = (
  1000 * 60 * 20
);


function createLoginLink({
  account,
  token,
}) {
  return (
    `${process.env.POLYNOME_SERVER_URL}` +
    `${process.env.POLYNOME_API_PATH}` +
    `/people/${account.personId}/loginWithLink` +
    `/${token.token}`
  );
}

function isTokenObjectValid(tokenObj) {
  if (!tokenObj) {
    return false;
  }

  const isExpired = (
    tokenObj.expiresAt &&
    tokenObj.expiresAt < Date.now()
  );

  if (isExpired) {
    return false;
  }

  const isRevoked = (
    tokenObj.revokedAt &&
    tokenObj.revokedAt > 0
  );

  if (isRevoked) {
    return false;
  }

  return true;
}


async function migrate({ personId }) {
  const person = await storage.getPersonById({ personId });

  if (!person) {
    return null;
  }

  const hasTokens = await storage.hasAuthTokens({ personId });

  if (hasTokens) {
    return null;
  }

  return storage.createAuthToken({ personId, expiresAt: null });
};


const auth = {

  async getValidToken({ token }) {
    const tokenObj = await storage.getAuthTokenByValue({ token });

    if (!tokenObj) {
      return null;
    }

    if (!isTokenObjectValid(tokenObj)) {
      return null;
    }

    return tokenObj;
  },

  async checkToken({ token }) {
    const tokenObj = await auth.getValidToken({ token });

    if (!tokenObj) {
      return null;
    }

    return tokenObj.personId;
  },

  async maybeMigrateDeprecated({ personId }) {
    // make sure we don't migrate several times
    let migration = migrationInProgress.get(personId);
    if (migration) {
      return migration;
    }

    migration = migrate({ personId });
    migrationInProgress.set(personId, migration);

    migration.then(() => migrationInProgress.delete(personId));

    return migration;
  },

  async login({ email, password }) {
    const account = await storage.getAccountByCredentials({
      email, password,
    });

    if (!account) {
      throw new PolyError(
        `There's no account with this email and password.`
      );
    }

    return storage.createAuthToken({
      personId: account.personId,
      expiresAt: null,
    });
  },

  async logout({ token }) {
    const tokenObj = await storage.getAuthTokenByValue({ token });

    if (!isTokenObjectValid(tokenObj)) {
      return false;
    }

    return storage.revokeAuthToken({ token });
  },

  async sendLoginLink({ email }) {
    const account = await storage.getAccountByEmail(email);

    if (!account) {
      throw new PolyError('Account ${email} not found');
    }

    const personId = account.personId;
    const person = await storage.getPersonById({ personId });

    const token = await storage.createLoginToken({
      personId,
      expiresAt: Date.now() + TIME_20_MINUTES,
    });

    const link = createLoginLink({
      account,
      token,
    });

    await messaging.sendLoginLink({
      account,
      person,
      link,
    });

    return true;
  },

  async loginToAuthToken({ personId, token }) {
    const tokenObj = await storage.getLoginTokenByPersonId(personId);

    if (!tokenObj) {
      return null;
    }

    if (!isTokenObjectValid(tokenObj)) {
      return null;
    }

    await storage.revokeLoginToken({ token });
    return storage.createAuthToken({ personId, expiresAt: null });
  },
};

module.exports = auth;

