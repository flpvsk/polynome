const mailgun = require('mailgun-js')({
  apiKey: process.env.POLYNOME_MAILGUN_API_KEY,
  domain: 'mailgun.polychops.com',
});
const PolyError = require('./PolyError');

async function sendLoginLink({
  account,
  person,
  link,
}) {
  const { email } = account;

  if (!email) {
    throw new PolyError(`Account doesn't have an email set`);
  }

  const message = {
    from: 'Poly the robot <poly@polychops.com>',
    to: email,
    subject: 'Sign in into Polychops using this link',
    text: (
      `Hi there,\n\n` +
      `Please use this link to sign in into your Polychops account:\n\n` +
      `${link}\n\n\n` +
      `With ❤️\n` +
      `Poly the Robot`
    ),
    html: (
      `<p>Hi there,</p>` +
      `<p>Please use ` +
      `<a href="${link}">this link</a> `+
      `to sign in into your Polychops account.</p>` +
      `<p>With ❤️<br/>` +
      `Poly the robot`
    ),
  };

  return mailgun.messages().send(message);
}

module.exports = {
  sendLoginLink,
};
