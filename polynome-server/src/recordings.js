const path = require('path');
const fs = require('fs-extra');
const multer = require('multer');
const sanitizeFilename = require('sanitize-filename');

const upload = multer();

const SERVER_URL = process.env.POLYNOME_SERVER_URL;
const RECORDINGS_ROOT = process.env.POLYNOME_RECORDINGS_DIR || 'recordings';

function configure({app, rootPath}) {
  app.put(
    `${rootPath}/:fileName`,
    upload.single('recording'),
    async (req, res) => {
      const fileName = (
        sanitizeFilename(req.params.fileName)
        .replace(/^\.*/g, '')
      );


      try {
        await fs.ensureDir(RECORDINGS_ROOT);

        await fs.writeFile(
          path.join(RECORDINGS_ROOT, fileName),
          req.file.buffer
        );

        res.json({
          data: {
            ok: true,
            url: `${SERVER_URL}${rootPath}/${fileName}`
          }
        });
      } catch (e) {
        res.status(500);

        res.json({
          errors: [{
            message: e.message
          }]
        });
      }

      res.end()
    });

  app.get(`${rootPath}/:fileName`, (req, res, next) => {
    const fileName = (
      sanitizeFilename(req.params.fileName)
      .replace(/^\.*/g, '')
    );

    const opts = {
      dotfiles: 'deny',
      root: RECORDINGS_ROOT,
    };

    res.sendFile(fileName, opts, err => {
      if (err) {
        next(err);
      }
    });
  });
}

module.exports = configure;
