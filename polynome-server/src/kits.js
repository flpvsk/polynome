const path = require('path');
const fs = require('fs-extra');
const sanitizeFilename = require('sanitize-filename');

const SERVER_URL = process.env.POLYNOME_SERVER_URL;
const KITS_ROOT = process.env.POLYNOME_KITS_DIR || 'kits';

function configure({app, rootPath}) {
  const rootRe = new RegExp(`^${rootPath}`);
  app.get(`${rootPath}/*`, (req, res, next) => {
    const filePath = req.path.replace(rootPath, '');

    const opts = {
      dotfiles: 'deny',
      root: KITS_ROOT,
    };


    res.sendFile(filePath, opts, err => {
      if (err) {
        next(err);
      }
    });
  });
};


module.exports = configure;
