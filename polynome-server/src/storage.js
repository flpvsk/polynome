const PolyError = require('./PolyError');
const { MongoClient, Binary, ObjectID } = require('mongodb');
const omit = require('lodash/omit');
const pick = require('lodash/pick');
const BSON = require('bson');
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const util = require('util');

const cryptoRandomBytes = util.promisify(crypto.randomBytes);

const AGGREGATE_NOTIFICATIONS_JOB = 'aggregateNotifications';
const WELCOMING_PERSON_ID = process.env.POLYNOME_WELCOMING_PERSON_ID;
const DB_URL = process.env.POLYNOME_DB_URL;
const SALT_ROUNDS = 10;
const AUTH_TOKEN_LENGTH = 48;

let _db;
async function getDb() {
  if (_db) {
    return _db;
  }

  const url = DB_URL || 'mongodb://localhost:27017/polychops';
  const client = await MongoClient.connect(url);
  let db = client.db();

  await db.collection('people').createIndex(
    { personId: 1 },
    { unique: true }
  );

  await db.collection('accounts').createIndex(
    { personId: 1 },
  );

  await db.collection('accounts').createIndex(
    { emailLowerCase: 1 },
  );

  await db.collection('chops').createIndex(
    { chopId: 1 },
    { unique: true }
  );

  await db.collection('chops').createIndex(
    { createdAt: -1 }
  );

  await db.collection('chops').createIndex(
    { composerId: 1 }
  );

  await db.collection('comments').createIndex(
    { commentId: 1 },
    { unique: true }
  );

  await db.collection('comments').createIndex(
    { personId: 1 },
  );

  await db.collection('comments').createIndex(
    { chopId: 1 },
  );

  await db.collection('claps').createIndex(
    { chopId: 1 },
  );

  await db.collection('claps').createIndex(
    { chopId: 1, personId: 1 },
  );

  await db.collection('tokens').createIndex(
    { token: 1 },
    { unique: true },
  );

  await db.collection('discounts').createIndex(
    { discountCode: 1 },
    { unique: true },
  );

  await db.collection('plans').createIndex(
    { stripePlanId: 1 },
    { unique: true }
  );
  _db = db;
  return _db;
}


async function findAccount({ personId }) {
  const db = await getDb();
  return db
    .collection('accounts')
    .findOne(
      { personId: toBsonUid(personId) },
      { sort: [['_id', -1]] }
    );
}

async function findAccountByEmail(email) {
  email = email.trim().toLowerCase();

  const db = await getDb();
  let account = await db.collection('accounts')
    .findOne(
      { emailLowerCase: email, },
      { sort: [['_id', -1]] }
    );

  if (!account) {
    return null;
  }

  let personAccount = await findAccount({
    personId: account.personId.toString(),
  });

  if (personAccount._id.toString() !== account._id.toString()) {
    console.warn(
      `Email was changed or removed for account. ` +
      `personId: ${account.personId} email: ${email}`
    );
    return null;
  }

  return account;
}


function toBsonUid(str) {
  return Binary(str, BSON.BSON_BINARY_SUBTYPE_UUID);
}


function toBeat(doc) {
  return {
    ...doc,
    mutedBeats: doc.mutedBeats || [],
  };
}

function toChop(doc) {
  if (!doc) {
    return doc;
  }

  doc = omit(doc, '_id', 'isRemoved');

  const beats = doc.beats || [];
  return {
    ...doc,
    composedBy: doc.composedBy.toString(),
    chopId: doc.chopId.toString(),
    beats: beats.map(toBeat),
  };
}


function toComment(doc) {
  if (!doc) {
    return doc;
  }

  doc = omit(doc, '_id');
  return {
    ...doc,
    commentId: doc.commentId.toString(),
    personId: doc.personId.toString(),
    chopId: doc.chopId.toString(),
  };
}


function toSubscription(doc) {
  if (!doc) {
    return doc;
  }


  const subscriptionId = doc._id.toString();
  doc = omit(doc, '_id');

  return {
    ...doc,
    subscriptionId,
    personId: doc.personId.toString(),
  };
}


function toPerson(doc) {
  if (!doc) {
    return doc;
  }

  doc = omit(doc, '_id', 'password');
  return {
    ...doc,
    personId: doc.personId.toString(),
  };
}


function toAccount(doc) {
  if (!doc) {
    return null;
  }

  const hasPassword = !!doc.password;

  const accountId = (
    doc._id ?
      doc._id.toString() :
      'acc_' + doc.personId.toString()
  );
  doc = omit(doc, '_id', 'password');

  return {
    ...doc,
    accountId,
    personId: doc.personId.toString(),
    hasPassword,
  };
}


function toToken(doc) {
  if (!doc) {
    return doc;
  }

  doc = omit(doc, '_id');
  return {
    ...doc,
    personId: doc.personId.toString(),
  };
}


function toNotificationGroup(doc) {
  if (!doc) {
    return doc;
  }

  const notificationGroupId = doc._id.toString();
  doc = omit(doc, '_id');

  return {
    ...doc,
    notificationGroupId,
    chopId: doc.chopId.toString(),
    notifications: doc.notifications.map(toNotification),
  };
}

function toNotification(doc) {
  if (!doc) {
    return doc;
  }

  const notificationId = doc._id.toString();
  doc = omit(doc, '_id');

  return {
    ...doc,
    notificationId,
    chopId: doc.chopId.toString(),
    fromPersonId: doc.fromPersonId.toString(),
    forPersonId: doc.forPersonId.toString(),
  };
}


function toDiscount(doc) {
  if (!doc) {
    return doc;
  }

  const discountId = doc._id.toString();
  doc = omit(doc, '_id');

  return {
    ...doc,
    discountId,
  };
}


function toKit(doc) {
  if (!doc) {
    return doc;
  }

  const kitId = doc._id.toString();
  doc = omit(doc, '_id');

  return {
    ...doc,
    id: kitId,
    kitId
  };
}


function toPlan(doc) {
  if (!doc) {
    return doc;
  }

  const planId = doc.stripePlanId;
  doc = omit(doc, '_id', 'stripePlanId');
  return {
    ...doc,
    planId,
  };
}


const storage = {
  async listChops({ composedBy, notComposedBy, personId, limit = 100 }) {
    const db = await getDb();
    const query = {
      isRemoved: {
        $ne: true
      }
    };

    if (composedBy) {
      query.composedBy = toBsonUid(composedBy);
    }

    if (notComposedBy) {
      query.composedBy = {
        $ne: toBsonUid(notComposedBy),
      };

      query.isDiscoverable = true;
      query.beats = {
        $exists: true,
        $ne: [],
      };
    }

    if (personId !== composedBy) {
      query.isDiscoverable = true;
    }

    const docs = await db.collection('chops')
      .aggregate([
        { $match: query },
        { $limit:  limit },
        {
          $lookup: {
            from: 'comments',
            localField: 'chopId',
            foreignField: 'chopId',
            as: 'comments'
          }
        },
        {
          $lookup: {
            from: 'chops',
            localField: 'remixOf',
            foreignField: 'chopId',
            as: 'remixOfChopId'
          }
        },
        {
          $lookup: {
            from: 'people',
            localField: 'composedBy',
            foreignField: 'personId',
            as: 'composer'
          }
        },
        {
          $lookup: {
            from: 'claps',
            localField: 'chopId',
            foreignField: 'chopId',
            as: 'claps'
          }
        },
        { $sort: { createdAt: -1 } }
      ])
      .toArray();

    const result = docs
      .map(d => {
        return {
          ...d,
          clapsCount: (d.claps || [])
            .reduce((acc, c) => acc + c.clapsCount, 0)
        };
      })
      .map(toChop);

    return result;
  },

  async getChopById({ chopId }) {
    const db = await getDb();
    const doc = await db.collection('chops')
      .findOne({ chopId: toBsonUid(chopId) });
    return toChop(doc);
  },

  async createAuthToken({ personId, expiresAt }) {
    const db = await getDb();
    const tokenBuffer = await cryptoRandomBytes(AUTH_TOKEN_LENGTH);

    const tokenDoc = {
      personId: toBsonUid(personId),
      type: 'auth',
      token: tokenBuffer.toString('hex'),
      expiresAt,
      createdAt: Date.now(),
      revokedAt: null,
    };

    await db.collection('tokens').insertOne(tokenDoc);

    return toToken(tokenDoc);
  },

  async createLoginToken({ personId, expiresAt }) {
    const db = await getDb();
    const tokenBuffer = await cryptoRandomBytes(AUTH_TOKEN_LENGTH);

    const tokenDoc = {
      personId: toBsonUid(personId),
      type: 'login',
      token: tokenBuffer.toString('hex'),
      expiresAt,
      createdAt: Date.now(),
      revokedAt: null,
    };

    await db.collection('tokens').insertOne(tokenDoc);

    return toToken(tokenDoc);
  },

  async getAuthTokenByValue({ token }) {
    const db = await getDb();
    const tokenDoc = await db.collection('tokens').findOne({
      token,
      type: 'auth',
    });

    return toToken(tokenDoc);
  },

  async revokeAuthToken({ token }) {
    const db = await getDb();
    const result = await db.collection('tokens')
      .updateOne(
        { token, type: 'auth', },
        { $set: { revokedAt: Date.now() } }
      );

    return result.result.nModified === 1;
  },

  async revokeLoginToken({ token }) {
    const db = await getDb();
    const result = await db.collection('tokens')
      .updateOne(
        { token, type: 'login', },
        { $set: { revokedAt: Date.now() } }
      );

    return result.result.nModified === 1;
  },

  async getLoginTokenByPersonId(personId) {
    const db = await getDb();
    const result = await db.collection('tokens')
      .find({ personId: toBsonUid(personId), type: 'login', })
      .sort({ _id: -1 })
      .limit(1)
      .toArray();

    return toToken(result[0]);
  },

  async hasAuthTokens({ personId }) {
    const db = await getDb();
    const result = await db.collection('tokens').count({
      personId: toBsonUid(personId),
      type: 'auth',
    });

    return result > 0;
  },

  async createPerson({ personId }) {
    const person = {
      personId: toBsonUid(personId),
      handle: '',
      userpic: '',
    };

    const db = await getDb();
    await db.collection('people').insertOne(person);

    if (WELCOMING_PERSON_ID) {
      storage.saveNotification({
        type: 'welcome',
        forPersonId: personId,
        fromPersonId: WELCOMING_PERSON_ID,
        createdAt: Date.now(),
      });
    }

    return {
      ...person,
      personId,
    };
  },


  async putChop(chop) {
    const db = await getDb();
    const chopDoc = {
      ...chop,
      composedBy: toBsonUid(chop.composedBy),
      chopId: toBsonUid(chop.chopId),
      updatedAt: Date.now(),
    };

    const upsert = await db.collection('chops').updateOne(
      { chopId: chopDoc.chopId },
      { $set: chopDoc },
      { upsert: true }
    );

    // TODO: potentially dangerous loop
    // Can create race conditions and run for a long time
    if (chop.remixOf && upsert.result.upserted) {
      let remixOf = chop.remixOf;

      while (remixOf) {
        const originalChop = await storage.getChopById({
          chopId: remixOf
        });

        storage.saveNotification({
          type: 'remix',
          chopId: chop.chopId,
          forPersonId: originalChop.composedBy,
          fromPersonId: chop.composedBy,
          createdAt: chop.createdAt,
        });

        remixOf = originalChop.remixOf;
      }
    }

    return toChop(chop);
  },

  async countChopsComposedBy(personId) {
    const db = await getDb();
    return db.collection('chops')
      .count({
        composedBy: toBsonUid(personId),
        isRemoved: { $ne: true },
      });
  },

  async getPersonById({ personId }) {
    const db = await getDb();
    const doc = await db.collection('people')
      .findOne({personId: toBsonUid(personId)});

    return toPerson(doc);
  },

  async getAccount({ personId }) {
    let doc = await findAccount({ personId });

    if (!doc) {
      doc = {
        personId: toBsonUid(personId),
        password: null,
        email: null,
        emailLowerCase: null,
      }
    }

    return toAccount(doc);
  },

  async getAccountByCredentials({ email, password, }) {
    email = email.trim();
    const account = await findAccountByEmail(email);

    if (!account) {
      return null;
    }

    const isPasswordValid = await bcrypt.compare(
      password,
      account.password
    );

    if (!isPasswordValid) {
      return null;
    }

    return toAccount(account);
  },

  async doesAccountWithEmailExist(email) {
    const account = await findAccountByEmail(email);
    return !!account;
  },

  async getAccountByEmail(email) {
    email = email.trim();
    const account = await findAccountByEmail(email);
    return toAccount(account);
  },

  async setAccountEmailAndPassword(personId, { email, password }) {
    email = email.trim();

    const db = await getDb();
    let account = await findAccount({ personId });

    if (!account) {
      account = {
        personId: toBsonUid(personId),
      };
    }

    if (account.email) {
      throw new PolyError(`Account email is already set`);
    }

    if (account.password) {
      throw new PolyError(`Account password is already set`);
    }

    if (!password || password.length < 4) {
      throw new PolyError('Password should be longer than 4 charecters.');
    }

    const accountByEmail = await findAccountByEmail(email);
    if (accountByEmail) {
      throw new PolyError('Email is used by another account');
    }

    const newPasswordHash = await bcrypt.hash(password, SALT_ROUNDS);

    const newDoc = {
      ...omit(account, '_id'),
      password: newPasswordHash,
      email,
      emailLowerCase: email.toLowerCase(),
      createdAt: Date.now(),
    };

    await db.collection('accounts').insertOne(newDoc);
    return toAccount(newDoc);
  },

  async updateAccount(personId, update) {
    const db = await getDb();

    update = pick(
      update,
      'stripeCustomerId',
      'billingAddress',
      'nameOnCard'
    );

    let account = await findAccount({ personId });
    const newDoc = {
      ...omit(account, '_id'),
      ...update,
      createdAt: Date.now(),
    };

    await db.collection('accounts').insertOne(newDoc);
    return toAccount(newDoc);
  },

  async changePassword(personId, { oldPassword, newPassword }) {
    const db = await getDb();
    let account = await findAccount({ personId });

    if (!account) {
      account = {
        personId: toBsonUid(personId),
      };
    }

    if (!newPassword || newPassword.length < 4) {
      throw new PolyError('Password should be longer than 4 charecters.');
    }

    if (account.password) {
      const isPasswordValid = await bcrypt.compare(
        oldPassword,
        account.password
      );

      if (!isPasswordValid) {
        throw new PolyError('Old password is not valid.');
      }

      const isPasswordSame = await bcrypt.compare(
        newPassword,
        account.password
      );

      if (isPasswordSame) {
        throw new PolyError(
          'New password can not be the same as the old one.'
        );
      }
    }

    const newPasswordHash = await bcrypt.hash(newPassword, SALT_ROUNDS);

    const newDoc = {
      ...omit(account, '_id'),
      password: newPasswordHash,
      createdAt: Date.now(),
    };

    await db.collection('accounts').insertOne(newDoc);
    return toAccount(newDoc);
  },


  async changeEmail(personId, { email }) {
    email = email.trim();

    const db = await getDb();
    let account = await findAccount({ personId });

    if (!account) {
      account = {
        personId: toBsonUid(personId),
      };
    }

    const accountByEmail = await findAccountByEmail(email);
    if (accountByEmail) {
      throw new PolyError('Email is used by another account');
    }

    const newDoc = {
      ...omit(account, '_id'),
      email,
      emailLowerCase: email.toLowerCase(),
      createdAt: Date.now(),
    };

    await db.collection('accounts').insertOne(newDoc);
    return toAccount(newDoc);
  },


  async changePerson(personId, update) {
    const db = await getDb();
    update = pick(
      update,
      'handle',
      'userpic',
    );

    const res = await db.collection('people')
      .findOneAndUpdate(
        {personId: toBsonUid(personId)},
        {$set: update},
        {returnOriginal: false}
      );

    return toPerson(res.value);
  },

  async saveComment(comment) {
    const db = await getDb();
    let commentDoc = {
      ...comment,
      commentId: toBsonUid(comment.commentId),
      personId: toBsonUid(comment.personId),
      chopId: toBsonUid(comment.chopId),
    };

    await db.collection('comments').insertOne(commentDoc);


    const chop = await storage.getChopById({ chopId: comment.chopId });
    await storage.saveNotification({
      type: 'comment',
      chopId: chop.chopId,
      forPersonId: chop.composedBy,
      fromPersonId: comment.personId,
      createdAt: comment.createdAt,
      commentId: comment.commentId,
    });

    return comment;
  },

  async listCommentsByChopId({ chopId }) {
    const db = await getDb();
    const docs = await db.collection('comments')
      .find({ chopId: toBsonUid(chopId) })
      .limit(100)
      .toArray();

     return docs.map(toComment);
  },

  async getCommentById({ commentId }) {
    const db = await getDb();
    const doc = await db.collection('comments')
      .findOne({ commentId: toBsonUid(commentId) });

    return toComment(doc);
  },

  async saveClap(clap) {
    const db = await getDb();
    let clapDoc = {
      ...clap,
      clapId: toBsonUid(clap.clapId),
      personId: toBsonUid(clap.personId),
      chopId: toBsonUid(clap.chopId),
    };

    await db.collection('claps').insertOne(clapDoc);

    const chop = await storage.getChopById({ chopId: clap.chopId });
    await storage.saveNotification({
      type: 'clap',
      chopId: chop.chopId,
      forPersonId: chop.composedBy,
      fromPersonId: clap.personId,
      createdAt: clap.createdAt,
    });

    return clap;
  },


  async getClapsCount({ chopId }) {
    const db = await getDb();
    chopId = toBsonUid(chopId);
    const result = await db.collection('claps').aggregate([
      {
        $match: { chopId }
      },
      {
        $group: {
          _id: null,
          clapsCount: { $sum: '$clapsCount' }
        }
      }
    ]).toArray();

    if (!result.length) {
      return 0;
    }

    return result[0].clapsCount;
  },

  async hasClapsByPerson({ chopId, personId }) {
    const db = await getDb();
    const doc = await db.collection('claps')
      .findOne({
        chopId: toBsonUid(chopId),
        personId: toBsonUid(personId)
      });

    return !!doc;
  },

  async saveNotification(notification) {
    const db = await getDb();

    const doc = {
      ...notification,
      chopId: toBsonUid(notification.chopId),
      commentId: toBsonUid(notification.commentId),
      forPersonId: toBsonUid(notification.forPersonId),
      fromPersonId: toBsonUid(notification.fromPersonId),
    };

    await db.collection('notifications')
      .insertOne(doc);

    // TODO: put in a process
    await storage.aggregateNotifications();
    return notification;
  },

  async aggregateNotifications() {
    const db = await getDb();
    const lastAggregateArr = await db.collection('jobs')
      .find({ type: AGGREGATE_NOTIFICATIONS_JOB })
      .sort({ toNotificationId: -1 })
      .limit(1)
      .toArray();

    let lastAggregate = null;
    if (lastAggregateArr && lastAggregateArr[0]) {
      lastAggregate = lastAggregateArr[0];
    }

    let lastProcessedNotificationId = null;
    if (lastAggregate) {
      lastProcessedNotificationId = lastAggregate.toNotificationId;
    }

    let notificationsQuery = {};
    if (lastProcessedNotificationId) {
      notificationsQuery = { _id: { $gt: lastProcessedNotificationId } };
    }

    const toProcess = await db.collection('notifications')
      .find(notificationsQuery)
      .sort({ _id: 1 })
      .toArray();

    if (toProcess.length === 0) {
      console.log('No notifications to process');
    }

    let fromNotificationId;
    let toNotificationid;
    let notificationsGroupsByKey = new Map();

    for (let notification of toProcess) {
      fromNotificationId = fromNotificationId || notification._id;
      toNotificationId = notification._id;

      const date = new Date(notification.createdAt).toISOString();
      const day = date.slice(0, 10);

      const key = (
        `${notification.type}-${notification.chopId}-${day}`
      );
      let notificationGroup = notificationsGroupsByKey.get(key);

      if (!notificationGroup) {
        notificationGroup = {
          key,
          date: notification.createdAt,
          chopId: notification.chopId,
          type: notification.type,
          forPersonId: notification.forPersonId,
          isRead: false,
          notifications: [],
        };

        notificationsGroupsByKey.set(key, notificationGroup);
      }

      notificationGroup.notifications.push(notification);
      notificationGroup.date = notification.createdAt;
    }

    let notificationsGroupsInsert = null;
    const groups = Array.from(
      notificationsGroupsByKey.values()
    ).sort((g1, g2) => g2.date - g1.date);

    try {
      const updates = [];
      for (let group of groups) {
        let update = omit(group, 'notifications');

        await db.collection('notificationsGroups').update(
          { key: group.key },
          {
            $set: update,
            $push: { notifications: { $each: group.notifications } }
          },
          { upsert: true }
        );
      }
    } catch (e) {
      console.error(
        'Failed to insert results of aggregate notifications',
        'Aborting...',
        e
      );

      return;
    }


    try {
      await db.collection('jobs').insertOne({
        type: AGGREGATE_NOTIFICATIONS_JOB,
        fromNotificationId,
        toNotificationId,
      });
    } catch (e) {
      console.error('Failed to insert a job', e);

      // TODO: prevent double notifications
      // notificationsGroupsInsert
    }
  },

  async listNotificationsGroups({ forPersonId, first, after }) {
    const db = await getDb();

    if (after) {
      after = ObjectID(after);
    }

    if (!forPersonId) {
      throw new Error('forPersonId required');
    }

    forPersonId = toBsonUid(forPersonId);

    first = first || 40;

    if (first > 40) {
      first = 40;
    }

    let query = { forPersonId };
    if (after) {
      query._id = { $lt: after };
    }

    let nodes = await db.collection('notificationsGroups')
      .find(query)
      .sort({ date: -1 })
      .limit(first + 1)
      .toArray();

    let endCursor = null;
    let hasNextPage = nodes.length > first;
    nodes = nodes.slice(0, first);

    const lastNode = nodes[nodes.length - 1];
    if (lastNode) {
      endCursor = lastNode._id.toString();
    }

    return {
      edges: nodes.map(n => ({
        node: toNotificationGroup(n),
        cursor: n._id.toString(),
      })),
      pageInfo: {
        endCursor,
        hasNextPage,
      }
    };
  },

  async markNotificationsAsRead({ personId, before }) {
    const db = await getDb();
    personId = toBsonUid(personId);
    before = ObjectID(before);

    const result = await db.collection('notificationsGroups')
      .updateMany(
        {
          _id: { $gte: before },
          isRead: false,
          forPersonId: personId,
        },
        { $set: { isRead: true } }
      );

    return result.modifiedCount;
  },

  async getActiveSubscription({ personId, }) {
    const db = await getDb();

    const result = await db.collection('subscriptions')
      .find({
        personId: toBsonUid(personId),
        $or: [
          { isCancelled: { $ne: true } },
          { isCancelled: true, validUntil: { $gt: Date.now() } },
        ]
      })
      .sort({ _id: -1 })
      .limit(1)
      .toArray();

    return toSubscription(result[0]);
  },

  async createSubscription({
    personId,
    planId,
    createdAt,
    stripeSubscriptionId,
  }) {
    const subscription = {
      personId,
      planId,
      createdAt,
      stripeSubscriptionId,
    };

    const db = await getDb();
    const insert = await db.collection('subscriptions')
      .insertOne({
        personId: toBsonUid(personId),
        planId,
        createdAt,
        stripeSubscriptionId,
        isCancelled: false,
        cancelledAt: null,
        validUntil: null,
      });

    return {
      subscriptionId: insert.insertedId,
      ...subscription,
    };
  },


  async cancelAllSubscriptions({ personId, periodEnd, }) {
    const db = await getDb();
    let update = {
      isCancelled: true,
      cancelledAt: Date.now(),
      validUntil: Date.now(),
    };

    if (periodEnd && periodEnd < Infinity) {
      update.validUntil = periodEnd;
    }

    await db.collection('subscriptions').updateMany({
      personId: toBsonUid(personId),
      isCancelled: false,
    }, {
      $set: update,
    });

    return true;
  },

  async getDiscount({
    personId,
    planId,
    discountCode,
  }) {
    const db = await getDb();

    discountCode = discountCode || '';

    const discount = await db.collection('discounts').findOne({
      discountCode: discountCode.toLowerCase(),
      isRemoved: { $ne: true },
    });

    return toDiscount(discount);
  },


  async createDiscount({
    discountCode,
    stripeCouponId,
    description,
  }) {
    const db = await getDb();
    if (!discountCode) {
      throw new PolyError(`discountCode required`);
    }

    if (!stripeCouponId) {
      throw new PolyError(`stripeCouponId required`);
    }

    if (!description) {
      throw new PolyError(`description required`);
    }

    const doc = {
      discountCode: discountCode.toLowerCase(),
      stripeCouponId,
      description,
    };

    const insert = await db.collection('discounts').insertOne(doc);

    doc._id = insert.insertedId;
    return toDiscount(doc);
  },

  async listKits({ includeProKits }) {
    const query = includeProKits ? {} : { isPro: { $ne: true } };
    const db = await getDb();

    const result = await db
      .collection('kits')
      .find(query)
      .sort({ order: 1 })
      .toArray();

    return result.map(toKit);
  },

  async listPlans() {
    const db = await getDb();
    const result = await db.collection('plans').find({
      isRemoved: { $ne: false }
    }).toArray();

    return result.map(toPlan);
  },

  async getPlanById(planId) {
    const db = await getDb();
    const result = await db.collection('plans').findOne({
      stripePlanId: planId,
    });
    return toPlan(result);
  },

};

module.exports = storage;
