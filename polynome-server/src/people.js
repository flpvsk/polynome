const path = require('path');
const fs = require('fs-extra');
const sharp = require('sharp');
const multer = require('multer');
const nanoid = require('nanoid');
const sanitizeFilename = require('sanitize-filename');
const storage = require('./storage');
const auth = require('./auth');

const upload = multer({
  limits: {
    fileSize: 5 * (10 ** 6),
    fieldSize: 5 * (10 ** 6),
  },
});

const SERVER_URL = process.env.POLYNOME_SERVER_URL;
const USERPICS_ROOT = process.env.POLYNOME_USERPICS_DIR || 'userpics';

function configure({app, rootPath}) {
  app.get(
    `${rootPath}/:personId/loginWithLink/:token`,
    async (req, res) => {
      const { personId, token } = req.params;
      res.type('html');

      res.set({
        'Cache-Control': 'no-cache, no-store, must-revalidate',
        'Pragma': 'no-cache',
        'Expires': 0
      });

      try {
        const tokenObj = await auth.loginToAuthToken({ personId, token });

        if (!tokenObj) {
          throw new Error('Link is not valid');
        }

        res.end(
          `<html><head><script>` +
          `(function() {` +
          `var l = window.localStorage;` +
          `l.setItem('poly/personId', '${personId}');` +
          `l.setItem('poly/token', '${tokenObj.token}');` +
          `window.location = '/';` +
          `})();` +
          `</script></head></html>`
        );
      } catch (e) {
        console.error(e);

        res.status(400);
        res.end(
          `<html><body><h3>${e.message}</h3></body></html>`
        );
      }
    }
  );

  app.post(
    `${rootPath}/:personId/userpic`,
    upload.single('userpic'),
    async (req, res) => {
      const { personId } = req.params;
      const fileName = `${nanoid()}.png`;
      try {
        const dir = path.join(USERPICS_ROOT, personId);
        await fs.ensureDir(dir);

        const person = await storage.getPersonById({ personId });

        if (!person) {
          throw new Error(`Person with id ${personId} not found`);
        }

        await sharp(req.file.buffer)
          .resize(160)
          .png()
          .toFile(path.join(dir, fileName));

        const userpicUrl = (
          `${SERVER_URL}${rootPath}/${personId}/${fileName}`
        );

        const updatedPerson = await storage.changePerson(personId, {
          userpic: userpicUrl
        });

        res.json({
          data: {
            ok: true,
            person: updatedPerson,
          }
        });
      } catch (e) {
        res.status(500);

        res.json({
          errors: [{
            message: e.message
          }]
        });
      }
  });

  app.get(`${rootPath}/:personId/:fileName`, (req, res, next) => {
    const fileName = sanitizeFilename(req.params.fileName);
    const personId = req.params.personId;
    const dir = path.join(USERPICS_ROOT, personId);

    const opts = {
      dotfiles: 'deny',
      root: dir,
    };

    res.sendFile(fileName, opts, err => {
      if (err) {
        next(err);
      }
    });
  });
}

module.exports = configure;
