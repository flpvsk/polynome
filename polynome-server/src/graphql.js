const {
  gql,
  AuthenticationError,
  ForbiddenError,
  ApolloError,
} =  require('apollo-server');
const PolyError = require('./PolyError');

const storage = require('./storage');
const auth = require('./auth');
const stripe = require('stripe')(process.env.POLYNOME_STRIPE_API_KEY);

const includes = require('lodash/includes');
const isEmpty = require('lodash/isEmpty');

const nanoid = require('nanoid');

const typeDefs = gql`

  type AdsrConfig {
    attackTime: Float
    attackLevel: Float
    decayTime: Float
    sustainLevel: Float
    sustainTime: Float
    releaseTime: Float
  }

  type BeatSoundEnvelop {
    type: String!
    beat: AdsrConfig
    time: AdsrConfig
  }

  type Comment {
    commentId: ID!
    personId: ID!
    chopId: ID!
    createdAt: Float!
    text: String!
    person: Person
  }

  type BeatSound {
    type: String!
    isAuto: Boolean!
    pitch: String
    soundId: ID
    envelop: BeatSoundEnvelop
  }

  type RecordingSound {
    type: String
    sampleRate: Int
    latency: Float
    deviceId: String
    url: String
  }

  type Beat {
    id: ID!
    personId: ID
    beatsCount: Int!
    lineStart: Int!
    lineDuration: Int!
    sound: BeatSound!
    mutedBeats: [Int]
    isMute: Boolean
    offset: Int
    tempo: Float
  }

  type Line {
    lineId: ID!
    lineName: String!

    beatsCount: Int
    lineStart: Int!
    lineDuration: Int!

    isMute: Boolean

    playingRecordingId: ID
  }

  type Recording {
    recordingId: ID!
    personId: ID
    lineId: ID
    name: String
    beatsCount: Int
    beatsTempo: Float
    sound: RecordingSound
  }

  type SoloItem {
    itemType: String!
    beatId: ID
    lineId: ID
  }

  type PageInfo {
    endCursor: ID
    hasNextPage: Boolean!
  }

  type Notification {
    notificationId: ID!
    chopId: ID
    commentId: ID
    comment: Comment
    forPersonId: ID!
    fromPersonId: ID
    fromPerson: Person
    createdAt: Float
  }

  type NotificationGroup {
    notificationGroupId: ID
    date: Float
    type: String
    chopId: ID
    isRead: Boolean
    chop: Chop
    notifications: [Notification]!
  }

  type NotificationGroupEdge {
    cursor: ID!
    node: NotificationGroup!
  }

  type NotificationsGroupsPaginated {
    edges: [NotificationGroupEdge]!
    pageInfo: PageInfo!
  }

  type Chop {
    chopId: ID!
    remixOf: ID
    remixOfChop: Chop

    beats: [Beat]!
    lines: [Line]!
    commonBeatsCount: Int!
    commonBeatsTempo: Float!

    composedBy: ID!
    composer: Person
    createdAt: Float!

    name: String
    description: String
    isDiscoverable: Boolean

    recordings: [Recording]!
    activeSoundType: String!
    activeKitId: String
    soloItem: SoloItem

    comments: [Comment]

    clapsCount: Int
    hasMyClaps(personId: ID): Boolean
  }

  type Subscription {
    subscriptionId: ID!
    planId: ID
    plan: Plan
    createdAt: Float
    isCancelled: Boolean
    cancelledAt: Float
    validUntil: Float
  }

  type KitSound {
    id: ID!
    name: String!
    url: String
  }

  type Kit {
    id: ID!
    name: String!
    sounds: [KitSound]
    isAvailable: Boolean
  }

  type Money {
    amount: Float
    currencyCode: ID
    currencySymbol: String
  }

  type Plan {
    planId: ID
    name: String
    description: String
    price: Money
    strikeOutPrice: Money
    frequencyAdverb: String
    frequencyNoun: String
    features: [String]
  }

  type Query {
    chops(personId: ID, composedBy: ID, notComposedBy: ID): [Chop]
    chop(chopId: ID, personId: ID): Chop
    kits: [Kit]
    person(personId: ID): Person
    myself: Person
    notificationsGroups(
      forPersonId: ID!,
      first: Int,
      after: ID
    ): NotificationsGroupsPaginated
    myAccount: Account
    discount(
      personId: ID,
      planId: ID,
      discountCode: ID!
    ): Discount
    plans: [Plan]
  }

  type Person {
    personId: ID!
    handle: String
    userpic: String
  }

  type Account {
    accountId: String
    personId: String
    hasPassword: Boolean
    email: String
    activeSubscription: Subscription
  }

  type Token {
    personId: ID
    token: String!
    expiresAt: Float
    createdAt: Float
  }

  type Clap {
    clapId: ID
    chopId: ID!
    personId: ID!
    createdAt: Float!
    clapsCount: Int!
  }

  type AuthenticatedPerson {
    person: Person!
    token: Token!
  }

  type Discount {
    discountId: ID
    discountCode: ID!
    description: String!
  }


  type BooleanResult {
    result: Boolean
  }

  input RecordingSoundInput {
    latency: Float
    sampleRate: Int
    type: String
    deviceId: String
    url: String
  }

  input RecordingInput {
    recordingId: ID!
    personId: ID
    lineId: ID!
    name: String!

    beatsCount: Int
    beatsTempo: Float

    sound: RecordingSoundInput
  }

  input AdsrConfigInput {
    attackTime: Float
    attackLevel: Float
    decayTime: Float
    sustainLevel: Float
    sustainTime: Float
    releaseTime: Float
  }

  input BeatSoundEnvelopInput {
    type: String!
    beat: AdsrConfigInput
    time: AdsrConfigInput
  }

  input BeatSoundInput {
    isAuto: Boolean!
    pitch: String
    soundId: String
    type: String
    envelop: BeatSoundEnvelopInput
  }

  input BeatInput {
    id: ID!
    personId: ID
    beatsCount: Int!
    lineDuration: Int!
    lineStart: Int!
    sound: BeatSoundInput!
    tempo: Float
    offset: Int
    mutedBeats: [Int]
    isMute: Boolean
  }

  input LineInput {
    lineId: ID!
    lineName: String!
    personId: ID

    beatsCount: Int
    lineStart: Int!
    lineDuration: Int!

    playingRecordingId: ID

    isMute: Boolean
  }

  input SoloItemInput {
    itemType: String!
    beatId: ID
    lineId: ID
  }

  input ChopInput {
    chopId: ID!
    remixOf: ID

    beats: [BeatInput]!
    lines: [LineInput]!
    commonBeatsCount: Int!
    commonBeatsTempo: Float!

    composedBy: ID!
    createdAt: Float!

    name: String
    description: String
    isDiscoverable: Boolean

    recordings: [RecordingInput]!
    activeSoundType: String!
    activeKitId: String
    soloItem: SoloItemInput

    isRemoved: Boolean
  }

  input CommentInput {
    commentId: ID!
    personId: ID!
    chopId: ID!
    createdAt: Float!
    text: String!
  }

  input ClapInput {
    clapId: ID!
    chopId: ID!
    personId: ID!
    createdAt: Float!
    clapsCount: Int!
  }

  input AddressComponentInput {
    shortName: String
    longName: String
    types: [String]
  }

  input AddressInput {
    addressString: String!
    addressComponents: [AddressComponentInput]
  }

  type Mutation {
    addChop(chop: ChopInput): Chop

    putChop(chop: ChopInput): Chop

    createPerson(personId: ID!): AuthenticatedPerson

    changeHandle(personId: ID!, handle: String!): Person

    saveComment(comment: CommentInput!): Comment

    saveClap(clap: ClapInput!): Clap

    markNotificationsAsRead(before: ID!, personId: ID!): Int

    createSubscription(
      personId: ID!
      planId: ID!
      stripeTokenId: ID
      email: String
      password: String
      nameOnCard: String
      billingAddress: AddressInput
      discountCode: String
    ): Subscription

    cancelSubscription: Subscription

    changePassword(
      personId: ID!
      oldPassword: String
      newPassword: String!
    ): Account

    changeEmail(
      personId: ID!
      email: String,
    ): Account

    login(
      email: String!
      password: String!
    ): AuthenticatedPerson

    logout: Boolean

    signup(
      email: String!
      password: String!
    ): AuthenticatedPerson

    loginAnonymously: AuthenticatedPerson

    emailLoginLink(email: String!): Boolean
  }
`;

async function getPersonFor(contextPersonId, personId) {
  return storage.getPersonById({ personId });
}

async function getValidDiscount({ personId, discountCode, planId }) {
  const discount = await storage.getDiscount({
    personId,
    discountCode,
    planId,
  });

  if (!discount) {
    return null;
  }

  const stripeCoupon = await stripe.coupons.retrieve(
    discount.stripeCouponId
  );

  if (!stripeCoupon || !stripeCoupon.valid) {
    return null;
  }

  return discount;
}

const resolvers = {
  Query: {
    chops: async (parent, { personId, composedBy, notComposedBy }) => {
      return storage.listChops({
        composedBy,
        notComposedBy,
        personId,
      });
    },

    chop: async (parent, { chopId }) => {
      const chop = await storage.getChopById({ chopId });

      if (!chop) {
        throw new ApolloError('Chop not found', 404);
      }

      return chop;
    },

    person: async (parent, { personId }, context) => {
      return getPersonFor(context.personId, personId);
    },

    myself: async (parent, _, context) => {
      if (!context.personId) {
        throw new AuthenticationError(`Not authenticated`);
      }

      return getPersonFor(context.personId, context.personId);
    },

    notificationsGroups: async (parent, { forPersonId, first, after }) => {
      return storage.listNotificationsGroups({
        forPersonId,
        first,
        after,
      });
    },

    myAccount: async (parent, opts, context) => {
      if (!context.personId) {
        throw new AuthenticationError(`Not authenticated`);
      }

      const subscription = await storage.getActiveSubscription({
        personId: context.personId
      });

      const account = await storage.getAccount({
        personId: context.personId
      });

      let activeSubscription = null

      if (subscription) {
        let plan = await storage.getPlanById(subscription.planId);
        activeSubscription = {
          ...subscription,
          plan,
        };
      }

      return {
        ...account,
        activeSubscription,
      };
    },

    discount: async (parent, {
      personId,
      discountCode,
      planId,
    }, context) => {
      if (context.personId !== personId) {
        throw new ForbiddenError(
          `Wrong authentication credentials.`
        );
      }

      return getValidDiscount({
        personId,
        discountCode,
        planId,
      });
    },

    kits: async (parent, _, context) => {
      let hasSubscription = true;

      // if (context.personId) {
      //   const activeSubscription = await storage.getActiveSubscription({
      //     personId: context.personId
      //   });

      //   hasSubscription =
      //    activeSubscription && activeSubscription.planId;
      // }

      const kits = await storage.listKits({ includeProKits: true });
      return kits.map(k => {
        return {
          ...k,
          isAvailable: (k.isPro && hasSubscription) || !k.isPro,
        };
      });
    },

    plans: async (parent, _, context) => {
      return storage.listPlans();
    },
  },

  Subscription: {
    plan: async (subscription) => {
      return storage.getPlanById(subscription.planId);
    },
  },

  Chop: {
    composer: async (chop, opts, context) => {
      const personId = chop.composedBy;
      return getPersonFor(context.personId, personId);
    },

    comments: async (chop) => {
      return storage.listCommentsByChopId({ chopId: chop.chopId });
    },

    clapsCount: async (chop) => {
      return storage.getClapsCount({ chopId: chop.chopId });
    },

    hasMyClaps: async (chop, { personId }) => {
      if (!personId) {
        return false;
      }

      return storage.hasClapsByPerson({ chopId: chop.chopId, personId });
    },

    remixOfChop: async (chop) => {
      if (!chop.remixOf) {
        return null;
      }

      return storage.getChopById({ chopId: chop.remixOf });
    },
  },

  Comment: {
    person: async (comment, opts, context) => {
      const { personId } = comment;
      return getPersonFor(context.personId, personId);
    }
  },

  NotificationGroup: {
    chop: async (notificationGroup) => {
      return storage.getChopById({ chopId: notificationGroup.chopId });
    },
  },

  Notification: {
    fromPerson: async ({ fromPersonId }) => {
      return storage.getPersonById({ personId: fromPersonId });
    },

    comment: async ({ commentId }) => {
      return storage.getCommentById({ commentId });
    },
  },

  Mutation: {
    addChop: async (root, { chop }, context) => {
      if (!context.personId) {
        throw new AuthenticationError(
          `You have to be authenticated to change the chop.`
        );
      }

      const originalChop = await storage.getChopById({
        chopId: chop.chopId
      });

      if (originalChop) {
        throw new ForbiddenError(`Chop with this id already exists.`);
      }

      if (chop.composedBy !== context.personId) {
        throw new ForbiddenError(`You have to be the chop's composer`);
      }

      return storage.putChop(chop);
    },


    putChop: async (root, { chop }, context) => {
      if (!context.personId) {
        throw new AuthenticationError(
          `You have to be authenticated to change the chop.`
        );
      }

      const originalChop = await storage.getChopById({
        chopId: chop.chopId
      });

      if (originalChop && originalChop.composedBy !== context.personId) {
        throw new ForbiddenError(`You have no access to that chop.`);
      }

      if (chop.composedBy !== context.personId) {
        throw new ForbiddenError(`You have to be the chop's composer`);
      }

      // Checking subscription
      // if (!originalChop) {
      //   const [
      //     chopsCount,
      //     activeSubscription,
      //   ] = await Promise.all([
      //     storage.countChopsComposedBy(context.personId),
      //     storage.getActiveSubscription({ personId: context.personId }),
      //   ]);

      //   const isSubscriptionValid = (
      //     activeSubscription &&
      //     activeSubscription.planId
      //   );

      //   if (!isSubscriptionValid && chopsCount >= 3) {
      //     throw new ForbiddenError(
      //       `You have reached the limits of your free subscription. ` +
      //       `Please consider upgrading to a pro plan.`,
      //     );
      //   }
      // }

      return storage.putChop(chop);
    },

    createPerson: async (root, { personId }, context) => {
      if (context.personId) {
        throw new ForbiddenError('Already logged in');
      }

      const result = { person: null, token: null };
      result.person = await storage.createPerson({ personId });
      result.token = await storage.createAuthToken({ personId });
      context.personId = personId;
      context.token = result.token.token;
      return result;
    },

    changeHandle: async (root, { personId, handle }, context) => {
      if (!context.personId) {
        throw new AuthenticationError(
          `Not authenticated. Please log in to perform the operation.`
        );
      }

      if (context.personId !== personId) {
        throw new ForbiddenError(
          `Wrong authentication credentials.`
        );
      }

      if (handle.length > 72) {
        throw new ApolloError(
          `Handle should not be longer than 72 characters`,
          400
        );
      }

      return storage.changePerson(personId, { handle });
    },

    saveComment: async (root, { comment }, context) => {
      if (!context.personId) {
        throw new AuthenticationError(
          `Not authenticated. Please log in to perform the operation.`
        );
      }

      const { personId } = comment;

      if (context.personId !== personId) {
        throw new ForbiddenError(
          `Wrong authentication credentials.`
        );
      }

      let text = comment.text || '';
      text = text.trim();

      if (text.length === 0) {
        throw new ApolloError(`Comments can not be empty`, 400);
      }

      if (text.length > 320) {
        throw new ApolloError(
          `Comments should not be longer than 320 characters`,
          400
        );
      }

      comment.text = text;

      return storage.saveComment(comment);
    },

    saveClap: async (root, { clap }, context) => {
      if (!context.personId) {
        throw new AuthenticationError(
          `Not authenticated. Please log in to perform the operation.`
        );
      }

      const { personId } = clap;

      if (context.personId !== personId) {
        throw new ForbiddenError(
          `Wrong authentication credentials.`
        );
      }

      return storage.saveClap(clap);
    },

    markNotificationsAsRead: async (root, { before, personId }) => {
      return storage.markNotificationsAsRead({ before, personId });
    },

    createSubscription: async (root, {
      personId,
      planId,
      stripeTokenId,
      email,
      password,
      nameOnCard,
      billingAddress,
      discountCode,
    }, context) => {
      if (personId !== context.personId) {
        throw new AuthenticationError(`Not authorized.`);
      }

      const account = await storage.getAccount({ personId });

      if (!account.email) {
        await storage.setAccountEmailAndPassword(personId, {
          email,
          password,
        });
      }

      if (account.email && !account.hasPassword) {
        await storage.changePassword(personId, { newPassword: password });
      }

      const accountUpdate = {
        billingAddress,
        nameOnCard,
      };
      let { stripeCustomerId } = account;

      if (!stripeCustomerId) {
        const stripeCustomer = await stripe.customers.create({
          email: email || account.email,
        });

        stripeCustomerId = stripeCustomer.id;
        accountUpdate.stripeCustomerId = stripeCustomer.id;
      }

      await storage.updateAccount(personId, accountUpdate);

      const stripeSource = await stripe.customers.createSource(
        stripeCustomerId, { source: stripeTokenId }
      );

      let stripeAddress = {};

      if (billingAddress && isEmpty(billingAddress.addressComponents)) {
        stripeAddress.address_line1 = billingAddress.addressString;
      }

      if (billingAddress && !isEmpty(billingAddress.addressComponents)) {
        let country = '';
        let streetName = '';
        let streetNumber = '';
        let state = '';
        let city = '';
        let postalCode = '';

        for (let component of billingAddress.addressComponents) {
          if (includes(component.types, 'country')) {
            country = component.shortName;
          }

          if (
            includes(component.types, 'locality') ||
            (includes(component.types, 'postal_town') && !city) ||
            (includes(component.types, 'sublocality_level_1') && !city)
          ) {
            city = component.longName;
          }

          if (includes(component.types, 'postal_code')) {
            postalCode = component.shortName;
          }

          if (includes(component.types, 'administrative_area_level_1')) {
            state = component.shortName;
          }

          if (includes(component.types, 'route')) {
            streetName = component.longName;
          }

          if (includes(component.types, 'street_number')) {
            streetNumber = component.shortName;
          }
        }

        stripeAddress = {
          ...stripeAddress,
          address_country: country,
          address_line1: `${streetName} ${streetNumber}`.trim(),
          address_state: state,
          address_city: city,
          address_zip: postalCode,
        };
      }

      const card = await stripe.customers.updateCard(
        stripeCustomerId,
        stripeSource.id,
        {
          name: nameOnCard,
          ...stripeAddress,
        }
      );

      const subscriptions = await stripe.subscriptions.list({
        customer: stripeCustomerId
      });

      if (
        subscriptions &&
        subscriptions.data &&
        subscriptions.data.length > 0
      ) {
        for (let subscription of subscriptions.data) {
          if (subscription.plan && subscription.plan.id === planId) {
            throw new Error(`You're already subscribed to this plan.`);
          }
        }

        throw new Error(
          `To change subscription please write us an email to ` +
          `billing@polychops.com`
        );
      }

      let discount;

      if (discountCode) {
        discount = await getValidDiscount({
          discountCode,
          personId,
          planId,
        });
      }

      const stripeSubscription = await stripe.subscriptions.create({
        customer: stripeCustomerId,
        items: [{ plan: planId }],
        coupon: discount && discount.stripeCouponId,
      });

      return await storage.createSubscription({
        personId: personId,
        planId,
        createdAt: Date.now(),
        stripeSubscriptionId: stripeSubscription.id,
      });
    },


    async cancelSubscription(root, _, context) {
      const { personId } = context;

      if (!personId) {
        throw new AuthenticationError(`Not authorized`);
      }

      const account = await storage.getAccount({ personId });
      const { stripeCustomerId } = account;

      const subscriptions = await stripe.subscriptions.list({
        customer: stripeCustomerId
      });

      if (
        !subscriptions ||
        !subscriptions.data ||
        subscriptions.data.length === 0
      ) {
        return false;
      }

      const cancelAll = subscriptions.data
        .map(s => stripe.subscriptions.update(s.id, {
          cancel_at_period_end: true
        }));

      const periodEnd = subscriptions.data.reduce((acc, s) => {
        return Math.min(acc, s.current_period_end * 1000);
      }, Infinity);

      await Promise.all(cancelAll);

      await storage.cancelAllSubscriptions({
        personId,
        periodEnd,
      });

      return storage.getActiveSubscription({ personId });
    },

    // async deprecatedLogin(root, { personId }) {
    //   const token = await auth.maybeMigrateDeprecated({ personId });

    //   if (!token) {
    //     throw new ForbiddenError(
    //       `Account was already migrated.`
    //     );
    //   }

    //   return token;
    // },

    async changeEmail(root, { personId, email }, context) {
      if (personId !== context.personId) {
        throw new AuthenticationError(`Not authorized`);
      }

      return storage.changeEmail(personId, { email });
    },

    async changePassword(root, {
      personId,
      oldPassword,
      newPassword
    }, context) {

      if (context.personId !== personId) {
        throw new AuthenticationError(`Not authorized`);
      }

      try {
        return storage.changePassword(personId, {
          oldPassword,
          newPassword,
        });
      } catch (e) {
        if (e instanceof PolyError) {
          throw new ApolloError(e.message);
        }

        throw e;
      }
    },

    async login(root, { email, password }, context) {
      if (context.personId) {
        throw new ForbiddenError('You are already logged in');
      }

      const token = await auth.login({ email, password, });
      context.token = token.token;
      context.personId = token.personId;

      return {
        person: await getPersonFor(token.personId, token.personId),
        token: token,
      };
    },

    async logout(root, _, context) {
      if (!context.personId) {
        return false;
      }

      try {
        await auth.logout({ token: context.token });
      } catch (e) {
        console.error('Error on logout for', context.personId, e);
        return false;
      }

      return true;
    },

    async signup(root, { email, password }, context) {
      if (context.personId) {
        throw new ForbiddenError('You already have an account');
      }

      let personId = nanoid();

      const isEmailUsed = await storage.doesAccountWithEmailExist(email);

      if (isEmailUsed) {
        throw new ApolloError('Email is used by another account', 400);
      }

      const person = await storage.createPerson({ personId });
      const account = await storage.setAccountEmailAndPassword(personId, {
        email,
        password,
      });
      const token = await storage.createAuthToken({ personId });

      return {
        person,
        token,
      };
    },

    async loginAnonymously(root, _, context) {
      if (context.personId) {
        throw new ForbiddenError('Already logged in');
      }

      const personId = nanoid();
      const result = { person: null, token: null };
      result.person = await storage.createPerson({ personId });
      result.token = await storage.createAuthToken({ personId });
      context.personId = personId;
      context.token = result.token.token;
      return result;
    },

    async emailLoginLink(root, { email }, context) {
      return auth.sendLoginLink({ email });
    },
  },
};


module.exports = {
  resolvers,
  typeDefs,
};
